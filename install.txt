****************************************************************
Prerequisites for SU3NCSM build

Last modified: 3/7/11 (mac)
****************************************************************

These prerequisites may be installed in either of two locations:

(1) The standard installation directory tree for libraries and
programs on your system.  For instance, this is often /usr/local, in
which case programs are in /usr/local/bin, include files in
/usr/local/include, and libraries in /usr/local/lib.  Compilers will
look here automatically for the include and library files they need.

(2) Since you may not wish to install all the prerequisites globally
in /usr/local, you might wish to create a directory in your home
directory for "private" installations.  For instance, if your home
directory is /home/jdoe, you might create /home/jdoe/local.  Then you
would install program in /home/jdoe/local/bin, include files in /home/jdoe/local/include,
and libraries in /home/jdoe/local/lib.

Convention: In the following instructions, it is assumed that you are
doing the installation to $HOME/local, as in the example above.  (The
HOME environment variable gives the name of your home directory.)
However, you may substitute another directory name of your choice, or
/usr/local for a global installation accessible to all users (you will
need root privileges).

Special configuration: If you do *not* use either /usr/local or
$HOME/local for the installation, you must edit the 
SU3NCSM build configuration file config.mk to contain the corect
directory name.   Find and edit the line

   search_prefix := $(HOME)/local

Note that *here*, and only here, parentheses are needed around the
name of the environment variable HOME (this is because of the syntax
of makefiles).

----------------------------------------------------------------
GNU Scientific Library (GSL)
----------------------------------------------------------------

Reason: Used internally for matrix operations and special functions.

- - - -

Available from: http://www.gnu.org/software/gsl

- - - -

Checking if already present: Look for a directory named gsl in any of
the systemwide include directories, typically /usr/local/include/gsl
or /usr/include/gsl.  Note: The version of GSL may be found by
inspecting the contents of the file /usr/include/gsl/gsl_version.h.
SU3NCSM development is being carried out with version 1.13 or higher.
However, older versions should suffice as well.

- - - -

Installation: Untar the archive contents and install using autoconf,
as follows:

  tar zxvf gsl-1.14.tar.gz
  cd gsl-1.14
  ./configure --prefix=$HOME/local  
  make
  make install

Some system-specific bugfixes: If you encounter linking errors, try adding the option
--enable-shared=no when you run configure.  If the make crashes with
an error involving "varargs" while compiling test/results.c, add the
directive "#define STDC_HEADERS" to the beginning of this file.

----------------------------------------------------------------
Eigen template library
Version 3
----------------------------------------------------------------

Reason: Used internally for matrix operations.

- - - -

Available from: http://eigen.tuxfamily.org

Shortcut: The beta version available as of March 2010, for instance,
may be downloaded by running

   wget http://bitbucket.org/eigen/eigen/get/3.0-beta4.tar.gz

- - - -

Checking if already present: Look for a directory named
/usr/local/include/eigen3.  If, alternatively, you find a directory
named /usr/local/include/Eigen, this *might* be Eigen 3, but it might
also be an older version, such as 2.0.  One way to tell the difference
is to look for the file /usr/local/include/Eigen/Array, which was
introduced in Eigen 3.0.

- - - -

Manual installation (faster and simpler option): Download the .tar.gz
archive of Eigen 3 into a convenient working directory.  Untar the
archive.  The output will be in a directory named
eigen-eigen-xxxxxxxxxxxx, within your current directory.  The
directory eigen-eigen-xxxxxxxxxxxx will in turn have a subdirectory
named Eigen.  Now create the directory $HOME/local/include/eigen3, and
move the subdirectory Eigen there, as a subdirectory.  You may
optionally also install the "unsupported" portion of the library.
E.g.,
  
  tar zxvf 3.0-beta4.tar.gz
  mkdir $HOME/local/include/eigen3
  mv eigen-eigen-xxxxxxxxxxxx/Eigen $HOME/local/eigen3/Eigen
  mkdir $HOME/local/include/eigen3/unsupported
  mv eigen-eigen-xxxxxxxxxxxx/unsupported/Eigen $HOME/local/eigen3/unsupported/Eigen
   
Automated installation (slower and more complicated option): Eigen
provides an automated installation based on the cmake system.  This is
completely unnecessary if your only goal is to install the Eigen
template library, as needed for SU3NCSM.  However, you can try this
option out if you also wish to build the documentation and examples or
the Eigen BLAS library.

----------------------------------------------------------------
GNU Make
version 3.80 or higher
----------------------------------------------------------------

Reason: GNU Make is required for the project's makefile.

- - - -

Available from: http://ftp.gnu.org/pub/gnu/make/

Shortcut: Version 3.82, for instance, may be downloaded by running

  wget http://ftp.gnu.org/pub/gnu/make/make-3.82.tar.gz

- - - -

Checking if already present: You can verify that the "make" program
on your system is GNU Make and check its version by running

  make -v

- - - -

Installation instructions: 

  tar zxvf make-3.82.tar.gz
  cd make-3.82
  ./configure --prefix=$HOME/local
  make all 
  make install

Now, when you run make for SU3LIB, either (1) make sure $HOME/local/bin
is in your $PATH ahead of all other make commands or (2) type the full
program name $HOME/local/bin/make to invoke make.

----------------------------------------------------------------
MPICH2
or alternate MCI C++ compiler 
----------------------------------------------------------------

Reason: Used for parallel code.

- - - -

Available from: http://www.mcs.anl.gov/research/projects/mpich2

- - - -

Installation instructions: 

   tar zxvf mpich2-xxxxx.tar.gz
   cd mpich20-xxxx
   ./configure --prefix=$HOME/local
   make
   make install

or see MPICH2 README file
