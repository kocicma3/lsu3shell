#include <SU3ME/global_definitions.h>
#include <SU3ME/distr2gamma2omega.h>
#include <SU3ME/ncsmSU3Basis.h>
#include <algorithm>
#include <iostream>

using namespace std;

void ShowBasis_fancy_style(IdenticalFermionsNCSMBasis& basis)
{
	//TODO: take output subroutines from SAMES and implement them here ...
}

void Print2(int i)
{
	if (i%2)
	{
		cout << i << "/2";
	}
	else
	{
		cout << i/2;
	}
}

void ShowBasis_basic_style(IdenticalFermionsNCSMBasis& basis)
{
	std::map<SU3xSU2::LABELS, size_t> Irreps;

	for (; basis.hasDistr(); basis.nextDistr())
	{
		const tSHELLSCONFIG& distribution = basis.getDistr(); 
		cout << "Distribution: ";
		ShowVector(distribution);
		cout << endl;
		continue;
		cout << "\t dim = " << basis.dimDistr() << "\t first state: " << basis.getFirstStateIdDistr() << endl;
		for (basis.rewindGamma(); basis.hasGamma(); basis.nextGamma())
		{
			const UN::SU3xSU2_VEC& gamma = basis.getGamma();

			int alpha = gamma_mult(gamma);

			cout << "Gamma: ";
			for (size_t i = 0; i < gamma.size(); ++i)
			{
				cout << "\ta=" << gamma[i].mult << "(" << (int)gamma[i].lm << " " << (int)gamma[i].mu << ")";
				Print2(gamma[i].S2);
				cout << "\t";
			}
			cout << "\t dim = " << basis.dimGamma() << "\t first state: " << basis.getFirstStateIdGamma() << endl;

			for (basis.rewindOmega(); basis.hasOmega(); basis.nextOmega())
			{
				const SU3xSU2_VEC& omega = basis.getOmega();

				int rho_mult = omega_mult(omega);

				cout << "\t\t\t";
				if (omega.empty())
				{
					cout << "single shell state ==> omega = {empty}";
					cout << "\tC2 = " << gamma.back().C2() << endl;

					Irreps[gamma.back()] += 1;
				}
				else
				{
					Irreps[omega.back()] += 1;

					for (size_t i = 0; i < omega.size(); ++i)
					{
						cout << "\trho=" << (int)omega[i].rho << "(" << (int)omega[i].lm << " " << (int)omega[i].mu << ")";
						Print2(omega[i].S2);
						cout << "\t";
					}
					cout << "\tC2 = " << omega.back().C2() << "\t dim = " << basis.dimOmega() << "\t first state: " << basis.getFirstStateIdOmega() << endl;
				}
/*
				int total_mult = alpha*rho_mult;
				SU3xSU2::Basis irrepBasis(basis.getCurrentSU3xSU2());
				for (int mult = 0; mult < total_mult; ++mult)
				{
					IdenticalFermionsNCSMBasis::IdType StartingStateId = basis.getFirstStateIdOmega() + mult*irrepBasis.dim();

					for (irrepBasis.rewind(); !irrepBasis.IsDone(); irrepBasis.nextL())
					{
						int LL = irrepBasis.L();
						for (int JJ = irrepBasis.Jmin(); JJ <= irrepBasis.Jmax(); JJ += 2)
						{
							for (int k = 0; k < irrepBasis.kmax(); ++k)
							{
								cout << "\t\t\t\t|a=" << mult+1 << " k=" << k+1 << " L=" << LL/2 << " J=";
								Print2(JJ);
								cout <<  ">\t" << StartingStateId + irrepBasis.getId(k, JJ) << endl;
							}
						}
					}
				}
*/				
			}
		}
	}

	cout << "List of unique SU(3)xSU(2):" << endl;
	for (std::map<SU3xSU2::LABELS, size_t>::const_iterator it = Irreps.begin(); it != Irreps.end(); ++it)
	{
		cout << it->first << "\t" << it->second << "\tC2=" << it->first.C2() << endl;
	}
}

int main() 
{
	const int Jcut = 999;
	int A = 4;
	IdenticalFermionsNCSMBasis::NCSMModelSpace modelSpace;
	modelSpace.push_back(IdenticalFermionsNCSMBasis::NhwSubspace(0, SU3C2GreaterEqual(0), SpinLessEqual(A)));
	modelSpace.push_back(IdenticalFermionsNCSMBasis::NhwSubspace(1, SU3C2GreaterEqual(0), SpinLessEqual(A)));
	modelSpace.push_back(IdenticalFermionsNCSMBasis::NhwSubspace(2, SU3C2GreaterEqual(0), SpinLessEqual(A)));
	modelSpace.push_back(IdenticalFermionsNCSMBasis::NhwSubspace(3, SU3C2GreaterEqual(0), SpinLessEqual(A)));
	modelSpace.push_back(IdenticalFermionsNCSMBasis::NhwSubspace(4, SU3C2GreaterEqual(0), SpinLessEqual(A)));
	modelSpace.push_back(IdenticalFermionsNCSMBasis::NhwSubspace(5, SU3C2GreaterEqual(0), SpinLessEqual(A)));
	modelSpace.push_back(IdenticalFermionsNCSMBasis::NhwSubspace(6, SU3C2GreaterEqual(0), SpinLessEqual(A)));

	IdenticalFermionsNCSMBasis basis(A, modelSpace, Jcut);

	cout << "dimension of the basis is " << basis.dim() << endl;

	char s;
	cout << "Do you want to list basis? [y/n]" << endl;
	if (basis.dim() > 60000)
	{
		cout << "Warning: the list may be way too looooooooooooooooooooooooooooooooong!!!" << endl;
	}
	cin >> s;
	if (s == 'y')
	{
		ShowBasis_basic_style(basis);
	}
}
