#include <iostream>

#include <SU3ME/SU3xSU2Basis.h>

void ShowJJBasis(int lm, int mu, int SS, int JJ) {
  SU3xSU2::BasisJfixed basis(SU3xSU2::LABELS(lm, mu, SS), JJ);

  for (basis.rewind(); !basis.IsDone(); basis.nextL()) {
    for (int jj = basis.Jmin(); jj <= basis.Jmax(); jj += 2) {
      for (int k = 0; k < basis.kmax(); ++k) {
        std::cout << " k:" << k << " 2L:" << basis.L() << " 2J:" << jj
                  << std::endl;
      }
    }
  }
}

void ShowFullBasis(int lm, int mu, int SS) {
  int JJmax = 2 * (lm + mu) + SS;

  SU3xSU2::BasisJcut basis(SU3xSU2::LABELS(lm, mu, SS), JJmax);

  for (basis.rewind(); !basis.IsDone(); basis.nextL()) {
    for (int jj = basis.Jmin(); jj <= basis.Jmax(); jj += 2) {
      for (int k = 0; k < basis.kmax(); ++k) {
        std::cout << " k:" << k << " 2L:" << basis.L() << " 2J:" << jj
                  << std::endl;
      }
    }
  }
}

int main() {
  int lm, mu, SS, JJ;

  std::cout << "Enter lambda:";
  std::cin >> lm;

  std::cout << "Enter mu:";
  std::cin >> mu;

  std::cout << "Enter 2S:";
  std::cin >> SS;

  std::cout << "Enter 2J [-1 if all values of J are permitted]:";
  std::cin >> JJ;

  if (JJ == -1) {
    ShowFullBasis(lm, mu, SS);
  } else {
    ShowJJBasis(lm, mu, SS, JJ);
  }
}
