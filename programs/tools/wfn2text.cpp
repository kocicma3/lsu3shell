#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <iomanip>

using namespace std;

int main(int argc, char **argv)
{
	if  (argc != 2)
	{
		cerr << "Usage: " << argv[0] << " <wfn file name>" << endl;
		return EXIT_FAILURE;
	}

	string filename(argv[1]);
	fstream wfn_file(filename.c_str(), std::ios::in | std::ios::binary | std::ios::ate); // open at the end of file so we can get file size
	if (!wfn_file)
	{
		cout << "Error: could not open '" << filename << "' wfn file" << endl;
		return EXIT_FAILURE;
	}

	size_t size = wfn_file.tellg();
	size_t nelems = size/sizeof(float);

	if (size%sizeof(float))
	{
		cout << "Error: size of file is not a multiple of float datatype size!" << endl;
		return false;
	}

	vector<float> wfn(nelems, 0);
	wfn_file.seekg (0, std::ios::beg);
	wfn_file.read((char*)wfn.data(), nelems*sizeof(float));

	cout << std::fixed;
	cout << std::setprecision(8);

	cout << "v={" << endl;
	for (size_t i = 0; i < nelems - 1; ++i)
	{
		cout << wfn[i] << "," << endl;
	}
	cout << wfn.back() << "};";
}
