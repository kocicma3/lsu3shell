#include <LSU3/ncsmSU3xSU2Basis.h>
#include <SU3ME/global_definitions.h>
#include <SU3ME/BaseSU3Irreps.h>
#include <vector>
#include <stack>
#include <ctime>
#include <sstream>
#include <LSU3/std.h>


using namespace cpp0x;
using std::vector;
using std::pair;
using std::map;
using std::string;
using std::fstream;
using std::make_pair;

using std::cout;
using std::endl;

enum {kSortByProb = 0, kSortByRatio = 1};
enum {kSSp = 0, kSSn = 1, kSS = 2, kLm = 3, kMu = 4, kProb = 5, kRatio = 6, kDim = 7};
typedef std::tuple<int, int, int, int, int, float, float, size_t> INFO;

struct Less_SU3
{
	inline bool operator() (const SU3::LABELS& l, const SU3::LABELS& r) const {return l.C2() < r.C2() || (l.C2() == r.C2() && l < r);}
};

struct SortByProbability
{
	inline bool operator() (const INFO& l, const INFO& r) const 
	{
		return std::get<kProb>(l) > std::get<kProb>(r);
	}
};

struct SortByRatio
{
	inline bool operator() (const INFO& l, const INFO& r) const 
	{
		return std::get<kRatio>(l) > std::get<kRatio>(r);
	}
};

void ConstructCMInvariantSpSnSlmmuSubspaces(const lsu3::CncsmSU3xSU2Basis& basis,	std::map<CTuple<int, 5>, size_t>& dimensions)
{
	for (int ipin_block = 0; ipin_block < basis.NumberOfBlocks(); ipin_block++)
	{
		if (!basis.NumberOfStatesInBlock(ipin_block))
		{
			continue;
		}
		uint32_t ip = basis.getProtonIrrepId(ipin_block);
		uint32_t in = basis.getNeutronIrrepId(ipin_block);

		uint16_t ap_max = basis.getMult_p(ip);
		uint16_t an_max = basis.getMult_n(in);

		SU3xSU2::LABELS wp = basis.getProtonSU3xSU2(ip);
		SU3xSU2::LABELS wn = basis.getNeutronSU3xSU2(in);

		for (int iwpn = basis.blockBegin(ipin_block); iwpn < basis.blockEnd(ipin_block); ++iwpn)
		{
			SU3xSU2::LABELS omega_pn = basis.getOmega_pn(ip, in, iwpn);
			uint32_t irrep_dim = ap_max*an_max*omega_pn.rho*basis.omega_pn_dim(iwpn);
			CTuple<int, 5> key;
			
			key[kSSp] = wp.S2; 
			key[kSSn] = wn.S2; 
			key[kSS] = omega_pn.S2; 
			key[kLm] = omega_pn.lm; 
			key[kMu] = omega_pn.mu;
			
			dimensions[key] += irrep_dim;
		}
	}
}

struct Less
{
	bool operator() (const pair<float, vector<int> >& l,  const pair<float, vector<int> >& r) {return l.first > r.first;}
};

bool ReadWfn(const string& wfn_file_name, lsu3::CncsmSU3xSU2Basis& basis, const int ndiag_wfn, vector<float>& wfn)
{
//	basis has to be generated for ndiag = 1 ==> it contains entire basis of a model space
	assert(basis.ndiag() == 1 && basis.SegmentNumber() == 0);

	fstream wfn_file(wfn_file_name.c_str(), std::ios::in | std::ios::binary | std::ios::ate); // open at the end of file so we can get file size
	if (!wfn_file)
	{
		cout << "Error: could not open '" << wfn_file_name << "' wfn file" << endl;
		return false;
	}

	size_t size = wfn_file.tellg();
	size_t nelems = size/sizeof(float);

	if (size%sizeof(float) || (nelems != basis.getModelSpaceDim()))
	{
		cout << "Error: file size == dim x sizeof(float): " << basis.getModelSpaceDim() << " x " << sizeof(float) << " = " << basis.getModelSpaceDim()*sizeof(float) << " bytes." << endl;
		cout << "The actual size of the file: " << size << " bytes!";
		return false;
	}

//	allocate memory for nelems floats
	wfn.resize(nelems); 
//	return file stream to beginning	
	wfn_file.seekg (0, std::ios::beg);

//	Load wave function generated for ndiag_wfn into a file that contains order
//	of basis states for ndiag=1	
	uint32_t ipos;
	uint16_t number_of_states_in_block;
	uint32_t number_ipin_blocks = basis.NumberOfBlocks();
	for (uint16_t i = 0; i < ndiag_wfn; ++i)
	{
		
//		Here I am using the fact that order of (ip in) blocks in basis split into ndiag_wfn sections is following:
//		
//	section --> {iblock0, iblock1, ...... }
//		----------------------------------
//		0     {0, ndiag_wfn, 2ndiag_wfn, 3ndiag_wfn ....}
//		1     {1, ndiag_wfn+1, 2ndiag_wfn+1, 3ndiag_wfn+1, ...}
//		2     {2, ndiag_wfn+2, 2ndiag_wfn+1, 3ndiag_wfn+2, ...}
//		.
//		.
//		.
		for (uint32_t ipin_block = i; ipin_block < number_ipin_blocks; ipin_block += ndiag_wfn)
		{
//	obtain position of the current block in model space basis [i.e. with ndiag=1] and its size
			ipos = basis.BlockPositionInSegment(ipin_block);
			number_of_states_in_block = basis.NumberOfStatesInBlock(ipin_block);
			
			wfn_file.read((char*)&wfn[ipos], number_of_states_in_block*sizeof(float));
		}
	}
	return true;
}


//	This utility helps in deciding what (Nmax+2)hw Sp Sn S (lm+2 mu) subspace include in the truncated model space. 
//	This is based either on the probability of (Nmax) Sp Sn S (lm mu) subspace and/or ratio
//   
//   	prob
//		----
//      dim
//	----------
//  ratio_max 
//
//	prob: probability of (Nmax)hw Sp Sn S (lm mu) subspace
//	dim: dimension of (Nmax+2)hw Sp Sn S (lm+2 mu) subspace
//	ratio_max: the highest prob/dim ratio
//
int main(int argc, char* argv[])
{
	if (argc != 6)
	{
		cout << endl;
	  	cout << "Usage: "; 
		cout << argv[0] << " <filename1: Nmax model space> <ndiag> <eigenstate> <filename2: full Nmax+2 subspace> <0 or 1: sort by prob or ratio>" << endl;
		cout << "filename1: model space in which a given <eigenstates> was evaluated" << endl;
		cout << "filename2: space that contains configurations carrying Nmax+2 ho quanta" << endl;
		cout << "0: resulting table is sorted in decreasing order according to the probability" << endl; 
		cout << "1: position of each Sp Sn S (lm mu) subspace the resulting table is resovled based on (prob/dim)/ratio_max" << endl; 
		cout << "where dim = dim [Sp Sn S (lm+2 mu)], of Nmax+2 model space" << endl; 
		cout << endl;
		return 0;
	}

	int sort_type;
	std::istringstream(argv[5]) >> sort_type;
	if (sort_type != 0 && sort_type != 1)
	{
		cout << "unknown sort type " << sort_type << endl;
		return 0;
	}

	
//	{Sp, Sn, S, lm, mu} quantum numbers of Nmax+2 model space associated with
//	their dimensions
	std::map<CTuple<int, 5>, size_t> nextNhw_dimensions;
	proton_neutron::ModelSpace nextNhwModelSpace(argv[4]);
	//TODO
	lsu3::CncsmSU3xSU2Basis  basis_nextNhw(nextNhwModelSpace, 0, 1);
	ConstructCMInvariantSpSnSlmmuSubspaces(basis_nextNhw, nextNhw_dimensions);

	unsigned int ndiag;
	std::istringstream(argv[2]) >> ndiag;
	const string wfn_file_name(argv[3]);

//	irreps_projections[i]: contains a map of {Sp, Sn, S, (lm mu)} irreps which
//	are associated with the probability amplitude. Irreps span ncsmModelSpace[i].
	map<CTuple<int, 5>, float> irreps_projections;
	proton_neutron::ModelSpace wfnModelSpace(argv[1]);
	int Nmax = wfnModelSpace.back().N();
	lsu3::CncsmSU3xSU2Basis  basis_full(wfnModelSpace, 0, 1);
	vector<float> wfn;
	cout << "Reading " << wfn_file_name << " that was saved with " << ndiag << " processors." << endl;
	ReadWfn(wfn_file_name, basis_full, ndiag, wfn);
	cout << "Done" << endl;

	if (wfnModelSpace.JJ() != nextNhwModelSpace.JJ())
	{
		cout << "Error: JJ for wfn model space JJ=" << wfnModelSpace.JJ() << " is different from JJ for Nmax+2 subspace JJ=" << nextNhwModelSpace.JJ() << endl;
		return 0;
	}

	uint32_t ipos = 0;
	for (unsigned int ipin_block = 0; ipin_block < basis_full.NumberOfBlocks(); ipin_block++)
	{
		if (basis_full.NumberOfStatesInBlock(ipin_block) == 0)
		{
			continue;
		}
			
		uint32_t ip = basis_full.getProtonIrrepId(ipin_block);
		uint32_t in = basis_full.getNeutronIrrepId(ipin_block);
		uint32_t Nhw =  basis_full.nhw_p(ip) + basis_full.nhw_n(in);

		SU3xSU2::LABELS w_ip(basis_full.getProtonSU3xSU2(ip));
		SU3xSU2::LABELS w_in(basis_full.getNeutronSU3xSU2(in));

		uint16_t aip_max = basis_full.getMult_p(ip);
		uint16_t ain_max = basis_full.getMult_n(in);

		uint32_t ibegin = basis_full.blockBegin(ipin_block);
		uint32_t iend = basis_full.blockEnd(ipin_block);
//	loop over wpn irreps that result from coupling ip x in	
		for (uint32_t iwpn = ibegin; iwpn < iend; ++iwpn)
		{
			IRREPBASIS irrep_basis(basis_full.Get_Omega_pn_Basis(iwpn));
			uint16_t amax = aip_max*ain_max*SU3::mult(w_ip, w_in, irrep_basis);

			float wpn_projection = 0.0; // the total projection of | (ap=* wp) x (an=* wn) -> rho=* wpn k=* L=* J=*>  states
//	The order of loops is extremely important and must be same as the one being
//	used to calculate the matrix elements.
			for (irrep_basis.rewind(); !irrep_basis.IsDone(); irrep_basis.nextL())
			{
				for (int J = irrep_basis.Jmin(); J <= irrep_basis.Jmax(); J += 2)
				{
					for (int k = 0; k < irrep_basis.kmax(); ++k)
					{
						size_t kfLfJId = irrep_basis.getId(k, J);
						for (int a = 0; a < amax; ++a)
						{
							float dcoeff = wfn[ipos++];
//						assert((basis_state_id + 1) == number_coeffs_read);
							if (!Negligible(dcoeff))
							{
								wpn_projection += dcoeff*dcoeff;
							}
						}
					}
				}
			}

			if (Nhw == Nmax)
			{
				CTuple<int, 5> spsnslmu;
				spsnslmu[kSSp] = w_ip.S2; spsnslmu[kSSn] = w_in.S2; spsnslmu[kSS] = irrep_basis.S2; spsnslmu[kLm] = irrep_basis.lm; spsnslmu[kMu] = irrep_basis.mu;
				irreps_projections[spsnslmu] += wpn_projection;
			}
		}
	}

	map<CTuple<int, 5>, float>::const_iterator it = irreps_projections.begin();
	float ratio_max = 0.0;
	for (; it != irreps_projections.end(); ++it)
	{
		CTuple<int, 5> key(it->first);
		float projection(it->second);
		key[3] += 2; // lm + 2
		size_t dim = nextNhw_dimensions[key];
		float ratio = projection / (float)dim;
		if (ratio > ratio_max)
		{
			ratio_max = ratio;
		}
	}

	it = irreps_projections.begin();
	vector<INFO> vresults; 
//	iterate over SU(3)xSU(2) subspaces of Nmax configurations
	for (; it != irreps_projections.end(); ++it)
	{
		CTuple<int, 5> key(it->first);
		float projection(it->second);
//	Sp Sn S (lm mu) ----> Sp Sn S (lm+2 mu)		
		key[3] += 2; // lm+2
		//	dim: dimension of Nmax+2 Sp Sn S (lm+2 0) subspace
		size_t dim = nextNhw_dimensions[key];
		float ratio = (projection/(float)dim)/ratio_max;
		INFO element(key[0], key[1], key[2], (key[3]), key[4], projection, ratio, dim);
		vresults.push_back(element);
	}

	if (sort_type == kSortByProb)
	{
		std::sort(vresults.begin(), vresults.end(), SortByProbability());
	}
	else if (sort_type == kSortByRatio)
	{
		std::sort(vresults.begin(), vresults.end(), SortByRatio());
	}

	cout << "List of " << Nmax+2 << "hw U(3)xSU(2) subspaces \t " << Nmax << "hw probabilities; conf. importance weight; dimension " << endl;
	for (int i = 0; i < vresults.size(); ++i)
	{
		INFO element(vresults[i]);
		int Sp = std::get<kSSp>(element);
		int Sn = std::get<kSSn>(element);
		int S  = std::get<kSS>(element);
		int lm = std::get<kLm>(element);
		int mu = std::get<kMu>(element);
		float proj = std::get<kProb>(element);
		float ratio = std::get<kRatio>(element);
		size_t dim  = std::get<kDim>(element);

//		cout << "Sp=" << Sp << " Sn=" << Sn << " S=" << S << "(" << lm << " " << mu << ")\t\t\t\t" << proj << "\t" << ratio << "\t\t" << dim << endl;

//		Show Nhw Sp Sn S lm mu quantum numbers and associated information
//		if (!Negligible(proj))
//		{
//			cout << Nmax+2 << " " << Sp << " " << Sn << " " << S << " " << lm << " " << mu << "\t" << proj << "\t" << ratio << "\t" << dim << endl;
//		}


//		Short output for generating a list of N Sp Sn S lm mu quantum numbers 
//		if (!Negligible(proj))
		{
			cout << Nmax+2 << " " << Sp << " " << Sn << " " << S << " " << lm << " " << mu << endl;
		}
	}
}
