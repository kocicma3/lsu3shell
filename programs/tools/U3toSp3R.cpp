#include <LSU3/std.h>
#include <LSU3/ncsmSU3xSU2Basis.h>
#include <SU3NCSMUtils/CTuple.h>

#include <vector>
#include <map>
#include <set>
#include <iostream>

#include <boost/serialization/serialization.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/utility.hpp>

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/text_oarchive.hpp>

using namespace std;

typedef CTuple<int, 5> SP3R_LABELS;
enum SP3R_LABELS_INDICES {kNsig = 0, kLMsig = 1, kMUsig = 2, kLMn = 3, kMUn = 4};

typedef CTuple<int, 6> U3_LABELS;
enum U3_LABELS_INDICES {kN = 0, kSSp = 1, kSSn = 2, kSS = 3, kLM = 4, kMU = 5};

typedef double NAB;
typedef std::map<U3_LABELS, std::vector<std::pair<SP3R_LABELS, NAB> > > MAP;


inline float C2SU3(int lm, int mu)
{
	return (2.0/3.0)*(lm*lm + mu*mu + lm*mu + 3*lm + 3*mu);
}

inline float C2Sp3R(int lm, int mu, float N)
{
	return C2SU3(lm, mu)+(1.0/3.0)*N*N - 4.0*N;
}

// Calculate total harmonic oscillator energy of nuclei at Nhw = n0hw space.
float getN0(int nprotons, int nneutrons, int n0hw)
{
	// N0 ... harmonic oscillator energy of a valence space configurations
	float N0 = 0;
	std::vector<uint8_t> proton_valence_distr;
	std::vector<uint8_t> neutron_valence_distr;

	for (int n = 0; true; ++n)
	{
		int8_t nferms = (n+1)*(n+2);
		if (nprotons > nferms)
		{
			proton_valence_distr.push_back(nferms);
			nprotons -= nferms;
		}
		else
		{
			proton_valence_distr.push_back(nprotons);
			break;
		}
	}

	for (int n = 0; true; ++n)
	{
		int8_t nferms = (n+1)*(n+2);
		if (nneutrons > nferms)
		{
			neutron_valence_distr.push_back(nferms);
			nneutrons -= nferms;
		}
		else
		{
			neutron_valence_distr.push_back(nneutrons);
			break;
		}
	}

	for (int n = 0; n < proton_valence_distr.size(); ++n)
	{
		N0 += proton_valence_distr[n]*(n + 1.5);
	}
	for (int n = 0; n < neutron_valence_distr.size(); ++n)
	{
		N0 += neutron_valence_distr[n]*(n + 1.5);
	}
//	now we have to add additional energy [n0hw] energy and subtract center-of-mass HO energy
	return (N0 + n0hw) - 1.5;
}

void Get20Coupling(int r, SU3_VEC& wn)
{
	int n1, n2, n3;
	for (n1 = 2*r; n1 >= 0; n1-=2)
	{
		for (n2 = 0; n2 <= n1; n2 += 2)
		{
			for (n3 = 0; n3 <= n2; n3 += 2)
			{
				if (n1+n2+n3 == 2*r) {
					wn.push_back(SU3::LABELS(1, (n1 - n2), (n2 - n3)));
				}
			}
		}
	}
}

void GenerateMapSpSnSlmmu(lsu3::CncsmSU3xSU2Basis& basis, MAP& resulting_map)
{ 
	U3_LABELS key;

	const uint32_t number_blocks = basis.NumberOfBlocks();
	for(int iblock = 0; iblock < number_blocks; ++iblock)
	{
		uint32_t ip = basis.getProtonIrrepId(iblock);
		uint32_t in = basis.getNeutronIrrepId(iblock);

		int Nhw = basis.nhw_p(ip) + basis.nhw_n(in);
		int Sp = (basis.getProtonSU3xSU2(ip)).S2;
		int Sn = (basis.getNeutronSU3xSU2(in)).S2;

		key[kN] = Nhw;
		key[kSSp] = Sp;
		key[kSSn] = Sn;
		
		uint32_t ibegin = basis.blockBegin(iblock);
		uint32_t iend = basis.blockEnd(iblock);
		for (uint32_t iwpn = ibegin; iwpn < iend; ++iwpn)
		{
			SU3xSU2::LABELS omega_pn(basis.getOmega_pn(ip, in, iwpn));
			key[kSS] = omega_pn.S2;
			key[kLM] = omega_pn.lm;
			key[kMU] = omega_pn.mu;
			MAP::iterator it = resulting_map.find(key);
			if (it == resulting_map.end())
			{
//	Here I suppose that each U(3) subspace has symplectic bandheads
//	This is however does not seem to be valid as, e.g. in 6Li 
//	I have encountered subspaces that did not have any bandheads
				SP3R_LABELS sp3r_labels;
				sp3r_labels[kNsig] = key[kN];
				sp3r_labels[kLMsig] = key[kLM];
				sp3r_labels[kMUsig] = key[kMU];
//				bandhead has (lm mu)_n = (0 0)
				sp3r_labels[3] = 0;
				sp3r_labels[4] = 0;
				resulting_map.insert(std::make_pair(key, std::vector<std::pair<SP3R_LABELS, NAB> > (1, std::make_pair(sp3r_labels, 0.0))));
			}
		}
	}
}


void GenerateSp3R(lsu3::CncsmSU3xSU2Basis& basis, MAP& resulting_map)
{
	float n0 = getN0(basis.NProtons(), basis.NNeutrons(), 0); // compute N_sigma corresponding to 0hw subspace
	for (MAP::iterator it = resulting_map.begin(); it != resulting_map.end(); ++it)
	{
//	Generate U3 quantum numbers of Sp(3,R) bandhead 
		int N0hw = (it->first)[kN];
		int SSp = (it->first)[kSSp];
		int SSn = (it->first)[kSSn];
		int SS  = (it->first)[kSS];
		int lm0  = (it->first)[kLM];
		int mu0  = (it->first)[kMU];

		float a2 = C2Sp3R(lm0, mu0, n0 + N0hw);

		SU3::LABELS ir_sigma(1, lm0, mu0);
		
//	bandhead labels are already stored in it->second[0]
//		assert((it->second[0])[kNsig] == N0hw);
//		assert((it->second[0])[kLMsig] == lm0); 
//		assert((it->second[0])[kMUsig] == mu0);
//		assert((it->second[0])[kLMn] == 0);
//		assert((it->second[0])[kMUn] == 0);
		for (int nhw = N0hw + 2; nhw <= basis.Nmax(); nhw += 2)
		{
			int r = (nhw - N0hw)/2;
			SU3_VEC wn;
			Get20Coupling(r, wn);
			for (int i = 0; i < wn.size(); ++i)
			{
				SU3_VEC w;
				SU3::Couple(ir_sigma, wn[i], w);
//	iterate over products of (lm_sig mu_sig) x (lm_n mu_n)				
				for (int j = 0; j < w.size(); ++j)
				{
					double a1 = C2Sp3R(w[j].lm, w[j].mu, (n0 + nhw));
					double nab = (1.0/(2.0*sqrt(6.0)))*(a1 - a2);
					
					U3_LABELS key;
					key[kN]   = nhw;
					key[kSSp] = SSp;
					key[kSSn] = SSn;
					key[kSS]  = SS;
					key[kLM]  = w[j].lm;
					key[kMU]  = w[j].mu;

					MAP::iterator it = resulting_map.find(key); // is resulting U(3) irrep nhw Sp Sn S (lm mu) in model space ?
					if (it != resulting_map.end())
					{
						SP3R_LABELS sp3r_labels;

						sp3r_labels[kNsig] = N0hw; // N_sigma
						sp3r_labels[kLMsig] = lm0;
						sp3r_labels[kMUsig] = mu0;
						sp3r_labels[kLMn] = wn[i].lm;
						sp3r_labels[kMUn] = wn[i].mu;

						it->second.push_back(std::make_pair(sp3r_labels, nab));
//						cout << n0 << "(" << (int)lm << " " << (int)mu << ") ";
//						cout << "(" << (int)wn[i].lm << " " << (int)wn[i].mu << ") ";
//						cout << "(" << (int)w[j].lm << " " << (int)w[j].mu << ") " << "\t" << c2 << endl;
					}
				}
			}
		}
	}
}

void Show(float n0, MAP& resulting_map)
{
	for (MAP::iterator it = resulting_map.begin(); it != resulting_map.end(); ++it)
	{
		cout << (it->first)[kN] << "hw" << " SSp:" << (it->first)[kSSp] << " SSn:" << (it->first)[kSSn] << " SS:" << (it->first)[kSS]; 
		cout << " (" << (it->first)[kLM] << " " << (it->first)[kMU] << ")" << endl;

		if (it->second.size() == 0)
		{
			cout << "empty" << endl;
		}
		else
		{
			int Nhw = (it->first)[kN];
			int lm = (it->first)[kLM];
			int mu = (it->first)[kMU];

			float a1 = C2Sp3R(lm, mu, n0 + Nhw); // lm_sig mu_sig N_sig
			for (int i = 0; i < it->second.size(); ++i)
			{
				SP3R_LABELS labels = it->second[i].first;
				int Nsighw = labels[kNsig];
				int lm_sig = labels[kLMsig];
				int mu_sig = labels[kMUsig];

				float a2 = C2Sp3R(lm_sig, mu_sig, n0 + Nsighw);
				float c2 = (1.0/(2.0*sqrt(6.0)))*(a1 - a2);
				cout << "\t" << Nsighw << "(" << lm_sig << " " << mu_sig << ") (" << labels[kLMn] << " " << labels[kMUn] << ")";
				cout << "\t" << c2 << "\t" << it->second[i].second << endl;
			}
		}
	}
}

int main(int argc, char** argv)
{
	if (argc != 3 && argc != 2)
	{
		cout << "Generate expansion of U(3)x U(2)_p x U(2)_n x U(2)_pn subspaces of a <model space> in terms of possible Sp(3, R) quantum numbers N_sigma (lm_sigma mu_sigma) (lm_n mu_n).\n";
		cout << "Usage: " << argv[0] << " <model space> [<output file>]\n";
		cout << "<model space> ... model space to decompose into Sp(3,R) quantum labels\n";
		cout << "<output file> ... resulting data file can be used by any code that needs a fast mapping N Sp Sn S (lm mu) ---> {N_sig (lm_sig mu_sig) (lm_n mu_n), ... } \n";
		cout << "                  if not provided, user friendly output is generated.\n";
		return EXIT_FAILURE;
	}
	
	MAP resulting_map;

	std::string model_space_file_name(argv[1]);
	lsu3::CncsmSU3xSU2Basis basis;
	bool modelSpaceProvided = model_space_file_name.find(".model_space") != std::string::npos; // true if the first argument contains ".model_space"
	if (!modelSpaceProvided) // ==> we need to load basis states from files
	{
		cout << "Reading basis from '" << MakeBasisName(model_space_file_name, 0, 1) << "'." << std::endl;
		basis.LoadBasis(model_space_file_name, 0, 1);
	}
	else
	{
		proton_neutron::ModelSpace ncsmModelSpace;
		ncsmModelSpace.Load(model_space_file_name);
		basis.ConstructBasis(ncsmModelSpace, 0, 1);
	}
	float n0 = getN0(basis.NProtons(), basis.NNeutrons(), 0); // compute N_sigma corresponding to 0hw subspace

	GenerateMapSpSnSlmmu(basis, resulting_map);
	GenerateSp3R(basis, resulting_map);
	if (argc == 2)
	{
		Show(n0, resulting_map);
	}
	else
	{
#ifdef useBinaryIO
		std::ofstream u3_sp3r_map_output(argv[2], std::ios::binary |  std::ios::out | std::ios::trunc);  
		boost::archive::binary_oarchive oa(u3_sp3r_map_output);
#else		
		std::ofstream u3_sp3r_map_output(argv[2]);
		boost::archive::text_oarchive oa(u3_sp3r_map_output);
#endif		
		oa << resulting_map;
		oa << n0;
	}
}
