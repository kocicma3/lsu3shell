#include <SU3ME/global_definitions.h>
#include <SU3ME/distr2gamma2omega.h>
#include <SU3ME/ncsmSU3Basis.h>
#include <algorithm>
#include <iostream>

using namespace std;

void Print2(int i)
{
	if (i%2)
	{
		cout << i << "/2";
	}
	else
	{
		cout << i/2;
	}
}

void ShowBasis_basic_style(IdenticalFermionsNCSMBasis& basis)
{
	int n[2], lambda, mu, SS, k, LL, JJ;
	for (; basis.hasDistr(); basis.nextDistr())
	{
		const tSHELLSCONFIG& distribution = basis.getDistr(); 
		for (basis.rewindGamma(); basis.hasGamma(); basis.nextGamma())
		{
			const UN::SU3xSU2_VEC& gamma = basis.getGamma();
			assert(gamma_mult(gamma) == 1);

			if (gamma.size() == 1)
			{
				n[0] = n[1] = std::find(distribution.begin(), distribution.end(), 2) - distribution.begin();
				lambda = gamma[0].lm;
				mu = gamma[0].mu;
				SS = gamma[0].S2;

				for (basis.rewindOmega(); basis.hasOmega(); basis.nextOmega())
				{
					SU3xSU2::BasisIdenticalFermsCompatible irrepBasis(gamma[0]);
					IdenticalFermionsNCSMBasis::IdType StartingStateId = basis.getFirstStateIdOmega();
					for (irrepBasis.rewind(); !irrepBasis.IsDone(); irrepBasis.nextL())
					{
						int LL = irrepBasis.L();
						for (int JJ = irrepBasis.Jmin(); JJ <= irrepBasis.Jmax(); JJ += 2)
						{
							for (int k = 0; k < irrepBasis.kmax(); ++k)
							{
								cout <<  StartingStateId + irrepBasis.getId(k, JJ) + 1 << "\t" << n[0] << " " << n[1] << " " << lambda << " " << mu << " " << SS << " " << k << " " << LL << " " << JJ << endl;
							}
						}
					}
				}
			}
			else
			{
				assert(gamma.size() == 2);

				for (size_t i = 0; i < gamma.size(); ++i)
				{
					assert(gamma[i].mu == 0 && gamma[i].S2 == 1);
					n[i] = gamma[i].lm;
				}

				for (basis.rewindOmega(); basis.hasOmega(); basis.nextOmega())
				{
					const SU3xSU2_VEC& omega = basis.getOmega();
					assert(omega.size() == 1);

					lambda = omega[0].lm;
					mu = omega[0].mu;
					SS = omega[0].S2;

					int rho_mult = omega_mult(omega);
					assert(rho_mult == 1);

					int total_mult = 1;
					SU3xSU2::BasisIdenticalFermsCompatible irrepBasis(basis.getCurrentSU3xSU2());
					for (int mult = 0; mult < total_mult; ++mult)
					{
						IdenticalFermionsNCSMBasis::IdType StartingStateId = basis.getFirstStateIdOmega() + mult*irrepBasis.dim();

						for (irrepBasis.rewind(); !irrepBasis.IsDone(); irrepBasis.nextL())
						{
							int LL = irrepBasis.L();
							for (int JJ = irrepBasis.Jmin(); JJ <= irrepBasis.Jmax(); JJ += 2)
							{
								for (int k = 0; k < irrepBasis.kmax(); ++k)
								{
									cout <<  StartingStateId + irrepBasis.getId(k, JJ) + 1 << "\t" << n[0] << " " << n[1] << " " << lambda << " " << mu << " " << SS << " " << k << " " << LL << " " << JJ << endl;
								}
							}
						}
					}
				}
			}
		}
	}
}

int main() 
{
	int Nmax = 6;
	const int Jcut = 999;
	int A = 2;

	assert(A == 2);

	IdenticalFermionsNCSMBasis::NCSMModelSpace modelSpace;
	for (size_t i = 0; i <= Nmax; ++i)
	{
		modelSpace.push_back(IdenticalFermionsNCSMBasis::NhwSubspace(i, SU3C2GreaterEqual(0), SpinLessEqual(A)));
	}
	IdenticalFermionsNCSMBasis basis(A, modelSpace, Jcut);

	cout << basis.dim() << endl;

	ShowBasis_basic_style(basis);
}
