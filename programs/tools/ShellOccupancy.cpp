#include <SU3ME/CShellConfigurations.h>
#include <map>
#include <iostream>
#include <bitset>
#include <cstdio>
#include <cstdlib>

using namespace std;

// This is just a short example how to use class CShellConfigurations
void Test_CShellConfigurations(size_t Z, size_t N, size_t Nmax)
{
	vector<unsigned> vMaxProtons;
	vector<unsigned> vMaxNeutrons;

	CShellConfigurations ProtonConfs(Nmax, Z);
	CShellConfigurations NeutronConfs(Nmax, N);

	ProtonConfs.GetMaxFermionsShells(vMaxProtons);
	NeutronConfs.GetMaxFermionsShells(vMaxNeutrons);

	cout << "ho shell" << "\t" << "number of protons" << endl;
	for (size_t i = 0; i < vMaxProtons.size(); ++i)
	{
		cout << i << "\t\t" ;
		unsigned min, max;
		min = 1;
		max = vMaxProtons[i];
		for (int j = min; j <= max; ++j)
		{
			cout << j << " ";
		}
		cout << endl;
	}
	cout << "ho shell" << "\t" << "number of neutrons" << endl;
	for (size_t i = 0; i < vMaxNeutrons.size(); ++i)
	{
		cout << i << "\t\t" ;
		unsigned min, max;
		min = 1;
		max = vMaxNeutrons[i];
		for (int j = min; j <= max; ++j)
		{
			cout << j << " ";
		}
		cout << endl;
	}
}

int main() 
{	
	size_t Z, N;
	size_t Nmax;
    cout << "Enter the number of protons ... ";
    cin >> Z;
    cout << "Enter the number of neutron ... ";
    cin >> N;
    cout << "Enter the maximal model space size Nmax=";
    cin >> Nmax;

//	HWSMemoryRequirementsForNmaxSpace(Z, Nmax);

	cout << endl << endl << "shell structure of the building configurations" << endl;
	Test_CShellConfigurations(Z, N, Nmax);		
}
