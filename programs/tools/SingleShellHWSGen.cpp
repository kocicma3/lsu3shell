/****************************************************************
  SingleShellHWSGen.cpp                      

  3/5/11 (mac): Include global_definitions.h for global variables.

****************************************************************/
#include <SU3ME/CHwsGen.h>
#include <SU3ME/global_definitions.h>
#include <iostream>
#include <stdexcept>

using namespace std;

int main() 
{
	unsigned n;
	unsigned A;
	unsigned S2;
	unsigned short iType;

	CNullSpaceSolver::SolverType NullSpaceMethodType;
	HWSOutputType OutputType;

  	cout << "Enter oscillator number quanta ... ";
    cin >> n;

	size_t nsps = nSPS(n);
	size_t nspsmax = sizeof(unsigned long)*8;

	if (nsps > nspsmax) {
		cerr << "HWS generator for harmonic ocillator shell n=" << n << " has not been implemented yet." << endl;
		exit(EXIT_FAILURE);
	}

    cout << "Enter number of fermions = ";
    cin >> A;
 	cout << "Enter 2*S ... ";
    cin >> S2;
 	cout << "Choose a numerical method for finding null space of Czx, Cxy and Sp operators:" << endl;
 	cout << "1\t QR with column pivoting [Eigen 2.0 library]" << endl; 
 	cout << "2\t QR with full pivoting [Eigen 2.0 library]" << endl; 
 	cout << "3\t QR with full pivoting. [GNU Scientific Library]" << endl; 
 	cout << "4\t SVD for matrices with m>=n & QR with column pivoting for matrices with m < n. [Eigen 2.0 library]" << endl; 
 	cout << "5\t SVD for matrices with m>=n & QR with full pivoting for matrices with m < n. [Eigen 2.0 library]" << endl; 
 	cout << "6\t SVD for matrices with m>=n & QR with full pivoting for matrices with m < n. [GNU Scientific Library]" << endl; 

//	enum SolverType {ColPivotQR_Eigen, FullPivotQR_Eigen, QR_GSL, SVD_ColPivotQR_Eigen, SVD_FullPivotQR_Eigen, SVD_QR_GSL};

	cin >> iType;
	switch (iType) 
	{
	case 1: 
		NullSpaceMethodType = CNullSpaceSolver::ColPivotQR_Eigen; 
		break;
	case 2: 
		NullSpaceMethodType = CNullSpaceSolver::FullPivotQR_Eigen; 
		break;
	case 3: 
		NullSpaceMethodType = CNullSpaceSolver::QR_GSL; 
		break;
	case 4: 
		NullSpaceMethodType = CNullSpaceSolver::SVD_ColPivotQR_Eigen; 
		break;
	case 5: 
		NullSpaceMethodType = CNullSpaceSolver::SVD_FullPivotQR_Eigen; 
		break;
	case 6: 
		NullSpaceMethodType = CNullSpaceSolver::SVD_QR_GSL; 
		break;
	default:
		cerr << "You did not specify a valid numerical method => QR with column pivoting is being selected.";
		NullSpaceMethodType = CNullSpaceSolver::ColPivotQR_Eigen;
	}
	cout << "Store SU(3)xSU(2) highest-weight-states in" << endl;
	cout << "1\t binary file " << endl;
	cout << "2\t text file" << endl;
	cin >> iType;
	if (iType == 1) {
		OutputType = HWSOutputBin;
	}
	else if (iType == 2) 
	{
		OutputType = HWSOutputText;
	}
	else 
	{
		cerr << "File format is not valid => binary format is being set." << endl;
		OutputType = HWSOutputBin;
	}


	if (nsps <= 32) 
	{
		CHwsGen<UN::BASIS_STATE_32BITS> HWSGEN(nmax, NullSpaceMethodType, OutputType);
		HWSGEN.GenerateHWS(n, A, S2);
	}
	else if (nsps <= 64) 
	{
		CHwsGen<UN::BASIS_STATE_64BITS> HWSGEN(nmax, NullSpaceMethodType, OutputType);
		HWSGEN.GenerateHWS(n, A, S2);
	}
	else if (nsps <= 128) 
	{
		CHwsGen<UN::BASIS_STATE_128BITS> HWSGEN(nmax, NullSpaceMethodType, OutputType);
		HWSGEN.GenerateHWS(n, A, S2);
	}
	else if (nsps <= 256) 
	{
		CHwsGen<UN::BASIS_STATE_256BITS> HWSGEN(nmax, NullSpaceMethodType, OutputType);
		HWSGEN.GenerateHWS(n, A, S2);
	}
	else
	{
		std::string error_message("Calculation of single-shell rme for harmonic oscillator shell n = ");
		error_message += n;
		error_message += " has not been implemented yet!";
		throw std::logic_error(error_message);
	}
}
