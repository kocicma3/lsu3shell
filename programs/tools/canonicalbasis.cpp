#include <UNU3SU3/UNU3SU3Basics.h>
#include <iostream>
#include <algorithm>

using namespace std;


int main() 
{
	int lm1, mu1, lm2, mu2;
	do 
	{
		std::vector<SU3::LABELS> vResultingIrreps;

	    cout << "Enter (lm1 mu1) " << endl;
	    cin >> lm1 >> mu1;

		vector<SU3::CANONICAL> Su3CanonicalBasis;
		SU3::GetCanonicalBasis(SU3::LABELS(lm1, mu1), Su3CanonicalBasis, true);
		for (size_t i = 0; i < Su3CanonicalBasis.size(); ++i)
		{
			cout << Su3CanonicalBasis[i] << "\t\t (1/3)*n + (1/6)*eps --> ";
			cout << (1.0/3.0)*(mu1) + (1.0/6.0)*Su3CanonicalBasis[i].eps <<endl;
		}
	} while(true);
}
