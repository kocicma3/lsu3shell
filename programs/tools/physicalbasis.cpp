#include <UNU3SU3/UNU3SU3Basics.h>
#include <iostream>
#include <algorithm>

using namespace std;


int main() 
{
	int lm1, mu1, lm2, mu2;
	do 
	{
		std::vector<SU3::LABELS> vResultingIrreps;

	    cout << "Enter (lm1 mu1) " << endl;
	    cin >> lm1 >> mu1;

		vector<SU3::SO3::LABELS> KL;
		SU3::GetSO3Labels(SU3::LABELS(lm1, mu1), KL);
		for (size_t i = 0; i < KL.size(); ++i)
		{
			cout << "k = " << (int)KL[i].K << " L=" << (int)KL[i].L2/2 << endl;
		}
	} while(true);
}
