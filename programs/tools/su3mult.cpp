#include <UNU3SU3/UNU3SU3Basics.h>
#include <iostream>
#include <algorithm>

using namespace std;


int main() 
{
	int lm1, mu1, lm2, mu2;
	do 
	{
		std::vector<SU3::LABELS> vResultingIrreps;

	    cout << "Enter (lm1 mu1) " << endl;
	    cin >> lm1 >> mu1;
    	cout << "Enter (lm2 mu2) " << endl;
	    cin >> lm2 >> mu2;
		
		SU3::LABELS ir1(lm1, mu1), ir2(lm2, mu2);

		SU3::Couple(ir1, ir2, vResultingIrreps);
		for (size_t i = 0; i < vResultingIrreps.size(); ++i)
		{
			cout << (int)vResultingIrreps[i].rho << " (" << (int)vResultingIrreps[i].lm << " " << (int)vResultingIrreps[i].mu << ")" << endl;
		}
	} while(true);
}
