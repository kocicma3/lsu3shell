#include <UNU3SU3/CUNMaster.h>
#include <vector>
using namespace std;

struct OrderSU3MULTByC2
{	
	inline bool operator() (const UN::SU3& L, const UN::SU3& R) const { return L.C2() > R.C2(); }
};

int main() 
{
	CUNMaster UNMaster;
	std::vector<std::pair<SU2::LABEL, UN::SU3_VEC> > SpinSU3multLabels;
	unsigned A;
	unsigned n;

    cout << "Enter oscillator number quanta ... ";
    cin >> n;
    cout << "Enter number of fermions = ";
    cin >> A;

	UNMaster.GetAllowedSU3xSU2Irreps(n, A, SpinSU3multLabels);

	std::vector<std::pair<SU2::LABEL, UN::SU3_VEC> >::iterator itSpin = SpinSU3multLabels.begin();
	std::vector<std::pair<SU2::LABEL, UN::SU3_VEC> >::iterator LAST_SPIN = SpinSU3multLabels.end();
	for (size_t ispin = 0; ispin < SpinSU3multLabels.size(); ++ispin)
	{
		int S2 = SpinSU3multLabels[ispin].first;

		std::sort(SpinSU3multLabels[ispin].second.begin(), SpinSU3multLabels[ispin].second.end(), OrderSU3MULTByC2());

		UN::SU3_VEC::const_iterator citSU3mult = SpinSU3multLabels[ispin].second.begin();
		UN::SU3_VEC::const_iterator LAST_SU3MULT = SpinSU3multLabels[ispin].second.end();
		for (; citSU3mult != LAST_SU3MULT; ++citSU3mult)
		{
			if (S2 % 2) 
			{
    		    cout << "(" << (int)citSU3mult->lm <<", " << (int)citSU3mult->mu << ") S=" << S2 << "/2\t" << (int)citSU3mult->mult << "\t" << citSU3mult->C2() << endl;
			} 
			else 
			{
	    	    cout << "(" << (int)citSU3mult->lm <<", " << (int)citSU3mult->mu << ") S=" << S2/2 << "\t" << (int)citSU3mult->mult << "\t" << citSU3mult->C2() << endl;
			}
		}
	}
}
