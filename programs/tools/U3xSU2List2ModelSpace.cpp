#include <vector>
#include <map>
#include <iostream>
#include <fstream>

using namespace std;

int main(int argc, char* argv[])
{
	if (argc != 2)
	{
		cout << endl;
	  	cout << "Usage: "; 
		cout << argv[0] << " <filename>" << endl;
		cout << endl;
		return EXIT_FAILURE;
	}

	ifstream list_file(argv[1]);

	if (!list_file)
	{
		cerr << "Not able to open `" << argv[1] << "` file." << endl;
	}

	typedef map<pair<int, int>, vector<vector<int> > > SSpSSnMAP;
	vector<SSpSSnMAP> model_space_data(40); // Nmax=40 will be enough

//	Read file and construct data structure describing model space
	while (true)
	{
		int N, SSp, SSn, SS, lm, mu;
		list_file >> N >> SSp >> SSn >> SS >> lm >> mu;
		if (!list_file)
		{
			break;
		}

		pair<int, int> key(SSp, SSn);

		SSpSSnMAP::iterator itSSpSSn = (model_space_data[N]).find(key);
		if (itSSpSSn == (model_space_data[N]).end())
		{
			int SSmax = SSp+SSn;
			vector<vector<int> >  lm_mu_tables(SSmax + 1, vector<int>());
			lm_mu_tables[SS].push_back(lm);
			lm_mu_tables[SS].push_back(mu);
			(model_space_data[N]).insert(make_pair(key, lm_mu_tables));
		}
		else
		{
			vector<vector<int> >& lm_mu_table = itSSpSSn->second;
			lm_mu_table[SS].push_back(lm);
			lm_mu_table[SS].push_back(mu);
		}
	}

//	loop over Nhw values
	for (int N = 0; N < model_space_data.size(); ++N)
	{
//		number of SSp SSn pairs in a given Nhw subspace
		int number_SSpSSn_pairs = model_space_data[N].size();
//		no SSp SSn pairs at Nhw subspace ==> nothing to do
		if (!number_SSpSSn_pairs)
		{
			continue;
		}
		cout << N << " " << number_SSpSSn_pairs << endl;

		SSpSSnMAP::iterator it = model_space_data[N].begin();
//		iterate over SSp SSn pairs		
		for (; it != model_space_data[N].end(); ++it)
		{
			int SSp = it->first.first;
			int SSn = it->first.second;

			int nSS = 0;
			vector<vector<int> >& lm_mu_table = it->second;

//			compute how many SS are present
			for (int SS = 0; SS < lm_mu_table.size();  ++SS)
			{
				if (lm_mu_table[SS].size() > 0)
				{
					nSS++;
				}
			}

			if (nSS)
			{
				cout << "\t\t" << SSp << " " << SSn << " " << nSS << endl;

				for (int SS = 0; SS < lm_mu_table.size();  ++SS)
				{
					if (lm_mu_table[SS].size() > 0)
					{
						cout << "\t\t\t" << SS << " " << lm_mu_table[SS].size()/2 << endl;
						for (int i = 0; i < lm_mu_table[SS].size(); i += 2)
						{
							int lm = (lm_mu_table[SS])[i];
							int mu = (lm_mu_table[SS])[i+1];
							cout << "\t\t\t\t\t" << lm << " " << mu << endl;
						}
					}
				}
			}
		}
	}

	return EXIT_SUCCESS;
}
