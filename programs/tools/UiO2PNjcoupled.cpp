#include <SU3ME/Interaction2SU3tensors.h>
#include <SU3ME/JTCoupled2BMe_Hermitian_SingleOperator.h>

#include <unordered_map>
#include <map>

using namespace std;

// array with single HO states quantum numbers n l 2j
// ordered according to logic of UiO's code
// 0  -1 -1 -1
//
// 1   0 0 1
//
// 2   1 1 3
// 3   1 1 1
//
// 4   2 2 5
// 5   2 2 3
// 6   2 0 1
//
// 7   3 3 7
// 8   3 3 5
// 9   3 1 3
// 10  3 1 1
enum {kN = 0, kL = 1, kJJ = 2};
std::vector<std::array<int, 3>> nljj_UiO;
std::map<std::array<int, 3>, size_t> iUiO_nljj;

void UiO_sps_labels(int nmax, std::vector<std::array<int, 3>>& nljj_UiO) {
   std::array<int, 3> nljj = {-1, -1, -1};
   nljj_UiO.push_back(nljj);

   for (int n = 0; n <= nmax; ++n) {
      nljj[kN] = n;
      for (int l = n; l >= 0; l -= 2) {
         nljj[kL] = l;
         nljj[kJJ] = 2 * l + 1;
         nljj_UiO.push_back(nljj);
         iUiO_nljj[nljj] = nljj_UiO.size() - 1; // 0th element is empty {-1, -1, -1}
         if (l == 0) {
            continue;
         }

         nljj[2] = 2 * l - 1;
         nljj_UiO.push_back(nljj);
         iUiO_nljj[nljj] = nljj_UiO.size() - 1; // 0th element is empty {-1, -1, -1}
      }
   }
}

int Get_Index_UiO(int n, int l, int jj) {
   auto elem = iUiO_nljj.find(std::array<int, 3>({n, l, jj}));
   assert(elem != iUiO_nljj.end());
   return elem->second;
}
// initially sps indices given in input files are given 
// for protons (1, 3, 5, 7, ... ) and neutrons (2 4 6 8 ...)
// This function turns them into 1, 2, 3, 4, ... sequence
// of HO sps indices
int remove_tz_from_UiO(int iUiO_tz) {return (iUiO_tz + iUiO_tz%2)/2;}

// give index of HO states with n l 2j
// ordered according to LSU3shell
// 1   0 0 1
//
// 2   1 1 1
// 3   1 1 3
//
// 4   2 0 1
// 5   2 2 3
// 6   2 2 5
//
// 7   3 1 1
// 8   3 1 3
// 9   3 3 5
// 10  3 3 7
int Get_Index_LSU3shell(int n, int l, int jj) {
   int index = n * (n + 1) / 2 + 1;
   if (n % 2) {
      index += 2 * (l / 2);
   } else {
      if (l == 0) {
         return index;
      } else {
         index += (1 + 2 * ((l / 2) - 1));
      }
   }
   return ((jj == (2 * l - 1)) ? index : index + 1);
}

// transform UiO index to LSU3shell index
int UiO_tz_to_LSU3shell(int iUiO) {
   // transform indices by remove proton/neutron dependency from indices i1, i2, i3, i4
   // iUiO = 1  n1 l1 jj1  tz = -1/2 (proton)  ---> iUiO = (1 + (1%2))/2 = 1    n1 l1 jj1
   // iUiO = 2  n1 l1 jj1  tz = +1/2 (neutron) ---> iUiO = (2 + (2%2))/2 = 1    n1 l1 jj1
   iUiO = remove_tz_from_UiO(iUiO);

   // get n l 2j associated with UiO index and compute its index in LSU3shell order of HO sps
   return Get_Index_LSU3shell(nljj_UiO.at(iUiO)[kN], nljj_UiO.at(iUiO)[kL], nljj_UiO.at(iUiO)[kJJ]);
}

// transform UiO index to LSU3shell index
int UiO_to_LSU3shell(int iUiO) {
   // get n l 2j associated with UiO index and compute its index in LSU3shell order of HO sps
   return Get_Index_LSU3shell(nljj_UiO.at(iUiO)[kN], nljj_UiO.at(iUiO)[kL], nljj_UiO.at(iUiO)[kJJ]);
}

void ShowTables() {
   for (size_t iUio = 1; iUio < nljj_UiO.size(); ++iUio) {
      int n = nljj_UiO[iUio][kN];
      int l = nljj_UiO[iUio][kL];
      int jj = nljj_UiO[iUio][kJJ];
      int iSU3 = Get_Index_LSU3shell(n, l, jj);

      cout << "I:" << iUio << " n:" << n << " l:" << l << " jj:" << jj << "\tIsu3:" << iSU3
           << std::endl;
   }
}

void Get_nlj(const int index, int& n, int& l, int& j) {
   n = (-1 + sqrt(1.0 + 8.0 * (index - 1))) / 2;
   int i = (index - 1) - n * (n + 1) / 2;
   if (n % 2) {
      l = 1 + 2 * (i / 2);
      j = (i % 2) ? (2 * l + 1) : (2 * l - 1);
   } else {
      l = 2 * ((i + 1) / 2);
      j = (i % 2) ? 2 * l - 1 : 2 * l + 1;
   }
}

void TestTransformation() {
   for (int iUiO = 1; iUiO < 2 * (nljj_UiO.size() - 1); ++iUiO) {
      int n1, l1, jj1, n2, l2, jj2;

      int i = (iUiO + iUiO % 2) / 2;
      n1 = nljj_UiO[i][0];
      l1 = nljj_UiO[i][1];
      jj1 = nljj_UiO[i][2];

      int iLSU = UiO_tz_to_LSU3shell(iUiO);

      Get_nlj(iLSU, n2, l2, jj2);
      if (n2 != n1 || l2 != l1 || jj2 != jj1) {
         cerr << "Error for iUiO: " << iUiO << " iLSU:" << iLSU << endl;
      }
   }
}

void GenerateAllCombinationsHOShells(const int Nmax, const int valence_shell,
                                     vector<int>& nanbncnd) {
   int maxShell = valence_shell + Nmax;

   for (int na = 0; na <= maxShell; ++na) {
      for (int nb = 0; nb <= maxShell; ++nb) {
         int adxad_Nhw = 0;
         if (na > valence_shell) {
            adxad_Nhw += (na - valence_shell);
         }
         if (nb > valence_shell) {
            adxad_Nhw += (nb - valence_shell);
         }
         if (adxad_Nhw > Nmax) {
            continue;
         }

         for (int nc = 0; nc <= maxShell; ++nc) {
            for (int nd = 0; nd <= maxShell; ++nd) {
               int taxta_Nhw = 0;
               if (nc > valence_shell) {
                  taxta_Nhw += (nc - valence_shell);
               }
               if (nd > valence_shell) {
                  taxta_Nhw += (nd - valence_shell);
               }
               if (taxta_Nhw > Nmax) {
                  continue;
               }

               // check if the parity is conserved
               if ((na + nb) % 2 ==
                   (nc + nd) % 2)  // if true ==> store {na, nb, nc, nd} combination
               {
                  nanbncnd.push_back(na);
                  nanbncnd.push_back(nb);
                  nanbncnd.push_back(nc);
                  nanbncnd.push_back(nd);
               }
            }
         }
      }
   }
}

struct JCoupledProtonNeutron2BMe_Hermitian_SingleOperator {
   static void hash_combine(std::size_t& seed, std::size_t value) {
      seed ^= value + 0x9e3779b9 + (seed << 6) + (seed >> 2);
   }

   struct container_hasher {
      template <class T>
      std::size_t operator()(const T& c) const {
         std::size_t seed = 0;
         for (const auto& elem : c) {
            hash_combine(seed, std::hash<typename T::value_type>()(elem));
         }
         return seed;
      }
   };

   enum InteractionType { kPP = 0, kNN = 1, kPN = 2 };
   // {i1, i2, i3, i4} --> {Vpp, Vnn, Vpn}
   typedef std::unordered_map<std::array<int, 4>, std::array<double, 3>, container_hasher>
       HASH_TABLE;
   JCoupledProtonNeutron2BMe_Hermitian_SingleOperator(const std::string& sInteractionFileName);

   bool MeJ(int i1, int i2, int i3, int i4, int jj0, double& mePP, double& meNN, double& mePN) const;
 

   void ShowContent() const;

   // meJJ_[JJ0] ---> hash table with {<i1 i2 JJ0 || Vpp/nn/pn || i3 i4 JJ0>}
   std::vector<HASH_TABLE> meJJ_;
};

void JCoupledProtonNeutron2BMe_Hermitian_SingleOperator::ShowContent() const {
   for (size_t jj0 = 0; jj0 < meJJ_.size(); ++jj0) {
      if (!meJJ_[jj0].empty()) {
         std::cout << "2J0:" << jj0 << std::endl;
         for (auto me : meJJ_[jj0]) {
            std::cout << me.first[0] << " " << me.first[1] << " " << me.first[2] << " "
                      << me.first[3] << "\t";
            std::cout << me.second[0] << " " << me.second[1] << " " << me.second[2] << std::endl;
         }
      }
   }
}

// read line from an interaction file produced by UiO code
// Tz = -1 ... proton-proton
// Tz =  0 ... proton-neutron [tz = -1/2 +1/2 -1/2 +1/2
// Tz = +1 ... nucleon-nucleon
// Par: 0 ... positive
//      1 ... negative
// JJ2:2J0
// HO sps indices i1 i2 i3 i4
//
// value <i1 i2 J0 || V_{Tz} || i3 i4 J0>
void ReadMeUiO(std::fstream& file, int& tz, int& par, int& jj, int& i1, int& i2, int& i3, int& i4,
               double& value) {
   file >> tz >> par >> jj;
   file >> i1;
   file >> i2;
   file >> i3;
   file >> i4;
   //   for (auto& index : i1i2i3i4) {
   //      file >> *index;
   //   }
   file >> value;
   //    cout << tz << " " << par << " " << jj << " " << i1 << " " << i2 << " " << i3 << " " << i4
   //    << " " << value << endl;
}

JCoupledProtonNeutron2BMe_Hermitian_SingleOperator::
    JCoupledProtonNeutron2BMe_Hermitian_SingleOperator(const std::string& sInteractionFileName)
    : meJJ_(255, HASH_TABLE()) {
   std::fstream file(sInteractionFileName.c_str());
   if (!file) {
      std::cout << "Could not open file '" << sInteractionFileName << "'!" << std::endl;
      exit(EXIT_FAILURE);
   }

   cout << "Start reading interaction file ... ";
   cout.flush();
   while (true) {
      int tz, par, jj0;
      int i1, i2, i3, i4;
      double v_tz_me; // <i1 i2 jj || V_{tz} || i3 i4 jj>

      // tz = -1 ==> PP
      // tz =  0 ==> pn
      // tz = +1 ==> nn
      // par: 0 for +; 1 for -;
      ReadMeUiO(file, tz, par, jj0, i1, i2, i3, i4, v_tz_me);

      if (!file) {
         break;
      }

      assert(tz == 0 || tz == -1 || tz == +1);
      assert(par == 0 || par == 1);
      // assert that UiO file contains the upper triangular interaction matrix
      assert((i1 < i3) || ((i1 == i3) && (i2 <= i4)));

      // transform indices i1, i2, i3, i4, given in the UiO ordering with Tz dependency into
      // Tz independent form
      i1 = remove_tz_from_UiO(i1);
      i2 = remove_tz_from_UiO(i2);
      i3 = remove_tz_from_UiO(i3);
      i4 = remove_tz_from_UiO(i4);

      // assert that removing Tz from UiO preserves the upper triangular interaction matrix elements
      assert((i1 < i3) || ((i1 == i3) && (i2 <= i4)));

      auto i1i2i3i4jj0_me_pp_nn_pn = meJJ_[jj0].find(std::array<int, 4>({i1, i2, i3, i4}));
      if (i1i2i3i4jj0_me_pp_nn_pn == meJJ_[jj0].end()) {
         // we need to initialize with zeros
         // otherwise value will be undefined
         std::array<double, 3> me_pp_nn_pn = {0, 0, 0};
         switch (tz) {
            case -1:
               me_pp_nn_pn[kPP] = v_tz_me;
               break;
            case 0:
              me_pp_nn_pn[kPN] = v_tz_me;
               break;
            case +1:
              me_pp_nn_pn[kNN] = v_tz_me;
         }
         meJJ_[jj0].insert(std::make_pair(std::array<int, 4>({i1, i2, i3, i4}), me_pp_nn_pn));
      } 
      else  // already in hash => store V value
      {
         switch (tz) {
            case -1:
               i1i2i3i4jj0_me_pp_nn_pn->second[kPP] = v_tz_me;
               break;
            case 0:
               i1i2i3i4jj0_me_pp_nn_pn->second[kPN] = v_tz_me;
               break;
            case +1:
               i1i2i3i4jj0_me_pp_nn_pn->second[kNN] = v_tz_me;
         }
      }
   }
   cout << "\t Done" << endl;
}

bool JCoupledProtonNeutron2BMe_Hermitian_SingleOperator::MeJ(int i1, int i2, int i3, int i4, int jj0,
                                                             double& mePP, double& meNN,
                                                             double& mePN) const {
   bool found = false;
   bool isUpperDiag = (i1 < i3) || ((i1 == i3) && (i2 <= i4));

   std::array<int, 4> keyPN;
   if (isUpperDiag) {
      keyPN = {i1, i2, i3, i4};
   } else { // matrix element from the lower triangular ==> find transposed matrix element
      keyPN = {i3, i4, i1, i2};
   }
   auto mePNelem = meJJ_[jj0].find(keyPN);
   if (mePNelem != meJJ_[jj0].end()) {
      mePN = mePNelem->second[kPN];
      found = true;
   } else {  // ==> <a b J || Vpn || c d J> does not exist
      mePN = 0.0;
      // try to find element with bra and ket exchanged
      if (isUpperDiag) {
         keyPN = {i3, i4, i1, i2};
      } else {
         keyPN = {i1, i2, i3, i4};
      }
      // The following assert fails if meJJ_[jj0] stores matrix
      // elements from both upper and lower triangular matrices
      assert(meJJ_[jj0].find(keyPN) == meJJ_[jj0].end());
   }

   int phase = 0;
   if (i1 > i2)  //	Use symmetry (A.5)
   {
      int jj1 = nljj_UiO.at(i1)[kJJ];
      int jj2 = nljj_UiO.at(i2)[kJJ];
      std::swap(i1, i2);
      phase += (jj1 + jj2 - jj0) / 2 + 1;
   }
   if (i3 > i4)  //	Use symmetry (A.6)
   {
      int jj3 = nljj_UiO.at(i3)[kJJ];
      int jj4 = nljj_UiO.at(i4)[kJJ];
      std::swap(i3, i4);
      phase += (jj3 + jj4 - jj0) / 2 + 1;
   }

   isUpperDiag = (i1 < i3) || ((i1 == i3) && (i2 <= i4));
   std::array<int, 4> keyPPNN; 
   if (isUpperDiag) 
   {
      keyPPNN = {i1, i2, i3, i4};  
   }
   else
   {
      keyPPNN = {i3, i4, i1, i2};
   }
   auto mePPNNelems = meJJ_[jj0].find(keyPPNN);
   if (mePPNNelems != meJJ_[jj0].end()) {
      mePP = 0.25 * mePPNNelems->second[kPP] * MINUSto(phase);
      meNN = 0.25 * mePPNNelems->second[kNN] * MINUSto(phase);
      found = true;
   } else {
      mePP = 0;
      meNN = 0;
   }
   return found;
}

int main(int argc, char* argv[]) {
	if (argc != 5)
	{
		cout << endl;
		cout << "This program takes as an input file in J-coupled format created by UiO code and creates file in J-coupled scheme which is ready for further processing.\n" << endl;
		cout << "Correct usage: " << argv[0] << " <input interaction filename> <Nmax> <valence shell> < output pnJ-coupled filename>" << endl;
		return 1;
	}

	uint32_t ntotal(0);

	int Nmax = atoi(argv[2]);
	int valence_shell = atoi(argv[3]);
   // initialize tables with SPS states of HO
   UiO_sps_labels(Nmax + valence_shell, nljj_UiO);
   // read matrix elements and store them in hash
   // indices are given in UiO order
   JCoupledProtonNeutron2BMe_Hermitian_SingleOperator mePNJUiO(argv[1]);

	std::ofstream output_file(argv[4], std::ofstream::binary);

	vector<int> nanbncnd;
	GenerateAllCombinationsHOShells(Nmax, valence_shell, nanbncnd);
	for (int i = 0; i < nanbncnd.size(); i += 4)
	{
		int na = nanbncnd[i + 0];
		int nb = nanbncnd[i + 1];
		int nc = nanbncnd[i + 2];
		int nd = nanbncnd[i + 3];

		output_file.write((char*)&nanbncnd[i], 4*sizeof(int));

		vector<uint16_t> vec_i1, vec_i2, vec_i3, vec_i4; 
		vector<uint8_t> vec_J;
		vector<double> vec_Vpp, vec_Vnn, vec_Vpn;

		for (int la = na%2; la <= na; la += 2)
		{
			for (int lb = nb%2; lb <= nb; lb += 2)
			{
				for (int lc = nc%2; lc <= nc; lc += 2)
				{
					for (int ld = nd%2; ld <= nd; ld += 2)
					{
//						|la + 1/2 - (lb + 1/2)| = |la - lb|
//						|la + 1/2 - (lb - 1/2)| = |la - lb + 1|
//						|la - 1/2 - (lb + 1/2)| = |la - lb -1|
//						|la - 1/2 - (lb - 1/2)| = |la - lb|
						int Jabmin = std::min(abs(la - lb), std::min(abs(la - lb + 1), abs(la - lb - 1)));
						int Jcdmin = std::min(abs(lc - ld), std::min(abs(lc - ld + 1), abs(lc - ld - 1)));
						int Jmax = std::min(la+lb, lc+ld)+1;

						for (int J = std::max(Jabmin, Jcdmin); J <= Jmax; J += 1)
						{
							for (int jja = abs(2*la - 1); jja <= 2*la + 1; jja += 2)
							{
								for (int jjb = abs(2*lb - 1); jjb <= 2*lb + 1; jjb += 2)
								{
									if (!SU2::mult(jja, jjb, 2*J))
									{
										continue;
									}
									int i1 = Get_Index_UiO(na, la, jja);
									int i2 = Get_Index_UiO(nb, lb, jjb);
									for (int jjc = abs(2*lc - 1); jjc <= 2*lc + 1; jjc += 2)
									{
										for (int jjd = abs(2*ld - 1); jjd <= 2*ld + 1; jjd += 2)
										{
											if (!SU2::mult(jjc, jjd, 2*J))
											{
												continue;
											}
											int i3 = Get_Index_UiO(nc, lc, jjc);
											int i4 = Get_Index_UiO(nd, ld, jjd);
										
											double Vpp(0), Vnn(0), Vpn(0);
                                 // obtain <i1 i2 J || Vpp/nn/pn || i3 i4 J>
											bool me_exist = mePNJUiO.MeJ(i1, i2, i3, i4, 2*J, Vpp, Vnn, Vpn);
											if (me_exist)
											{
                                    assert(Get_Index_LSU3shell(na, la, jja) == UiO_to_LSU3shell(i1));
                                    assert(Get_Index_LSU3shell(nb, lb, jjb) == UiO_to_LSU3shell(i2));
                                    assert(Get_Index_LSU3shell(nc, lc, jjc) == UiO_to_LSU3shell(i3));
                                    assert(Get_Index_LSU3shell(nd, ld, jjd) == UiO_to_LSU3shell(i4));

												vec_i1.push_back(UiO_to_LSU3shell(i1));
												vec_i2.push_back(UiO_to_LSU3shell(i2));
												vec_i3.push_back(UiO_to_LSU3shell(i3));
												vec_i4.push_back(UiO_to_LSU3shell(i4));
												vec_J.push_back(J);
												vec_Vpp.push_back(Vpp);
												vec_Vnn.push_back(Vnn);
												vec_Vpn.push_back(Vpn);

//                                    cout << i1 << " " << i2 << " " << i3  << " " << i4 << " J:" << J << " " << Vpp << " " << Vnn << " " << Vpn << std::endl;
//                                    cout << UiO_to_LSU3shell(i1) << " " << UiO_to_LSU3shell(i2) << " " << UiO_to_LSU3shell(i3)  << " " << UiO_to_LSU3shell(i4) << " J:" << J << " " << Vpp << " " << Vnn << " " << Vpn << std::endl;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		uint32_t number_matrix_elements = vec_i1.size();
		ntotal += number_matrix_elements;
		output_file.write((char*)&number_matrix_elements, sizeof(uint32_t));
		if (number_matrix_elements)
		{
			output_file.write((char*)&vec_i1[0], number_matrix_elements*sizeof(uint16_t));
			output_file.write((char*)&vec_i2[0], number_matrix_elements*sizeof(uint16_t));
			output_file.write((char*)&vec_i3[0], number_matrix_elements*sizeof(uint16_t));
			output_file.write((char*)&vec_i4[0], number_matrix_elements*sizeof(uint16_t));
			output_file.write((char*)&vec_J[0] , number_matrix_elements*sizeof(uint8_t));
			output_file.write((char*)&vec_Vpp[0], number_matrix_elements*sizeof(double));
			output_file.write((char*)&vec_Vnn[0], number_matrix_elements*sizeof(double));
			output_file.write((char*)&vec_Vpn[0], number_matrix_elements*sizeof(double));

		}
	}
	cout << "number of matrix elements stored:" << ntotal << endl;
	return EXIT_SUCCESS;
}
