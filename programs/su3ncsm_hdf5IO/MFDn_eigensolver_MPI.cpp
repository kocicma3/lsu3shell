#include <boost/mpi.hpp>

#include <iostream>
#include <fstream>
#include <vector>

#include <mascot/Props.h>
#include <mascot/MatrixReader.h>

#include <LSU3/ncsmSU3xSU2Basis.h>

// Definition of Fortran "module" variable naming conventions 
// TODO: replace NDCRCGNU with the preprocessor directive that GNU compiler @ UND cluster uses
#if (defined NDCRCGNU) 
	#define FORTRAN_MODULE_VARIABLE(s) __nodeinfo__##s
#elif (defined __GNUC__ && !defined __INTEL_COMPILER)
	#define FORTRAN_MODULE_VARIABLE(s) __nodeinfo_MOD_##s
#elif (defined __INTEL_COMPILER)
	#define FORTRAN_MODULE_VARIABLE(s) nodeinfo_mp_##s##_
#else
	#define FORTRAN_MODULE_VARIABLE(s) __nodeinfo_mp_##s_
#endif	

extern "C" 
{ 
#if (defined __GNUC__ || defined __INTEL_COMPILER)
    extern void matrix_diagonalize_(int&, int&, int&, int[], int[], float[], int&, int&, float&, int&,   float[],   int&,   long int&);
    extern void splitprocs_();
#else
    extern void matrix_diagonalize(int&, int&, int&, int[], int[], float[], int&, int&, float&, int&,   float[],   int&,   long int&);
    extern void splitprocs();
#endif
	extern int FORTRAN_MODULE_VARIABLE(myrank);
	extern int FORTRAN_MODULE_VARIABLE(mypid); 
	extern int FORTRAN_MODULE_VARIABLE(nproc); 
	extern int FORTRAN_MODULE_VARIABLE(ndiag); 
	extern int FORTRAN_MODULE_VARIABLE(icomm);
}

using namespace std;

using mascot::caughtAt;
using mascot::DataType;
using mascot::EntryType;
using mascot::MatrixReader;
using mascot::Part;
using mascot::Props;
using mascot::Symmetry;

int main(int argc,char **argv)
{ 
    if (argc != 3)
    {
        cout << "Usage:" << argv[0] << "<model space> <directory with matrix>" << endl;
        return EXIT_FAILURE;
    }
	boost::mpi::environment env(argc, argv);
	boost::mpi::communicator mpi_comm_world;

    int my_rank(mpi_comm_world.rank());
	int nprocs(mpi_comm_world.size()); 
	int ndiag;

//  The following are internal variables of MFDn that has to be set 
//  before calling splitprocs()
//
    FORTRAN_MODULE_VARIABLE(myrank) = FORTRAN_MODULE_VARIABLE(mypid) = my_rank;
    FORTRAN_MODULE_VARIABLE(nproc)  = nprocs;
    FORTRAN_MODULE_VARIABLE(ndiag)  = ndiag = (-1 + sqrt(1.0 + 8.0*nprocs))/2;    // nprocs = ndiag*(ndiag+1)/2

//  nodeinfo_mp_icomm_  = MPI_COMM_WORLD;
    splitprocs_();

    if (0 == my_rank)
    {
        cout << "Number of diagonal processes = " << ndiag << endl;
    }
//	TODO: instead of reading these parameters from diag.in they should
//	be provided in command line
    int neigen, maxitr, startitr;
    float tol; 
    fstream fdiagin("diag.in");
    if (!fdiagin)
    {
        cerr << "Could not open diag.in that contains parameters of the MFDn eigensolver: neigen, maxitr, startir, tol";
		MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
    }
    else
    {
       fdiagin >> neigen;
       fdiagin >> maxitr;
       fdiagin >> startitr;
       fdiagin >> tol;
       if (my_rank == 0)
       {
           cout << "neigen = " << neigen << " maxitr = " << maxitr << " startitr = " << startitr << " tol = " << tol << endl;
       }
    }

    vector<float> nonzero_me;
    vector<int>   columns;
    vector<int>   rowPtrs;

    int vectors = true; 
    long int nstates_sofar(0);

    proton_neutron::ModelSpace ncsmModelSpace;

	if (my_rank == 0)
	{ 
		ncsmModelSpace.Load(argv[1]);
	}
	
	boost::mpi::broadcast(mpi_comm_world, ncsmModelSpace, 0);

    lsu3::CncsmSU3xSU2Basis  basis(ncsmModelSpace, 0, ndiag);

//  Fortran loop from MatrixDiag.f rewritten into logic of lsu3::CncsmSU3xSU2Basis
    if (vectors && (my_rank < ndiag))
    {
		nstates_sofar = basis.getFirstStateId(my_rank);
    }

    try {
        Props p;

        MatrixReader r;
        r.open(argv[2], p, mpi_comm_world, 1024);

        if (0 == my_rank) 
        {
            cout << "Global number of rows:     " << p.globalNRows.getSize()     << endl;
            cout << "Global number of columns:  " << p.globalNColumns.getSize()  << endl;
            cout << "Global number of nonzeros: " << p.globalNNonzeros.getSize() << endl;
            if (p.globalNRows.getSize() == p.globalNColumns.getSize()) {
                cout << "Symmetry:                  " << p.symmetry.getDesc() << endl;
            }

            cout << "Scheme                     " << p.scheme.getDesc() << endl;
            cout << endl;
        }
/*
        else
        {
            cout << "rank = " << my_rank << ": local number of rows:      " << p.localNRows.getSize()      << endl;
            cout << "rank = " << my_rank << ": local number of columns:   " << p.localNColumns.getSize()   << endl;
            cout << "rank = " << my_rank << ": local number of nonzeros:  " << p.localNNonzeros.getSize()  << endl;
            cout << "rank = " << my_rank << ": row offset:                " << p.rowOffset                 << endl;
            cout << "rank = " << my_rank << ": column offset:             " << p.columnOffset              << endl;
        }
*/
        r.readCsr(nonzero_me, columns, rowPtrs);
        r.close();

//      make column and row indices [C++ compatible, i.e. first element of
//      matrix has indices i=0 j = 0] compatible with fortran array indexing 
        std::transform(columns.begin(), columns.end(), columns.begin(), bind2nd(plus<int>(), +1));
        std::transform(rowPtrs.begin(), rowPtrs.end(), rowPtrs.begin(), bind2nd(plus<int>(), +1));

//	Notice that MFDn works with lower triangular matrix
    	int ncols, nrows, nnz; 
        ncols = p.localNRows.getSize();
        nrows = p.localNColumns.getSize();
        nnz   = p.localNNonzeros.getSize(); 

		vector<float> eigenvals(neigen); 
//      Eigenvalues and eigenvectors are saved on disk by matrix_diagonalize_ 
//      Question: I assume that it is guaranteed that &vector[0] gives me a pointer a contiguous array of memory
#if (defined __GNUC__	|| defined __INTEL_COMPILER)
		matrix_diagonalize_(ncols, nrows, nnz, (int*)&rowPtrs[0], (int*)&columns[0], (float*)&nonzero_me[0], startitr, maxitr, tol, neigen, (float*)&eigenvals[0], vectors, nstates_sofar);
#else
		matrix_diagonalize(ncols, nrows, nnz, (int*)&rowPtrs[0], (int*)&columns[0], (float*)&nonzero_me[0], startitr, maxitr, tol, neigen, (float*)&eigenvals[0], vectors, nstates_sofar);
#endif	
    }
    catch (const std::exception& e) {
        cerr << e.what() << caughtAt(__FILE__, __LINE__);
		boost::mpi::environment::abort(EXIT_FAILURE);
    }
}
