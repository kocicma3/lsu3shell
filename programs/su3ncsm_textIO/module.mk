$(eval $(begin-module))

################################################################
# unit definitions
################################################################

# module_units_h := 
# module_units_cpp-h := 
# module_units_f := 
module_programs_cpp := nuclearMECalculator nuclearMECalculator_textIO_MPI nuclearMECalculator_new_basis\
nuclearMECalculatorAnalysis_new_basis nuclearMECalculator_textIO_MPI_OpenMP \

# module_programs_f :=
# module_generated :=

################################################################
# library creation flag
################################################################

# $(eval $(library))

################################################################
# special variable assignments, rules, and dependencies
################################################################

# TODO define dependencies

$(eval $(end-module))
