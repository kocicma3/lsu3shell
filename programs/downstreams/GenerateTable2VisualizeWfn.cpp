#include <SU3ME/global_definitions.h>
#include <LSU3/ncsmSU3xSU2Basis.h>
#include <SU3ME/BaseSU3Irreps.h>
#include <vector>
#include <stack>
#include <ctime>
#include <sstream>

using namespace std;

enum eRecord {kN = 0, kSSp = 1, kSSn = 2, kSS = 3, kLM = 4, kMU = 5};

bool ReadWfn(const string& wfn_file_name, lsu3::CncsmSU3xSU2Basis& basis, const int ndiag_wfn, vector<float>& wfn)
{
//	basis has to be generated for ndiag = 1 ==> it contains entire basis of a model space
	assert(basis.ndiag() == 1 && basis.SegmentNumber() == 0);

	fstream wfn_file(wfn_file_name.c_str(), std::ios::in | std::ios::binary | std::ios::ate); // open at the end of file so we can get file size
	if (!wfn_file)
	{
		cout << "Error: could not open '" << wfn_file_name << "' wfn file" << endl;
		return false;
	}

	size_t size = wfn_file.tellg();
	size_t nelems = size/sizeof(float);

	if (size%sizeof(float) || (nelems != basis.getModelSpaceDim()))
	{
		cout << "Error: file size == dim x sizeof(float): " << basis.getModelSpaceDim() << " x " << sizeof(float) << " = " << basis.getModelSpaceDim()*sizeof(float) << " bytes." << endl;
		cout << "The actual size of the file: " << size << " bytes!";
		return false;
	}

//	allocate memory for nelems floats
	wfn.resize(nelems); 
//	return file stream to beginning	
	wfn_file.seekg (0, std::ios::beg);

//	Load wave function generated for ndiag_wfn into a file that contains order
//	of basis states for ndiag=1	
	uint32_t ipos;
	uint16_t number_of_states_in_block;
	uint32_t number_ipin_blocks = basis.NumberOfBlocks();
	for (uint16_t i = 0; i < ndiag_wfn; ++i)
	{
		
//		Here I am using the fact that order of (ip in) blocks in basis split into ndiag_wfn sections is following:
//		
//	section --> {iblock0, iblock1, ...... }
//		----------------------------------
//		0     {0, ndiag_wfn, 2ndiag_wfn, 3ndiag_wfn ....}
//		1     {1, ndiag_wfn+1, 2ndiag_wfn+1, 3ndiag_wfn+1, ...}
//		2     {2, ndiag_wfn+2, 2ndiag_wfn+1, 3ndiag_wfn+2, ...}
//		.
//		.
//		.
		for (uint32_t ipin_block = i; ipin_block < number_ipin_blocks; ipin_block += ndiag_wfn)
		{
//	obtain position of the current block in model space basis [i.e. with ndiag=1] and its size
			ipos = basis.BlockPositionInSegment(ipin_block);
			number_of_states_in_block = basis.NumberOfStatesInBlock(ipin_block);
			
			wfn_file.read((char*)&wfn[ipos], number_of_states_in_block*sizeof(float));
		}
	}
	return true;
}


int main(int argc, char* argv[])
{
	if (argc != 5)
	{
		cout << endl;
	  	cout << "Usage: "; 
		cout << argv[0] << "<model space> <ndiag> <wave function> <output table filename>" << endl;
		return EXIT_FAILURE;
	}

	unsigned int ndiag;
	const string model_space_definition_file_name(argv[1]);
	// get number of diagonal processes
	std::istringstream(argv[2]) >> ndiag;
	// get filename of the input wave function
	const string wfn_file_name(argv[3]);

	
	proton_neutron::ModelSpace ncsmModelSpace(model_space_definition_file_name);
	lsu3::CncsmSU3xSU2Basis basis(ncsmModelSpace, 0, 1);
	vector<float> wfn;

	cout << "Reading " << wfn_file_name << " that was saved with " << ndiag << " processors . . . "; cout.flush();
	ReadWfn(wfn_file_name, basis, ndiag, wfn);
	cout << "Done" << endl;

	cout << "Computing projections . . . "; cout.flush();
//	irreps_dim_projections: contains a map of {N, Sp, Sn, S, (lm mu)} irreps which
//	are associated with the dimension & probability amplitude. Irreps span ncsmModelSpace[i].
	map<vector<int>, pair<size_t, float> > irreps_dims_projections;
//	max_prob[N] = maximal probability found in Nhw space
	vector<float> max_prob(basis.Nmax() + 1, 0.0);	// Nmax == 0 ==> I still need to reserve one element 

	uint32_t ipos = 0;
	for (unsigned int ipin_block = 0; ipin_block < basis.NumberOfBlocks(); ipin_block++)
	{
		if (basis.NumberOfStatesInBlock(ipin_block) == 0)
		{
			continue;
		}
			
		uint32_t ip = basis.getProtonIrrepId(ipin_block);
		uint32_t in = basis.getNeutronIrrepId(ipin_block);
		uint32_t Nhw =  basis.nhw_p(ip) + basis.nhw_n(in);

		SU3xSU2::LABELS w_ip(basis.getProtonSU3xSU2(ip));
		SU3xSU2::LABELS w_in(basis.getNeutronSU3xSU2(in));

		uint16_t aip_max = basis.getMult_p(ip);
		uint16_t ain_max = basis.getMult_n(in);

		uint32_t ibegin = basis.blockBegin(ipin_block);
		uint32_t iend = basis.blockEnd(ipin_block);
		for (uint32_t iwpn = ibegin; iwpn < iend; ++iwpn)
		{
			SU3xSU2::LABELS w_pn(basis.getOmega_pn(ip, in, iwpn));
			size_t afmax = aip_max*ain_max*w_pn.rho;
			uint32_t nstates = afmax*basis.omega_pn_dim(iwpn);
			float wpn_projection = 0.0;
			for (int i = 0; i < nstates; ++i, ++ipos)
			{
				wpn_projection += wfn[ipos]*wfn[ipos];
			}

			vector<int> NSpSnlmmu(6);
			NSpSnlmmu[kN]   = Nhw;
			NSpSnlmmu[kSSp] = w_ip.S2; 
			NSpSnlmmu[kSSn] = w_in.S2; 
			NSpSnlmmu[kSS]  = w_pn.S2; 
			NSpSnlmmu[kLM]  = w_pn.lm; 
			NSpSnlmmu[kMU]  = w_pn.mu;

			//  find key
			map<vector<int>, pair<size_t, float> >::iterator it = irreps_dims_projections.find(NSpSnlmmu);
			if (it == irreps_dims_projections.end())
			{
				// key does not exist => initialize it
				irreps_dims_projections[NSpSnlmmu] = make_pair(nstates, wpn_projection);
			}
			else
			{
				// a given N Sp S n S (l m) subspace has been  
				it->second.first += nstates;
				it->second.second += wpn_projection;
			}
		}
	}

	cout << " Done" << endl;

	map<int, float> N_max_prob;
	for (map<vector<int>, pair<size_t, float> >::iterator it = irreps_dims_projections.begin(); it != irreps_dims_projections.end(); ++it)
	{
		int N = it->first[kN]; 
		float current_prob = it->second.second;

		if (max_prob[N] < current_prob)
		{
			max_prob[N] = current_prob;
		}
	}

	ofstream output_table_file(argv[4]);
	for (map<vector<int>, pair<size_t, float> >::iterator it = irreps_dims_projections.begin(); it != irreps_dims_projections.end(); ++it)
	{
		const vector<int>& NSpSnlmmu = it->first; 
		const pair<size_t, float>& dim_prob = it->second;
		float dmax = max_prob[NSpSnlmmu[kN]];
		output_table_file << NSpSnlmmu[kN] << " ";
		output_table_file << NSpSnlmmu[kSSp] << " ";
		output_table_file << NSpSnlmmu[kSSn] << " ";
		output_table_file << NSpSnlmmu[kSS] << " "; 
		output_table_file << NSpSnlmmu[kLM] << " "; 
		output_table_file << NSpSnlmmu[kMU] << " ";
		output_table_file << dim_prob.first << " ";
		output_table_file << dim_prob.second/dmax << endl;
	}
	return EXIT_SUCCESS;
}
