#include <LSU3/ncsmSU3xSU2Basis.h>
#include <SU3ME/SU3xSU2Basis.h>
#include <getopt.h>
#include <unistd.h>

#include <sstream>
#include <string>
using namespace std;

//
// I N P U T
//
// su3xsu2basisJJ ... basis states of a SU(3)xSU(2) irrep (lm mu)S that carry a given value of J
// [i.e. all  L k J states]
//
// wfn_file_name ... name of the file with an Sp(3,R) state |sigma (lm mu)S k L Jmax=lm+mu+S. Note
// that dim [(lm mu)S k L Jmax] = 1.
//
// single_segment_basis_jjmax ... basis of all Nhw (lm mu) Sp Sp S irreps and their Jmax basis
// states. The value of Jmax is chosen such that each (lm mu) S
//                         SU(3)xSU(2) irrep has exactly one basis state.
//
// ndiag_wfn ... input wave function was saved by ndiag_wfn diagonal processes
//
// wfnsJJ    ... wfnsJJ[ikL] = |sigma Sp Sn S Nhw (lm mu) k L J=jj> = \sum_{(ip, in)a0} c_{(ip,
// in)a0} | (ip,in) a0 Nhw Sp Sn S (lm mu) k L J=jj>
// < sigma Nhw (lm mu) S k L J | states where the order of k L J states is set
//	Example:
//	4hw (2 2) S=1 L = 0 k = 0 J = 1> .... wfn[0]
//	4hw (2 2) S=1 L = 2 k = 0 J = 1> .... wfn[1]
//	4hw (2 2) S=1 L = 2 k = 1 J = 1> .... wfn[2]
//
bool ReadSp3RWfnCreateSp3RJJStates(SU3xSU2::BasisJfixed& su3xsu2basisJJ,
                                   const string& wfn_file_name,
                                   const lsu3::CncsmSU3xSU2Basis& single_segment_basis_jjmax,
                                   const int ndiag_wfn, vector<vector<float>>& wfnsJJ) {
   assert(single_segment_basis_jjmax.omega_pn_dim(0) == 1);
   //	number of L k J=jj states in (lm mu)S irrep
   uint32_t dim_jj =
       su3xsu2basisJJ.dim();  // number of k L JJ=jj states in (lm mu)S SU(3)xSU(2) irrep
   wfnsJJ.resize(dim_jj);

   //	basis has to be generated for ndiag = 1 ==> it contains entire single_segment_basis_jjmax of
   //a model space
   if (single_segment_basis_jjmax.ndiag() != 1) {
      cerr << "The many-particle SU(3)xSU(2) basis must be made up of a single segment!" << endl;
      return false;
   }

   fstream wfn_file(wfn_file_name.c_str(),
                    std::ios::in | std::ios::binary |
                        std::ios::ate);  // open at the end of file so we can get file size
   if (!wfn_file) {
      cout << "Error: could not open '" << wfn_file_name << "' wfn file" << endl;
      return false;
   }

   size_t file_size = wfn_file.tellg();
   if (file_size % sizeof(float)) {
      cout << "ERROR: the size of the file with the input Sp(3,R) wave function '" << wfn_file_name
           << "' is not a multiple of sizeof(float)!" << endl;
      return false;
   }
   wfn_file.seekg(0, std::ios::beg);

   size_t nelems = file_size / sizeof(float);  // this number should correspond to number of irreps
                                               // in Nhw Sp Sn S (lm mu) basis
   if (nelems != single_segment_basis_jjmax.dim()) {
      cerr << "ERROR: the dimension of the input Sp(3,R) wave function stored in file `"
           << wfn_file_name << "` which we want to use to generate Sp(3,R) basis states with 2J="
           << su3xsu2basisJJ.JJ();
      cerr << ":" << nelems
           << ". This is not equal to the dimension of many-particle SU(3)xSU(2) basis:"
           << single_segment_basis_jjmax.dim() << ". ";
      cerr << "This could be caused by the fact that the input wave function was generated for J "
              "value which is degenerated in";
      cerr << " the basis of SU(3)xSU(2) irrep, but this program works under assumption that J "
              "value is not degenerated, i.e. it is unique in a given SU(3)xSU(2) irrep."
           << endl;
      return false;
   }

   // dim[Sp Sn S (lm mu) Jmax]:1
   // ==> single_segment_basis_jjmax.dim() == (number of irreps) * (dim:1) = number of irreps
   // size of basis with J=jj is equal to dim[Sp Sn S (lm mu) J=jj] * (number if irreps)
   size_t dim_model_space_jj =
       single_segment_basis_jjmax.dim() *
       dim_jj;  // total number of basis states in Nhw Sp Sn S (lm mu) JJ basis
   for (size_t i = 0; i < wfnsJJ.size(); ++i) {
      wfnsJJ[i].resize(dim_model_space_jj);
      std::fill(wfnsJJ[i].begin(), wfnsJJ[i].end(), 0.0);
   }

   //	Read input wavefunction produced by ndiag_wfn diagonal processes
   //	and store it in the order with ndiag=1
   float* wfn_single_segment_jjmax;
   wfn_single_segment_jjmax =
       new float[single_segment_basis_jjmax.dim()];  // number of irreps Nhw (lm mu) SSp SSn SS
   //	Load wave function generated for ndiag_wfn segments into a vector that contains order
   //	of basis states for single segment (i.e. ndiag=1)
   uint32_t number_ipin_blocks = single_segment_basis_jjmax.NumberOfBlocks();
   // iterate over (ip, in) blocks in order given by ndiag:ndiag_wfn
   for (uint16_t isection = 0; isection < ndiag_wfn; ++isection) {
      //		Here I am using the fact that order of (ip in) blocks in a basis split into
      //ndiag_wfn sections is following:
      //
      //	section --> {iblock0, iblock1, ...... }
      //		----------------------------------
      //		0     {0, ndiag_wfn, 2ndiag_wfn, 3ndiag_wfn ....}
      //		1     {1, ndiag_wfn+1, 2ndiag_wfn+1, 3ndiag_wfn+1, ...}
      //		2     {2, ndiag_wfn+2, 2ndiag_wfn+1, 3ndiag_wfn+2, ...}
      //		.
      //		.
      //		.
      //		iterate over ipin_block in this order: 0, ndiag_wfn, 2ndiag_wfn, 3ndiag_wfn
      //.... 1, ndiag_wfn+1, 2ndiag_wfn+1, 3ndiag_wfn+1, ...
      for (uint32_t ipin_block = isection; ipin_block < number_ipin_blocks;
           ipin_block += ndiag_wfn) {
         //	obtain position of the current block in the single segment model space basis [i.e.
         //with ndiag=1] and its size
         uint32_t ipos = single_segment_basis_jjmax.BlockPositionInSegment(ipin_block);
         uint16_t number_of_states_in_block = single_segment_basis_jjmax.NumberOfStatesInBlock(
             ipin_block);  // this number should be equal to apmax x anmax x rho0max x (dim [JJ]=1)
                           // read expansion coefficient for ipin_block
         wfn_file.read((char*)&wfn_single_segment_jjmax[ipos],
                       number_of_states_in_block * sizeof(float));
      }
   }

   //	Construct Sp(3,R) wave functions associated with all physically permissible k L J
   for (uint32_t ipin_block = 0; ipin_block < number_ipin_blocks; ipin_block++) {
      int amax = single_segment_basis_jjmax.NumberOfStatesInBlock(
          ipin_block);  // apmax x anmax x rhomax but only if basis is restricted to a single U(3)
                        // irrep state
      if (amax == 0) {
         continue;
      }

      // single_segment_basis_jjmax: basis (ndiag:1) of model space spanned by Sp Sn S Nhw (lm mu)
      // JJmax states
      // ipos: position of |(ip, in) a0=0 gamma nhw (lm mu) ssp ssn ss Jmax> basis state of (ip, in)
      // block of
      // a0max states a0max = apmax * anmax *rho0max. As dimension of each irrep is 1,
      // ipos also corresponds to the position of the first irrep in (ip, in) block in single
      // segment basis.
      uint32_t ipos = single_segment_basis_jjmax.BlockPositionInSegment(
          ipin_block);  // points to the first element in full_wfn which corresponds to a=0 (ip in)
                        // Nhw (lm mu) S irrep
                        // dim_jj: dim [|(lm mu) ssp ssn ss k L J=jj>]
      // iposJJ: position of first state in | a=0 (ip in) Nhw (lm mu) J=jj L=Lmin k=kmin J=jj> state
      // in basis made up of a single segment.
      size_t iposJJ = ipos * dim_jj;  //
      // The only difference between this hypothetical basis and single_segment_basis_jjmax is that
      // each block contains dim[JJ] states instead of just a single one.

      // we deal with a single (lm mu) SS irrep ==> number of wpn in a block ipin_block: 1
      assert((single_segment_basis_jjmax.blockEnd(ipin_block) -
              single_segment_basis_jjmax.blockBegin(ipin_block)) == 1);
      //	Now iterate over L k basis states of of (lm  mu) S J=jj irrep
      int ikL = 0;
      // iterate over L
      for (su3xsu2basisJJ.rewind(); !su3xsu2basisJJ.IsDone(); su3xsu2basisJJ.nextL()) {
         // iterate over k
         for (int k = 0; k < su3xsu2basisJJ.kmax(); ++k, ++ikL) {
            for (int a = 0; a < amax; ++a) {
               wfnsJJ[ikL][iposJJ++] = wfn_single_segment_jjmax[ipos + a];
            }
         }
      }
   }
   // Note that vectors wfnsJJ[ikL] are stored with ndiag=1 order of basis states

   delete[] wfn_single_segment_jjmax;
   return true;
}

bool is_number(const std::string& s) {
   std::string::const_iterator it = s.begin();
   while (it != s.end() && std::isdigit(*it)) ++it;
   return !s.empty() && it == s.end();
}

bool ParseInputParemeters(int argc, char** argv, int& nprotons, int& nneutrons, int& nhw, int& lm,
                          int& mu, int& ssp, int& ssn, int& ss, int& jj, string& wfn_filename,
                          int& ndiag_wfn) {
   bool IsSuccess = true;
   enum OPTIONS {
      kNDiag = 'd',
      kZ = 'Z',
      kN = 'N',
      kNhw = 'h',
      kLM = 'l',
      kMU = 'm',
      kSSp = 'p',
      kSSn = 'n',
      kSS = 's',
      kJJ = 'j'
   };

   struct option code_options[] = {{"ndiag", required_argument, NULL, kNDiag},
                                   {"N", required_argument, NULL, kN},
                                   {"Z", required_argument, NULL, kZ},
                                   {"Nhw", required_argument, NULL, kNhw},
                                   {"lm", required_argument, NULL, kLM},
                                   {"mu", required_argument, NULL, kMU},
                                   {"SSp", required_argument, NULL, kSSp},
                                   {"SSn", required_argument, NULL, kSSn},
                                   {"SS", required_argument, NULL, kSS},
                                   {"JJ", required_argument, NULL, kJJ},
                                   {NULL, 0, NULL, 0}};

   //	First try to obtain values of all required options
   int option_index = 0;
   int option = 0;
   while ((option = getopt_long_only(argc, argv, "", code_options, &option_index)) != -1) {
      switch (option) {
         case kNDiag:
            ndiag_wfn = atoi(optarg);
            break;
         case kN:
            nneutrons = atoi(optarg);
            break;
         case kZ:
            nprotons = atoi(optarg);
            break;
         case kNhw:
            nhw = atoi(optarg);
            break;
         case kLM:
            lm = atoi(optarg);
            break;
         case kMU:
            mu = atoi(optarg);
            break;
         case kSSp:
            ssp = atoi(optarg);
            break;
         case kSSn:
            ssn = atoi(optarg);
            break;
         case kSS:
            ss = atoi(optarg);
            break;
         case kJJ:
            jj = atoi(optarg);
            break;
      }
   }

   if (argc != 22) {
      cerr << "\nThis code generates Sp(3,R) basis states [spanning equivalent U(3) irreps Nhw (lm "
              "mu) SSp SSn SS] that carry a given value of total angular momentum J.\n\n";
      cerr << "Usage: " << argv[0] << " <Sp3R wave function> -ndiag <ndiag_wfn> -Z <Z> -N <N> -Nhw "
                                      "<Nhw> -lm <lm> -mu <mu> -SSp <SSp> -SSn <SSn> -SS <SS> -JJ "
                                      "<JJ>\n";
      cerr << "------\n";
      cerr << "<Sp3R wave function> ... linear combination of equivalent U(3) irreps that have a "
              "good Sp(3,R) symmetry. This function was generated by the Sp3RGen_slepc_MPI code.\n";
      cerr << "-ndiag <ndiag wfn> ... number of sections in many-particle basis used for "
              "computation of <Sp3R wave function>\n";
      cerr << " -Z <Z> ... number of protons \n";
      cerr << " -N <N> ... number of neutrons \n";
      cerr << " -Nhw <Nhw> ... HO excitations \n";
      cerr << " -lm <lm> ... SU(3) quantum number lambda \n";
      cerr << " -mu <mu> ... SU(3) quantum number mu \n";
      cerr << " -SSp <SSp> ... twice proton spin \n";
      cerr << " -SSn <SSn> ... twice neutron spin \n";
      cerr << " -SS <SS> ... twice total spin \n";
      cerr << " -JJ <JJ> ... twice total angular momentum.\n";
      IsSuccess = false;
   }

   //	We need to reset optind before repeating getopt_long_only
   optind = 1;
   //	Check whether all input arguments are numbers
   while ((option = getopt_long_only(argc, argv, "", code_options, &option_index)) != -1) {
      if (option != '?' &&
          !is_number(optarg))  // an option is permitted but its argument is not a number
      {
         IsSuccess = false;
         cerr << "ERROR: ";
         switch (option) {
            case kNDiag:
               cerr << "ndiag option ";
               break;
            case kN:
               cerr << "N option ";
               break;
            case kZ:
               cerr << "Z option ";
               break;
            case kNhw:
               cerr << "Nhw option ";
               break;
            case kLM:
               cerr << "lm option ";
               break;
            case kMU:
               cerr << "mu option ";
               break;
            case kSSp:
               cerr << "SSp option ";
               break;
            case kSSn:
               cerr << "SSn option ";
               break;
            case kSS:
               cerr << "SS option ";
               break;
            case kJJ:
               cerr << "JJ option ";
               break;
         }
         cerr << " has argument:" << optarg << " which is not a number!" << endl;
      }
   }

   if (ndiag_wfn == -1) {
      cerr << "ndiag parameter for input Sp(3,R) state is missing.\n";
   }
   if (nprotons == -1) {
      cerr << "Number of protons Z is missing.\n";
   }
   if (nneutrons == -1) {
      cerr << "Number of neutrons N is missing.\n";
   }
   if (nhw == -1) {
      cerr << "Number of harmonic oscillator excitation energy Nhw is missing.\n";
   }
   if (lm == -1) {
      cerr << "SU(3) quantum number lambdais missing.\n";
   }
   if (mu == -1) {
      cerr << "SU(3) quantum number mu is missing.\n";
   }
   if (ssp == -1) {
      cerr << "Proton intrinsic spin SSp is missing.\n";
   }
   if (ssn == -1) {
      cerr << "Neutron intrinsic spin SSn is missing.\n";
   }
   if (ss == -1) {
      cerr << "Total intrinsic spin SS is missing.\n";
   }
   if (jj == -1) {
      cerr << "Total orbital angular momentum JJ is missing.\n";
   }

   if (IsSuccess) {
      wfn_filename = argv[optind];
   }

   return IsSuccess;
}

string CreateName(const string& wfn, int k, int L, int jj) {
   std::ostringstream oss;
   oss << wfn;
   oss << "_k" << k << "LL" << L << "JJ" << jj;
   return oss.str();
}

int main(int argc, char** argv) {
   int nprotons(-1), nneutrons(-1), nhw(-1), lm(-1), mu(-1), ssp(-1), ssn(-1), ss(-1), jj(-1);
   int ndiag_wfn(-1);
   string wfn_file_name;

   if (!ParseInputParemeters(argc, argv, nprotons, nneutrons, nhw, lm, mu, ssp, ssn, ss, jj,
                             wfn_file_name, ndiag_wfn)) {
      return EXIT_FAILURE;
   }

   // first check if dimension is negligible
   SU3xSU2::BasisJfixed su3xsu2basisJJ(SU3xSU2::LABELS(1, lm, mu, ss), jj);
   //	check if the given J belongs to basis of (lm mu) S irrep
   if (su3xsu2basisJJ.dim() == 0) {
      cerr << "A given total angular momentum [2J:" << jj << "] does not belong to the basis of ("
           << lm << " " << mu << ") SS:" << ss << " irrep." << endl;
      return EXIT_FAILURE;
   }

   //	Construct model space spanned by nhw (lm mu) ssp ssn ss irreps made up of a
   //	single segment and with J = Jmax = lm + mu + S note that dimension of each
   //	irrep is equal to 1.  NOTE: for large U(3) model spaces it will be
   //	neccessary to carry out parallel construction
   proton_neutron::ModelSpace ncsmModelSpace(nprotons, nneutrons, nhw, ssp, ssn, ss, lm, mu);
   lsu3::CncsmSU3xSU2Basis single_segment_basis_jjmax(ncsmModelSpace, 0, 1);

   //	check if the given ndiag option has a resonable value
   if (ndiag_wfn <= 0 || ndiag_wfn > 299) {
      cerr << "Error: the value <ndiag wfn>: " << ndiag_wfn
           << " is out of allowed range [1 ... 299].\n";
      return EXIT_FAILURE;
   }

   cout << "Generating " << su3xsu2basisJJ.dim() << " Sp(3,R) states " << endl;

   // vector of Sp(3,R) basis states |Sp(3,R) (lm mu) ssp ssn s k L jj> expanded in SU(3)-coupled
   // basis
   // wfnsJJ[ikL]=|n0hw Sp(3,R) nhw (lm mu) ssp ssn s k L jj>=\sum_{gamma k' L'} c_{gamma k' L'}
   // |gamma Nhw (lm mu) ssp ssn ss k' L' jj>
   // where c_{gamma k' L'} ~ c_{gamma} * delta_k k' * delta L L'
   vector<vector<float>> wfnsJJ;
   bool ok = ReadSp3RWfnCreateSp3RJJStates(su3xsu2basisJJ, wfn_file_name,
                                           single_segment_basis_jjmax, ndiag_wfn, wfnsJJ);

   if (!ok) {
      return EXIT_FAILURE;
   }

   int ikL = 0;
   vector<string> output_filenames(su3xsu2basisJJ.dim());
   for (su3xsu2basisJJ.rewind(); !su3xsu2basisJJ.IsDone(); su3xsu2basisJJ.nextL()) {
      int L = su3xsu2basisJJ.L();
      for (int k = 0; k < su3xsu2basisJJ.kmax(); ++k, ++ikL) {
         output_filenames[ikL] = CreateName(wfn_file_name, k, L, jj);
      }
   }

   assert(output_filenames.size() == wfnsJJ.size());

   ikL = 0;
   for (su3xsu2basisJJ.rewind(); !su3xsu2basisJJ.IsDone(); su3xsu2basisJJ.nextL()) {
      int L = su3xsu2basisJJ.L();
      for (int k = 0; k < su3xsu2basisJJ.kmax(); ++k, ++ikL) {
         cout << "k=" << k << " LL=" << L << " JJ=" << jj << "--->" << output_filenames[ikL]
              << endl;
         std::ios_base::openmode open_mode = (std::ios::binary | std::ios::trunc);
         ofstream f(output_filenames[ikL], open_mode);
         f.write((const char*)wfnsJJ[ikL].data(), wfnsJJ[ikL].size() * sizeof(float));
//         for (auto coeff : wfnsJJ[ikL]) {
//            cout << coeff << endl;
//         }
//         cout << "NEXT state:" << endl;
      }
   }
//   std::cout << "Note: resulting wavefunctions are stored with ndiag:1 ordering" << std::endl;
}
