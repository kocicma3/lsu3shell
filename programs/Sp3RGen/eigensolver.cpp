#include <mpi.h>

#include <cassert>
#include <cstdint>
#include <fstream>
#include <iostream>
#include <regex>
#include <stdexcept>
#include <string>
#include <vector>

// Definition of Fortran "module" variable naming conventions
#if (defined __GNUC__ && !defined __INTEL_COMPILER)
#define FORTRAN_MODULE_VARIABLE(s) __nodeinfo_MOD_##s
#elif (defined __INTEL_COMPILER)
#define FORTRAN_MODULE_VARIABLE(s) nodeinfo_mp_##s##_
#else
#define FORTRAN_MODULE_VARIABLE(s) __nodeinfo_mp_##s_
#endif

extern "C" {
extern void matrix_diagonalize_coo_(
      int64_t* ncols, int64_t* nrows, int64_t* nnz,
      float* xme, int32_t* colind, int32_t* rowind,
      int32_t* maxitr, float* tol,
      int32_t* neigen, float* eigenvals, float* eigenvectors,
      int64_t* nstates_sofar);

extern void splitprocs_();

extern int FORTRAN_MODULE_VARIABLE(myrank);
extern int FORTRAN_MODULE_VARIABLE(mypid);
extern int FORTRAN_MODULE_VARIABLE(nproc);
extern int FORTRAN_MODULE_VARIABLE(ndiag);
}

void resolve_parameters(const int argc, char const * const * argv,
      std::string& matrix_list_filename, int& neigen, int& maxitr)
{
   if (argc < 2) {
      std::cerr
         << "Program parameters:" << std::endl
         << "<matrix list file name> - file with list of matrix files, one per line" << std::endl
         << "<neigen> - number of required eigenvalues, optional, 10 by default" << std::endl
         << "<niters> - number of maximum eigensolver iterations, optional, 100 by default" << std::endl;
      MPI_Abort(MPI_COMM_WORLD, -1);
   }

   matrix_list_filename = argv[1];

   neigen = (argc > 2) ? atoi(argv[2]) : 10;
   maxitr = (argc > 3) ? atoi(argv[3]) : 100;
}

int main(int argc, char** argv)
{
   MPI_Init(&argc, &argv);

   int nprocs, rank;
   MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
   MPI_Comm_rank(MPI_COMM_WORLD, &rank);

   // resolve program parameters
   std::string matrix_list_filename;
   int neigen, maxitr;
   resolve_parameters(argc, argv, matrix_list_filename, neigen, maxitr);

   // process mapping
   int ndiag;
   for (ndiag = 1; ndiag < 65536; ndiag++)
      if ((ndiag * (ndiag + 1) / 2) == nprocs)
         break;
   assert(ndiag < 65536);

   int idiag = 0, jdiag = 0;
   for (int temp = 0; temp < nprocs; temp++) {
      if (rank == temp) break;
      idiag++;
      jdiag++;
      if (jdiag == ndiag) {
         jdiag -= idiag - 1;
         idiag = 0;
      }
   }

   // matrix data:
   std::vector<int32_t> rows, cols;
   std::vector<float> vals;

   // temporary buffers, put here because of memory efficiency reasons
   std::vector<int32_t> file_rows, file_cols;
   std::vector<float> file_vals;

   // matrix list file
   std::ifstream matrix_list_file(matrix_list_filename);

   long n;
   matrix_list_file >> n;

   long per_process = n / ndiag;
   assert (per_process > 0);

   long first_row = per_process * idiag;
   long last_row = per_process * (idiag + 1) - 1;
   long first_col = per_process * jdiag;
   long last_col = per_process * (jdiag + 1) - 1;
   if ((last_row > n) || (idiag == ndiag - 1)) last_row = n - 1;
   if ((last_col > n) || (jdiag == ndiag - 1)) last_col = n - 1;
   assert(last_row >= first_row);
   assert(last_col >= first_col);

   int64_t nrows = last_row - first_row + 1;
   int64_t ncols = last_col - first_col + 1;

   std::string line;
   while(std::getline(matrix_list_file, line)) {
      std::string matrix_filename = std::regex_replace(line, std::regex("^[ \t]+|[ \t]+$"), "");

      if (matrix_filename.empty())
         continue;

      std::ifstream matrix_file(matrix_filename, std::ios::in | std::ios::binary);

      int64_t nnz;
      matrix_file.read((char*)&nnz, sizeof(int64_t));

      file_rows.resize(nnz);
      file_cols.resize(nnz);
      file_vals.resize(nnz);

      matrix_file.read((char*)file_rows.data(), sizeof(int32_t) * nnz);
      matrix_file.read((char*)file_cols.data(), sizeof(int32_t) * nnz);
      matrix_file.read((char*)file_vals.data(), sizeof(float) * nnz);

      // append nonzero elements from file to process local elements:
      for (long i = 0; i < nnz; i++) {
         assert(file_cols[i] >= file_rows[i]);
         if (file_cols[i] < file_rows[i]) // UT matrix elements considered only
            continue;

         // check if element belongs to local process
         if ((file_rows[i] >= first_row) && (file_rows[i] <= last_row)
               && (file_cols[i] >= first_col) && (file_cols[i] <= last_col)) {
            rows.push_back(file_rows[i] - first_row);
            cols.push_back(file_cols[i] - first_col);
            vals.push_back(file_vals[i]);
         }
      }
   }

   assert(rows.size() == cols.size());
   assert(rows.size() > 0);
   int64_t nnz = rows.size();

   // call the eigensolver
   FORTRAN_MODULE_VARIABLE(myrank) = rank;
   FORTRAN_MODULE_VARIABLE(mypid) = rank;
   FORTRAN_MODULE_VARIABLE(nproc) = nprocs;
   FORTRAN_MODULE_VARIABLE(ndiag) = ndiag;

   splitprocs_();

   float tol = 0.0f;
   long int nstates_sofar = (rank < ndiag) ? first_row : 0;

   std::vector<float> eigenvals(neigen);
   std::vector<float> eigenvectors; // eigenvectors distributed among diagonal processes
   if (rank < ndiag) {
      assert (nrows == ncols);
      eigenvectors.resize(nrows * neigen);
   }

   // transform to fortran 1-based indexing:
   for (auto& row : rows) row++;
   for (auto& col : cols) col++;

   matrix_diagonalize_coo_(
         &nrows, &ncols, &nnz, // notice that MFDn works with lower triangular matrix
         vals.data(), rows.data(), cols.data(),
         &maxitr, &tol, &neigen, eigenvals.data(), eigenvectors.data(),
         &nstates_sofar);

   MPI_Finalize();
}
