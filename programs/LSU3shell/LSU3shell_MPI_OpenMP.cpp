#include <LSU3/ncsmSU3xSU2Basis.h>
#include <LookUpContainers/CWig9lmLookUpTable.h>
#include <LookUpContainers/lock.h>
#include <SU3ME/ComputeOperatorMatrix.h>
#include <SU3ME/proton_neutron_ncsmSU3BasisFastIteration.h>
#include <SU3NCSMUtils/CRunParameters.h>

#include <algorithm>
#include <cmath>
#include <stack>
#include <stdexcept>
#include <vector>

#include <boost/chrono.hpp>
#include <boost/mpi.hpp>
//	To be able to load and distribute basis from a binary archive file
#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>

// Definition of Fortran "module" variable naming conventions
// TODO: replace NDCRCGNU with the preprocessor directive that GNU compiler @ UND cluster uses
#if (defined NDCRCGNU)
#define FORTRAN_MODULE_VARIABLE(s) __nodeinfo__##s
#elif (defined __GNUC__ && !defined __INTEL_COMPILER)
#define FORTRAN_MODULE_VARIABLE(s) __nodeinfo_MOD_##s
#elif (defined __INTEL_COMPILER)
#define FORTRAN_MODULE_VARIABLE(s) nodeinfo_mp_##s##_
#else
#define FORTRAN_MODULE_VARIABLE(s) __nodeinfo_mp_##s_
#endif

#ifndef DISABLE_DIAG
extern "C" {
#if (defined __GNUC__ || defined __INTEL_COMPILER)
extern void matrix_diagonalize_vbc_(int64_t&, int64_t&, int64_t&, float[], int64_t&, int[], int[],
                                    int[], int[], int&, int&, float&, int&, float[], int&,
                                    long int&);
extern void splitprocs_();
#else
extern void matrix_diagonalize_vbc(int64_t&, int64_t&, int64_t&, float[], int64_t&, int[], int[],
                                   int[], int[], int&, int&, float&, int&, float[], int&,
                                   long int&);
extern void splitprocs();
#endif
extern int FORTRAN_MODULE_VARIABLE(myrank);
extern int FORTRAN_MODULE_VARIABLE(mypid);
extern int FORTRAN_MODULE_VARIABLE(nproc);
extern int FORTRAN_MODULE_VARIABLE(ndiag);
extern int FORTRAN_MODULE_VARIABLE(icomm);
}
#endif

using namespace std;

void MapRankToColRow_MFDn_Compatible(const int ndiag, const int my_rank, int& row, int& col) {
   assert(ndiag * (ndiag + 1) / 2 >= my_rank);
   /*
           if (ndiag % 2 == 0)
           {
                   if (my_rank == 0)
                   {
                           cout << "number of diagonal processes = " << ndiag << " must be an odd
      number." << endl;
                   }
                   MPI_Finalize();
                   return EXIT_FAILURE;
           }
   */
   int executing_process_id(0);
   for (size_t i = 0; i < ndiag; ++i) {
      row = 0;
      for (col = i; col < ndiag; ++col, ++row, ++executing_process_id) {
         if (my_rank == executing_process_id) {
            return;
         }
      }
   }
}

uintmax_t CalculateME(const std::string& hilbert_space_definition_file_name,
                      const std::string& hamiltonian_file_name, int ndiag, int idiag, int jdiag,
                      lsu3::VBC_Matrix& vbc, long int& nstates_sofar, uint64_t& dim,
                      bool simulate) {
   boost::mpi::communicator mpi_comm;
   int my_rank = mpi_comm.rank();
   boost::chrono::system_clock::time_point start;
   boost::chrono::duration<double> duration;

   InitSqrtLogFactTables();

   //  test if the first parameter is model space file name
   bool modelSpaceProvided =
       hilbert_space_definition_file_name.find(".model_space") !=
       std::string::npos;  // true if the first argument contains ".model_space"
                           //	time how long it will take to create bra and ket basis
   start = boost::chrono::system_clock::now();
   lsu3::CncsmSU3xSU2Basis ket, bra;
   if (simulate) {
      if (!modelSpaceProvided)  // ==> we need to load basis states from files
      {
         cout << "Reading basis from '"
              << MakeBasisName(hilbert_space_definition_file_name, idiag, ndiag) << "'."
              << std::endl;
         bra.LoadBasis(hilbert_space_definition_file_name, idiag, ndiag);

         cout << "Reading basis from '"
              << MakeBasisName(hilbert_space_definition_file_name, jdiag, ndiag) << "'."
              << std::endl;
         ket.LoadBasis(hilbert_space_definition_file_name, jdiag, ndiag);
      } else {
         proton_neutron::ModelSpace ncsmModelSpace;
         ncsmModelSpace.Load(hilbert_space_definition_file_name);

         ket.ConstructBasis(ncsmModelSpace, jdiag, ndiag);
         bra.ConstructBasis(ncsmModelSpace, ket, idiag, ndiag);
      }
   } else {
      if (!modelSpaceProvided)  // ==> we need to load basis states from files
      {
         if (my_rank == 0) {
            cout << "Reading basis from " << ndiag << " input files, starting with '"
                 << MakeBasisName(hilbert_space_definition_file_name, 0, ndiag) << "'."
                 << std::endl;
         }

         MPI_Comm ROW_COMM, COL_COMM;
         //	All processes with the same value of idiag are grouped and within
         //	each group ordered based on jdiag value
         MPI_Comm_split(mpi_comm, idiag, my_rank, &ROW_COMM);
         //	All processes with the same value of jdiag are grouped and within
         //	each group ordered based on idiag value
         MPI_Comm_split(mpi_comm, jdiag, my_rank, &COL_COMM);

         std::string bra_buffer, ket_buffer;
         int bra_buffer_length, ket_buffer_length;

         if (idiag == jdiag)  // I am diagonal process
         {
            assert(idiag == my_rank && my_rank < mpi_comm.size());

            //	load basis states from file that contains an idiag segment of a
            //	basis split into ndiag segments
            ket.LoadBasis(hilbert_space_definition_file_name, jdiag, ndiag);
            //	create a binary archive from basis
            std::ostringstream ss;
            boost::archive::binary_oarchive oa(ss);
            oa << ket;
            //	copy memory content of a binary archive into a buffer
            ket_buffer = ss.str();
            ket_buffer_length = ket_buffer.size();
            //	diagonal processes have bra and ket basis equal to each other
            bra = ket;
            bra_buffer = ket_buffer;
            bra_buffer_length = ket_buffer_length;

            //				cout << "Size of serialized class: " <<
            // sizeof(char)*bra_buffer_length/(1024.0*1024.0) << " MB." << endl;
         }
         // broadcast length of buffers ...
         // process with my_rank = idiag is a root for each group of row processes
         // process with my_rank = jdiag is a root for each group of column processes
         MPI_Bcast((void*)&bra_buffer_length, 1, MPI_INT, 0, ROW_COMM);
         MPI_Bcast((void*)&ket_buffer_length, 1, MPI_INT, 0, COL_COMM);

         if (idiag != jdiag)  // I am not a diagonal process ==> need to allocate buffer for bra and
                              // ket basis
         {
            bra_buffer.resize(bra_buffer_length);
            ket_buffer.resize(ket_buffer_length);
         }

         MPI_Bcast((void*)bra_buffer.data(), bra_buffer_length, MPI_CHAR, 0, ROW_COMM);
         MPI_Bcast((void*)ket_buffer.data(), ket_buffer_length, MPI_CHAR, 0, COL_COMM);

         if (idiag !=
             jdiag)  // If not diagonal process => create bra and ket basis from buffers received
         {
            std::istringstream bra_ss(bra_buffer);
            boost::archive::binary_iarchive bra_oa(bra_ss);
            bra_oa >> bra;

            std::istringstream ket_ss(ket_buffer);
            boost::archive::binary_iarchive ket_oa(ket_ss);
            ket_oa >> ket;
         }
      } else {
         proton_neutron::ModelSpace ncsmModelSpace;
         std::string model_space_buffer;
         int model_space_buffer_length;
         if (my_rank == 0) {
            ncsmModelSpace.Load(hilbert_space_definition_file_name);

            std::ostringstream ss;
            boost::archive::binary_oarchive oa(ss);
            oa << ncsmModelSpace;
            //	copy memory content of a binary archive into a buffer
            model_space_buffer = ss.str();
            model_space_buffer_length = model_space_buffer.size();
         }

         MPI_Bcast((void*)&model_space_buffer_length, 1, MPI_INT, 0, mpi_comm);
         if (my_rank != 0) {
            model_space_buffer.resize(model_space_buffer_length);
         }
         MPI_Bcast((void*)model_space_buffer.data(), model_space_buffer_length, MPI_CHAR, 0,
                   mpi_comm);
         if (my_rank != 0) {
            std::istringstream model_space_ss(model_space_buffer);
            boost::archive::binary_iarchive model_space_oa(model_space_ss);
            model_space_oa >> ncsmModelSpace;
         }

         ket.ConstructBasis(ncsmModelSpace, jdiag, ndiag);
         bra.ConstructBasis(ncsmModelSpace, ket, idiag, ndiag);
      }
   }

   dim = ket.getModelSpaceDim();

   if (my_rank == 0) {
      duration = boost::chrono::system_clock::now() - start;
      cout << "Time to construct basis: ... " << duration << endl;
   }

   unsigned long firstStateId_bra = bra.getFirstStateId();
   unsigned long firstStateId_ket = ket.getFirstStateId();

   //	stringstream interaction_log_file_name;
   //	interaction_log_file_name << "interaction_loading_" << my_rank << ".log";
   //	ofstream interaction_log_file(interaction_log_file_name.str().c_str());
   ofstream interaction_log_file("/dev/null");

   CBaseSU3Irreps baseSU3Irreps(bra.NProtons(), bra.NNeutrons(), bra.Nmax());

   //	since root process will read rme files, it is save to let this
   //	process to create missing rme files (for PPNN interaction) if they do not exist
   //	=> true
   bool log_is_on = false;
   CInteractionPPNN interactionPPNN(baseSU3Irreps, log_is_on, interaction_log_file);

   //	PN interaction is read after PPNN, and hence all rmes should be already in memory.
   //	if rme file does not exist, then false
   bool generate_missing_rme = false;
   CInteractionPN interactionPN(baseSU3Irreps, generate_missing_rme, log_is_on,
                                interaction_log_file);

   try {
      start = boost::chrono::system_clock::now();
      CRunParameters run_params;
      run_params.LoadHamiltonian(my_rank, hamiltonian_file_name, interactionPPNN, interactionPN,
                                 bra.NProtons() + bra.NNeutrons());
   } catch (const std::logic_error& e) {
      std::cerr << e.what() << std::endl;
      mpi_comm.abort(-1);
   }

   if (my_rank == 0) {
      duration = boost::chrono::system_clock::now() - start;
      cout << "Process 0: LoadInteractionTerms took " << duration << endl;
      if (simulate) {
         //       interactionPPNN.ShowRMETables();
         cout << "Size of memory: "
              << interactionPPNN.GetRmeLookUpTableMemoryRequirements() / (1024.0 * 1024.0) << " MB."
              << endl;
      }

      // Check minimal and maximal rme values. If they are too large then
      // do not proceed.
      float min, max;
      interactionPPNN.min_max_values(min, max);
      std::cout << "minimal rme value read from tables: " << min << std::endl;
      std::cout << "maximal rme value read from tables: " << max << std::endl;
      if (fabs(min) > 100 || fabs(max) > 100) {
         std::cerr << "Error: magnitude of minimal/maximal rme values are suspitious!" << std::endl;
         std::cerr << "Data stored in rme tables may be corrupted." << std::endl;
         mpi_comm.abort(-1);
      }
   }

   // matrix data
   uintmax_t local_nnz(0);

   try {
      vbc.irow = firstStateId_bra;
      vbc.icol = firstStateId_ket;
      vbc.nrows = bra.dim();
      vbc.ncols = ket.dim();

      //		The order of coefficients is given as follows:
      //  	index = k0*rho0max*2 + rho0*2 + type, where type == 0 (1) for protons (neutrons)
      //		TransformTensorStrengthsIntoPP_NN_structure turns that into:
      //  	index = type*k0max*rho0max + k0*rho0max + rho0
      interactionPPNN.TransformTensorStrengthsIntoPP_NN_structure();

      cout << "Process " << my_rank << " starts calculation of me" << endl;
      boost::chrono::system_clock::time_point start = boost::chrono::system_clock::now();

#ifdef TIME_LOCK
      for (int i = 0; i < 1024; ++i)
         openmp_locking::locktime[i] = boost::chrono::duration<double>::zero();
#endif
#pragma omp parallel reduction(+ : local_nnz)
      {
         local_nnz = CalculateME_Scalar_UpperTriang_OpenMP(interactionPPNN, interactionPN, bra, ket,
                                                           idiag, jdiag, vbc);
      }
#ifdef TIME_LOCK
      std::cout << "reporting locking time" << std::endl;
      for (int i = 0; i < 1024; ++i)
         std::cout << "Thread " << i << " : " << openmp_locking::locktime[i] << std::endl;
#endif

      boost::chrono::duration<double> duration = boost::chrono::system_clock::now() - start;
      cout << "Resulting time: " << duration << endl;
   } catch (const std::exception& e) {
      cerr << e.what() << std::endl;
      mpi_comm.abort(-1);
   }
   mpi_comm.barrier();

   // nstates_sofar is needed by MFDn eigensolver
   if (my_rank < ndiag)  // ==> diagonal process ... i.e. bra = ket
   {
      nstates_sofar = bra.getFirstStateId(my_rank);
   }

   /*
   // TODO - REMOVE THE FOLLOWING PRINT OUTS :)
       cout << "Process " << my_rank << ": "
            << "nblocks: " << vbc.rowind.size()
            << ", nrows: " << vbc.nrows
            << ", ncols: " << vbc.ncols
            << ", row: " << vbc.irow
            << ", col: " << vbc.icol
            << ", nnz: " << vbc.nnz
            << ", rowind: " << vbc.rowind.size()
            << ", colind: " << vbc.colind.size()
            << ", rownnz: " << vbc.rownnz.size()
            << ", colnnz: " << vbc.colnnz.size()
            << ", vals: " << vbc.vals.size();
       if (vbc.rowind.size() > 0)
           cout << ", rowind[0]: " << vbc.rowind[0];
       if (vbc.colind.size() > 0)
           cout << ", colind[0]: " << vbc.colind[0];
       if (vbc.rownnz.size() > 0)
           cout << ", rownnz[0]: " << vbc.rownnz[0];
       if (vbc.colnnz.size() > 0)
           cout << ", colnnz[0]: " << vbc.colnnz[0];
       if (vbc.vals.size() > 0)
           cout << ", vals[0]: " << vbc.vals[0];
       cout << endl;
   */
   return local_nnz;
}

int main(int argc, char** argv) {
   boost::mpi::environment env(argc, argv);

   boost::chrono::system_clock::time_point start;
   boost::chrono::duration<double> duration;

   int my_rank, nprocs, idiag, jdiag;
   unsigned int ndiag;
   bool simul =
       false;  // if simul then a the process simulate what happens on a particuliar mpi_rank

   boost::mpi::communicator mpi_comm_world;
   my_rank = mpi_comm_world.rank();
   nprocs = mpi_comm_world.size();

   if (su3shell_data_directory == NULL) {
      if (my_rank == 0) {
         cerr << "System variable 'SU3SHELL_DATA' was not defined!" << endl;
      }
      mpi_comm_world.abort(EXIT_FAILURE);
   }

   if (argc != 5) {
      if (my_rank == 0) {
         cerr << "Usage: " << argv[0]
              << " <[model space]/[precalculated basis] file name> <Hamiltonian file name> <#eigen "
                 "vectors>  <#Lanczos iterations>"
              << endl;
         cerr << "By setting environment variables U9SIZE, U6SIZE, Z6SIZE, and U9SIZE_FREQ one can "
                 "set the "
                 "size of look-up tables for 9lm, u6lm, z6lm, and \"frequent\" 9lm symbols."
              << endl;
         cerr << "It is highly advisable to set environment variable NNZ to preallocates storage "
                 "for non vanishing matrix elements."
              << endl;
         cerr << "By setting environment variables NDIAG, IDIAG and JDIAG, one can execute only "
                 "one block of an hypothetical parallel execution."
              << endl;
      }
      mpi_comm_world.abort(EXIT_FAILURE);
   }

   long nnz_reserve = 1L << 20;  // default reserved space for 1M elements
   if (getenv("NNZ"))
      nnz_reserve = atol(getenv("NNZ"));
   else {
      if (my_rank == 0) {
         std::cout << "By setting environment variable NNZ one can preallocate array for "
                      "non-vanishing matrix elements. HIGHLY RECOMMENDED!"
                   << std::endl;
      }
   }
   if (my_rank == 0) {
      std::cout << "Reserved number of non-vanishing matrix elements: " << nnz_reserve << std::endl;
   }

   if (getenv("NDIAG") || getenv("IDIAG") || getenv("JDIAG")) {
      simul = true;
      if (nprocs != 1) {
         if (my_rank == 0) {
            cerr << "When simulating, use a single MPI rank." << endl;
         }
         mpi_comm_world.abort(EXIT_FAILURE);
      }
      if (getenv("NDIAG") == NULL || getenv("IDIAG") == NULL || getenv("JDIAG") == NULL) {
         cerr << "When simulating, all the following environment variables must be defined: NDIAG, "
                 "IDIAG, JDIAG."
              << endl;
         return EXIT_FAILURE;
      }
      ndiag = atoi(getenv("NDIAG"));
      if (ndiag % 2 == 0) {
         cerr << "ndiag:" << ndiag << " ... parameter ndiag must be an odd number!" << endl;
         return EXIT_FAILURE;
      }
      idiag = atoi(getenv("IDIAG"));
      jdiag = atoi(getenv("JDIAG"));
   } else {
      ndiag = (-1 + sqrt(1 + 8 * nprocs)) / 2;
      if (fabs(ndiag - (double)(-1.0 + sqrt(1 + 8 * nprocs)) / 2.0) > 1.0e-7) {
         if (my_rank == 0) {
            cerr << "number of processes = " << nprocs
                 << " is wrong: must be qual to ndiag*(ndiga+1)/2, where ndiag is number of "
                    "diagonal processes."
                 << endl;
         }
         mpi_comm_world.abort(EXIT_FAILURE);
      }
      MapRankToColRow_MFDn_Compatible(ndiag, my_rank, idiag, jdiag);
   }

   CWig9lmLookUpTable<RME::DOUBLE>::AllocateMemory(my_rank == 0);

   std::string hilbert_space_definition_file_name(argv[1]);
   std::string hamiltonian_file_name(argv[2]);

   lsu3::VBC_Matrix vbc;
   vbc.vals.reserve(nnz_reserve);
   //	these two variable are needed for MFDn
   int vectors = true;
   long int nstates_sofar(0);
   uintmax_t local_nnz;
   uint64_t dim;
   local_nnz = CalculateME(hilbert_space_definition_file_name, hamiltonian_file_name, ndiag, idiag,
                           jdiag, vbc, nstates_sofar, dim, simul);

   //	Sometimes it may be useful to store have matrix market compatible matrix on disk ...
   //	vbc.save_matrix_market("vbc_dummy.mtx");

   if (simul) {
      cout << "#nnz:" << local_nnz << endl;
      cout << "required NNZ:" << vbc.vals.size() << endl;
      cout << "Size of matrix in memory:" << vbc.memory_usage() / (1024.0 * 1024.0) << " MB."
           << endl;

      size_t frequent_u9_size;
      size_t u9_size;
      size_t u6_size;
      size_t z6_size;
      CWig9lmLookUpTable<RME::DOUBLE>::memory_usage(frequent_u9_size, u9_size, u6_size, z6_size);
      cout << "Size of frequent U9: " << frequent_u9_size / (1024.0 * 1024.0) << " MB." << endl;
      cout << "Size of U9: " << u9_size / (1024.0 * 1024.0) << " MB." << endl;
      cout << "Size of U6: " << u6_size / (1024.0 * 1024.0) << " MB." << endl;
      cout << "Size of Z6: " << z6_size / (1024.0 * 1024.0) << " MB." << endl;

      size_t total = vbc.memory_usage() + frequent_u9_size + u9_size + u6_size + z6_size;
      cout << "Total size of memory:" << total / (1024.0 * 1024.0) << " MB." << endl;

      CWigEckSU3SO3CGTablesLookUpContainer::ShowInfo();  // clear memory allocated for SU(3)>SO(3)
                                                         // coefficients
      CWig9lmLookUpTable<RME::DOUBLE>::ShowInfo();
      CWig9lmLookUpTable<RME::DOUBLE>::ShowMemoryUsage();
   } else {
      uintmax_t local_vbc_size = vbc.memory_usage();
      uintmax_t total_vbc_size, maxlocmatsize;
      uintmax_t local_vbc_nnz = vbc.vals.size();
      uintmax_t total_vbc_nnz, total_nnz;
      uintmax_t max_vbc_nnz = 0;
      boost::mpi::reduce(mpi_comm_world, local_vbc_size, total_vbc_size, std::plus<uintmax_t>(), 0);
      boost::mpi::reduce(mpi_comm_world, local_vbc_size, maxlocmatsize,
                         boost::mpi::maximum<uintmax_t>(), 0);
      boost::mpi::reduce(mpi_comm_world, local_nnz, total_nnz, std::plus<uintmax_t>(), 0);
      boost::mpi::reduce(mpi_comm_world, local_vbc_nnz, total_vbc_nnz, std::plus<uintmax_t>(), 0);
      boost::mpi::reduce(mpi_comm_world, local_vbc_nnz, max_vbc_nnz,
                         boost::mpi::maximum<uintmax_t>(), 0);

      size_t num_frequent_u9, frequent_u9_size, max_num_frequent_u9;
      size_t num_u9, u9_size, max_num_u9;
      size_t num_u6, u6_size, max_num_u6;
      size_t num_z6, z6_size, max_num_z6;
      CWig9lmLookUpTable<RME::DOUBLE>::coeffs_info(num_frequent_u9, num_u9, num_u6, num_z6);
      boost::mpi::reduce(mpi_comm_world, num_frequent_u9, max_num_frequent_u9,
                         boost::mpi::maximum<size_t>(), 0);
      boost::mpi::reduce(mpi_comm_world, num_u9, max_num_u9, boost::mpi::maximum<size_t>(), 0);
      boost::mpi::reduce(mpi_comm_world, num_u6, max_num_u6, boost::mpi::maximum<size_t>(), 0);
      boost::mpi::reduce(mpi_comm_world, num_z6, max_num_z6, boost::mpi::maximum<size_t>(), 0);

      CWig9lmLookUpTable<RME::DOUBLE>::memory_usage(frequent_u9_size, u9_size, u6_size, z6_size);

      size_t memory_usage_coeffs = frequent_u9_size + u9_size + u6_size + z6_size;
      size_t max_memory_usage_coeffs;
      boost::mpi::reduce(mpi_comm_world, memory_usage_coeffs, max_memory_usage_coeffs,
                         boost::mpi::maximum<size_t>(), 0);

      if (my_rank == 0) {
         // model space dimension: getModelSpaceDim()
         size_t total_csr_size =
             total_nnz * sizeof(float) + total_nnz * sizeof(int) + dim * sizeof(int);

         std::cout << "Total size of Hamiltonian matrix in VBC format: "
                   << total_vbc_size / (1024.0 * 1024.0) << " MB." << std::endl;
         std::cout << "Total size of Hamiltonian matrix in CRS format: "
                   << total_csr_size / (1024.0 * 1024.0) << " MB." << std::endl;
         std::cout << "Maximum local matrix block size: " << maxlocmatsize / (1024.0 * 1024.0)
                   << " MB." << std::endl;
         std::cout << "Number of matrix elements stored in vbc:" << total_vbc_nnz << std::endl;
         std::cout << "Required value of NNZ: " << max_vbc_nnz << std::endl;
         std::cout << "Number of non vanishing matrix elements:" << total_nnz << std::endl;
         std::cout << "\n\nmax number of u9: " << max_num_u9 << std::endl;
         std::cout << "max number of u6: " << max_num_u6 << std::endl;
         std::cout << "max number of z6: " << max_num_z6 << std::endl;
         std::cout << "max number of frequent u9: " << max_num_frequent_u9 << std::endl;
         std::cout << "max amount of memory for SU(3) coeffs: "
                   << max_memory_usage_coeffs / (1024.0 * 1024.0) << " MB.\n\n";
      }
   }

   CWig9lmLookUpTable<RME::DOUBLE>::ReleaseMemory();  // clears memory allocated for U9, U6, and Z6
                                                      // coefficients
   CSSTensorRMELookUpTablesContainer::ReleaseMemory();  // clear memory allocated for single-shell
                                                        // SU(3) rmes
   CWigEckSU3SO3CGTablesLookUpContainer::ReleaseMemory();  // clear memory allocated for SU(3)>SO(3)
                                                           // coefficients

   if (simul) {
      cout << "Simulation is done" << endl;
      return 0;
   }

   mpi_comm_world.barrier();

//  The following are internal variables of MFDn that has to be set
//  before calling splitprocs()
//
#ifndef DISABLE_DIAG
   FORTRAN_MODULE_VARIABLE(myrank) = FORTRAN_MODULE_VARIABLE(mypid) = my_rank;
   FORTRAN_MODULE_VARIABLE(nproc) = nprocs;
   FORTRAN_MODULE_VARIABLE(ndiag) = ndiag;

   //  nodeinfo_mp_icomm_  = MPI_COMM_WORLD;
   splitprocs_();
#endif

   int neigen;
   int maxitr;
   int startitr(0);
   float tol(0);

   neigen = atoi(argv[3]);
   maxitr = atoi(argv[4]);

   if (my_rank == 0) {
      cout << "neigen = " << neigen << " maxitr = " << maxitr << " startitr = " << startitr
           << " tol = " << tol << endl;
   }

   try {
      //      make column and row indices [C++ compatible, i.e. first element of
      //      matrix has indices i=0 j = 0] compatible with fortran array indexing
      std::transform(vbc.rowind.begin(), vbc.rowind.end(), vbc.rowind.begin(),
                     bind2nd(plus<int>(), +1));
      std::transform(vbc.colind.begin(), vbc.colind.end(), vbc.colind.begin(),
                     bind2nd(plus<int>(), +1));

      //	Notice that MFDn works with lower triangular matrix
      int64_t ncols, nrows, nnz;
      ncols = vbc.nrows;
      nrows = vbc.ncols;
      nnz = vbc.nnz;

      int64_t nblocks = vbc.rowind.size();

      vector<float> eigenvals(neigen);
//      Eigenvalues and eigenvectors are saved on disk by matrix_diagonalize_
//      Question: I assume that it is guaranteed that &vector[0] gives me a pointer a contiguous
//      array of memory
#ifndef DISABLE_DIAG
#if (defined __GNUC__ || defined __INTEL_COMPILER)
      matrix_diagonalize_vbc_(ncols, nrows, nnz, (float*)(&vbc.vals[0]), nblocks,
                              (int*)(&vbc.rowind[0]), (int*)(&vbc.colind[0]),
                              (int*)(&vbc.rownnz[0]), (int*)(&vbc.colnnz[0]), startitr, maxitr, tol,
                              neigen, (float*)&eigenvals[0], vectors, nstates_sofar);
#else
      matrix_diagonalize_vbc(ncols, nrows, nnz, (float*)(&vbc.vals[0]), nblocks,
                             (int*)(&vbc.rowind[0]), (int*)(&vbc.colind[0]), (int*)(&vbc.rownnz[0]),
                             (int*)(&vbc.colnnz[0]), startitr, maxitr, tol, neigen,
                             (float*)&eigenvals[0], vectors, nstates_sofar);
#endif
#endif
   } catch (const std::exception& e) {
      cerr << e.what() << std::endl;
      cerr << "Process reporting error ... rank: " << my_rank << " idiag:" << idiag
           << " jdiag:" << jdiag << std::endl;
      boost::mpi::environment::abort(EXIT_FAILURE);
   }
   return EXIT_SUCCESS;
}
