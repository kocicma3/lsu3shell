#include <algorithm>
#include <cassert>
#include <cstdint>
#include <cstdio>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <string>
#include <vector>

//#define IT_DEBUG
#include <LSU3/CImportanceTruncation.h>

#include <LSU3/COO_Matrix.h>
#include <LSU3/lsu3shell.h>
#include <LSU3/mfdn.h>
#include <LSU3/ncsmSU3xSU2Basis.h>
#include <LSU3/VBC_Matrix.h>
#include <LookUpContainers/CWig9lmLookUpTable.h>
#include <SU3NCSMUtils/CRunParameters.h>

#include <boost/mpi.hpp>

// color printing modifiers:
static const std::string red("\033[0;31m");
static const std::string green("\033[1;32m");
static const std::string yellow("\033[1;33m");
static const std::string cyan("\033[0;36m");
static const std::string magenta("\033[0;35m");
static const std::string reset("\033[0m");

int main(int argc, char** argv) {
   boost::mpi::environment env(argc, argv);

   int my_rank, nprocs, idiag, jdiag, ndiag;

   boost::mpi::communicator mpi_comm_world;
   my_rank = mpi_comm_world.rank();
   nprocs = mpi_comm_world.size();

   if (su3shell_data_directory == NULL) {
      if (my_rank == 0) 
         std::cerr << "System variable 'SU3SHELL_DATA' was not defined!" << std::endl;
      mpi_comm_world.abort(EXIT_FAILURE);
   }

   if ((argc < 8) || (argc > 10)) {
      if (my_rank == 0) {
         std::cerr << "Usage: " << argv[0] << " <[model space]/[precalculated basis] file name>"
            " <Hamiltonian file name> <#eigen vectors>  <#Lanczos iterations>"
            " <#IT iterations> <eps> <chi> <#ref. states> <#initial Nmax>" << std::endl;
         std::cerr << "By setting environment variables U9SIZE, U6SIZE, Z6SIZE, and U9SIZE_FREQ one can "
            "set the size of look-up tables for 9lm, u6lm, z6lm, and \"frequent\" 9lm symbols." << std::endl;
         std::cerr << "It is highly advisable to set environment variable NNZ to preallocates storage "
            "for non vanishing matrix elements." << std::endl;
         std::cerr << "By setting environment variables NDIAG, IDIAG and JDIAG, one can execute only "
            "one block of an hypothetical parallel execution." << std::endl;
      }
      mpi_comm_world.abort(EXIT_FAILURE);
   }

   long nnz_reserve = 1L << 20;  // default reserved space for 1M elements
   if (getenv("NNZ"))
      nnz_reserve = atol(getenv("NNZ"));
   else {
      if (my_rank == 0) 
         std::cout << "By setting environment variable NNZ one can preallocate array for "
            "non-vanishing matrix elements. HIGHLY RECOMMENDED!" << std::endl;
   }
   if (my_rank == 0) 
      std::cout << "Reserved number of non-vanishing matrix elements: " << nnz_reserve << std::endl;

   ndiag = (-1 + sqrt(1 + 8 * nprocs)) / 2;
   if (fabs(ndiag - (double)(-1.0 + sqrt(1 + 8 * nprocs)) / 2.0) > 1.0e-7) {
      if (my_rank == 0) 
         std::cerr << "number of processes = " << nprocs << " is wrong: must be qual to"
            " ndiag*(ndiga+1)/2, where ndiag is number of diagonal processes." << std::endl;
      mpi_comm_world.abort(EXIT_FAILURE);
   }
   lsu3::MapRankToColRow_MFDn_Compatible(ndiag, my_rank, idiag, jdiag);

   CWig9lmLookUpTable<RME::DOUBLE>::AllocateMemory(my_rank == 0);

   std::string hilbert_space_definition_file_name(argv[1]);
   std::string hamiltonian_file_name(argv[2]);

   // calculate matrix:
   lsu3::VBC_Matrix vbc;
   vbc.vals.reserve(nnz_reserve);
   //	these two variable are needed for MFDn
   uintmax_t local_nnz;
   uint64_t dim;
   int64_t nstates_sofar = 0;

   // bases need to be visible below
   lsu3::CncsmSU3xSU2Basis ket, bra;
   // run parameters as well
   CRunParameters run_params;

   local_nnz = lsu3::CalculateME(
         hilbert_space_definition_file_name, hamiltonian_file_name,
         ndiag, idiag, jdiag, vbc, nstates_sofar, dim, bra, ket, run_params, false);

   uintmax_t local_vbc_size = vbc.memory_usage();
   uintmax_t total_vbc_size, maxlocmatsize;
   uintmax_t local_vbc_nnz = vbc.vals.size();
   uintmax_t total_vbc_nnz, total_nnz;
   uintmax_t max_vbc_nnz = 0;
   boost::mpi::reduce(mpi_comm_world, local_vbc_size, total_vbc_size, std::plus<uintmax_t>(), 0);
   boost::mpi::reduce(mpi_comm_world, local_vbc_size, maxlocmatsize,
                      boost::mpi::maximum<uintmax_t>(), 0);
   boost::mpi::reduce(mpi_comm_world, local_nnz, total_nnz, std::plus<uintmax_t>(), 0);
   boost::mpi::reduce(mpi_comm_world, local_vbc_nnz, total_vbc_nnz, std::plus<uintmax_t>(), 0);
   boost::mpi::reduce(mpi_comm_world, local_vbc_nnz, max_vbc_nnz,
                      boost::mpi::maximum<uintmax_t>(), 0);

   size_t num_frequent_u9, frequent_u9_size, max_num_frequent_u9;
   size_t num_u9, u9_size, max_num_u9;
   size_t num_u6, u6_size, max_num_u6;
   size_t num_z6, z6_size, max_num_z6;
   CWig9lmLookUpTable<RME::DOUBLE>::coeffs_info(num_frequent_u9, num_u9, num_u6, num_z6);
   boost::mpi::reduce(mpi_comm_world, num_frequent_u9, max_num_frequent_u9,
                      boost::mpi::maximum<size_t>(), 0);
   boost::mpi::reduce(mpi_comm_world, num_u9, max_num_u9, boost::mpi::maximum<size_t>(), 0);
   boost::mpi::reduce(mpi_comm_world, num_u6, max_num_u6, boost::mpi::maximum<size_t>(), 0);
   boost::mpi::reduce(mpi_comm_world, num_z6, max_num_z6, boost::mpi::maximum<size_t>(), 0);

   CWig9lmLookUpTable<RME::DOUBLE>::memory_usage(frequent_u9_size, u9_size, u6_size, z6_size);

   size_t memory_usage_coeffs = frequent_u9_size + u9_size + u6_size + z6_size;
   size_t max_memory_usage_coeffs;
   boost::mpi::reduce(mpi_comm_world, memory_usage_coeffs, max_memory_usage_coeffs,
                      boost::mpi::maximum<size_t>(), 0);

   if (my_rank == 0) {
      // model space dimension: getModelSpaceDim()
      size_t total_csr_size =
          total_nnz * sizeof(float) + total_nnz * sizeof(int) + dim * sizeof(int);

      std::cout << "Total size of Hamiltonian matrix in VBC format: "
                << total_vbc_size / (1024.0 * 1024.0) << " MB." << std::endl;
      std::cout << "Total size of Hamiltonian matrix in CRS format: "
                << total_csr_size / (1024.0 * 1024.0) << " MB." << std::endl;
      std::cout << "Maximum local matrix block size: " << maxlocmatsize / (1024.0 * 1024.0)
                << " MB." << std::endl;
      std::cout << "Number of matrix elements stored in vbc:" << total_vbc_nnz << std::endl;
      std::cout << "Required value of NNZ: " << max_vbc_nnz << std::endl;
      std::cout << "Number of non vanishing matrix elements:" << total_nnz << std::endl;
      std::cout << "\n\nmax number of u9: " << max_num_u9 << std::endl;
      std::cout << "max number of u6: " << max_num_u6 << std::endl;
      std::cout << "max number of z6: " << max_num_z6 << std::endl;
      std::cout << "max number of frequent u9: " << max_num_frequent_u9 << std::endl;
      std::cout << "max amount of memory for SU(3) coeffs: "
                << max_memory_usage_coeffs / (1024.0 * 1024.0) << " MB.\n\n";
   }

   // release memory
   CWig9lmLookUpTable<RME::DOUBLE>::ReleaseMemory(); 
   CSSTensorRMELookUpTablesContainer::ReleaseMemory();
   CWigEckSU3SO3CGTablesLookUpContainer::ReleaseMemory();  

   mpi_comm_world.barrier();

//  The following are internal variables of MFDn that has to be set before calling splitprocs()
   FORTRAN_MODULE_VARIABLE(myrank) = FORTRAN_MODULE_VARIABLE(mypid) = my_rank;
   FORTRAN_MODULE_VARIABLE(nproc) = nprocs;
   FORTRAN_MODULE_VARIABLE(ndiag) = ndiag;

   splitprocs_();

   int neigen = atoi(argv[3]);
   int maxitr = atoi(argv[4]);
   float tol = 0.0f;

   // IMPORTANCE TRUNCATION:
   std::ofstream log;

   long n = (my_rank < ndiag) ? vbc.nrows : 0;
   MPI_Allreduce(MPI_IN_PLACE, &n, 1, MPI_LONG, MPI_SUM, MPI_COMM_WORLD);
   if (my_rank == 0) {
      std::cout << "Original matrix dimension: " << cyan << n << reset<< std::endl;
      log.open("it.log", std::ios::out); log << "Original matrix dimension: " << n << "\n"; log.close();
   }

   int initial_nmax = (argc > 9) ? atoi(argv[9]) : 2;

   lsu3::CImportanceTruncation it(bra, ket, vbc, run_params.hw(),
         initial_nmax, static_cast<MPI_Comm>(mpi_comm_world));

   vbc.release(); // is not needed any more; release memory

   // IT main loop:
   int niter = atoi(argv[5]);
   double eps = atof(argv[6]);
   double chi = atof(argv[7]);
   int nrefstates = (argc > 8) ? std::min(atoi(argv[8]), neigen) : neigen;

   for (int iter = 0; iter < niter; iter++) {
      if (my_rank == 0) {
         log.open("it.log", std::ios::app); log << "\n***** IT iterations #" << iter << ":\n\n"; log.close();
      }

      lsu3::COO_Matrix& coo = it.reduce();

      n = (my_rank < ndiag) ? coo.nrows : 0;
      MPI_Allreduce(MPI_IN_PLACE, &n, 1, MPI_LONG, MPI_SUM, MPI_COMM_WORLD);
      if (my_rank == 0) {
         std::cout << "Reduced matrix dimension: " << yellow << n << reset << std::endl;
         log.open("it.log", std::ios::app); log << "Reduced matrix dimension: " << n << "\n"; log.close();
      }

      // transform to fortran 1-based indexing:
      for (auto& row : coo.rows) row++;
      for (auto& col : coo.cols) col++;

      std::vector<float> eigenvals(neigen);
      std::vector<float> eigenvectors; // eigenvectors distributed among diagonal processes
      if (my_rank < ndiag) {
         assert (coo.nrows == coo.ncols);
         eigenvectors.resize(coo.nrows * neigen);
      }

      if (my_rank < ndiag) {
         assert (coo.irow == coo.icol);
         nstates_sofar = coo.irow;
      }

      matrix_diagonalize_coo_(
            &coo.nrows, &coo.ncols, &coo.nnz, // notice that MFDn works with lower triangular matrix
            coo.vals.data(), coo.rows.data(), coo.cols.data(),
            &maxitr, &tol, &neigen, eigenvals.data(), eigenvectors.data(),
            &nstates_sofar);

      it.store_complete_eigenvectors(eigenvectors, neigen, iter);

      if (my_rank == 0) {
         for (int i = 1; i <= neigen; i++) {
            std::stringstream iss, oss;

            iss << "eigenvector" << std::setw(2) << std::setfill('0') << i << ".dat";
            oss << "eigenvector_"
               << std::setw(3) << std::setfill('0') << iter << "_"
               << std::setw(2) << std::setfill('0') << i << ".dat";

            std::rename(iss.str().c_str(), oss.str().c_str());
         }
      }

      if (my_rank == 0) {
         log.open("it.log", std::ios::app); log << "Eigenvalues: ";
         log.setf(std::ios::fixed, std::ios::floatfield);
         for (int i = 0; i < neigen; i++) log << std::setw(12) << std::setprecision(6) << eigenvals[i] << " ";
         log << "\n"; log.close();
      }

      std::stringstream ss;
      ss << "it-states-" << std::setw(3) << std::setfill('0') << iter << ".dat";
      it.print(ss.str());

      it.multiply(eigenvectors, nrefstates);

      eigenvectors.clear();
      eigenvectors.shrink_to_fit();

      it.reset_bitmask(eps, nrefstates, iter);
      eps /= chi;
   }

   it.release(); // should be called before MPI_Finalize to release the duplicated communicator

   return EXIT_SUCCESS;
}
