/* 
 * File:   RMEMultiplicityGetter.h
 * Author: oberhuber
 *
 * Created on September 11, 2015, 10:55 PM
 */

#ifndef RMEMULTIPLICITYGETTER_H
#define	RMEMULTIPLICITYGETTER_H

#include <LSU3/ncsmSU3xSU2Basis.h>
#include <vector>

template< typename Index = int >
class RMEMultiplicityGetter
{
   public:
      RMEMultiplicityGetter( const CTensorStructure* tensorStructure,
                             const lsu3::CncsmSU3xSU2Basis *bra,
                             const lsu3::CncsmSU3xSU2Basis *ket )
      : tensorStructure( tensorStructure ),
        bra( bra ),
        ket( ket )
      {}
      
      Index getMultiplicity( const Index ip, const Index jp ) const
      {
         Index ipmult = this->bra->getMult_p(ip);
			Index jpmult = this->ket->getMult_p(jp);
         //	first obtain the total multiplicity of the tensor 
			Index tmult = 1;
			SU3xSU2_VEC labels;
			std::vector<char> structure;
			this->tensorStructure->GetStructure(structure, labels);
			for (size_t k = 0; k < labels.size(); ++k)
			{
			   tmult *= labels[k].rho;
			}
         //	get rho0t multiplicity ...
			Index rhotmax = SU3::mult( this->ket->getProtonSU3xSU2(jp), labels.back(), this->bra->getProtonSU3xSU2(ip));
         return ipmult*jpmult*tmult*rhotmax;
      }
      
      protected:
      
         const CTensorStructure* tensorStructure;
         
         const lsu3::CncsmSU3xSU2Basis *bra, *ket;
}; 

#endif	/* RMEMULTIPLICITYGETTER_H */

