$(eval $(begin-module))

################################################################
# unit definitions
################################################################

module_units_h :=  CSRMEMatrix
# module_units_cpp-h := 
# module_units_f := 
module_programs_cpp := ComputeMultiShellRMEs ReadRMEs

# module_programs_f :=
# module_generated :=

################################################################
# library creation flag
################################################################

# $(eval $(library))

################################################################
# special variable assignments, rules, and dependencies
################################################################

# TODO define dependencies

$(eval $(end-module))
