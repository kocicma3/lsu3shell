/* 
 * File:   CSRMEMatrix.h
 * Author: oberhuber
 *
 * Created on September 11, 2015, 5:54 PM
 */

#ifndef LSU3SHELL_CSRMEMATRIX_H
#define	LSU3SHELL_CSRMEMATRIX_H

#include <cassert>
#include <vector>

template <typename Real = float, typename Index = int>
class CSRMEMatrix
{
    public:
        using real_type = Real;
        using index_type = Index;
        using real_vector_type = std::vector<real_type>;
        using index_vector_type = std::vector<index_type>;

        CSRMEMatrix(const Index n_rows, const Index n_cols, const Index my_first_row, const Index my_last_row)
            : n_rows_(n_rows), n_cols_(n_cols), my_first_row_(my_first_row), my_last_row_(my_last_row),
              n_local_rows_(my_last_row - my_first_row + 1)
        {
            // assertions
            assert(n_rows_ > 0); assert(my_first_row_ < n_rows_);
            assert(n_cols_ > 0); assert(my_last_row_  < n_rows_);
            assert(my_first_row_ <= my_last_row_);

            // row pointers for local rows only; initialize with 0
            columnIndexesRowPointers_.resize(n_local_rows_ + 1, 0);
            valuesRowPointers_.resize(n_local_rows_ + 1, 0);
        }
      
        // avoid copying and moving (if not necessary)
        CSRMEMatrix(const CSRMEMatrix&) = delete;
        CSRMEMatrix& operator=(const CSRMEMatrix&) = delete;
        CSRMEMatrix(CSRMEMatrix&& matrix) = delete;
        CSRMEMatrix& operator=(CSRMEMatrix&&) = delete;

        // elements must be added in lexicographical order !!!
        void addRMEs(const Index row, const Index col, const Real* data, const Index count = 1)
        {
            // assertions
            assert(row >= 0); assert(row < n_rows_); assert(row >= my_first_row_);
            assert(col >= 0); assert(col < n_cols_); assert(row <= my_last_row_ );
            assert(data != nullptr);
            assert(count > 0);

            // column index
            columnIndexes_.push_back(col);

            // values
            for (Index i = 0; i < count; i++)
                values_.push_back(data[i]);

            // row pointers
            Index local_row = row - my_first_row_;
            columnIndexesRowPointers_[local_row + 1] = columnIndexes_.size();
            valuesRowPointers_[local_row + 1] = values_.size();
        }
      
        // must be called at the end
        void finalize()
        {
            for (Index row = 1; row <= n_local_rows_; row++) {
                if (columnIndexesRowPointers_[row] == 0)
                    columnIndexesRowPointers_[row] = columnIndexesRowPointers_[row - 1];
                if (valuesRowPointers_[row] == 0)
                    valuesRowPointers_[row] = valuesRowPointers_[row - 1];
            }            

            assert(columnIndexesRowPointers_[n_local_rows_] == columnIndexes_.size());
            assert(valuesRowPointers_[n_local_rows_] == values_.size());
        }

        void shift_values_rowptrs(Index shift)
        {
            for (auto i = valuesRowPointers_.begin(), e = valuesRowPointers_.end(); i != e; ++i)
                *i += shift;
        }

        void shift_colinds_rowptrs(Index shift)
        {
            for (auto i = columnIndexesRowPointers_.begin(), e = columnIndexesRowPointers_.end(); i != e; ++i)
                *i += shift;
        }

        Index get_n_rows() const { return n_rows_; }
        Index get_n_cols() const { return n_cols_; }

        const real_vector_type& get_values() const { return values_; }
        const index_vector_type& get_colinds() const { return columnIndexes_; }
        const index_vector_type& get_values_rowptrs() const { return valuesRowPointers_; }
        const index_vector_type& get_colinds_rowptrs() const { return columnIndexesRowPointers_; }
      
    private:
         Index n_rows_, n_cols_; // global matrix size
         Index my_first_row_, my_last_row_, n_local_rows_; // my row band
         index_vector_type columnIndexesRowPointers_, valuesRowPointers_, columnIndexes_;
         real_vector_type values_;
};

#endif // LSU3SHELL_CSRMEMATRIX_H
