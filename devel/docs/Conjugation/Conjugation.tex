\documentclass{report}
\usepackage{algorithmic}
\usepackage{amsmath,amssymb,graphicx,color,epsfig}
\textheight     240.0mm
\textwidth      200.0mm 
\topmargin        -20.0mm
\oddsidemargin    -20.0mm
\evensidemargin    -20.0mm
\parindent        0.0mm

\DeclareMathOperator{\Tr}{Tr}
\newcommand{\vect}[1]{\mathbf{#1}}
\newcommand{\vgamma}[0]{\boldsymbol{\gamma}}
\newcommand{\vomega}[0]{\boldsymbol{\omega}}

%
% group notation
%
\newcommand{\SU}[1]{\ensuremath{\mathrm{SU}( #1 )}}
\newcommand{\Un}[1]{\ensuremath{\mathrm{U}( #1 )}}
\newcommand{\SO}[1]{\ensuremath{\mathrm{SO}( #1 )}}
\newcommand{\On}[1]{\ensuremath{\mathrm{O}( #1 )}}
\newcommand{\Spn}[1]{\ensuremath{\mathrm{Sp}( #1 )}}
\newcommand{\SpR}[1]{\ensuremath{\mathrm{Sp}( #1,\mathbb{R} )}}

%
% algebra notation
%

\newcommand{\IR}[1]{\ensuremath{(\lambda_{#1}\,\mu_{#1})}}
\newcommand{\su}[1]{\ensuremath{\mathfrak{su}( #1 )}}
\newcommand{\un}[1]{\ensuremath{\mathfrak{u}( #1 )}}
\newcommand{\so}[1]{\ensuremath{\mathfrak{so}( #1 )}}
\newcommand{\on}[1]{\ensuremath{\mathfrak{o}( #1 )}}
\newcommand{\spn}[1]{\ensuremath{\mathfrak{sp}( #1 )}}
\newcommand{\spR}[1]{\ensuremath{\mathfrak{sp}( #1, \mathbb{R} )}}

%
% boson and fermion
%
\newcommand{\bdag}{\ensuremath{ {b^\dagger} }}
\newcommand{\adagg}{\ensuremath{ {a^\dagger} }} % extra space outside the dagger
\newcommand{\adag}{\ensuremath{ a^\dagger }}
\newcommand{\B}{\ensuremath{\bullet}}
\newcommand{\C}{\ensuremath{\circ}}

%
% CG and Racah
%
\newcommand{\NonCG}[3]{\ensuremath{\{#1;#2|\,#3\}}}
\newcommand{\CG}[3]{\ensuremath{\langle#1;#2|\,#3\rangle}}
\newcommand{\NonRedCG}[3]{\ensuremath{\{#1;#2\| \,#3\}}}
\newcommand{\RedCG}[3]{\ensuremath{\langle#1;#2\|#3\rangle}}
\newcommand{\Wigsixj}[6]{\ensuremath{\left\{\begin{matrix}#1 & #2 & #3\cr
#4 & #5 & #6 \end{matrix}\right\}}}
\newcommand{\Wigninej}[9]{\ensuremath{\left\{\begin{matrix}#1 & #2 & #3\cr
#4 & #5 & #6 \cr #7 & #8 & #9 \end{matrix}\right\}}}
\newcommand{\Unininej}[9]{\ensuremath{\left[\begin{matrix}#1 & #2 & #3\cr
#4 & #5 & #6 \cr #7 & #8 & #9 \end{matrix}\right]}}
\newcommand{\ket}[1]{\ensuremath{\left| #1 \right\rangle}}
\newcommand{\bra}[1]{\ensuremath{\left\langle #1 \right|}}
\newcommand{\braket}[2]{\ensuremath{\left\langle #1 | #2 \right\rangle}}
\newcommand{\braketop}[3]{\ensuremath{\left\langle #1 | #2 | #3 \right\rangle}}
\newcommand{\ME}[3]{\ensuremath{\langle #1 | #2 | #3 \rangle}}
\newcommand{\RedME}[3]{\ensuremath{\langle #1 \| #2 \| #3 \rangle}}
\newcommand{\TRME}[3]{\ensuremath{\langle #1 ||| #2 ||| #3 \rangle}}
\newcommand{\roundket}[1]{\ensuremath{\left| #1 \right)}}

%
% Latin abbr.
%
\newcommand{\ie}{\emph{i.e.}}
\newcommand{\eg}{\emph{e.g.}}
\newcommand{\etal}{\emph{et al.}}
\newcommand{\etc}{\emph{etc.}}

%
% Miscellany
%
\newcommand{\half}{\ensuremath{\frac{1}{2}}}
\newcommand{\threehalves}{\ensuremath{\textstyle{\frac{3}{2}}}}

\newcommand{\one}{\ensuremath{\mathit{1}}}
\newcommand{\two}{\ensuremath{\mathit{2}}}

\newcommand{\betb}{\begin{tabular}{p{4.0cm}p{9.0cm}}}
\newcommand{\entb}{\end{tabular}}
\newcommand{\MPC}[2]{\begin{minipage}[t]{#1\textwidth}
  \begin{center}~#2\end{center}\end{minipage}}
\newcommand{\MPL}[2]{~\begin{minipage}[t]{#1\textwidth}#2\end{minipage}}

\newcommand{\matrixindex}[2]{\ensuremath{\left(\mathcal{#1} #2 \right)}}

\newcommand{\bsigma}{\boldsymbol{\sigma}}
\newcommand{\bn}{\boldsymbol{n}}
\newcommand{\bomega}{\boldsymbol{\omega}}
\newcommand{\ba}{\ensuremath{\boldsymbol{a}}}
\newcommand{\adagger}{\ensuremath{a^\dagger}}
\newcommand{\badagger}{\ensuremath{\boldsymbol{a}^\dagger}}

% normalization coefficient for SU(3) 2-fermion state
\newcommand{\N}[1]{\ensuremath{\mathcal{N}_{#1} }}

\newcommand{\LS}[1]{\left(#1\half\right)}
\newcommand{\irrep}[2]{\left(#1 #2\right)}

\newcommand{\can}[1]{\ensuremath{\epsilo\eta_{#1}\Lambda_{#1}M_{\Lambda_{#1}}}}

%
% color scheme
%
\newcommand{\red}[1]{{ \color{red} #1 }}
\newcommand{\green}[1]{{ \color{green} #1 }}
\newcommand{\blue}[1]{{ \color{blue} #1 }}
\newcommand{\magenta}[1]{{ \color{magenta} #1 }}

\begin{document}
{\bf Goal}: Derive relations between proton-neutron \texttt{LSU3shell} compatible tensors and their conjugate counterparts.
\texttt{LSU3shell} compatible tensor has each pair of creation and annihilation operators for protons and neutrons ordered by HO shell.
In general, one has to consider all four possible types:
\begin{eqnarray}
\left\{a^{\dagger(\eta_{1}\,0)} \times \tilde{a}^{(0\,\eta_{2})}\right\}\times \left\{a^{\dagger(\eta_{3}\,0)} \times \tilde{a}^{(0\,\eta_{4})}\right\} \label{eq:type1} \\
\left\{a^{\dagger(\eta_{1}\,0)} \times \tilde{a}^{(0\,\eta_{2})}\right\}\times \left\{\tilde{a}^{(0\,\eta_{3})} \times a^{\dagger(\eta_{4}\,0)}\right\} \label{eq:type2}\\
\left\{\tilde{a}^{(0\,\eta_{1})} \times a^{\dagger(\eta_{2}\,0)}\right\}\times \left\{\tilde{a}^{(0\,\eta_{3})} \times a^{\dagger(\eta_{4}\,0)}\right\} \label{eq:type3}\\
\left\{\tilde{a}^{(0\,\eta_{1})} \times a^{\dagger(\eta_{2}\,0)}\right\}\times \left\{a^{\dagger(\eta_{3}\,0)} \times \tilde{a}^{(0\,\eta_{4})}\right\} \label{eq:type4}
\end{eqnarray}

Annihilation and creation operators and their conjugates when expressed as \SU{3} tensors:
\begin{eqnarray}
a_{nlm\half\sigma}&=&(-)^{n+l-m+\half-\sigma}\tilde{a}^{(0\,n)}_{l-m\half-\sigma} \\
\tilde{a}^{(0\,n)}_{lm\half\sigma}&=&(-)^{n+l+m+\half+\sigma}a_{nl-m\half-\sigma} \\
\left(\tilde{a}^{(0\,n)}_{l m \half \sigma}\right)^{\dagger}&=&(-)^{n+l+m+\half+\sigma}a^{\dagger (n\,0)}_{l-m\half-\sigma} \\
\left(a^{\dagger (n\,0)}_{l m \half \sigma}\right)^{\dagger}&=&(-)^{n+l-m+\half-\sigma}\tilde{a}^{(0\,n)}_{l-m\half-\sigma} \\
\end{eqnarray}

To derive the first couple of relations we will use the following formulas:
\begin{eqnarray}
C_{a\alpha b\beta}^{c\gamma}&=&C_{b-\beta a-\alpha}^{c-\gamma} \\
\CG{\omega_{1}\alpha_{1}}{\omega_{2}\alpha_{2}}{\omega_{0}\alpha_{0}}&=&(-)^{l_{1}+l_{2}-L_{0}} \CG{\tilde{\omega}_{2}\tilde{\alpha}_{2}}{\tilde{\omega}_{1}\tilde{\alpha}_{1}}{\tilde{\omega}_{0}\tilde{\alpha}_{0}},
\end{eqnarray}
where $\omega_{1}=(\eta_{1}\,0)$ \& $\omega_{2}=(0\,\eta_{2})$ or $\omega_{1}=(0\,\eta_{1})$ \& $\omega_{2}=(\eta_{2}\,0)$. Note that conjugate of irrep $\omega=(\lambda\,\mu)$ is $\tilde{\omega}=(\mu\,\lambda)$.
Further, $\alpha\equiv \kappa L M$ and $\tilde{\alpha}=\kappa L -M$.
Also it is important to realize that 
\begin{equation}
(-)^{{1+\sigma_{2}-\sigma_{1}}}=(-)^{\sigma_{1}+\sigma_{2}},
\end{equation}
where $\sigma_{i}=\pm\half$.
Even more evident is the fact that for any two integers, e.g. $s$ and $t$,
\begin{equation}
(-)^{s-t}=(-)^{s+t}.
\end{equation}

\begin{multline}
\left(\left\{a^{\dagger(\eta_{1}\,0)}_{\half}\times \tilde{a}^{(0\,\eta_{2})}_{\half} \right\}^{(\lambda\,\mu)S \Sigma}_{\kappa L M}\right)^{\dagger}
=
\sum_{
	\substack{
\sigma_{1}+\sigma_{2}=\Sigma \\
l_{1} l_{2} \\
m_{1}+m_{2}=M \\ 
}} 
C_{\half \sigma_{1}\half \sigma_{2}}^{S\Sigma}
\CG{(\eta_{1}\,0)l_{1}m_{1}}{(0\,\eta_{2})l_{2}m_{2}}{(\lambda\,\mu)\kappa L M}
\left(a^{\dagger(\eta_{1}\,0)}_{l_{1}m_{1}\half\sigma}\tilde{a}^{(0\,\eta_{2})}_{l_{2}m_{2}\half\sigma_{2}} \right)^{\dagger} = \\
\sum_{
	\substack{
\sigma_{1}+\sigma_{2} \\
l_{1}, l_{2} \\
m_{1},m_{2} \\ 
}} 
C_{\half -\sigma_{2}\half -\sigma_{1}}^{S-\Sigma}
(-)^{l_{1}+l_{2}-L}
\CG{(\eta_{2}\,0)l_{2}-m_{2}}{(0\,\eta_{1})l_{1}-m_{1}}{(\mu\,\lambda)\kappa L -M}
(-)^{\eta_{2}+l_{2}+m_{2}+\half+\sigma_{2}}a^{\dagger (\eta_{2}\,0)}_{l_{2}-m_{2}\half-\sigma_{2}}
(-)^{\eta_{1}+l_{1}-m_{1}+\half-\sigma_{1}}\tilde{a}^{(0\,\eta_{1})}_{l_{1}-m_{1}\half-\sigma_{1}}
\\
=
(-)^{\eta_{1}+\eta_{2}-L+M+\Sigma}\left\{ a^{\dagger(\eta_{2}\,0)}_{\half}\times \tilde{a}^{(0\,\eta_{1})}_{\half}\right\}^{(\mu\,\lambda)S -\Sigma}_{\kappa L -M}
\label{eq:adta_dagger}
\end{multline}
Similarly, it is easy to derive the second formula:
\begin{equation}
\left(\left\{\tilde{a}^{(0\,\eta_{1})}_{\half}\times a^{\dagger(\eta_{2}\,0)}_{\half} \right\}^{(\lambda\,\mu)S \Sigma}_{\kappa L M}\right)^{\dagger}
=
(-)^{\eta_{1}+\eta_{2}-L+M+\Sigma}\left\{\tilde{a}^{(0\,\eta_{2})}_{\half}\times a^{\dagger(\eta_{1}\,0)}_{\half} \right\}^{(\mu\,\lambda)S -\Sigma}_{\kappa L -M}.
\end{equation}

\begin{multline}
\left(\left[
	\left\{a^{\dagger(\eta_{1}\,0)}_{\half} \times \tilde{a}^{(0\,\eta_{2})}_{\half} \right\}^{\omega_{p}}_{S_{p}} 
		\times 
	\left\{ a^{\dagger(\eta_{3}\,0)}_{\half} \times \tilde{a}^{(0\,\eta_{4})}_{\half}\right\}^{\omega_{n}}_{S_{n}} 
\right]^{\rho_{0}\omega_{0}S_{0}\Sigma_{0}}_{\alpha_{0}} \right)^{\dagger} \\
=
\sum_{
	\substack{
\Sigma_{p},\Sigma_{n} \\
\alpha_{p}, \alpha_{n} 
	} } 
	C_{S_{p}\Sigma_{p}S_{n}\Sigma_{n}}^{S_{0}\Sigma_{0}}
	\CG{\omega_{p}\alpha_{p}}{\omega_{n}\alpha_{n}}{\omega_{0}\alpha_{0}}_{\rho_{0}}
\left(
	\left\{a^{\dagger(\eta_{3}\,0)}_{\half} \times \tilde{a}^{(0\,\eta_{4})}_{\half}\right\}^{\omega_{n}S_{n}\Sigma_{n}}_{\alpha_{n}} 
\right)^{\dagger} 
\left(
	\left\{a^{\dagger(\eta_{1}\,0)}_{\half} \times \tilde{a}^{(0\,\eta_{2})}_{\half} \right\}^{\omega_{p}S_{p}\Sigma_{p}}_{\alpha_{p}} 
\right)^{\dagger}
\\
=
\sum_{
	\substack{
\Sigma_{p},\Sigma_{n} \\
\alpha_{p}, \alpha_{n} 
	} } 
	C_{S_{p}\Sigma_{p}S_{n}\Sigma_{n}}^{S_{0}\Sigma_{0}}
	\CG{\omega_{p}\alpha_{p}}{\omega_{n}\alpha_{n}}{\omega_{0}\alpha_{0}}_{\rho_{0}}
(-)^{\eta_{3}+\eta_{4}-L_{n}+M_{n}+\Sigma_{n}+\eta_{1}+\eta_{2}-L_{p}+M_{p}+\Sigma_{p}}\left\{ a^{\dagger(\eta_{4}\,0)}_{\half}\times \tilde{a}^{(0\,\eta_{3})}_{\half}\right\}^{\tilde{\omega}_{n}S_{n} -\Sigma_{n}}_{\tilde{\alpha}_{n}}
\left\{ a^{\dagger(\eta_{2}\,0)}_{\half}\times \tilde{a}^{(0\,\eta_{1})}_{\half}\right\}^{\tilde{\omega}_{p}S_{p} -\Sigma_{p}}_{\tilde{\alpha}_{p}}
\end{multline}

Due to the anticommutation of proton and neutron creation/annihilation operators, i.e.
\begin{displaymath}
\left\{a^{\dagger}_{p}, a^{\dagger}_{n}\right\} =
\left\{a^{\dagger}_{p}, \tilde{a}_{n}\right\} =
\left\{\tilde{a}_{p}, a^{\dagger}_{n}\right\} = 
\left\{\tilde{a}_{p}, \tilde{a}_{n}\right\} = 0,
\end{displaymath}
we can switch the order of proton and neutron tensors. Note that there is not additional phase as we are performing two interchanges:
\begin{equation}
\left\{ a^{\dagger(\eta_{4}\,0)}_{\half}\times \tilde{a}^{(0\,\eta_{3})}_{\half}\right\}^{\tilde{\omega}_{n}S_{n} -\Sigma_{n}}_{\tilde{\alpha}_{n}}
\left\{ a^{\dagger(\eta_{2}\,0)}_{\half}\times \tilde{a}^{(0\,\eta_{1})}_{\half}\right\}^{\tilde{\omega}_{p}S_{p} -\Sigma_{p}}_{\tilde{\alpha}_{p}}
=
\left\{ a^{\dagger(\eta_{2}\,0)}_{\half}\times \tilde{a}^{(0\,\eta_{1})}_{\half}\right\}^{\tilde{\omega}_{p}S_{p} -\Sigma_{p}}_{\tilde{\alpha}_{p}}
\left\{ a^{\dagger(\eta_{4}\,0)}_{\half}\times \tilde{a}^{(0\,\eta_{3})}_{\half}\right\}^{\tilde{\omega}_{n}S_{n} -\Sigma_{n}}_{\tilde{\alpha}_{n}}
\end{equation}

I will also use the following relations:
\begin{eqnarray}
M_{p}+M_{n}&=&M_{0} \\
\Sigma_{p} + \Sigma_{n}&=&\Sigma_{0} \\
C_{S_{p}\Sigma_{p}S_{n}\Sigma_{n}}^{S_{0}\Sigma_{0}} &=&(-)^{S_{p}+S_{n}-S_{0}} C_{S_{p}-\Sigma_{p}S_{n}-\Sigma_{n}}^{S_{0}-\Sigma_{0}} \\
\CG{\omega_{p}\alpha_{p}}{\omega_{n}\alpha_{n}}{\omega_{0}\alpha_{0}}_{\rho_{0}} &=&(-)^{\omega_p + \omega_n - \omega_0 + \rho_{0}^{\max} - \rho_{0} + L_{p} + L_{n} - L_{0}}
\CG{\tilde{\omega}_{p}\tilde{\alpha}_{p}}{\tilde{\omega}_{n}\tilde{\alpha}_{n}}{\tilde{\omega}_{0}\tilde{\alpha}_{0}}_{\rho_{0}}
\end{eqnarray}
We substitute and get
\begin{multline}
=(-)^{\eta_{1}+\eta_{2}+\eta_{3}+\eta_{4}+\omega_p + \omega_n - \omega_0 + S_{p}+S_{n}-S_{0} + \rho_{0}^{\max} - \rho_{0} - L_{0}+M_{0}+\Sigma_{0}}
\sum_{
	\substack{
\Sigma_{p},\Sigma_{n} \\
\alpha_{p},\alpha_{n}
	} } 
C_{S_{p}-\Sigma_{p}S_{n}-\Sigma_{n}}^{S_{0}-\Sigma_{0}}
\CG{\tilde{\omega}_{p}\tilde{\alpha}_{p}}{\tilde{\omega}_{n}\tilde{\alpha}_{n}}{\tilde{\omega}_{0}\tilde{\alpha}_{0}}_{\rho_{0}}
\\
\times
\left\{ a^{\dagger(\eta_{2}\,0)}_{\half}\times \tilde{a}^{(0\,\eta_{1})}_{\half}\right\}^{\tilde{\omega}_{p}S_{p} -\Sigma_{p}}_{\tilde{\alpha}_{p}}
\left\{ a^{\dagger(\eta_{4}\,0)}_{\half}\times \tilde{a}^{(0\,\eta_{3})}_{\half}\right\}^{\tilde{\omega}_{n}S_{n} -\Sigma_{n}}_{\tilde{\alpha}_{n}}
\end{multline}

Realistic interaction has $\lambda_{0} + \mu_{0}$ equals to an even number. Therefore, we can drop $(-)^{-\omega_{0}}$ part of phase.
We get the final relation:
\begin{multline}
\left(\left[
	\left\{a^{\dagger(\eta_{1}\,0)}_{\half} \times \tilde{a}^{(0\,\eta_{2})}_{\half} \right\}^{\omega_{p}}_{S_{p}} 
		\times 
	\left\{ a^{\dagger(\eta_{3}\,0)}_{\half} \times \tilde{a}^{(0\,\eta_{4})}_{\half}\right\}^{\omega_{n}}_{S_{n}} 
\right]^{\rho_{0}\omega_{0}S_{0}\Sigma_{0}}_{\alpha_{0}} \right)^{\dagger} 
= \\
(-)^{\eta_{1}+\eta_{2}+\eta_{3}+\eta_{4}+\omega_p + \omega_n + S_{p}+S_{n}-S_{0}+\rho_{0}^{\max} - \rho_{0} - L_{0} +M_{0}+\Sigma_{0}}
\left[
\left\{ a^{\dagger(\eta_{2}\,0)}_{\half}\times \tilde{a}^{(0\,\eta_{1})}_{\half}\right\}^{\tilde{\omega}_{p}}_{S_{p}}
		\times 
\left\{ a^{\dagger(\eta_{4}\,0)}_{\half}\times \tilde{a}^{(0\,\eta_{3})}_{\half}\right\}^{\tilde{\omega}_{n}}_{S_{n}}
\right]^{\rho_{0}\tilde{\omega}_{0}S_{0}-\Sigma_{0}}_{\tilde{\alpha}_{0}}
\end{multline}
It is important to note that resulting phase obtained due to conjugation is identical for all types of \texttt{LSU3shell} compatible proton-neutron tensors~(\ref{eq:type1})-(\ref{eq:type4}).
\medskip

\noindent
Since our operator is a scalar, i.e. $J_{0}=M_{0}=0$, we have to derive relation for conjugation of a scalar operator.
Scalar operator can be expressed as
\begin{equation}
T^{S_{0}}_{L_{0}=S_{0}J_{0}=0M_{0}=0}=\sum_{\mu=-S_{0}}^{S_{0}}C_{S_{0}\mu S_{0}-\mu}^{00}T^{S_{0}\mu}_{L_{0}-\mu}=\sum_{\mu=-S_{0}}^{S_{0}}C_{S_{0}-\mu S_{0}\mu}^{00}T^{S_{0}\mu}_{L_{0}-\mu}
\end{equation}
\begin{multline}
\left(\left[
	\left\{a^{\dagger(\eta_{1}\,0)}_{\half} \times \tilde{a}^{(0\,\eta_{2})}_{\half} \right\}^{\omega_{p}}_{S_{p}} 
		\times 
	\left\{ a^{\dagger(\eta_{3}\,0)}_{\half} \times \tilde{a}^{(0\,\eta_{4})}_{\half}\right\}^{\omega_{n}}_{S_{n}} 
\right]^{\rho_{0}\omega_{0}S_{0}}_{\kappa_{0}L_{0}=S_{0} J_{0}=0M_{0}=0} \right)^{\dagger} 
= \\
\sum_{\mu} 
C_{S_{0}\mu S_{0}-\mu}^{00}
\left(\left[
	\left\{a^{\dagger(\eta_{1}\,0)}_{\half} \times \tilde{a}^{(0\,\eta_{2})}_{\half} \right\}^{\omega_{p}}_{S_{p}} 
		\times 
	\left\{ a^{\dagger(\eta_{3}\,0)}_{\half} \times \tilde{a}^{(0\,\eta_{4})}_{\half}\right\}^{\omega_{n}}_{S_{n}} 
\right]^{\rho_{0}\omega_{0}S_{0}\mu}_{\kappa_{0}L_{0}=S_{0} -\mu} \right)^{\dagger} 
= \\
\sum_{\mu} 
C_{S_{0}\mu S_{0}-\mu}^{00}
(-)^{\eta_{1}+\eta_{2}+\eta_{3}+\eta_{4}+\omega_p + \omega_n + S_{p}+S_{n}-S_{0}+\rho_{0}^{\max} - \rho_{0} - L_{0}+(-\mu)+\mu}
\left[
\left\{ a^{\dagger(\eta_{2}\,0)}_{\half}\times \tilde{a}^{(0\,\eta_{1})}_{\half}\right\}^{\tilde{\omega}_{p}}_{S_{p}}
		\times 
\left\{ a^{\dagger(\eta_{4}\,0)}_{\half}\times \tilde{a}^{(0\,\eta_{3})}_{\half}\right\}^{\tilde{\omega}_{n}}_{S_{n}}
\right]^{\rho_{0}\tilde{\omega}_{0}S_{0}-\mu}_{\kappa_{0} L_{0}=S_{0} \mu}
=\\
=
(-)^{\eta_{1}+\eta_{2}+\eta_{3}+\eta_{4}+\omega_p + \omega_n + S_{p}+S_{n}-S_{0}+\rho_{0}^{\max} - \rho_{0} - L_{0}}
\sum_{\mu} 
C_{S_{0}\mu S_{0}-\mu}^{00}
\left[
\left\{ a^{\dagger(\eta_{2}\,0)}_{\half}\times \tilde{a}^{(0\,\eta_{1})}_{\half}\right\}^{\tilde{\omega}_{p}}_{S_{p}}
		\times 
\left\{ a^{\dagger(\eta_{4}\,0)}_{\half}\times \tilde{a}^{(0\,\eta_{3})}_{\half}\right\}^{\tilde{\omega}_{n}}_{S_{n}}
\right]^{\rho_{0}\tilde{\omega}_{0}S_{0}-\mu}_{\kappa_{0} L_{0}=S_{0} \mu}
=\\
=
(-)^{\eta_{1}+\eta_{2}+\eta_{3}+\eta_{4}+\omega_p + \omega_n + S_{p}+S_{n}+\rho_{0}^{\max} - \rho_{0}}
\left[
\left\{ a^{\dagger(\eta_{2}\,0)}_{\half}\times \tilde{a}^{(0\,\eta_{1})}_{\half}\right\}^{\tilde{\omega}_{p}}_{S_{p}}
		\times 
\left\{ a^{\dagger(\eta_{4}\,0)}_{\half}\times \tilde{a}^{(0\,\eta_{3})}_{\half}\right\}^{\tilde{\omega}_{n}}_{S_{n}}
\right]^{\rho_{0}\tilde{\omega}_{0}S_{0}}_{\kappa_{0} L_{0}=S_{0} J_{0}=0 M_{0}=0}
\end{multline}

Since \texttt{LSU3shell} requires that operators are provided with increasing shells we need to perform additional transformation
in case $\eta_{2} > \eta_{1}$ and/or $\eta_{4} > \eta_{1}$.
There are three scenarios:
\begin{enumerate}
\item $\eta_{1}<\eta_{2}$ \& $\eta_{3}<\eta_{4} \longrightarrow$ we interchange order of proton operators and neutron operators
and multiply by the additional phase $(-)(-)^{\eta_{1}+\eta_{2}-\tilde{\omega}_{p}+1-S_{p}}(-)(-)^{\eta_{3}+\eta_{4}-\tilde{\omega}_{n}+1-S_{n}}$
and hence we get:
\begin{multline}
\left(\left[
	\left\{a^{\dagger(\eta_{1}\,0)}_{\half} \times \tilde{a}^{(0\,\eta_{2})}_{\half} \right\}^{\omega_{p}}_{S_{p}} 
		\times 
	\left\{ a^{\dagger(\eta_{3}\,0)}_{\half} \times \tilde{a}^{(0\,\eta_{4})}_{\half}\right\}^{\omega_{n}}_{S_{n}} 
\right]^{\rho_{0}\omega_{0}S_{0}}_{\kappa_{0}L_{0}=S_{0} J_{0}=0 M_{0}=0}\right)^{\dagger} 
= \\
(-)^{\rho_{0}^{\max} - \rho_{0}}
\left[
\left\{ \tilde{a}^{(0\,\eta_{1})}_{\half}\times a^{\dagger(\eta_{2}\,0)}_{\half}\right\}^{\tilde{\omega}_{p}}_{S_{p}}
		\times 
\left\{ \tilde{a}^{(\eta_{3}\,0)}_{\half}\times a^{\dagger (0\,\eta_{4})}_{\half}\right\}^{\tilde{\omega}_{n}}_{S_{n}}
\right]^{\rho_{0}\tilde{\omega}_{0}S_{0}}_{\kappa_{0}L_{0}=S_{0}J_{0}=0M_{0}=0}
\label{eq:pn1}
\end{multline}
\item $\eta_{1}<\eta_{2}$ \& $\eta_{3}==\eta_{4} \longrightarrow$ we interchange proton operators and 
multiply by the additional phase $(-)(-)^{\eta_{1}+\eta_{2}-\tilde{\omega}_{p}+1-S_{p}}$.
Furthemore, coupling $(n\,0)\times (0\,n) \rightarrow (\lambda\,\lambda)$ and
hence we can drop $(-)^{\omega_n}$:
\begin{multline}
\left(\left[
	\left\{a^{\dagger(\eta_{1}\,0)}_{\half} \times \tilde{a}^{(0\,\eta_{2})}_{\half} \right\}^{\omega_{p}}_{S_{p}} 
		\times 
	\left\{ a^{\dagger(n\,0)}_{\half} \times \tilde{a}^{(0\,n)}_{\half}\right\}^{\omega_{n}}_{S_{n}} 
\right]^{\rho_{0}\omega_{0}S_{0}}_{\kappa_{0}L_{0}=S_{0} J_{0}=0 M_{0}=0}\right)^{\dagger} 
= \\
(-)^{S_{n}+\rho_{0}^{\max} - \rho_{0}}
\left[
\left\{ \tilde{a}^{(0\,\eta_{1})}_{\half}\times a^{\dagger(\eta_{2}\,0)}_{\half}\right\}^{\tilde{\omega}_{p}}_{S_{p}}
		\times 
\left\{ a^{\dagger(n\,0)}_{\half}\times \tilde{a}^{(0\,n)}_{\half}\right\}^{\tilde{\omega}_{n}}_{S_{n}}
\right]^{\rho_{0}\tilde{\omega}_{0}S_{0}}_{\kappa_{0}L_{0}=S_{0}J_{0}=0M_{0}=0}
\label{eq:pn2}
\end{multline}
\item $\eta_{1}==\eta_{2}$ \& $\eta_{3}>\eta_{4} \longrightarrow$ we switch order of $a^{\dagger (\eta_{4}\,0)}$ and $\tilde{a}^{(0\,\eta_{3})}_{\half}$
and multiply by the phase $(-)(-)^{\eta_{3}+\eta_{4}-\tilde{\omega}_{n}+1-S_{n}}$. Furthemore, coupling $(n\,0)\times (0\,n) \rightarrow (\lambda\,\lambda)$ and
hence we can drop $(-)^{\omega_p}$.
\begin{multline}
\left(\left[
	\left\{a^{\dagger(n\,0)}_{\half} \times \tilde{a}^{(0\,n)}_{\half} \right\}^{\omega_{p}}_{S_{p}} 
		\times 
	\left\{ a^{\dagger(\eta_{3}\,0)}_{\half} \times \tilde{a}^{(0\,\eta_{4})}_{\half}\right\}^{\omega_{n}}_{S_{n}} 
\right]^{\rho_{0}\omega_{0}S_{0}}_{\kappa_{0}L_{0}=S_{0} J_{0}=0 M_{0}=0}\right)^{\dagger} 
= \\
(-)^{S_{p}+\rho_{0}^{\max} - \rho_{0}}
\left[
\left\{ \tilde{a}^{(0\,n)}_{\half}\times a^{\dagger(n\,0)}_{\half}\right\}^{\tilde{\omega}_{p}}_{S_{p}}
		\times 
\left\{ \tilde{a}^{(\eta_{3}\,0)}_{\half}\times a^{\dagger (0\,\eta_{4})}_{\half}\right\}^{\tilde{\omega}_{n}}_{S_{n}}
\right]^{\rho_{0}\tilde{\omega}_{0}S_{0}}_{\kappa_{0}L_{0}=S_{0}J_{0}=0M_{0}=0}
\label{eq:pn3}
\end{multline}
\end{enumerate}
Note that the resulting phase does not depend on type of \texttt{LSU3shell} compatible proton-neutron tensor.

\newpage
{\bf Goal}: Derive relations between strenghts of a two-body tensor 
and its conjugate counterpart,
$\alpha^{\vec{\eta}\omega_1 \omega_2 \rho_0\omega_0}_{S_1 S_2\kappa_{0} L_{0}=S_{0}00}$
and 
$\alpha^{\vec{\tilde{\eta}}\tilde{\omega}_2 \tilde{\omega}_1 \rho_0\tilde{\omega}_0}_{S_2 S_1\kappa_{0} L_{0}=S_{0}00}$,
respectively, which are associated with tensor
\begin{equation}
\left[\left\{a^{\dagger(\eta_{1}\,0)}_{t_{1}\,\half} \times a^{\dagger(\eta_{2}\,0)}_{t_{2}\,\half}\right\}^{\omega_1}_{S_1}\times \left\{\tilde{a}^{(0\,\eta_{3})}_{t_{3}\,\half} \times \tilde{a}^{(0\,\eta_{4})}_{t_{4}\,\half}\right\}^{\omega_2}_{S_2}\right]^{\rho_0 \omega_0}_{\kappa_0 L_0=S_0 J_0=0 M_0=0}
\end{equation}
and its conjugate counterpart
\begin{equation}
\left[\left\{a^{\dagger(\eta_{4}\,0)}_{t_{4}\,\half} \times a^{\dagger(\eta_{3}\,0)}_{t_{3}\,\half}\right\}^{\tilde{\omega}_{2}}_{S_2}\times \left\{\tilde{a}^{(0\,\eta_{2})}_{t_{2}\,\half} \times \tilde{a}^{(0\,\eta_{1})}_{t_{1}\,\half}\right\}^{\tilde{\omega}_1}_{S_1}\right]^{\rho_0\tilde{\omega}_{0}}_{\kappa_0 L_0=S_0 J_0=0 M_0=0}. \label{eq:type_pnnp} 
\end{equation}
Note that the final phase is a result should be independent of quantum numbers $t_{i}$,
where 
$\left\{t_{1},t_{2},t_{3},t_{4}\right\} \in \left[
\text{\{p,p,p,p\},\{n,n,n,n\}, \{p,n,n,p\}} \right]$, and hence we can discard them from our labeling. 
\medskip

\noindent
Following derivation provided in~(\ref{eq:adta_dagger}), one can show that
\begin{equation}
\left(
\left\{\tilde{a}^{(0\,\eta_{1})}_{\half}\times \tilde{a}^{(0\,\eta_{2})}_{\half}\right\}^{\omega S \Sigma}_{\alpha}
\right)^{\dagger}	
=(-)^{\eta_{1}+\eta_{2}-L+M+1+\Sigma} 
\left\{ 
	a^{\dagger(\eta_{2}\,0)}_{\half}\times a^{\dagger (\eta_{1}\,0)}_{\half}
	\right\}^{\tilde{\omega} S -\Sigma}_{\tilde{\alpha}}
\end{equation}
The resulting phase is obtained as a product of conjugation of \SU{3} Wigner coefficients and both operators,
\begin{displaymath}
(-)^{l_{1}+l_{2}-L}(-)^{\eta_{1}+l_{1}+m_{1}+\half+\sigma_1}(-)^{\eta_{2}+l_{2}+m_{2}+\half+\sigma_2}.
\end{displaymath}
Similarly,
\begin{equation}
\left(
\left\{ 
	a^{\dagger(\eta_{1}\,0)}_{\half}\times a^{\dagger (\eta_{2}\,0)}_{\half}
\right\}^{\omega S \Sigma}_{\alpha}
\right)^{\dagger}
=(-)^{\eta_{1}+\eta_{2}-L-M+1-\Sigma} 
\left\{\tilde{a}^{(0\,\eta_{2})}_{\half}\times \tilde{a}^{(0\,\eta_{1})}_{\half}\right\}^{\tilde{\omega} S -\Sigma}_{\tilde{\alpha}},
\end{equation}
where phase is obtained from
\begin{displaymath}
(-)^{l_{1}+l_{2}-L}(-)^{\eta_{1}+l_{1}-m_{1}+\half-\sigma_1}(-)^{\eta_{2}+l_{2}-m_{2}+\half-\sigma_2}.
\end{displaymath}
In the following derivation, we will use the formula for switching order
$\omega_1$ and $\omega_2$ followed by conjugation of all three irreps in \SU{3}
Wigner coefficients, 
\begin{equation}
\CG{\omega_1\alpha_1}{\omega_2 \alpha_2}{\omega_0 \alpha_0}_{\rho_0}=(-)^{\omega_1+\omega_2-\omega_0+\rho_0^{\max}-\rho_0+l_1+l_2-L_0}
\sum_{\rho'}\Phi_{\rho_0 \rho'}\left[\tilde{\omega}_1\tilde{\omega}_2\tilde{\omega}_0\right]\CG{\tilde{\omega}_2\tilde{\alpha}_2}{\tilde{\omega}_1\tilde{\alpha}_1}{\tilde{\omega}_0\tilde{\alpha}_0}_{\rho'}.
\end{equation}

\begin{multline}
\left(
\left[
\left\{
	a^{\dagger(\eta_{1}\,0)}_{p\,\half} \times a^{\dagger(\eta_{2}\,0)}_{n\,\half}
\right\}^{\omega_1}_{S_1}
\times 
\left\{
	\tilde{a}^{(0\,\eta_{3})}_{n\,\half} \times \tilde{a}^{(0\,\eta_{4})}_{p\,\half}
\right\}^{\omega_2}_{S_2}
\right]^{\rho_0 (\lambda_0\,\mu_0)S_0\Sigma_0}_{\kappa_0 L_0 M_0}
\right)^{\dagger}= \\
\sum_{
	\substack{
\Sigma_{1},\Sigma_{2} \\
\alpha_{1}, \alpha_{2} 
	} } 
	C_{S_{1}\Sigma_{1}S_{2}\Sigma_{2}}^{S_{0}\Sigma_{0}}
	\CG{\omega_{1}\alpha_{1}}{\omega_{2}\alpha_{2}}{\omega_{0}\alpha_{0}}_{\rho_{0}}
\left(
\left\{
	\tilde{a}^{(0\,\eta_{3})}_{n\,\half} \times \tilde{a}^{(0\,\eta_{4})}_{p\,\half}
\right\}^{\omega_2}_{S_2}
\right)^{\dagger} 
\left(
\left\{
	a^{\dagger(\eta_{1}\,0)}_{p\,\half} \times a^{\dagger(\eta_{2}\,0)}_{n\,\half}
\right\}^{\omega_1}_{S_1}
\right)^{\dagger}
=\\
=
\sum_{
	\substack{
\Sigma_{1},\Sigma_{2} \\
\alpha_{1}, \alpha_{2} 
	} }
	C_{S_{2}-\Sigma_{2}S_{1}-\Sigma_{1}}^{S_{0}-\Sigma_{0}}
(-)^{\omega_1+\omega_2-\omega_0+\rho_0^{\max}-\rho_0+L_1+L_2-L_0}
\sum_{\rho'}\Phi_{\rho_0 \rho'}\left[\tilde{\omega}_1\tilde{\omega}_2\tilde{\omega}_0\right]\CG{\tilde{\omega}_2\tilde{\alpha}_2}{\tilde{\omega}_1\tilde{\alpha}_1}{\tilde{\omega}_0\tilde{\alpha}_0}_{\rho'}
	\\
(-)^{\eta_{3}+\eta_{4}-L_2+M_2+1+\Sigma_2} 
\left\{ 
	a^{\dagger(\eta_{4}\,0)}_{\half}\times a^{\dagger (\eta_{3}\,0)}_{\half}
\right\}^{\tilde{\omega}_2 S_2 -\Sigma_2}_{\tilde{\alpha}_2}
(-)^{\eta_{1}+\eta_{2}-L_1-M_1+1-\Sigma_1} 
\left\{\tilde{a}^{(0\,\eta_{2})}_{\half}\times \tilde{a}^{(0\,\eta_{1})}_{\half}\right\}^{\tilde{\omega}_1 S_1 -\Sigma_1}_{\tilde{\alpha}_1}
=\\
=
(-)^{\eta_{1}+\eta_{2}+\eta_{3}+\eta_{4}+\omega_1 + \omega_2-\omega_0+\rho_0^{\max}-\rho_0-L_0+M_{0}+\Sigma_0}
\sum_{\rho'}\Phi_{\rho_0 \rho'}\left[\tilde{\omega}_1\tilde{\omega}_2\tilde{\omega}_0\right]
\sum_{
	\substack{
\Sigma_{1},\Sigma_{2} \\
\alpha_{1}, \alpha_{2} 
	} }
	C_{S_{2}-\Sigma_{2}S_{1}-\Sigma_{1}}^{S_{0}-\Sigma_{0}}
\CG{\tilde{\omega}_2\tilde{\alpha}_2}{\tilde{\omega}_1\tilde{\alpha}_1}{\tilde{\omega}_0\tilde{\alpha}_0}_{\rho'}
\\
\left\{ 
	a^{\dagger(\eta_{4}\,0)}_{\half}\times a^{\dagger (\eta_{3}\,0)}_{\half}
\right\}^{\tilde{\omega}_2 S_2 -\Sigma_2}_{\tilde{\alpha}_2}
\left\{\tilde{a}^{(0\,\eta_{2})}_{\half}\times \tilde{a}^{(0\,\eta_{1})}_{\half}\right\}^{\tilde{\omega}_1 S_1 -\Sigma_1}_{\tilde{\alpha}_1}
\end{multline}
Note that we used the fact that $(-)^{M_2-M_1}=(-)^{M_2+M_1}=(-)^{M_0}$ and analogue for spin.
So we get the final formula:
\begin{multline}
\left(
\left[
\left\{
	a^{\dagger(\eta_{1}\,0)}_{p\,\half} \times a^{\dagger(\eta_{2}\,0)}_{n\,\half}
\right\}^{\omega_1}_{S_1}
\times 
\left\{
	\tilde{a}^{(0\,\eta_{3})}_{n\,\half} \times \tilde{a}^{(0\,\eta_{4})}_{p\,\half}
\right\}^{\omega_2}_{S_2}
\right]^{\rho_0 \omega_0 S_0\Sigma_0}_{\kappa_0 L_0 M_0}
\right)^{\dagger}= \\
(-)^{\eta_{1}+\eta_{2}+\eta_{3}+\eta_{4}+\omega_1 + \omega_2-\omega_0+\rho_0^{\max}-\rho_0-L_0+M_{0}+\Sigma_0}
\sum_{\rho'}\Phi_{\rho_0 \rho'}\left[\tilde{\omega}_1\tilde{\omega}_2\tilde{\omega}_0\right]
\left[
\left\{ 
	a^{\dagger(\eta_{4}\,0)}_{p\,\half}\times a^{\dagger (\eta_{3}\,0)}_{n\,\half}
\right\}^{\tilde{\omega}_2}_{S_2}
\times
\left\{\tilde{a}^{(0\,\eta_{2})}_{n\,\half}\times \tilde{a}^{(0\,\eta_{1})}_{p\,\half}\right\}^{\tilde{\omega}_1}_{S_1}
\right]^{\rho'\tilde{\omega}_{0} S_0 -\Sigma_0}_{\kappa_0 L_0 -M_0}
\end{multline}
Note that for each two-body parity conserving operator the sum of HO shell numbers is always even number, and 
hence we can drop it from the phase. This follows from the fact that
$\eta_{1}+\eta_{2}$ is equal to $\eta_{3}+\eta_{4}$. Similarly, for realistic interaction $\lambda_{0}+\mu_{0}$ is always even.
For a scalar tensor we thus get
\begin{multline}
\left(
\left[\left\{a^{\dagger(\eta_{1}\,0)}_{p\,\half} \times a^{\dagger(\eta_{2}\,0)}_{n\,\half}\right\}^{\omega_1}_{S_1}\times \left\{\tilde{a}^{(0\,\eta_{3})}_{n\,\half} \times \tilde{a}^{(0\,\eta_{4})}_{p\,\half}\right\}^{\omega_2}_{S_2}\right]^{\rho_0 \omega_0}_{\kappa_0 L_0=S_0 J_0=0 M_0=0}
\right)^{\dagger}= \\
=
(-)^{\omega_1 + \omega_2+\rho_0^{\max}-\rho_0-S_0}
\sum_{\rho'}\Phi_{\rho_0 \rho'}\left[\tilde{\omega}_1\tilde{\omega}_2\tilde{\omega}_0\right]
\left[\left\{a^{\dagger(\eta_{4}\,0)}_{p\,\half} \times a^{\dagger(\eta_{3}\,0)}_{n\,\half}\right\}^{\tilde{\omega}_{2}}_{S_2}\times \left\{\tilde{a}^{(0\,\eta_{2})}_{n\,\half} \times \tilde{a}^{(0\,\eta_{1})}_{p\,\half}\right\}^{\tilde{\omega}_1}_{S_1}\right]^{\rho' \tilde{\omega}_{0}}_{\kappa_0 L_0=S_0 J_0=0 M_0=0}. \label{eq:general} 
\end{multline}
In multiplicity free case, i.e. $\rho_{0}^{\max}=1$, the previous formula simplifies to
\begin{multline}
\left(
\left[\left\{a^{\dagger(\eta_{1}\,0)}_{p\,\half} \times a^{\dagger(\eta_{2}\,0)}_{n\,\half}\right\}^{\omega_1}_{S_1}\times \left\{\tilde{a}^{(0\,\eta_{3})}_{n\,\half} \times \tilde{a}^{(0\,\eta_{4})}_{p\,\half}\right\}^{\omega_2}_{S_2}\right]^{\omega_{0}}_{\kappa_0 L_0=S_0 J_0=0 M_0=0}
\right)^{\dagger}= \\
=
(-)^{S_0}
\left[\left\{a^{\dagger(\eta_{4}\,0)}_{p\,\half} \times a^{\dagger(\eta_{3}\,0)}_{n\,\half}\right\}^{\tilde{\omega}_{2}}_{S_2}\times \left\{\tilde{a}^{(0\,\eta_{2})}_{n\,\half} \times \tilde{a}^{(0\,\eta_{1})}_{p\,\half}\right\}^{\tilde{\omega}_1}_{S_1}\right]^{\tilde{\omega}_0}_{\kappa_0 L_0=S_0 J_0=0 M_0=0}. \label{eq:single_rho} 
\end{multline}
It is important to note that the final labels of a tensor in
$\SU{3}\supset\SO{3}$ chain should include quantum numbers desribing an \SU{3}
extreme state $G_{E}$ which gives rise to a given set of $\kappa_{0} L_{0}$ quantum
numbers. Jerry Draayer's SU3 library imposes the following meaning the extreme state $G_{E}$ labels: 
\begin{equation}
G_{E}
\left\{
\begin{array}{c}
LW' \quad \text{for } \lambda\ge \mu \\
HW' \quad \text{for } \lambda < \mu 
\end{array}\right.
\end{equation}
For tensors in the interaction file~(\ref{eq:general}) and~(\ref{eq:single_rho}) are valid if $\lambda_{0} \ne \mu_{0}$.
Note that for $\lambda_{0}=\mu_{0}$ the formula, e.g.~(\ref{eq:general}), reads as:
\begin{multline}
\left(
\left[\left\{a^{\dagger(\eta_{1}\,0)}_{p\,\half} \times a^{\dagger(\eta_{2}\,0)}_{n\,\half}\right\}^{\omega_1}_{S_1}\times \left\{\tilde{a}^{(0\,\eta_{3})}_{n\,\half} \times \tilde{a}^{(0\,\eta_{4})}_{p\,\half}\right\}^{\omega_2}_{S_2}\right]^{\rho_0 \text{\color{red}LW'} (\lambda_0\,\lambda_0)}_{\kappa_0 L_0=S_0 J_0=0 M_0=0}
\right)^{\dagger}= \\
=
(-)^{\omega_1 + \omega_2+\rho_0^{\max}-\rho_0-S_0}
\sum_{\rho'}\Phi_{\rho_0 \rho'}\left[\tilde{\omega}_1\tilde{\omega}_2\tilde{\omega}_0\right]
\left[\left\{a^{\dagger(\eta_{4}\,0)}_{p\,\half} \times a^{\dagger(\eta_{3}\,0)}_{n\,\half}\right\}^{\tilde{\omega}_{2}}_{S_2}\times \left\{\tilde{a}^{(0\,\eta_{2})}_{n\,\half} \times \tilde{a}^{(0\,\eta_{1})}_{p\,\half}\right\}^{\tilde{\omega}_1}_{S_1}\right]^{\rho' \text{\color{red} HW'} (\lambda_0\,\lambda_0)}_{\kappa_0 L_0=S_0 J_0=0 M_0=0}. 
\end{multline}
and similarly~(\ref{eq:single_rho}). 
Note that all strenghts of tensors $(\lambda_0 \lambda_0)$ in the interaction files are defined with respect $LW'$.
To obtain conjugate counterpart of $LW'(\lambda_0 \, \mu_0)$ tensor expressed again in terms of $LW'(\lambda_0\,\lambda_0)$ 
\begin{displaymath}
(-)^{\lambda_0 L_0}
\left[\left\{a^{\dagger(\eta_{4}\,0)}_{p\,\half} \times a^{\dagger(\eta_{3}\,0)}_{n\,\half}\right\}^{\tilde{\omega}_{2}}_{S_2}\times \left\{\tilde{a}^{(0\,\eta_{2})}_{n\,\half} \times \tilde{a}^{(0\,\eta_{1})}_{p\,\half}\right\}^{\tilde{\omega}_1}_{S_1}\right]^{\rho' \text{\color{red} LW'} (\lambda_0\,\lambda_0)}_{\kappa_0 L_0=S_0 J_0=0 M_0=0}
=
\left[\left\{a^{\dagger(\eta_{4}\,0)}_{p\,\half} \times a^{\dagger(\eta_{3}\,0)}_{n\,\half}\right\}^{\tilde{\omega}_{2}}_{S_2}\times \left\{\tilde{a}^{(0\,\eta_{2})}_{n\,\half} \times \tilde{a}^{(0\,\eta_{1})}_{p\,\half}\right\}^{\tilde{\omega}_1}_{S_1}\right]^{\rho' \text{\color{red} HW'} (\lambda_0\,\lambda_0)}_{\kappa_0 L_0=S_0 J_0=0 M_0=0}
\end{displaymath}
As a result we will get the following formulas for $\lambda_{0} \ne \mu_{0}$:
\begin{eqnarray}
\alpha^{\vec{\tilde{\eta}}\tilde{\omega}_2 \tilde{\omega}_1 \rho_0\tilde{\omega}_0}_{S_2 S_1\kappa_{0} L_{0}=S_{0}00}
&=&(-)^{\omega_1 + \omega_2+\rho_0^{\max}-S_0}\sum_{\rho_{0}'} (-)^{\rho_{0}'}\alpha^{\vec{\eta}\omega_1 \omega_2 \rho_{0}'\omega_0}_{S_1 S_2\kappa_{0} L_{0}=S_{0}00}\Phi[\tilde{\omega}_{1}\tilde{\omega}_{2}\tilde{\omega}_{0}] 
\\
\alpha^{\vec{\tilde{\eta}}\tilde{\omega}_2 \tilde{\omega}_1 1\tilde{\omega}_0}_{S_2 S_1\kappa_{0} L_{0}=S_{0}00} 
&=&(-)^{S_{0}}\alpha^{\vec{\eta}\omega_1 \omega_2 1\omega_0}_{S_1 S_2\kappa_{0} L_{0}=S_{0}00}
\quad \text{if}\,\,\, \rho_{0}^{\max}=1 \\
\end{eqnarray}
If $\lambda_{0}=\mu_{0}$, one has to multiply the right-hand side of both relations
by phase $(-)^{\lambda_0 S_{0}}$. 
\medskip

\noindent
The validity of these formulas was tested using \texttt{adadaa\_test} using
interaction file for $N_{\max}=6$ obtained transformation into \SU{3} tensorial
form as implemented by code \texttt{InteractionJTtoSU3}.
\medskip

\noindent
{\it\red{Hypothesis}}:
Relations for conjugating proton-neutron tensors~(\ref{eq:pn1}), (\ref{eq:pn2}), and
(\ref{eq:pn3}) are valid for $\lambda_0 \ne \mu_0$. If $\lambda_0=\mu_0$, additional phase $(-)^{\lambda_0 S_0}$ has to be applied.
\end{document}
