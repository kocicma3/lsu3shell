################################################################
# project name
################################################################

project_name := su3shell

################################################################
# modules -- list of directories in which to search 
# for module.mk include files
################################################################

# libraries
# Caution: Order is important since used also in linking.
modules := libraries/LSU3 libraries/SU3ME libraries/LookUpContainers libraries/UNU3SU3 libraries/SU3NCSMUtils libraries/eigensolver_MFDn libraries/su3lib

#programs
modules += \
  programs/LSU3shell \
  programs/downstreams \
  programs/upstreams \
  programs/tools \
#  programs/su3ncsm_textIO \
#  programs/su3ncsm_hdf5IO \
#  tests/LSU3 \


#  programs/tools \
#  programs/upstreams \
#  programs/su3ncsm_hdf5IO \
#  programs/downstreams \
################################################################
# extras -- list of extra files to be included
# in distribution tar file
################################################################

extras := su3shell-install.txt devel

################################################################
# additional project-specific make settings and rules
################################################################

# MPI pattern rules
#   functions to determine which files in this project require 
#   compilation with MPI
list-mpi-programs-cpp = $(filter %MPI,$(programs_cpp))
list-mpi-programs-cpp += $(filter %HDF5,$(programs_cpp))
list-mpi-objects-cpp = $(addsuffix $(o_ext),$(list-mpi-programs-cpp)) 
