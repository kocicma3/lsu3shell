################################################################
# directory trees
################################################################

# search prefix
#   additional path to search for required libraries
#   (e.g., su3lib and eigen)
<<<<<<< HEAD
search_prefix :=  $(BOOST_ROOT) $(HOME)/local
=======
search_prefix := $(BOOST_ROOT) $(HOME)/local
>>>>>>> 8433bbe1a64718ab6c0d29fdb1908b32e40b5a95
search_dirs_include  := $(HOME)/local/include/eigen-3.1.1
search_dirs_lib := 

# install prefix
install_prefix := $(current-dir)
# Note: You should reset to /user/local to do a systemwide 
# installation.  This is analagous to the --prefix= option of 
# autoconf installations.

################################################################
# machine-specific library configuration
################################################################

# SU3LIB numerical precision
#   Set flag SU3DBL for double precision or SU3QUAD for quad precision.
#   Note: quad precision requires ifort compiler

FFLAGS += -DSU3DBL
#FFLAGS += -DSU3QUAD

## Gnu Scientific library
LDLIBS += -lgsl 
<<<<<<< HEAD
LDLIBS += -lflapack -lfblas -lgslcblas
=======
LDLIBS += -lgslcblas -lflapack -lfblas
>>>>>>> 8433bbe1a64718ab6c0d29fdb1908b32e40b5a95

# binary output using hdf5 
#LDLIBS += -lmascot -lz -lhdf5

LDLIBS += -lpthread 
LDLIBS += -lboost_mpi -lboost_serialization -lboost_system -lboost_chrono

################################################################
#  Parameters required for creating Sp3RGen_slepc
# SLEPc & PETSc header files and libraries. Needed for Sp(3, R) generator only!
################################################################
search_prefix += $(PETSC_DIR)/$(PETSC_ARCH) $(SLEPC_DIR)/$(PETSC_ARCH) 
search_dirs_include  +=  $(SLEPC_DIR)/include $(PETSC_DIR)/include
search_dirs_lib += /usr/X11R6/lib

LDLIBS += -lslepc -lpetsc -lX11
################################################################
# C++ compiler-specific configuration
################################################################

# C++ compiler
CXX := mpicxx 
#CXX := icpc

# C++ compiler optimization and debugging
<<<<<<< HEAD
CXXFLAGS += -cxx=g++ -std=c++0x -DHAVE_INLINE -DNDEBUG -DCPP0X_STD_TR1 -O3 -fopenmp #-Wall -W
=======
#CXXFLAGS += -cxx=g++ -std= -DHAVE_INLINE -DNDEBUG -DCPP0X_STD_TR1 -O3  -fopenmp #-Wall -W
CXXFLAGS += -cxx=g++ -std=c++0x -DHAVE_INLINE -DNDEBUG -DCPP0X_STD_TR1 -g  -fopenmp #-Wall -W
>>>>>>> 8433bbe1a64718ab6c0d29fdb1908b32e40b5a95

# parallel C++ compiler
#   used in module.mk files as
#   program program.o: CXX := $(MPICXX)
<<<<<<< HEAD
MPICXX := mpicxx $(CXXFLAGS)
=======
MPICXX := mpicxx 
>>>>>>> 8433bbe1a64718ab6c0d29fdb1908b32e40b5a95

################################################################
# FORTRAN compiler-specific configuration
################################################################

# FORTRAN compiler
# Example values:
#   for GCC 3.x: f77
#   for GCC 4.x: gfortran
#   for Intel: ifort
#FC := gfortran
#FC := ifort 
#FC := mpif77 -f77=ifort
#FC := mpif77 -f77=/sw/bin/gfortran
FC := mpif77 -f77=gfortran -m64 -frecursive #frecursive needed for multithread safe su3lib
#FC := mpif90

# FORTRAN compiler optimization and debugging
#FFLAGS += -O3 
FFLAGS += -g

################################################################
# C++/FORTRAN linking 
#    with C++ main()
################################################################

# FORTRAN object libraries (added to LDLIBS)
# Example values, depending on the compiler you are using to compile
# the FORTRAN objects:
#   for GCC 3.x f77: -lg2c
#   for GCC 4.x gfortran: -lgfortran
#   for Intel ifort: -lifport -lifcore -limf
fortran_libs := -lgfortran
#fortran_libs += -lifport -lifcore -limf

# FORTRAN linking flags (added to LDFLAGS)
# Not yet needed but provided as hook.
fortran_flags := 
