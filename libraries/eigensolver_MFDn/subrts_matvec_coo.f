c---2--------------------------------------------------------------------
c\BeginDoc
c
c\Name: matveccoodiag
c
c\EndDoc
c-----------------------------------------------------------------------

      subroutine matveccoodiag(rowind, colind,
     $     asize, a, ncols, x, y)
       
      implicit none
      integer*8, intent(in) :: ncols, asize
      integer, dimension(asize), intent(in) :: rowind, colind
      real*4, dimension(asize), intent(in) :: a
      real*4, dimension(ncols), intent(in) :: x
      real*4, dimension(ncols), intent(inout) :: y  
      real*4, dimension(ncols) :: yy 
      integer*8 :: i
C
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(yy, i)
      yy = 0.0
!$OMP DO SCHEDULE(RUNTIME)
      do i = 1, asize
         yy(rowind(i)) = yy(rowind(i)) + a(i) * x(colind(i))
         if (rowind(i).ne.colind(i)) then
            yy(colind(i)) = yy(colind(i)) + a(i) * x(rowind(i))
         endif
      enddo
!$OMP END DO
!$OMP CRITICAL
      y = y + yy
!$OMP END CRITICAL
!$OMP END PARALLEL
    
      return
      end subroutine matveccoodiag
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
c-----------------------------------------------------------------------
c\BeginDoc
c
c\Name: matveccoooffiag
c
c\EndDoc
c-----------------------------------------------------------------------

      subroutine matveccoooffdiag(
     $     rowind, colind, asize, a,
     $     nrows, ncols, x, xt, y, yt)
     
      implicit none
      integer*8, intent(in) :: nrows, ncols, asize
      integer, dimension(asize), intent(in) :: rowind, colind
      real*4, dimension(asize) :: a
      real*4, dimension(ncols), intent(in) :: x
      real*4, dimension(nrows), intent(in) :: xt
      real*4, dimension(nrows), intent(inout) :: y
      real*4, dimension(ncols), intent(inout) :: yt
      real*4, dimension(nrows) :: yy 
      real*4, dimension(ncols) :: yyt
      integer*8 :: i
C
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(yy, yyt, i)
      yy = 0.0
      yyt = 0.0
!$OMP DO SCHEDULE(RUNTIME)
      do i = 1, asize
         yy(rowind(i)) = yy(rowind(i)) + a(i) * x(colind(i))
         yyt(colind(i)) = yyt(colind(i)) + a(i) * xt(rowind(i))
      enddo
!$OMP END DO
!$OMP CRITICAL
      y = y + yy
      yt = yt + yyt
!$OMP END CRITICAL
!$OMP END PARALLEL

      return
      end subroutine matveccoooffdiag
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
