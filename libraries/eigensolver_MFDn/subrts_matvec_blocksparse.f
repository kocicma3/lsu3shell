c---2--------------------------------------------------------------------
c\BeginDoc
c
c\Name: matvecblockdiag
c
c
c\Description: 
c  Use precomputed J-Scheme projection to carry out matvec on the 
c  symmetric Hamiltonian (only portion of Hamiltonian stored)
c  one block at a time.  Note that the Hamiltonian stored is a little more than
c  just the lower diagnol of the Hamiltonian (diagonal blocks are stored fully).
c  For the transpose operation, these diag Hblocks are ignored.
c
c\Usage:
c  call matvecdiag(nblocks, rowind, colind, rownnz, colnnz,
c                   asize, a, ncols, in, out)
c     
c
c\Arguments
c
c  nblocks,  rowind, colind, rownnz, colnnz,  asize, a,  ncols, in, out
c
c       
c
c  nblocks	     number of matrix blocks 
c				 (determined by j-scheme and input paramters) 
c
c  rowind	     array index of size(nblock)  containing the  
c				 relative row position of the nblocks
c
c  colind	     array index of size(nblock)  containing the  
c				 relative column position of the nblocks
c
c  rownnz	     array index of size(nblock)  containing the number of rows in each block
c
c  colnnz	     array index of size(nblock)  containing the number of columns in each block
c
c  asize		 length of array a which contains the J-scheme Hamiltonian
c                entries for interacting many-body states
c  a			 array mentioned above
c
c  ncols         "N-local" i.e. size of matrix chunk distributed over 
c                a given column communicator
c
c  in            local chunk of lanczos vector of size(ncols)
c
c  out			 result of applying full (ncolsxncols) matvec to "in" 
c
c\EndDoc
c-----------------------------------------------------------------------

      subroutine matvecblockdiag(nblocks, rowind, colind,
     $     rownnz, colnnz, asize, a, ncols, current, x, y)
       
      implicit none
      integer*8, intent(in) :: nblocks, ncols, asize
      integer, dimension(nblocks), intent(in) :: rowind, colind
      integer, dimension(nblocks), intent(in) :: rownnz, colnnz
      real*4, dimension(asize), intent(in) :: a
      integer*8, dimension(nblocks), intent(in) :: current
      real*4, dimension(ncols), intent(in) :: x
      real*4, dimension(ncols), intent(inout) :: y  
      real*4, dimension(ncols) :: yy 
      integer*8 :: i, j, k, l
C
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(yy, i, j, k, l)
      yy = 0.0
!$OMP DO SCHEDULE(RUNTIME)
      do i = 1, nblocks
C
C        call sgemv('N', rownnz(i), colnnz(i), 1.0, a(current(i)),
C    $        rownnz(i), x(colind(i)), 1, 1.0, private_y(rowind(i)), 1)
C
         l = current(i)
         do k = colind(i), colind(i) + colnnz(i) - 1
            do j = rowind(i), rowind(i) + rownnz(i) - 1
               yy(j) = yy(j) + a(l) * x(k)
               l = l + 1
            enddo
         enddo
         if (rowind(i).ne.colind(i)) then
C
C        call sgemv('T', rownnz(i), colnnz(i), 1.0, a(current(i)),
C    $        rownnz(i), x(rowind(i)), 1, 1.0, private_y(colind(i)), 1)
C
            l = current(i)
            do k = colind(i), colind(i) + colnnz(i) - 1
               do j = rowind(i), rowind(i) + rownnz(i) - 1
                  yy(k) = yy(k) + a(l) * x(j)
                  l = l + 1
               enddo
            enddo
         endif
      enddo
!$OMP END DO
!$OMP CRITICAL
      y = y + yy
!$OMP END CRITICAL
!$OMP END PARALLEL
    
      return
      end subroutine matvecblockdiag
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
c-----------------------------------------------------------------------
c\BeginDoc
c
c\Name: matvecblockoffiag
c
c
c\Description: 
c  Use precomputed J-Scheme projection to carry out matvec on the 
c  symmetric Hamiltonian (only portion of Hamiltonian stored)
c  one block at a time.  Note that the Hamiltonian stored is a little more than
c  just the lower diagnol of the Hamiltonian (diagonal blocks are stored fully).
c  For the transpose operation, these diag Hblocks are ignored.
c
c\Usage:
c  call matvecoffdiag(nblocks, rowind, colind, rownnz, colnnz,
c                     asize, a, nrows, ncols, in, intrans, out, outtrans)
c     
c
c\Arguments
c
c  nblocks,  rowind, colind, rownnz, colnnz,  asize, a,  ncols, in, 
c  intrans, out, outtrans
c
c       
c
c  nblocks	     number of matrix blocks 
c				 (determined by j-scheme and input paramters) 
c
c  rowind	     array index of size(nblock)  containing the  
c				 relative row position of the nblocks
c  colind	     array index of size(nblock)  containing the  
c				 relative column position of the nblocks
c
c  rownnz	     array index of size(nblock)  containing the number of rows in each block
c
c  colnnz	     array index of size(nblock)  containing the number of columns in each block
c
c
c  asize		 length of array a which contains the J-scheme Hamiltonian
c                entries for interacting many-body states
c  a			 array mentioned above
c
c  nrows         "M-local" i.e. size of MxN  matrix chunk distributed over 
c                a given row communicator
c  ncols         "N-local" i.e. size of matrix chunk distributed over 
c                a given column communicator
c
c  in            local chunk of lanczos vector of size(ncols)
c
c  out           local result of applying full (nrowsxncols) matvec to "in"
c
c  intrans       another local chunk of lanczos vector of size(nrows)
c
c  outtrans      local result of applying full (ncolsxnrows) matvec to "intrans"
c
c 			 note that on off-diag processors there are two 
c 			 mappings  1) tampnew <- A*vamp
c			           2) vampnew <- A^T*tamp
c\EndDoc
c-----------------------------------------------------------------------

      subroutine matvecblockoffdiag(nblocks,
     $     rowind, colind, rownnz, colnnz, asize, a,
     $     nrows, ncols, current, x, xt, y, yt)
     
      implicit none
      integer*8, intent(in) :: nblocks, nrows, ncols, asize
      integer, dimension(nblocks), intent(in) :: rowind, colind
      integer, dimension(nblocks), intent(in) :: rownnz, colnnz
      real*4, dimension(asize) :: a
      real*4, dimension(ncols), intent(in) :: x
      real*4, dimension(nrows), intent(in) :: xt
      real*4, dimension(nrows), intent(inout) :: y
      real*4, dimension(ncols), intent(inout) :: yt
      integer*8, dimension(nblocks), intent(in) :: current
      real*4, dimension(nrows) :: yy 
      real*4, dimension(ncols) :: yyt
      integer*8 :: i, j, k, l
C
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(yy, yyt, i, j, k, l)
      yy = 0.0
      yyt = 0.0
!$OMP DO SCHEDULE(RUNTIME)
      do i = 1, nblocks
C        call sgemv('N', rownnz(i), colnnz(i), 1.0, a(current(i)),
C    $        rownnz(i), x(colind(i)), 1, 1.0, y(rowind(i)), 1)
         l = current(i)
         do k = colind(i), colind(i) + colnnz(i) - 1
            do j = rowind(i), rowind(i) + rownnz(i) - 1
               yy(j) = yy(j) + a(l) * x(k)
               l = l + 1
            enddo
         enddo
C        call sgemv('T', rownnz(i), colnnz(i), 1.0, a(current(i)),
C    $        rownnz(i), xt(rowind(i)), 1, 1.0,
C    $        yt(colind(i)), 1)
         l = current(i)
         do k = colind(i), colind(i) + colnnz(i) - 1
            do j = rowind(i), rowind(i) + rownnz(i) - 1
               yyt(k) = yyt(k) + a(l) * xt(j)
               l = l + 1
            enddo
         enddo
      enddo
!$OMP END DO
!$OMP CRITICAL
      y = y + yy
      yt = yt + yyt
!$OMP END CRITICAL
!$OMP END PARALLEL

      return
      end subroutine matvecblockoffdiag
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
