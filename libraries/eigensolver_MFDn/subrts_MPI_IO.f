C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
      subroutine vector2file(communicator,
     $     disp, offset, numstates, vector, filename)
C
      use nodeinfo
      implicit none
C      
      include "mpif.h"
      integer(kind=MPI_OFFSET_KIND), intent(in) :: disp, offset
      integer, intent(in) :: communicator, numstates
      real*4, dimension(numstates), intent(in) :: vector
      character(17), intent(in) :: filename
C
      integer :: fh, filemode, ierr, status(MPI_STATUS_SIZE)
C
C     fh = filehandler -- similar to Fortran unit number
      fh = 17
C     create and open write-only
      filemode = IOR(MPI_MODE_WRONLY,MPI_MODE_CREATE)
      call MPI_FILE_OPEN(communicator, filename,
     $   filemode, MPI_INFO_NULL, fh, ierr)
C
      call MPI_FILE_SET_VIEW(fh, disp, MPI_REAL4, MPI_REAL4,
     $      'native', MPI_INFO_NULL, ierr)
C
      call MPI_FILE_WRITE_AT(fh, offset, vector(1:numstates),
     $     numstates, MPI_REAL4, status, ierr)
C
      call MPI_FILE_CLOSE(fh, ierr)
C
      return
C
      end subroutine vector2file
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
