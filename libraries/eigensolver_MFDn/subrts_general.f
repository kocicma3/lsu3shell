CTD This module was copy-pasted from the original MatrixDiag.f
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      module nodeinfo
      integer :: myrank, mypid, npid, nproc, ndiag, lvecgroups, ilvec
C     communicators
      integer :: icomm, diag_comm, col_comm, row_comm, mycol, myrow
      integer :: lvec_sto_comm, lvec_ovl_comm, my_sto_id, my_ovl_id
      end module nodeinfo
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      subroutine SplitProcs
C
      use nodeinfo
C
      implicit none
C
      include "mpif.h"
C
C     local variables
      integer :: ierr, jj, kk, ikey
      integer :: klass, klasssto, klassovl, klass1, klass3
      integer :: nodrun, nodr1, nodr3, myrank_i1, myrank_i3
      real :: diag
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
    

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CTD:  
      icomm = MPI_COMM_WORLD
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      if (ndiag*(ndiag+1)/2.ne.nproc) then
         if (mypid.eq.0) print*, ' Error: unbalanced processors '
         call cancelall(101)
         stop
      endif
C
      if (mypid.lt.ndiag) then
         klass = 0
      else
         klass = 1
      endif
      call MPI_comm_split(icomm,klass,1,diag_comm,ierr)
C
C     Define communicators for column (i1) and row (i3) states
      nodrun = 0
      do jj = 1, ndiag
         nodr1 = 0
         nodr3 = jj - 1
         do kk = 1, ndiag + 1 - jj
            nodrun = nodrun + 1
            nodr1 = nodr1 + 1
            nodr3 = nodr3 + 1
            if (mypid+1.eq.nodrun) then
               klass1 = nodr1
               klass3 = nodr3
            endif
         enddo
      enddo
      mycol = klass1
      myrow = klass3

      ikey = 1
      call MPI_comm_split(icomm,klass1,ikey,col_comm,ierr)
      call MPI_comm_split(icomm,klass3,ikey,row_comm,ierr)

ccc   Note that, within communicators, all processes have sequential
ccc   rank values commencing with 0.  After a split, their ranks are
ccc   ordered according to the "key" in the split; if the keys are
ccc   equal, they are ordered accoring to their proc_id in the "parent"
ccc   communicator.  In this particular case, myrank_i1 should be equal
ccc   to myrank_i3; this rank is used below for the correct ordering of
ccc   the 2-d distribution of the Lanczos vectors
      call MPI_comm_rank(col_comm,myrank_i1,ierr)
      call MPI_comm_rank(row_comm,myrank_i3,ierr)

      if (mypid.eq.0) then
         print*, ' Defined communicators: diag_comm '
         print*, ' Defined communicators: col_comm, row_comm '
      endif
C
C     Define communicators for storing Lanczos vectors
C     across all processors (in a "2-dimensional" grid)
C     
C     Lanczos vectors stored in segments of length ilvecreach
C     col-like:  vamp(1:icolreach) if ilvec = 1
C     row-like:  tamp(1:irowreach) if ilvec = 3
C
C     "lvec_sto_comm" Lanczos vector storage communicator
C     ndiag: # of distinct communicator groups,
C     all diagonal processor are root within this communicator
C     each communicator has size lvecgroups
C
C     "lvec_ovl_comm" Lanczos overlap (dot_product) communicator:
C     lvecgroups: # of distinct communicator groups,
C     each communicator covers an entire vector and has size ndiag
C
C     Note: this only works if ndiag is indeed odd!
C
      lvecgroups = (ndiag+1) / 2
      if ((2*lvecgroups).ne.(ndiag+1)) then
         print*, 'Error: not quite balanced processors'
         call cancelall(111)
         stop
      endif
C
      if (myrank_i1.lt.lvecgroups) then
         klassovl = myrank_i1
         ilvec = 1
      else
         klassovl = ndiag - myrank_i1
         ilvec = 3
      endif
C
      if (ilvec.eq.1) then
         klasssto = klass1
         ikey = myrank_i1
      else
         klasssto = klass3
         ikey = ndiag - myrank_i1
      endif      
C
      call MPI_comm_split(icomm,klassovl,1,lvec_ovl_comm,ierr)
      call MPI_comm_split(icomm,klasssto,ikey,lvec_sto_comm,ierr)
C
C     need processor ID within storage communicator
C     (should be equal to key set above)
      call MPI_COMM_RANK(lvec_sto_comm,my_sto_id,ierr)
C     and within overlap communicator
      call MPI_COMM_RANK(lvec_ovl_comm,my_ovl_id,ierr)
C
      if (mypid.eq.0) then
         print*, ' Defined communicators: lvec_sto_comm'
         print*, ' Defined communicators: lvec_ovl_comm'
         print*
      endif
C
      return
C
      end subroutine SplitProcs
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
      integer function myphase(i)
      implicit none
      integer i
C
c      myphase = (-1)**i
C
c      myphase = 1 - 2 * modulo(i,2)
C
      if (mod(i,2).eq.0) then
         myphase = 1
      else
         myphase = -1
      endif
C
      return
      end function myphase
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
      integer*4 function intfour(neight, icode)
C
      use nodeinfo
      include "mpif.h"
      integer*8 neight
      integer*4 icode, ierr
C
C     converts integer*8 to integer*4
C     aborts MPI and stops if neight > 2^31
C
      intfour = neight
      if (intfour.ne.neight) then
         if (mypid.eq.0) then         
            write(0,*) ' Fatal error in program MFDn'
            write(0,*) ' integer*8 too big in function intfour', neight
            write(0,*) ' Error code', icode
         endif
         call MPI_Abort(MPI_COMM_WORLD,icode,ierr)
         stop
      endif
C
      return
      end
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
      subroutine cancelall(ierror)
C
      use nodeinfo
      include 'mpif.h'
C
C     writes error code and cancels all processes
C
      print*, ' Fatal error in program MFDn'
      print*, ' Processor rank ', mypid, ' Error code ', ierror
C     
      call MPI_Abort(MPI_COMM_WORLD,ierror,ierr)
C     
      stop
C
      return
      end      
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
