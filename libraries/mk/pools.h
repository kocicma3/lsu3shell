#pragma once

#include <boost/pool/pool.hpp>
#include <boost/pool/pool_alloc.hpp>
#include <boost/pool/object_pool.hpp>
#include <boost/pool/singleton_pool.hpp>

namespace mk
{

    namespace alloc
    {
        using byte = boost::pool_allocator<unsigned char>;
    }

    namespace pool
    {
        extern thread_local boost::pool<> rme;

        extern thread_local boost::pool<> crmecalculator;
    }

}
