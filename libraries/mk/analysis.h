#pragma once

#include <cstdint>

#include <atomic>
#include <array>

namespace mk
{
    namespace analysis
    {
        const size_t register_count = 32;

        extern std::array<std::atomic<uint64_t>, register_count> registers;

        void bin(size_t);

        void print_registers();
    }
}
