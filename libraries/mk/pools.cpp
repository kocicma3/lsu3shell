#include "pools.h"

#include <SU3ME/RME.h>
#include <SU3ME/CRMECalculator.h>

thread_local boost::pool<> mk::pool::rme(sizeof(SU3xSU2::RME));

thread_local boost::pool<> mk::pool::crmecalculator(sizeof(CRMECalculator));
