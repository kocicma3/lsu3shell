#include "analysis.h"

#include <iostream>
#include <iomanip>

std::array<std::atomic<uint64_t>, mk::analysis::register_count> mk::analysis::registers {};

void mk::analysis::bin(size_t value) {
    for (size_t i = 0; i < registers.size() - 1; ++i) {
        if (value <= (static_cast<size_t>(1) << i)) {
            registers[i].fetch_add(1, std::memory_order_relaxed);
            return;
        }
    }

    registers[registers.size() - 1].fetch_add(1, std::memory_order_relaxed);
}

void mk::analysis::print_registers() {
    #pragma omp critical
    {
        std::cout << "|   ";
        size_t column_count = 4;
        for (size_t i = 0; i < register_count; ++i) {
            size_t row = i / column_count;
            size_t column = i % column_count;
            size_t base = column * (register_count / column_count);
            size_t index = base + row;
            std::cout << '[' << std::setw(2) << std::setfill('0') << index << ']' << std::setw(12) << std::setfill(' ') << registers[index];
            if ((i + 1) % 4 != 0) {
                std::cout << "   |   ";
            } else {
                std::cout << "   |\n";
                if (i != register_count - 1) {
                    std::cout << "|   ";
                }
            }
        }
        std::cout << '\n';
    }
}