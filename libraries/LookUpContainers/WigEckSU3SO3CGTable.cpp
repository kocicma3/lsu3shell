#include <LookUpContainers/WigEckSU3SO3CGTable.h>
#include <iostream>
#include <boost/chrono.hpp>

std::vector<WigEckSU3SO3CGTable*> CWigEckSU3SO3CGTablesLookUpContainer::m_WigEckSU3SO3CGTables;

WigEckSU3SO3CG* WigEckSU3SO3CGTable::GetWigEckSU3SO3CG(const SU3::LABELS& omegaf, const SU3::LABELS& omegai) //	omegaf == Bra, omegai == Ket 
{
	assert(SU3::mult(omegai, m_IR0, omegaf));
	WigEckSU3SO3CG* result;

//	static boost::chrono::duration<double> locktime = boost::chrono::duration<double>::zero();
//	static boost::chrono::duration<double> locktime_last = boost::chrono::duration<double>::zero();

		MAPKEYCOEFF::iterator it = m_SU3SO3CGTable.find(SU3SU3Key(omegaf, omegai));
		if (it != m_SU3SO3CGTable.end())	//	a given SU3SO3CG already exist
		{
		  //result = it->second;
		  result = *it;
		}
		else	// a given SU3SO3 CG set does not exist => construct/compute and store
		{
			//	Insert new element of map: wfwi and pointer to WigEckSU3SO3CG whose
			//	constructor computes all CGs.
		  WigEckSU3SO3CG* p = NULL;

		  //#pragma omp critical
		  {
		    p = new WigEckSU3SO3CG(omegaf, omegai, m_IR0, m_LL0);
		  }
			
			m_SU3SO3CGTable.insert(SU3SU3Key(omegaf, omegai), p);
//	gcc 4.2.1 is not able to compile the next line due the known error in this version of GNU compiler			
//			assert(Result.second == true); // assert that a new set of CGs has been succesfully saved in m_SU3SO3CGTable 
			result = p; // return pointer to WigEckSU3SO3CG <omegai *; m_IR0 * L0||omegaf *>_{*}
		}
	
	return result;
}


//	calculate <wi * *; w0 * L0 || wf * *>_* for all possible values 
//	index = kf * kimax * k0max * rhotmax + ki * k0max * rhotmax + k0 * rhotmax + rhot
//	Input argument L0 is expected to be 2*L0 !!!!
//	however, calculation inside proceeds using Lf, Li, L0 values and not 2*Lf, 2*Li, and 2*L0.
WigEckSU3SO3CG::WigEckSU3SO3CG(const SU3::LABELS& omegaf, const SU3::LABELS& omegai, const SU3::LABELS& Omega0, const SO3::LABEL& L0)
{
	assert(!(L0%2)); // L0 must be equal 2*L0 !!!
	double dCG[MAX_K][MAX_K][MAX_K][MAX_K]; // this array is required by Fortran void wru3_

    int lmi(omegai.lm), mui(omegai.mu), lm0(Omega0.lm), mu0(Omega0.mu), lmf(omegaf.lm), muf(omegaf.mu);
	// Li, Lf, l0 contains angular momenta (and not 2*Lf, 2*Li, and 2*L0
	int Li, Lf, l0 = (L0/2); // NOTE: wu3r3w takes as argument Lf, Li, L0 not 2*Lf, 2*Li, 2*L0 !!!
	int Lfmax = omegaf.lm + omegaf.mu;
	int Limax = omegai.lm + omegai.mu;
	int ki, k0, kf, max_ki, max_k0, max_kf, max_rhot, irhot;
	
	std::vector<SU3::WIGNER> WigCoeffs;
	std::vector<LfLiCGIndex> LfLiCGIndexVector;
	LfLiCGIndex LfLiCGindex;

//	order in which SU(3) Wigner coefficients are stored is due to the
//	order of loops. However, keep in mind that since not all Li x L0 --> Lf ...
//	we store only those that couple.
	size_t index = 0;
//	Note that in this case we iterate over Lf and L0 	
	for (int Lf = 0; Lf <= Lfmax; ++Lf)
	{
		if (!SU3::kmax(omegaf, Lf))
		{
			continue;
		}
		for (int Li = 0; Li <= Limax; ++Li)
		{
			if (!SU3::kmax(omegai, Li))
			{
				continue;
			}
			if (SO3::mult(2*Li, L0, 2*Lf) == 0) // SO3::mult expects Li == 2*Li, L0 == 2*L0, and Lf = 2*Lf
			{
				continue;
			}
			LfLiCGindex.SetLfLi(2*Lf, 2*Li); // IMPORTANT: note that LfLi are stored as 2*Lf and 2*Li !!!
			LfLiCGindex.CGIndex = index; // m_WigCoeffs[index] = <wf 1 Lf; w0 1 L0 || wf 1 Lf>_1, i.e. kf = 1, ki = 1, k0 = 1, rhot = 1
			LfLiCGIndexVector.push_back(LfLiCGindex);

			memset(dCG, sizeof(dCG), 0);
#ifndef AIX			
			wu3r3w_(lmi, mui, lm0, mu0, lmf, muf, Li, l0, Lf, max_rhot, max_ki, max_k0, max_kf, dCG);
#else
			wu3r3w (lmi, mui, lm0, mu0, lmf, muf, Li, l0, Lf, max_rhot, max_ki, max_k0, max_kf, dCG);
#endif
//			index += max_kf*max_ki*max_k0*max_rhot;
		   	for (kf = 0; kf < max_kf; kf++) // for all values of k3
			{ 
				for (ki = 0; ki < max_ki; ki++) // for all values of k2
		   		{
					for (k0 = 0; k0 < max_k0; k0++)  // for all values of k1
		   		    {
			    		for (irhot = 0; irhot < max_rhot; irhot++) // for all possible multiplicities
						{
							WigCoeffs.push_back(dCG[kf][k0][ki][irhot]); // store SU(3) wigner coefficient
							index++;
    				    }
		   			}
   	    		}
    		}
		}
	}
//	assert(index == WigCGCoeffs.size());
	m_nLfLi = LfLiCGIndexVector.size();
	m_nWigCoeffs = WigCoeffs.size(); // for some cases m_nWingCoeffs is too small and should be integer
	assert(m_nWigCoeffs == WigCoeffs.size());	// if that happen ==> this assert will fail
	//	calling sort may not be neccessary since LfLi are generated in order such that LfLi[i] < LfLi[i+1], where i denotes i-th step.
	//	and hence LfLiCGIndexVector is already sorted with respect to std::less<WigEckSU3SO3CG::LfLiCGIndex>()
	//	TODO: test if I can remove this line:
	std::sort(LfLiCGIndexVector.begin(), LfLiCGIndexVector.end(), std::less<WigEckSU3SO3CG::LfLiCGIndex>());

	m_WigCoeffs = new SU3::WIGNER[WigCoeffs.size()];
	m_LfLiCGIndexArray = new LfLiCGIndex[m_nLfLi];

	std::copy(WigCoeffs.begin(), WigCoeffs.end(), m_WigCoeffs);
	std::copy(LfLiCGIndexVector.begin(), LfLiCGIndexVector.end(), m_LfLiCGIndexArray);
}

WigEckSU3SO3CG::~WigEckSU3SO3CG()
{
	delete []m_WigCoeffs;
	delete []m_LfLiCGIndexArray;
}


void WigEckSU3SO3CG::Show(const SU3::LABELS& omegaf, const SU3::LABELS& IR0, const SO3::LABEL L0, const SU3::LABELS& omegai)
{
	int kf_max, ki_max, k0_max, rhot_max, Limax, Lfmax;
	size_t index = 0;
	std::cout << "< (" << (int)omegai.lm << " " << (int)omegai.mu << ") *;";
	std::cout << "(" << (int)IR0.lm << " " << (int)IR0.mu << ") * L0=" << L0/2 << "||(" << (int)omegaf.lm << " " << (int)omegaf.mu << ") *>_*" << std::endl;
//	we are using the bare pointer to CGs which is stored as it->second == WigEckSU3SO3CG* 
	SU3::WIGNER* pSU3SO3CG = m_WigCoeffs;
	Lfmax = omegaf.lm + omegaf.mu;
	Limax = omegai.lm + omegai.mu;
	rhot_max = SU3::mult(omegai, IR0, omegaf);
	k0_max  = SU3::kmax(IR0, L0/2);
	for (int Lf = 0; Lf <= Lfmax; ++Lf)
	{
		kf_max = SU3::kmax(omegaf, Lf);
		if (!kf_max)
		{
			continue;
		}
		for (int Li = 0; Li <= Limax; ++Li)
		{
			ki_max = SU3::kmax(omegai, Li); // Li is NOT qual to 2*Li ==> I do not need to divide by two ...
			if (!ki_max || !SO3::mult(2*Li, L0, 2*Lf)) // mult expects Li = 2*Li, m_LL0 = 2*L0, and Lf = 2*Lf due to S1+S2+S3%3
			{
				continue;
			}
			for (int kf = 0; kf < kf_max; ++kf)
			{
				for (int ki = 0; ki < ki_max; ++ki)
				{
					for(int k0 = 0; k0 < k0_max; ++k0)
					{
						for (int rhot = 0; rhot < rhot_max; ++rhot)
						{
							std::cout << "ki = " << ki+1 << " Li = " << Li << " ";
							std::cout << "k0 = " << k0+1 << " L0 = " << L0/2 << " ";
							std::cout << "kf = " << kf+1 << " Lf = " << Lf << " rhot = " << rhot << "\t" << pSU3SO3CG[index] << std::endl;
							index++;
						}
					}
				}
			}
		}
	}
}

void WigEckSU3SO3CGTable::Show() const
{
	int kf_max, ki_max, k0_max, rhot_max, Limax, Lfmax;
	int l0 = m_LL0 >> 1; // Lf, Li, and L0 contain actual labels of these operators 
	size_t index;
	std::cout << m_IR0 << " L0 = " << (int)m_LL0 << std::endl;
	MAPKEYCOEFF::const_iterator it = m_SU3SO3CGTable.begin();

	for (; it != m_SU3SO3CGTable.end(); ++it)
	{
		SU3::LABELS omegaf, omegai;
		it.k().Get(omegaf, omegai); // it->k() == SU3SU3Key == wfwi
		std::cout << omegaf << "\t" << omegai << std::endl;
//	we are using the bare pointer to CGs which is stored as it->v() == WigEckSU3SO3CG* 
		SU3::WIGNER* pSU3SO3CG = it.v()->m_WigCoeffs;
		Lfmax = omegaf.lm + omegaf.mu;
		Limax = omegai.lm + omegai.mu;
		index = 0;
		rhot_max = SU3::mult(omegai, m_IR0, omegaf);
		k0_max  = SU3::kmax(m_IR0, l0);
		for (int Lf = 0; Lf <= Lfmax; ++Lf)
		{
			kf_max = SU3::kmax(omegaf, Lf);
			if (!kf_max)
			{
				continue;
			}
			for (int Li = 0; Li <= Limax; ++Li)
			{
				ki_max = SU3::kmax(omegai, Li); // Li is NOT qual to 2*Li ==> I do not need to divide by two ...
				if (!ki_max || !SO3::mult(2*Li, m_LL0, 2*Lf)) // mult expects Li = 2*Li, m_LL0 = 2*L0, and Lf = 2*Lf due to S1+S2+S3%3
				{
					continue;
				}
				std::cout << "Lf = " << Lf << " Li = " << Li << std::endl;
				for (int kf = 0; kf < kf_max; ++kf)
				{
					for (int ki = 0; ki < ki_max; ++ki)
					{
						for(int k0 = 0; k0 < k0_max; ++k0)
						{
							for (int rhot = 0; rhot < rhot_max; ++rhot)
							{
								std::cout << "ki = " << ki+1 << " k0 = " << k0+1 << " kf = " << kf+1 << " rhot = " << rhot << "\t" << pSU3SO3CG[index] << std::endl;
								index++;
							}
						}
					}
				}
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////
////////////////////       CWigEckSU3SO3CGTablesLookUpContainer     //////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
struct SU3SO3CGTABLE_EQUAL
{
	private:
    SU3::LABELS m_IR0;
	SO3::LABEL m_LL0;
	public:
	SU3SO3CGTABLE_EQUAL(const SU3::LABELS& IR0, const SO3::LABEL& L0): m_IR0(IR0.lm, IR0.mu), m_LL0(L0) {};
    inline bool operator()(const WigEckSU3SO3CGTable* CGTable) const
    {
        return (CGTable->m_IR0.lm == m_IR0.lm && CGTable->m_IR0.mu == m_IR0.mu  && CGTable->m_LL0 == m_LL0);
    }
};

void CWigEckSU3SO3CGTablesLookUpContainer::ShowInfo()
{
	uint32_t nmax_su3su3 = 0;
	std::cout << "Number of tensors with different values of w0 LL0:" << m_WigEckSU3SO3CGTables.size() << std::endl;
	for (size_t i = 0; i < m_WigEckSU3SO3CGTables.size(); ++i)
	{
		uint32_t nsu3su3 = m_WigEckSU3SO3CGTables[i]->GetNumberSU3SU3();
//		std::cout << "w0:" << m_WigEckSU3SO3CGTables[i]->m_IR0 << " L0:" << (int) m_WigEckSU3SO3CGTables[i]->m_LL0 << "  #(wf,wi): " << nsu3su3 << std::endl;
		if (nmax_su3su3 < nsu3su3)
		{
			nmax_su3su3 = nsu3su3;
		}
	}
	std::cout << "max size of HashFixed:" << nmax_su3su3 << std::endl;
}

void CWigEckSU3SO3CGTablesLookUpContainer::ReleaseMemory()
{
	for (size_t i = 0; i < m_WigEckSU3SO3CGTables.size(); ++i)
	{
		delete m_WigEckSU3SO3CGTables[i];
	}
	m_WigEckSU3SO3CGTables.clear();
}

WigEckSU3SO3CGTable* CWigEckSU3SO3CGTablesLookUpContainer::GetSU3SO3CGTablePointer(const SU3::LABELS& IR0, const SO3::LABEL L0)
{
	assert(!(L0%2)); // L0 must be always divisible by 2 since L0 == 2*L0
	std::vector<WigEckSU3SO3CGTable*>::const_iterator it = std::find_if(m_WigEckSU3SO3CGTables.begin(), m_WigEckSU3SO3CGTables.end(), SU3SO3CGTABLE_EQUAL(IR0, L0));
	if (it == m_WigEckSU3SO3CGTables.end())
	{
		m_WigEckSU3SO3CGTables.push_back(new WigEckSU3SO3CGTable(IR0, L0));
		return m_WigEckSU3SO3CGTables.back();
	}
	else
	{
		return *it;
	}
}

void CWigEckSU3SO3CGTablesLookUpContainer::ShowWigEckSU3SO3CGTables()
{
	size_t nTotal = 0;
	std::cout << "Tensors: " << std::endl;
	for (size_t i = 0; i < m_WigEckSU3SO3CGTables.size(); ++i)
	{
		nTotal += m_WigEckSU3SO3CGTables[i]->GetNumberSU3SO3CGs();
		std::cout << m_WigEckSU3SO3CGTables[i]->m_IR0 << " L0 = " << m_WigEckSU3SO3CGTables[i]->m_LL0/2; 
		std::cout << "\t\t #CGs = " << m_WigEckSU3SO3CGTables[i]->GetNumberSU3SO3CGs() << std::endl;
//		m_WigEckSU3SO3CGTables[i]->Show();
	}
	std::cout << "Totally " << nTotal << " of SU3SU3 Wigner coefficients == " << sizeof(SU3::WIGNER)*nTotal/1024.0 << " kB." << std::endl; 
}
