#ifndef LOCK_HASH_H
#define LOCK_HASH_H

struct no_locking
{
  void acquire(){}
  void release(){}
};


#include <omp.h>
#ifndef TIME_LOCK
#include <boost/chrono.hpp> 
#endif


class openmp_locking
{
  omp_lock_t writelock;
  

 public:
#ifdef TIME_LOCK
  static boost::chrono::duration<double> locktime[1024];
#endif

  openmp_locking()
  {
    omp_init_lock(&writelock);
  }


  void acquire(){
#ifdef TIME_LOCK
    boost::chrono::system_clock::time_point start = boost::chrono::system_clock::now();    
#endif
    omp_set_lock(&writelock);
#ifdef TIME_LOCK
    boost::chrono::duration<double> duration = boost::chrono::system_clock::now() - start;
    locktime[omp_get_thread_num()] += duration;
#endif
  }
  
  void release(){
    omp_unset_lock(&writelock);
  }

  ~openmp_locking()
  {
    omp_destroy_lock(&writelock);
  }
 
};

#endif
