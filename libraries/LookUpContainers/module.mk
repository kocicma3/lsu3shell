$(eval $(begin-module))

################################################################
# unit definitions
################################################################

module_units_h := CWig9lmLookUpTable HashFixed LRUCache lock\

module_units_cpp-h := CSSTensorRMELookUpTablesContainer WigEckSU3SO3CGTable CSU39lm  CWig6lmLookUpTable lock
# module_units_f := 
# module_programs_cpp :=

################################################################
# library creation flag
################################################################

$(eval $(library))

################################################################
# special variable assignments, rules, and dependencies
################################################################


$(eval $(end-module))
