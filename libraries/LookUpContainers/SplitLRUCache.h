#ifndef CACHE_SPLIT
#define CACHE_SPLIT

#include <LookUpContainers/LRUCache.h>


template <typename Key, typename Value, typename Hasher = CombineHash, typename Policy = no_eviction_policy<Key,Value>, typename Locking = openmp_locking, bool DEBUG=false , bool PROFILE=false, unsigned int ignore_bit_in_hash=4>
  class SplitLRUCache {
  
  typedef LRUCache<Key, Value, Hasher, Policy, Locking, DEBUG, PROFILE, ignore_bit_in_hash> lrucache;

  lrucache* thecaches[1<<(ignore_bit_in_hash+1)];

  std::size_t mask;

  Hasher h;

  std::size_t which_cache(const Key& k) const {
    return h(k)&mask;
  }    

 public:
  typedef typename LRUCache<Key, Value, Hasher, Policy, Locking, DEBUG, PROFILE, ignore_bit_in_hash>::iterator iterator;
  
  iterator end() const {
    return iterator(-1, NULL);
  }

  void ReleaseMemory() {
    for (int c = 0; c <= 1<<ignore_bit_in_hash; ++c) {
      if (thecaches[c]) {delete thecaches[c]; thecaches[c] = NULL;}
    }
    
  }

  void insert (const Key& k, const Value & v) {
    thecaches[which_cache(k)] -> insert (k,v);
  }

  iterator find (const Key& k){
    return thecaches[which_cache(k)] -> find(k);
  }

  SplitLRUCache (int size) {
    assert (ignore_bit_in_hash>0);
    
    mask = 1;
    for (int i=1; i< ignore_bit_in_hash; ++i)
      mask = mask <<1 | 1;
    
    for (int c = 0; c <= 1<<ignore_bit_in_hash; ++c) {
      thecaches[c] = new lrucache(size>>ignore_bit_in_hash);
   }
  }

  ~SplitLRUCache() {
    ReleaseMemory();
  }
  

  /** provide the memory requirements for the SplitLRUCache if you
   *  construct it with the nelements parameter to the
   *  constructor. This assumes that Key and Value are static type
   *  that do not contain dynamically allocated memory.
  **/
  static size_t memory_requirements(size_t nelements)
  {
    return (1<<ignore_bit_in_hash)*lrucache::memory_requirements(nelements>>ignore_bit_in_hash) + (1<<(ignore_bit_in_hash+1))*sizeof(lrucache*)+sizeof(std::size_t);
  }

  /**
   *  provide the memory requirements for the SplitLRUCache. This
   *  assumes that Key and Value are static type that do not contain
   *  dynamically allocated memory. This one is for a particular
   *  instance of the LRUCache.
   **/
  size_t static_memory_usage() const
  {
    size_t sum = 0;
    for (int c = 0; c <= 1<<ignore_bit_in_hash; ++c) {
      sum += thecaches[c]->static_memory_usage();
    }
    return sum;
  }
  /**
   * Computes the memory usage of a particular instance of
   * LRUCache. It uses a functor passed as a parameter to compute the
   * size of all the objects contained within the Cache. It calls
   * size_t SizeOf::operator (const Key&, const Value&) function to
   * obtain the size of the dynamically allocated memory of a given
   * (Key,Value) pair. The returned value also takes into
   * consideration the "static" footprint of the cache.
   *
   * See the end of this file for a simple example on how to use it.
   **/
  template < typename SizeOf >
  size_t dynamic_memory_usage(SizeOf& so) const
  {
    std::size_t sum = 0;
    for (int c = 0; c <= 1<<ignore_bit_in_hash; ++c) {
      sum += thecaches[c]->dynamic_memory_usage(so);
    }
    return sum;
  }
};

#endif
