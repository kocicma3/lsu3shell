#include <LookUpContainers/CSU39lm.h>

/** 
 * Calculates 9(l m) symbols using wru3optimized and wzu3optimized functions, that is, without using LRU cache for storing 6(l m) symbols
 * This method is not intended to be used in a production code and serves just for testing/debugging purposes.
 */ 
void Get9lmWithout6lmCache(const SU3::LABELS& ir1, const SU3::LABELS& ir2, const SU3::LABELS& ir12,
									const SU3::LABELS& ir3, const SU3::LABELS& ir4, const SU3::LABELS& ir34, 
									const SU3::LABELS& ir13, const SU3::LABELS& ir24, const SU3::LABELS& ir, 
									double* su39lm)
{
	const int num_6lm_max(MAX_K*MAX_K*MAX_K*MAX_K);
	double u6_a[num_6lm_max], u6_b[num_6lm_max], u6_c[num_6lm_max];

	int lm1(ir1.lm), mu1(ir1.mu), lm2(ir2.lm), mu2(ir2.mu), lm3(ir3.lm), mu3(ir3.mu), lm4(ir4.lm), mu4(ir4.mu); 
	int lm12(ir12.lm), mu12(ir12.mu), lm34(ir34.lm), mu34(ir34.mu), lm13(ir13.lm), mu13(ir13.mu), lm24(ir24.lm), mu24(ir24.mu), lm(ir.lm), mu(ir.mu); 
	int lm0, mu0;

	int rho12max 	= SU3::mult(ir1, ir2, ir12);
	int rho34max 	= SU3::mult(ir3, ir4, ir34);
	int rho13max 	= SU3::mult(ir1, ir3, ir13);
	int rho24max 	= SU3::mult(ir2, ir4, ir24);
	int rho1234max	= SU3::mult(ir12, ir34, ir);
	int rho1324max	= SU3::mult(ir13, ir24, ir);
	size_t nsu39lm = rho12max*rho34max*rho13max*rho24max*rho1234max*rho1324max;

	int rho123max, rho04max, rho132max; // multiplicities for 6lm and z6lm coefficients

	size_t index_9lm, index_u6_a, index_u6_b, index_u6_c;

	size_t n1_a, n2_a, n3_a;
	size_t n1_b, n2_b, n3_b;
	size_t n1_c, n2_c, n3_c;

	int num_6lm_a, num_6lm_b, num_6lm_c;

	memset(su39lm, 0, nsu39lm*sizeof(double));

	SU3_VEC ir0_vector;
	SU3::Couple(ir13, ir2, ir0_vector);

	for (SU3_VEC::const_iterator ir0 = ir0_vector.begin(); ir0 != ir0_vector.end(); ++ir0)
	{
		rho123max = SU3::mult(ir12, ir3, *ir0);
		if (!rho123max) {
			continue;
		}
		rho04max = SU3::mult(*ir0, ir4, ir);
		if (!rho04max) {
			continue;
		}
		rho132max = ir0->rho;
		lm0 = ir0->lm;
		mu0 = ir0->mu;
   
		num_6lm_a = rho132max*rho04max*rho24max*rho1324max;
		num_6lm_b = rho12max*rho123max*rho13max*rho132max;
		num_6lm_c = rho123max*rho04max*rho34max*rho1234max;

		memset(u6_a, 0, num_6lm_a*sizeof(double));
		memset(u6_b, 0, num_6lm_b*sizeof(double));
		memset(u6_c, 0, num_6lm_c*sizeof(double));
#ifndef AIX
		wru3optimized_(lm13, mu13, lm2, mu2, lm, mu, lm4, mu4, lm0, mu0, lm24, mu24, rho132max, rho04max, rho24max, rho1324max, u6_a, num_6lm_a);
		wzu3optimized_(lm2, mu2, lm1, mu1, lm0, mu0, lm3, mu3, lm12, mu12, lm13, mu13, rho12max, rho123max, rho13max, rho132max, u6_b, num_6lm_b);
		wru3optimized_(lm12, mu12, lm3, mu3, lm, mu, lm4, mu4, lm0, mu0, lm34, mu34, rho123max, rho04max, rho34max, rho1234max, u6_c, num_6lm_c);
#else
		wru3optimized(lm13, mu13, lm2, mu2, lm, mu, lm4, mu4, lm0, mu0, lm24, mu24, rho132max, rho04max, rho24max, rho1324max, u6_a, num_6lm_a);
		wzu3optimized(lm2, mu2, lm1, mu1, lm0, mu0, lm3, mu3, lm12, mu12, lm13, mu13, rho12max, rho123max, rho13max, rho132max, u6_b, num_6lm_b);
		wru3optimized(lm12, mu12, lm3, mu3, lm, mu, lm4, mu4, lm0, mu0, lm34, mu34, rho123max, rho04max, rho34max, rho1234max, u6_c, num_6lm_c);
#endif

		index_9lm = 0;

		n1_a = rho132max;
		n2_a = rho04max*n1_a;
		n3_a = rho24max*n2_a;

		n1_b = rho12max;
		n2_b = rho123max*n1_b;
		n3_b = rho13max*n2_b;

		n1_c = rho123max;
		n2_c = rho04max*n1_c;
		n3_c = rho34max*n2_c;

		for (size_t rho1324 = 0; rho1324 < rho1324max; ++rho1324)
		{
			for (size_t rho24 = 0; rho24 < rho24max; ++rho24)
			{
				for (size_t rho13 = 0; rho13 < rho13max; ++rho13)
				{
					for (size_t rho1234 = 0; rho1234 < rho1234max; ++rho1234)
					{
						for (size_t rho34 = 0; rho34 < rho34max; ++rho34)
						{
							for (size_t rho12 = 0; rho12 < rho12max; ++rho12, ++index_9lm)
							{
								for (size_t rho04 = 0; rho04 < rho04max; ++rho04)
								{
									for (size_t rho132 = 0; rho132 < rho132max; ++rho132)
									{
										for (size_t rho123 = 0; rho123 < rho123max; ++rho123)
										{
											index_u6_a = rho1324*n3_a + rho24*n2_a + rho04*n1_a  + rho132;
											index_u6_c = rho1234*n3_c + rho34*n2_c + rho04*n1_c  + rho123;
											index_u6_b = rho132*n3_b  + rho13*n2_b + rho123*n1_b + rho12;

											su39lm[index_9lm] += u6_a[index_u6_a]*u6_b[index_u6_b]*u6_c[index_u6_c];
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
}

void Get9lmWithout6lmCache(const SU3::LABELS& ir1, const SU3::LABELS& ir2, const SU3::LABELS& ir12,
									const SU3::LABELS& ir3, const SU3::LABELS& ir4, const SU3::LABELS& ir34, 
									const SU3::LABELS& ir13, const SU3::LABELS& ir24, const SU3::LABELS& ir, 
									const size_t number_9lm, float* su39lm)
{
	double tmp[number_9lm];
	Get9lmWithout6lmCache(ir1, ir2, ir12, ir3,  ir4, ir34, ir13, ir24, ir, tmp);
	std::copy(tmp, tmp + number_9lm, su39lm);
}
