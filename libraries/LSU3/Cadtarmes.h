#ifndef LSU3_CADTARMES_H
#define LSU3_CADTARMES_H
#include <SU3ME/CRMECalculator.h>
#include <SU3ME/CTensorStructure.h>
#include <LookUpContainers/CSSTensorRMELookUpTablesContainer.h>
#include <SU3ME/CInteractionPN.h>

#include <LSU3/types.h>
#include <LSU3/ncsmSU3xSU2Basis.h>
#include <LSU3/Cadta_table.h>

namespace lsu3
{

class Cadtarmes
{
	public:
	enum {kTensorIdPtr = 0, kRmesPtr = 1, kNumberOfTensors = 2};
	typedef cpp0x::tuple<uint32_t, uint32_t, uint16_t> RMES_RECORD;

	void add(const uint32_t bra_index, const uint32_t ket_index, const std::vector<uint32_t>& tensor_ids, const std::vector<float>& rmes);

	inline const RMES_RECORD& get(const uint32_t bra_index, const uint32_t ket_index) const
	{
		cpp0x::unordered_map<uint64_t, RMES_RECORD>::const_iterator it = data_.find(key(bra_index, ket_index));
		return (it != data_.end()) ? it->second : empty_rms_record_;
	}

	inline const RMES_RECORD& get(const std::pair<uint32_t, uint32_t>&  ij_pair) const
	{
		cpp0x::unordered_map<uint64_t, RMES_RECORD>::const_iterator it = data_.find(key(ij_pair.first, ij_pair.second));
		return (it != data_.end()) ? it->second : empty_rms_record_;
	}

	inline uint32_t rmes_size() const {return rmes_.size();}
	inline uint32_t tensor_ids_size() const {return tensor_ident_numbers_.size();}
	inline const float* rmes_begin() const {return &rmes_[0];}
	inline const uint32_t* tensor_ids_begin() const {return &tensor_ident_numbers_[0];}

//	This method checks whether a given RMES_RECORD carry vanishing number of tensors	
	inline static bool IsEmpty(const RMES_RECORD& rmes_record) {return cpp0x::get<kNumberOfTensors>(rmes_record) == 0;}
	private:
	inline uint64_t key(const uint32_t bra_index, const uint32_t ket_index) const { return ((uint64_t)bra_index << 32) | (uint64_t)ket_index; }
	private:
//	triplet associated with a non-existent key
	static RMES_RECORD empty_rms_record_;
//	hash table of key index and associated triplet: first index in tensor
//	array, first index in rmes array, and number of tensors. 
	cpp0x::unordered_map<uint64_t, RMES_RECORD> data_;
//	array with reduced matrix elements	
	std::vector<float> rmes_;
//	array with tensor indices that point to an external table of (lm0 mu0) 2S0
//	quantum labels
	std::vector<uint32_t> tensor_ident_numbers_;
};

// function that calculates rmes and stores them in Cadtarmes container class 
void Calculate_adta_rmes( 
// INPUT
//	nucleon::NEUTRON or nucleon::PROTON 
				const int32_t nucleon_type, 
				const CBaseSU3Irreps& baseSU3Irreps,
//	map that associates (n1, n2) with (lm0 mu0)S0 tensor labels of (a+a) tensor				
				const std::map<std::pair<int8_t, int8_t>, SU3xSU2_VEC> adta_tensors,
				const std::vector<std::pair<uint32_t, uint32_t> > ipjp_pairs,
				const CncsmSU3xSU2Basis& bra_basis,
				const CncsmSU3xSU2Basis& ket_basis, 
				const bool generate_missing_rme_files, const bool log_is_on,
				const Cadta_table& adta_table, 
// OUTPUT				
				Cadtarmes& rmes);
}	// namespace lsu3
#endif
