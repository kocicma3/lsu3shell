#pragma once

// Definition of Fortran "module" variable naming conventions
#if (defined __GNUC__ && !defined __INTEL_COMPILER)
#define FORTRAN_MODULE_VARIABLE(s) __nodeinfo_MOD_##s
#elif (defined __INTEL_COMPILER)
#define FORTRAN_MODULE_VARIABLE(s) nodeinfo_mp_##s##_
#else
#define FORTRAN_MODULE_VARIABLE(s) __nodeinfo_mp_##s_
#endif

extern "C"
{
   extern void matrix_diagonalize_coo_(
         int64_t* ncols, int64_t* nrows, int64_t* nnz,
         float* xme, int32_t* colind, int32_t* rowind,
         int32_t* maxitr, float* tol,
         int32_t* neigen, float* eigenvals, float* eigenvectors,
         int64_t* nstates_sofar);

   extern void splitprocs_();

   extern int FORTRAN_MODULE_VARIABLE(myrank);
   extern int FORTRAN_MODULE_VARIABLE(mypid);
   extern int FORTRAN_MODULE_VARIABLE(nproc);
   extern int FORTRAN_MODULE_VARIABLE(ndiag);
   extern int FORTRAN_MODULE_VARIABLE(icomm);
}
