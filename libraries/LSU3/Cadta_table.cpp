#include <LSU3/Cadta_table.h>

#include <map>
#include <set>

namespace lsu3
{

Cadta_table::Cadta_table(const std::map<std::pair<int8_t, int8_t>, SU3xSU2_VEC>& proton_adta, const std::map<std::pair<int8_t, int8_t>, SU3xSU2_VEC>& neutron_adta) 
{
	std::set<LmMuS2> data;
	std::map<std::pair<int8_t, int8_t>, SU3xSU2_VEC>::const_iterator it;

	for (it = proton_adta.begin(); it != proton_adta.end(); ++it)
	{
		const SU3xSU2_VEC& tensors = it->second;
		for (int i = 0; i < tensors.size(); ++i)
		{
			data.insert(cpp0x::tie(tensors[i].lm, tensors[i].mu, tensors[i].S2));
		}
	}

	for (it = neutron_adta.begin(); it != neutron_adta.end(); ++it)
	{
		const SU3xSU2_VEC& tensors = it->second;
		for (int i = 0; i < tensors.size(); ++i)
		{
			data.insert(cpp0x::tie(tensors[i].lm, tensors[i].mu, tensors[i].S2));
		}
	}
	data_.insert(data_.begin(), data.begin(), data.end());
}

} // namespace lsu3
