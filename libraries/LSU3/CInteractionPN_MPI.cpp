#include <iterator>
#include <vector>

#include <mascot/DataSet.h>
#include <mascot/File.h>


#include "CInteractionPN_MPI.h"

namespace lsu3
{

uint64_t CInteractionPN::get_nCoeffs(T_lm lm, T_mu mu, T_S2 s2) 
{
    return 1 * SU3::kmax(SU3::LABELS(1, lm, mu), s2 / 2);
}

void CInteractionPN::readFile(const std::string& fileName, MPI_Comm comm, float alpha)
{
    int rank;
    MPI_Comm_rank(comm, &rank);

    // only root read the file
    if (0 != rank)
        return;

    mascot::File* file;

    mascot::DataSet* d_n;
    mascot::DataSet* d_nTensors;
    mascot::DataSet* d_irholmmuS2;
    mascot::DataSet* d_coeffs;
    
    file = mascot::File::open(fileName);

    d_n          = file->openDataSet("n",          1024);
    d_nTensors   = file->openDataSet("nTensors",   1024);
    d_irholmmuS2 = file->openDataSet("irholmmuS2", 1024);
    d_coeffs     = file->openDataSet("coeffs",     1024);

    uint64_t nRecords = d_nTensors->getLength();

    for (uint64_t iRecord = 0; iRecord < nRecords; ++iRecord) {
        T_n n[6];
        for (int k = 0; k < 6; ++k)
            d_n->read(n[k]);

        uint32_t nTensors;
        d_nTensors->read(nTensors);
        for (uint32_t iTensor = 0; iTensor < nTensors; ++iTensor) {
            std::vector<uint8_t> irholmmuS2(12);
            for (int k = 0; k < 12; ++k)
                d_irholmmuS2->read(irholmmuS2[k]);

            PNIntKey key;
            get_na(key)  = n[0];
            get_nb(key)  = n[1];

            get_lmp(key) = irholmmuS2[ 1];
            get_mup(key) = irholmmuS2[ 2];
            get_S2p(key) = irholmmuS2[ 3];

            get_nc(key)  = n[3];
            get_nd(key)  = n[4];

            get_lmn(key) = irholmmuS2[ 5];
            get_mun(key) = irholmmuS2[ 6];
            get_S2n(key) = irholmmuS2[ 7];
 
            get_lm0(key) = irholmmuS2[ 9];
            get_mu0(key) = irholmmuS2[10];
            get_S20(key) = irholmmuS2[11];

            uint64_t nCoeffs = get_nCoeffs(irholmmuS2[9],
                                           irholmmuS2[10], irholmmuS2[11]);

            if (pnIntTemp_.end() == pnIntTemp_.find(key)) {
                pnIntTemp_[key] = coeffs_.size();

                for (uint64_t iCoeff = 0; iCoeff < nCoeffs; ++iCoeff) {
                    float f;
                    d_coeffs->read(f);
                    coeffs_.push_back(f * alpha);
                }
            }
            else {
                uint64_t pos = pnIntTemp_[key];
                for (uint64_t iCoeff = 0; iCoeff < nCoeffs; ++iCoeff) {
                    float f;
                    d_coeffs->read(f);
                    coeffs_[pos + iCoeff] += f * alpha;
                }
            }
        }
    }

    file->closeDataSet(d_n);
    file->closeDataSet(d_nTensors);
    file->closeDataSet(d_irholmmuS2);
    file->closeDataSet(d_coeffs);

    mascot::File::close(file); 

#ifdef DEEP_DEBUG
    for (T_pnIntTemp_::const_iterator record = pnIntTemp_.begin(); record != pnIntTemp_.end(); ++record) {
        const PNIntKey& key = (*record).first;
        std::cout << "((("
                  << (int)get_na (key) << ", "
                  << (int)get_nb (key) << "),("
                  << (int)get_lmp(key) << ", "
                  << (int)get_mup(key) << ", "
                  << (int)get_S2p(key) << ")),(("
                  << (int)get_nc (key) << ", "
                  << (int)get_nd (key) << "),("
                  << (int)get_lmn(key) << ", "
                  << (int)get_mun(key) << ", "
                  << (int)get_S2n(key) << ")),("
                  << (int)get_lm0(key) << ", "
                  << (int)get_mu0(key) << ", "
                  << (int)get_S20(key) << ")): ";

        uint64_t nCoeffs = get_nCoeffs(get_lm0(key), get_mu0(key), get_S20(key));
        uint64_t pos = (*record).second;
        for (uint64_t k = 0; k < nCoeffs; ++k)
            std::cout << coeffs_[pos + k] << ", "; 
        std::cout << std::endl;
    }
    std::cout << std::endl;
#endif
}

void CInteractionPN::finalize(MPI_Comm comm)
{
    int rank;
    MPI_Comm_rank(comm, &rank);

    if (0 == rank) {
        for (T_pnIntTemp_::const_iterator record = pnIntTemp_.begin(); record != pnIntTemp_.end(); ++record) {
            const PNIntKey& key = (*record).first;
            temp_p_.insert(get_NNLmMuS2_p(key));
            temp_n_.insert(get_NNLmMuS2_n(key));
        }

#ifdef DEEP_DEBUG
        for (T_temp_::const_iterator  record = temp_p_.begin(); record != temp_p_.end(); ++record) {
            const NNLmMuS2& nnlmmus2 = *record;

            std::cout << std::distance(temp_p_.begin(), record) << ": "
                      << "(("
                      << (int)get_n1(nnlmmus2) << ", "
                      << (int)get_n2(nnlmmus2) << "),("
                      << (int)get_lm(nnlmmus2) << ", "
                      << (int)get_mu(nnlmmus2) << ", "
                      << (int)get_S2(nnlmmus2) << "))" << std::endl;
        }
        std::cout << std::endl;

        for (T_temp_::const_iterator record = temp_n_.begin(); record != temp_n_.end(); ++record) {
            const NNLmMuS2& nnlmmus2 = *record;

            std::cout << std::distance(temp_n_.begin(), record) << ": "
                      << "(("
                      << (int)get_n1(nnlmmus2) << ", "
                      << (int)get_n2(nnlmmus2) << "),("
                      << (int)get_lm(nnlmmus2) << ", "
                      << (int)get_mu(nnlmmus2) << ", "
                      << (int)get_S2(nnlmmus2) << "))" << std::endl;
        }
        std::cout << std::endl;
#endif
    }
        
    // send protons table
    uint64_t size_p;
    if (0 == rank) 
        size_p = temp_p_.size();
    MPI_Datatype mdt = boost::mpi::get_mpi_datatype(size_p);
    MPI_Bcast(&size_p, 1, mdt, 0, comm);

    na_ .resize(size_p);
    nb_ .resize(size_p);
    lmp_.resize(size_p);
    mup_.resize(size_p);
    s2p_.resize(size_p);

    if (0 == rank) {
        uint64_t k = 0;
        for (T_temp_::const_iterator record = temp_p_.begin(); record != temp_p_.end(); ++record) {
            const NNLmMuS2& nnlmmus2 = *record;
            na_ [k] = get_n1(nnlmmus2);
            nb_ [k] = get_n2(nnlmmus2);
            lmp_[k] = get_lm(nnlmmus2);
            mup_[k] = get_mu(nnlmmus2);
            s2p_[k] = get_S2(nnlmmus2);
            ++k;
        }
    }

    mdt = boost::mpi::get_mpi_datatype(na_ [0]);
    MPI_Bcast(&na_ [0], size_p, mdt, 0, MPI_COMM_WORLD);
    mdt = boost::mpi::get_mpi_datatype(nb_ [0]);
    MPI_Bcast(&nb_ [0], size_p, mdt, 0, MPI_COMM_WORLD);
    mdt = boost::mpi::get_mpi_datatype(lmp_[0]);
    MPI_Bcast(&lmp_[0], size_p, mdt, 0, MPI_COMM_WORLD);
    mdt = boost::mpi::get_mpi_datatype(mup_[0]);
    MPI_Bcast(&mup_[0], size_p, mdt, 0, MPI_COMM_WORLD);
    mdt = boost::mpi::get_mpi_datatype(s2p_[0]);
    MPI_Bcast(&s2p_[0], size_p, mdt, 0, MPI_COMM_WORLD);

    // send neutron table
    uint64_t size_n;
    if (0 == rank) 
        size_n = temp_n_.size();
    mdt = boost::mpi::get_mpi_datatype(size_n);
    MPI_Bcast(&size_n, 1, mdt, 0, comm);

    nc_ .resize(size_n);
    nd_ .resize(size_n);
    lmn_.resize(size_n);
    mun_.resize(size_n);
    s2n_.resize(size_n);

    if (0 == rank) {
        uint64_t k = 0;
        for (T_temp_::const_iterator record = temp_n_.begin(); record != temp_n_.end(); ++record) {
            const NNLmMuS2& nnlmmus2 = *record;
            nc_ [k] = get_n1(nnlmmus2);
            nd_ [k] = get_n2(nnlmmus2);
            lmn_[k] = get_lm(nnlmmus2);
            mun_[k] = get_mu(nnlmmus2);
            s2n_[k] = get_S2(nnlmmus2);
            ++k;
        }
    }

    mdt = boost::mpi::get_mpi_datatype(nc_ [0]);
    MPI_Bcast(&nc_ [0], size_p, mdt, 0, MPI_COMM_WORLD);
    mdt = boost::mpi::get_mpi_datatype(nd_ [0]);
    MPI_Bcast(&nd_ [0], size_p, mdt, 0, MPI_COMM_WORLD);
    mdt = boost::mpi::get_mpi_datatype(lmn_[0]);
    MPI_Bcast(&lmn_[0], size_p, mdt, 0, MPI_COMM_WORLD);
    mdt = boost::mpi::get_mpi_datatype(mun_[0]);
    MPI_Bcast(&mun_[0], size_p, mdt, 0, MPI_COMM_WORLD);
    mdt = boost::mpi::get_mpi_datatype(s2n_[0]);
    MPI_Bcast(&s2n_[0], size_p, mdt, 0, MPI_COMM_WORLD);
}

void CInteractionPN::get_map_p(CInteractionPN::Map& map_p, MPI_Comm comm) const
{
    int rank;
    MPI_Comm_rank(comm, &rank);

    for (size_t k = 0; k < na_.size(); ++k) {
        std::pair<T_n, T_n> key(na_[k], nb_[k]);
        SU3xSU2::LABELS labels(1, lmp_[k], mup_[k], s2p_[k]);

        map_p[key].push_back(labels);
    }

#ifdef DEEP_DEBUG
    if (0 == rank) {
        for (Map::const_iterator record = map_p.begin(); record != map_p.end(); ++record) {
            std::pair<T_n, T_n> key = (*record).first;
            SU3xSU2_VEC vec = (*record).second;

            std::cout << "(" << (int)key.first << ", " << (int)key.second << "): ";
            for (size_t k = 0; k < vec.size(); ++k) {
                const SU3xSU2::LABELS& labels = vec[k];
                std::cout << "(" << (int)labels.lm << ", "
                    << (int)labels.mu << ", " << (int)labels.S2 << "), ";
            }
            std::cout << std::endl;
        }
        std::cout << std::endl;
    }
#endif
}

void CInteractionPN::get_map_n(CInteractionPN::Map& map_n, MPI_Comm comm) const
{
    int rank;
    MPI_Comm_rank(comm, &rank);

    for (size_t k = 0; k < nc_.size(); ++k) {
        std::pair<T_n, T_n> key(nc_[k], nd_[k]);
        SU3xSU2::LABELS labels(1, lmn_[k], mun_[k], s2n_[k]);

        map_n[key].push_back(labels);
    }

#ifdef DEEP_DEBUG
    if (0 == rank) {
        for (Map::const_iterator record = map_n.begin(); record != map_n.end(); ++record) {
            std::pair<T_n, T_n> key = (*record).first;
            SU3xSU2_VEC vec = (*record).second;

            std::cout << "(" << (int)key.first << ", " << (int)key.second << "): ";
            for (size_t k = 0; k < vec.size(); ++k) {
                const SU3xSU2::LABELS& labels = vec[k];
                std::cout << "(" << (int)labels.lm << ", "
                    << (int)labels.mu << ", " << (int)labels.S2 << "), ";
            }
            std::cout << std::endl;
        }
        std::cout << std::endl;
    }
#endif
}

} // namespace lsu3
