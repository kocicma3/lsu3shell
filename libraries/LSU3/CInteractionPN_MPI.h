#ifndef LSU3_CINTERACTIONPN_H
#define LSU3_CINTERACTIONPN_H

#include <functional>
#include <map>
#include <ostream>
#include <set>
#include <string>
#include <utility>
#include <vector>

#include <UNU3SU3/UNU3SU3Basics.h>

#include "std.h"
#include "types.h"

namespace lsu3
{
    namespace aux
    {
        struct ihash : std::unary_function<PNIntKey, std::size_t>
        {
            std::size_t operator()(const PNIntKey& key) const
            {
                return hash_combine(key);
            }
        };

        struct iequal_to : std::binary_function<PNIntKey, PNIntKey, bool>
        {
            bool operator()(const PNIntKey& key1, const PNIntKey& key2) const 
            {
                return (key1 == key2);
            }
        };
    }

    class CInteractionPN
    {
        public:
            typedef std::map<std::pair<T_n, T_n>, SU3xSU2_VEC> Map;

            static uint64_t get_nCoeffs(T_lm lm, T_mu mu, T_S2 s2);

            void readFile(const std::string& fileName, MPI_Comm comm, float alpha = 1.0);

            void finalize(MPI_Comm comm);

            void get_map_p(Map& map_p, MPI_Comm comm) const;
            void get_map_n(Map& map_n, MPI_Comm comm) const;

        private:
            typedef cpp0x::unordered_map<PNIntKey, uint64_t, aux::ihash, aux::iequal_to> T_pnIntTemp_;
            T_pnIntTemp_ pnIntTemp_;
            std::vector<float> coeffs_;

            typedef std::set<NNLmMuS2> T_temp_;
            T_temp_ temp_p_, temp_n_;

            std::vector<T_n>  na_, nb_, nc_, nd_;
            std::vector<T_lm> lmp_, lmn_;
            std::vector<T_mu> mup_, mun_;
            std::vector<T_S2> s2p_, s2n_;

    };
}

#endif
