#pragma once

#ifdef IT_DEBUG
   #include <mpi.h>

   #include <iostream>
   #include <string>
   #include <vector>

   #include <unistd.h>

   // color printing modifiers:
   const std::string red("\033[0;31m");
   const std::string green("\033[1;32m");
   const std::string yellow("\033[1;33m");
   const std::string cyan("\033[0;36m");
   const std::string magenta("\033[0;35m");
   const std::string reset("\033[0m");
#endif

#ifdef IT_DEBUG
   // debugging for 12C_Nmax0 and 3 diagonal processes:
   #define IT_DEBUG_SET_BITMASK() \
      do { \
         assert(nprocs_ == 6); \
         \
         bra_bitmask_.clear(); \
         bra_bitmask_.resize(bra_dim_, 1); \
         ket_bitmask_.clear(); \
         ket_bitmask_.resize(ket_dim_, 1); \
         \
         switch (rank_) { \
            case 0: \
               bra_bitmask_[0] = bra_bitmask_[1] = bra_bitmask_[3] = 0; \
               ket_bitmask_[0] = ket_bitmask_[1] = ket_bitmask_[3] = 0; \
               break; \
            \
            case 1: \
               bra_bitmask_[0] = 0; \
               ket_bitmask_[0] = 0; \
               break; \
            \
            case 2: \
               bra_bitmask_[1] = 0; \
               ket_bitmask_[1] = 0; \
               break; \
            \
            case 3: \
               bra_bitmask_[0] = bra_bitmask_[1] = bra_bitmask_[3] = 0; \
               ket_bitmask_[0] = 0; \
               break; \
            \
            case 4: \
               bra_bitmask_[0] = 0; \
               ket_bitmask_[1] = 0; \
               break; \
            \
            case 5: \
               bra_bitmask_[0] = bra_bitmask_[1] = bra_bitmask_[3] = 0; \
               ket_bitmask_[1] = 0; \
               break; \
         } \
      } while (0)
#else
   #define IT_DEBUG_SET_BITMASK() do { } while (0)
#endif

#ifdef IT_DEBUG
   // print out multiplication and transposed multiplication results:
   #define IT_DEBUG_PMTM_RESULTS() \
   do { \
      if (rank_ == 0) \
         std::cerr << "Multiplication results: " << std::endl; \
      for (int p = 0; p < nprocs_; p++) { \
         if (rank_ == p) { \
            std::cerr << yellow << "Process " << rank_ << ": " << reset << std::endl; \
            \
            for (int i = 0; i < coo_.nrows; i++) { \
               for (int j = 0; j < 1; j++)  \
                  std::cerr << magenta << x[coo_.nrows * j + i] << " "; \
               std::cerr << std::endl; \
            } \
            std::cerr << reset; \
         } \
         usleep(10000); \
         MPI_Barrier(comm_); \
      } \
      \
      if (rank_ == 0) \
         std::cerr << "Transposed multiplication results: " << std::endl; \
      for (int p = 0; p < nprocs_; p++) { \
         if (rank_ == p) { \
            std::cerr << yellow << "Process " << rank_ << ": " << reset << std::endl; \
            \
            for (int i = 0; i < coo_.ncols; i++) { \
               for (int j = 0; j < 1; j++) \
                  std::cerr << magenta << x_T[coo_.ncols * j + i] << " "; \
               std::cerr << std::endl; \
            } \
            std::cerr << reset; \
         } \
         usleep(10000); \
         MPI_Barrier(comm_); \
      } \
   } while (0)
#else 
   #define IT_DEBUG_PMTM_RESULTS() do { } while (0)
#endif

#ifdef IT_DEBUG
   // print out reduced multiplication results:
   #define IT_DEBUG_PRM_RESULTS() \
   do { \
      if (rank_ == 0) \
         std::cerr << "Reduced multiplication results: " << std::endl; \
      for (int p = 0; p < ndiag_; p++) { \
         if (rank_ == p) { \
            for (int i = 0; i < coo_.nrows; i++) { \
               for (int j = 0; j < neigen; j++)  \
                  std::cerr << magenta << x_[coo_.nrows * j + i] << " "; \
               std::cerr << std::endl; \
            } \
            std::cerr << reset; \
         } \
         usleep(10000); \
         MPI_Barrier(comm_); \
      } \
   } while (0)
#else
   #define IT_DEBUG_PRM_RESULTS() do { } while (0)
#endif

#ifdef IT_DEBUG
   // print out reduced matrix as a 2d array:
   #define IT_DEBUG_PRINT_REDUCED() \
   do { \
      long m = (rank_ < ndiag_) ? reduced_.nrows : 0; \
      long n = (rank_ < ndiag_) ? reduced_.ncols : 0; \
      MPI_Allreduce(MPI_IN_PLACE, &m, 1, MPI_LONG, MPI_SUM, MPI_COMM_WORLD); \
      MPI_Allreduce(MPI_IN_PLACE, &n, 1, MPI_LONG, MPI_SUM, MPI_COMM_WORLD); \
      assert(m == n); \
      \
      std::vector<float> mat2d(n * n, 0.0); \
      for (long i = 0; i < reduced_.nnz; i++) { \
         long row = reduced_.irow + reduced_.rows[i]; \
         long col = reduced_.icol + reduced_.cols[i]; \
         mat2d[row * n + col] += reduced_.vals[i]; \
         if (row != col) \
            mat2d[col * n + row] += reduced_.vals[i]; \
      } \
      MPI_Allreduce(MPI_IN_PLACE, mat2d.data(), mat2d.size(), MPI_FLOAT, MPI_SUM, MPI_COMM_WORLD); \
      if (rank_ == 0) { \
         std::cerr << green << "Reduced matrix = [ " << std::endl; \
         for (long row = 0; row < n; row++) { \
            for (long col = 0; col < n; col++) \
               std::cerr << mat2d[row * n + col] << " "; \
            std::cerr << std::endl; \
         } \
         std::cerr << "]" << reset << std::endl; \
      } \
   } while (0)
#else
   #define IT_DEBUG_PRINT_REDUCED() do { } while (0)
#endif 

#ifdef IT_DEBUG
   // check first row and column
   #define IT_DEBUG_PRINT_IROWCOL() \
   do { \
      for (int p = 0; p < nprocs_; p++) { \
         if (rank_ == p) \
            std::cerr << "Process " << cyan << rank_ << reset << ": " \
               << yellow << "irow = " << reduced_.irow << ", icol = " << reduced_.icol  \
               << reset << std::endl; \
         MPI_Barrier(MPI_COMM_WORLD); \
      } \
   } while (0)
#else
   #define IT_DEBUG_PRINT_IROWCOL() do { } while (0)
#endif

#ifdef IT_DEBUG
   // print out eigenvectors:
   #define IT_DEBUG_PRINT_EIGVECS() \
   do { \
      if (rank_ == 0) \
         std::cerr << "Returned eigenvectors: " << std::endl; \
      for (int p = 0; p < ndiag_; p++) { \
         if (rank_ == p) { \
            for (int i = 0; i < reduced_.nrows; i++) { \
               for (int j = 0; j < neigen; j++)  \
                  std::cerr << cyan << eigenvectors[reduced_.nrows * j + i] << " "; \
               std::cerr << std::endl; \
            } \
            std::cerr << reset; \
         } \
         usleep(10000); \
         MPI_Barrier(MPI_COMM_WORLD); \
      } \
   } while (0)
#else
   #define IT_DEBUG_PRINT_EIGVECS() do { } while (0)
#endif
