$(eval $(begin-module))

################################################################
# unit definitions
################################################################

module_units_h := std types BroadcastDataContainer VBC_Matrix \
                  COO_Matrix CImportanceTruncation mfdn \
                  CImportanceTruncation_debug

module_units_cpp-h := ncsmSU3xSU2Basis lsu3shell \


# module_units_f := 
# module_programs_cpp :=


################################################################
# library creation flag
################################################################

$(eval $(library))

################################################################
# special variable assignments, rules, and dependencies
################################################################


$(eval $(end-module))




