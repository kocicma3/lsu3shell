#pragma once

#include <mpi.h>

#include <cassert>
#include <cstdint>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <vector>

#include <LSU3/COO_Matrix.h>
#include <LSU3/ncsmSU3xSU2Basis.h>
#include <LSU3/VBC_Matrix.h>

#include "CImportanceTruncation_debug.h"

namespace lsu3
{
/*
   class CncsmSU3xSU2Basis_iterator
   {
      public:
         explicit CncsmSU3xSU2Basis_iterator(const CncsmSU3xSU2Basis& basis)
            : basis_(basis), su3xsu2_basis_(nullptr)
         { }

         ~CncsmSU3xSU2Basis_iterator() { if (su3xsu2_basis_ != nullptr) delete su3xsu2_basis_; }

         bool next_state()
         {
            if (su3xsu2_basis_ == nullptr) return first_state();

            assert(su3xsu2_basis_ != nullptr);

            if ((a0_ + 1) < a0_max_) { a0_++; index_++; return true; } else a0_ = 0; 
            if ((k_ + 1) < su3xsu2_basis_->kmax()) { k_++; index_++; return true; } else k_ = 0;

            if (su3xsu2_basis_->isDone() == false) {
               su3xsu2_basis_->nextL();
               if (su3xsu2_basis->isDone() == false) { index_++; return true; }
            }
            delete su3xsu2_basis_;
            su3xsu2_basis_ = nullptr;

            if ((iwpn_ + 1) < basis_.blockEnd(ipin_block_)) {
               iwpn_++;

               SU3xSU2::LABLES omega_pn(basis_.getOmega_pn(ip_, in_, iwpn_));
               a0_max_ = ap_max_ * an_max_ * omega_pn.rho;

               su3xsu2_basis_ = new IRREPBASIS(basis_.Get_Omega_pn_Basis(iwpn_));
               su3xsu2_basis_->rewind();

               index_++;
               return true;
            }

            do {
               if (++ipin_block >= basis_.NumberOfBlocks())
                  return false;
            } while (basis_.NumberOfStatesInBlock(ipin_block_) == false);

            ip_ = basis_.getProtonIrrepId(ipin_block_);
            in_ = basis_.getNeutronIrrepId(ipin_block_);
            N_ = basis_.nhw_p(ip_) + basis_.nhw_n(in_);
            ap_max_ = basis_.getMult_p(ip_);
            an_max_ = basis_.getMult_n(in_);

            iwpn_ = basis_.blockBegin(ipin_block_);

            index_++;
            return true;
         } 

         long index() const { return index_; }
         int N() const { return N_; }

      private:
         const CncsmSU3xSU2Basis& basis_;

         int ipin_block_;
         uint32_t ip_, in_;
         int N_;
         uint16_t ap_max_, an_max_;
         int iwpn_;
         int a0_max_;
         IRREPBASIS* su3xsu2_basis_;
         int k_, a0_;
         long index_;

         bool first_state()
         {
            for (ipin_block_ = 0; ipin_block_ < basis_.NumberOfBlocks(); ipin_block_++) {
               if (basis_.NumberOfStatesInBlock(ipin_block_) == false)
                  continue;

               ip_ = basis_.getProtonIrrepId(ipin_block_);
               in_ = basis_.getNeutronIrrepId(ipin_block_);
               N_ = basis_.nhw_p(ip_) + basis_.nhw_n(in_);
               ap_max_ = basis_.getMult_p(ip_);
               an_max_ = basis_.getMult_n(in_);

               for (iwpn_ = basis_.blockBegin(ipin_block_), iwpn_ < basis_.blockEnd(ipin_block_), iwpn_++) {
                  SU3xSU2::LABLES omega_pn(basis_.getOmega_pn(ip_, in_, iwpn_));
                  a0_max_ = ap_max_ * an_max_ * omega_pn.rho;

                  if (su3xsu2_basis_) delete su3xsu2_basis_;
                  su3xsu2_basis_ = new IRREPBASIS(basis_.Get_Omega_pn_Basis(iwpn_));
                  for (su3xsu2_basis_->rewind(); !su3xsu2_basis_->IsDone(); su3xsu2_basis_->nextL())
                     for (k_ = 0; k_ < su3xsu2_basis_->kmax(); k++)
                        for (a0_ = 0; a0_ < a0_max_; a0_++) {
                           index_ = 0;
                           return true;
                        }
               }
            }

            return false; // no first state
         }
   };
*/

   class CImportanceTruncation
   {
      public:
         CImportanceTruncation(
               const CncsmSU3xSU2Basis& bra, const CncsmSU3xSU2Basis& ket,
               const VBC_Matrix& vbc, double hw, int initial_nmax, const MPI_Comm comm)
            : bra_(bra), ket_(ket), hw_(hw),
            comm_(MPI_COMM_NULL), comm_diag_(MPI_COMM_NULL),
            comm_row_(MPI_COMM_NULL), comm_col_(MPI_COMM_NULL)
         {
            coo_.construct_from_vbc(vbc);

            bra_dim_ = bra.dim();
            ket_dim_ = ket.dim();

            // check
            assert(coo_.nrows == bra_dim_);
            assert(coo_.ncols == ket_dim_);

            long temp = 0;
            for (int idiag = 0; idiag < bra.SegmentNumber(); idiag++)
               temp += bra.dim(idiag);
            assert(coo_.irow == temp);

            temp = 0;
            for (int jdiag = 0; jdiag < ket.SegmentNumber(); jdiag++)
               temp += ket.dim(jdiag);
            assert(coo_.icol == temp);

            idiag_ = bra_.SegmentNumber();
            jdiag_ = ket_.SegmentNumber();
            assert (bra_.ndiag() == ket_.ndiag());
            ndiag_ = bra_.ndiag();

            MPI_Comm_dup(comm, &comm_);
            MPI_Comm_size(comm_, &nprocs_);
            MPI_Comm_rank(comm_, &rank_);

            MPI_Comm_split(comm_, idiag_ == jdiag_, rank_, &comm_diag_);
            MPI_Comm_split(comm_, idiag_, rank_, &comm_row_);
            MPI_Comm_split(comm_, jdiag_, rank_, &comm_col_);

            bra_bitmask_.resize(bra_dim_, 1);
            ket_bitmask_.resize(ket_dim_, 1);

            // initial bitmask according to Nmax = initial_nmax
            temp = 0;
            for (int ipin_block = 0; ipin_block < bra_.NumberOfBlocks(); ipin_block++) {
               if (!bra_.NumberOfStatesInBlock(ipin_block)) continue;

               uint32_t ip = bra_.getProtonIrrepId(ipin_block);
               uint32_t in = bra_.getNeutronIrrepId(ipin_block);
               int N = bra_.nhw_p(ip) + bra_.nhw_n(in);
               uint16_t ap_max = bra_.getMult_p(ip);
               uint16_t an_max = bra_.getMult_n(in);

               for (int iwpn = bra_.blockBegin(ipin_block); iwpn < bra_.blockEnd(ipin_block); ++iwpn) {
                  SU3xSU2::LABELS omega_pn(bra_.getOmega_pn(ip, in, iwpn));
                  int a0_max = ap_max * an_max * omega_pn.rho;

                  IRREPBASIS su3xsu2_basis(bra_.Get_Omega_pn_Basis(iwpn));
                  for (su3xsu2_basis.rewind(); !su3xsu2_basis.IsDone(); su3xsu2_basis.nextL()) 
                     for (int k = 0; k < su3xsu2_basis.kmax(); ++k) 
                        for (int a0 = 0; a0 < a0_max; ++a0) {
                           if (N > initial_nmax)
                              bra_bitmask_[temp] = 0;
                           temp++;
                        }
               }
            }
            temp = 0;
            for (int ipin_block = 0; ipin_block < ket_.NumberOfBlocks(); ipin_block++) {
               if (!ket_.NumberOfStatesInBlock(ipin_block)) continue;

               uint32_t ip = ket_.getProtonIrrepId(ipin_block);
               uint32_t in = ket_.getNeutronIrrepId(ipin_block);
               int N = ket_.nhw_p(ip) + ket_.nhw_n(in);
               uint16_t ap_max = ket_.getMult_p(ip);
               uint16_t an_max = ket_.getMult_n(in);

               for (int iwpn = ket_.blockBegin(ipin_block); iwpn < ket_.blockEnd(ipin_block); ++iwpn) {
                  SU3xSU2::LABELS omega_pn(ket_.getOmega_pn(ip, in, iwpn));
                  int a0_max = ap_max * an_max * omega_pn.rho;

                  IRREPBASIS su3xsu2_basis(ket_.Get_Omega_pn_Basis(iwpn));
                  for (su3xsu2_basis.rewind(); !su3xsu2_basis.IsDone(); su3xsu2_basis.nextL()) 
                     for (int k = 0; k < su3xsu2_basis.kmax(); ++k) 
                        for (int a0 = 0; a0 < a0_max; ++a0) {
                           if (N > initial_nmax)
                              ket_bitmask_[temp] = 0;
                           temp++;
                        }
               }
            }
            IT_DEBUG_SET_BITMASK();

            reduced_.release();
         }

         ~CImportanceTruncation()
         {
            release();
         }

         void release()
         {
            if (comm_diag_ != MPI_COMM_NULL)
               MPI_Comm_free(&comm_diag_);

            if (comm_row_ != MPI_COMM_NULL)
               MPI_Comm_free(&comm_row_);

            if (comm_col_ != MPI_COMM_NULL)
               MPI_Comm_free(&comm_col_);

            if (comm_ != MPI_COMM_NULL)
               MPI_Comm_free(&comm_);
         }

         COO_Matrix& reduce()
         {
            reduced_.release();

            // indexes to be removed:
            std::vector<int32_t> bra_itbr, ket_itbr;

            for (long i = 0; i < bra_bitmask_.size(); i++)
               if (bra_bitmask_[i] == 0)
                  bra_itbr.push_back(i);

            for (long i = 0; i < ket_bitmask_.size(); i++)
               if (ket_bitmask_[i] == 0)
                  ket_itbr.push_back(i);

            for (long k = 0; k < coo_.nnz; k++) {
               int32_t row = coo_.rows[k];
               int32_t col = coo_.cols[k];
               float val = coo_.vals[k];

               long i = binary_search(bra_itbr, row);
               long j = binary_search(ket_itbr, col);

               if ((found(bra_itbr, row, i) == false) && (found(ket_itbr, col, j) == false)) {
                  reduced_.rows.push_back(row - i);
                  reduced_.cols.push_back(col - j);
                  reduced_.vals.push_back(val);
               }
            }

            reduced_.nrows = coo_.nrows - bra_itbr.size();
            reduced_.ncols = coo_.ncols - ket_itbr.size();
            reduced_.nnz = reduced_.vals.size();

            // find first row and column in global matrix
            if (rank_ < ndiag_) {
               assert(reduced_.nrows == reduced_.ncols);

               MPI_Exscan(&reduced_.nrows, &reduced_.irow, 1, MPI_INT64_T, MPI_SUM, comm_diag_);
               if (rank_ == 0)
                  reduced_.irow = 0;
               reduced_.icol = reduced_.irow;
            }
            MPI_Bcast(&reduced_.irow, 1, MPI_INT64_T, 0, comm_row_);
            MPI_Bcast(&reduced_.icol, 1, MPI_INT64_T, 0, comm_col_);

            IT_DEBUG_PRINT_REDUCED();
            IT_DEBUG_PRINT_IROWCOL();

            return reduced_;
         }

         void multiply(std::vector<float>& eigenvectors, const int nrefstates)
         {
            IT_DEBUG_PRINT_EIGVECS();

            // col transformation from coo to reduced
            std::vector<long> c2r(coo_.ncols, 0);
            for (long i = 1; i < coo_.ncols; i++) {
               c2r[i] = c2r[i - 1];
               if (ket_bitmask_[i - 1] == 1)
                  c2r[i]++;
            }
            // col transformation from coo to reduced for transposed multiplication
            std::vector<long> c2r_T(coo_.nrows, 0);
            for (long i = 1; i < coo_.nrows; i++) {
               c2r_T[i] = c2r_T[i - 1];
               if (bra_bitmask_[i - 1] == 1)
                  c2r_T[i]++;
            }

            // broadcast eigenvector parts
            long x_size = eigenvectors.size();
            MPI_Bcast(&x_size, 1, MPI_LONG, 0, comm_col_);

            if (rank_ >= ndiag_)
               eigenvectors.resize(x_size);
            MPI_Bcast(eigenvectors.data(), x_size, MPI_FLOAT, 0, comm_col_);
            // broadcast eigenvector parts for transposed multiplication
            std::vector<float> eigenvectors_T(eigenvectors); // copy for transposed multiplication
            long x_T_size = eigenvectors_T.size();
            MPI_Bcast(&x_T_size, 1, MPI_LONG, 0, comm_row_);

            if (rank_ >= ndiag_)
               eigenvectors_T.resize(x_T_size);
            MPI_Bcast(eigenvectors_T.data(), x_T_size, MPI_FLOAT, 0, comm_row_);

            // multiplication and transposed multiplication
            std::vector<float> x(nrefstates * coo_.nrows, 0.0f);
            std::vector<float> x_T(nrefstates * coo_.ncols, 0.0f);

            for (long k = 0; k < coo_.nnz; k++) {
               int32_t row = coo_.rows[k];
               int32_t col = coo_.cols[k];
               float val = coo_.vals[k];

               if ((bra_bitmask_[row] == 0) && (ket_bitmask_[col] == 1)) 
                  for (int irefstate = 0; irefstate < nrefstates; irefstate++) 
                     x[irefstate * coo_.nrows + row] +=
                        val * eigenvectors[irefstate * reduced_.ncols + c2r[col]];

               if ((rank_ < ndiag_) && (row == col))
                     continue;

               if ((ket_bitmask_[col] == 0) && (bra_bitmask_[row] == 1))
                  for (int irefstate = 0; irefstate < nrefstates; irefstate++) 
                     x_T[irefstate * coo_.ncols + col] +=
                        val * eigenvectors_T[irefstate * reduced_.nrows + c2r_T[row]];
            }

            IT_DEBUG_PMTM_RESULTS();

            // reduce results to diagonal processes
            if (rank_ < ndiag_) {
               x_.clear();
               x_.resize(x.size());
               MPI_Reduce(x.data(), x_.data(), x.size(), MPI_FLOAT, MPI_SUM, 0, comm_row_);
            }
            else
               MPI_Reduce(x.data(), nullptr, x.size(), MPI_FLOAT, MPI_SUM, 0, comm_row_);

            // reduce transposed results to diagonal processes
            if (rank_ < ndiag_)
               MPI_Reduce(MPI_IN_PLACE, x_T.data(), x_T.size(), MPI_FLOAT, MPI_SUM, 0, comm_col_);
            else
               MPI_Reduce(x_T.data(), nullptr, x_T.size(), MPI_FLOAT, MPI_SUM, 0, comm_col_);

            if (rank_ < ndiag_) {
               assert(x.size() == x_T.size());

               for (long i = 0; i < x.size(); i++)
                  x_[i] += x_T[i];
            }

            IT_DEBUG_PRM_RESULTS();
         }

         void store_complete_eigenvectors(
               const std::vector<float>& eigenvectors, const int neigen, const int iter)
         {
            if (rank_ < ndiag_) {
               for (int rank = 0; rank < ndiag_; rank++) {
                  if (rank == rank_) {
                     {
                        // store bit mask:
                        std::stringstream ss;
                        ss << "bitmask_" << std::setw(3) << std::setfill('0') << iter << ".dat";
                        std::ofstream f;
                        if (rank_ == 0)
                           f.open(ss.str(), std::ios::binary | std::ios::out);
                        else
                           f.open(ss.str(), std::ios::binary | std::ios::app);

                        for (long i = 0; i < bra_bitmask_.size(); i++) 
                           f.write((const char*)(&(bra_bitmask_[i])), sizeof(int8_t));
                     }

                     // store process local vector parts:
                     for (int ieigen = 0; ieigen < neigen; ieigen++) {
                        std::stringstream ss;
                        ss << "complete_eigenvector_"
                           << std::setw(3) << std::setfill('0') << iter << "_"
                           << std::setw(2) << std::setfill('0') << ieigen << ".dat";
                        std::ofstream f;
                        if (rank_ == 0)
                           f.open(ss.str(), std::ios::binary | std::ios::out);
                        else
                           f.open(ss.str(), std::ios::binary | std::ios::app);

                        long ii = 0;
                        const float zero = 0.0f;
                        for (long i = 0; i < bra_bitmask_.size(); i++) {
                           if (bra_bitmask_[i]) {
                              f.write((const char*)(eigenvectors.data() + ieigen * reduced_.nrows + ii),
                                    sizeof(float));
                              ii++;
                           }
                           else
                              f.write((const char*)(&zero), sizeof(float));
                        }
                     }
                  }

                  MPI_Barrier(comm_diag_);
               }
            }

            MPI_Barrier(comm_);
         }

         void reset_bitmask(double epsilon, const int nrefstates, const int iter) {
            // set according to IT rules
            if (rank_ < ndiag_) {
               std::ofstream f;
               if ((nprocs_ == 1) && (rank_ == 0)) {
                  f.open("it.log", std::ios::app); f.setf(std::ios::fixed, std::ios::floatfield);
               }

               std::stringstream ss;
               ss << "kappa_it-" << std::setw(3) << std::setfill('0') << iter
                  << "_proc-" << std::setw(3) << std::setfill('0') << rank_ << ".dat";
               std::ofstream fkap(ss.str(), std::ios::out);
               fkap.setf(std::ios::fixed, std::ios::floatfield);

               long temp = 0;
               for (int ipin_block = 0; ipin_block < bra_.NumberOfBlocks(); ipin_block++) {
                  if (!bra_.NumberOfStatesInBlock(ipin_block)) continue;

                  uint32_t ip = bra_.getProtonIrrepId(ipin_block);
                  uint32_t in = bra_.getNeutronIrrepId(ipin_block);
                  int N = bra_.nhw_p(ip) + bra_.nhw_n(in);
                  uint16_t ap_max = bra_.getMult_p(ip);
                  uint16_t an_max = bra_.getMult_n(in);

                  for (int iwpn = bra_.blockBegin(ipin_block); iwpn < bra_.blockEnd(ipin_block); ++iwpn) {
                     SU3xSU2::LABELS omega_pn(bra_.getOmega_pn(ip, in, iwpn));
                     int a0_max = ap_max * an_max * omega_pn.rho;

                     IRREPBASIS su3xsu2_basis(bra_.Get_Omega_pn_Basis(iwpn));
                     for (su3xsu2_basis.rewind(); !su3xsu2_basis.IsDone(); su3xsu2_basis.nextL()) 
                        for (int k = 0; k < su3xsu2_basis.kmax(); ++k) 
                           for (int a0 = 0; a0 < a0_max; ++a0) {
                              if (bra_bitmask_[temp] == 0) {
                                 int irefstate;
/*
                                 // OPTION #1: all kappas GEQ than epsilon
                                 for (ieigen = 0; ieigen < neigen; ieigen++) {
                                    double kappa = fabs(x_[temp + ieigen * coo_.nrows]) / ((double)N * hw_);
                                    if (kappa < epsilon)
                                       break;
                                 }
                                 if (ieigen == neigen)
                                    bra_bitmask_[temp] = ket_bitmask_[temp] = 1;
*/
                                 
                                 // OPTION #2: any kappa GEQ than epsilon
                                 for (irefstate = 0; irefstate < nrefstates; irefstate++) {
                                    double kappa = fabs(x_[temp + irefstate * coo_.nrows]) / ((double)N * hw_);
                                    if (kappa >= epsilon) {
                                       bra_bitmask_[temp] = ket_bitmask_[temp] = 1;

                                       if ((nprocs_ == 1) && (rank_ == 0)) {
                                          f << "Adding state " << std::setw(10) << temp << ", mult. results: ";
                                          for (int i = 0; i < nrefstates; i++)
                                             f << std::setw(12) << std::setprecision(6)
                                                << x_[temp + i * coo_.nrows] << " ";
                                          f << "\n";
                                       }

                                       fkap << std::setw(10) << coo_.irow + temp;
                                       for (int irefstate = 0; irefstate < nrefstates; irefstate++) {
                                          double kappa = fabs(x_[temp + irefstate * coo_.nrows]) / ((double)N * hw_);
                                          fkap << " " << std::setw(12) << std::setprecision(6) << kappa;
                                       }
                                       fkap << "\n";

                                       break;
                                    }
                                 }
                              }

                              temp++;
                           }
                  }
               }

               if ((nprocs_ == 1) && (rank_ == 0)) f.close();
            }

            // broadcast bitmasks to non-diagonal processes:
            MPI_Bcast(bra_bitmask_.data(), bra_bitmask_.size(), MPI_INT8_T, 0, comm_row_);
            MPI_Bcast(ket_bitmask_.data(), ket_bitmask_.size(), MPI_INT8_T, 0, comm_col_);
         }

         void print(const std::string& filename) {
            if (rank_ < ndiag_) {
               for (int iproc = 0; iproc < ndiag_; iproc++) {
                  if (rank_ == iproc) {
                     std::ofstream f;
                     if (rank_ == 0)
                        f.open(filename, std::ios::out);
                     else 
                        f.open(filename, std::ios::app);

                     f.setf(std::ios::fixed, std::ios::floatfield);

                     // iterate over states:
                     long state_index = 0;
                     for (int ipin_block = 0; ipin_block < bra_.NumberOfBlocks(); ipin_block++) {
                        if (!bra_.NumberOfStatesInBlock(ipin_block)) continue;

                        uint32_t ip = bra_.getProtonIrrepId(ipin_block);
                        uint32_t in = bra_.getNeutronIrrepId(ipin_block);
                        int N = bra_.nhw_p(ip) + bra_.nhw_n(in);
                        uint16_t ap_max = bra_.getMult_p(ip);
                        uint16_t an_max = bra_.getMult_n(in);

                        SU3xSU2::LABELS irrep_p(bra_.getProtonSU3xSU2(ip));
                        SU3xSU2::LABELS irrep_n(bra_.getNeutronSU3xSU2(in));
                        int ssp(irrep_p.S2);
                        int ssn(irrep_n.S2);

                        for (int iwpn = bra_.blockBegin(ipin_block); iwpn < bra_.blockEnd(ipin_block); ++iwpn) {
                           SU3xSU2::LABELS omega_pn(bra_.getOmega_pn(ip, in, iwpn));
                           int lm = omega_pn.lm;
                           int mu = omega_pn.mu;
                           int ss = omega_pn.S2;
                           int a0_max = ap_max * an_max * omega_pn.rho;

                           IRREPBASIS su3xsu2_basis(bra_.Get_Omega_pn_Basis(iwpn));
                           for (su3xsu2_basis.rewind(); !su3xsu2_basis.IsDone(); su3xsu2_basis.nextL()) {
                              int ll = su3xsu2_basis.L();

                              for (int k = 0; k < su3xsu2_basis.kmax(); ++k) 
                                 for (int a0 = 0; a0 < a0_max; ++a0) {
                                    assert(bra_bitmask_[state_index] == ket_bitmask_[state_index]);
                                    
                                    if (bra_bitmask_[state_index]) 
                                       f << state_index << " " << ip << " " << in << " "
                                         << N << " " << ssp << " " << ssn << " " << ss << " "
                                         << lm << " " << mu << " " 
                                         << k << " " << ll << " " << a0 << "\n";

                                    state_index++;
                                 }
                           }
                        }
                     }


                  }

                  MPI_Barrier(comm_diag_);
               }
            }
         }

      private:
         template <typename Int>
         long binary_search(const std::vector<Int>& itbr, const Int i)
         {
            long lo = 0;
            long hi = itbr.size() - 1;

            while (lo <= hi) {
               long mid = (lo + hi) / 2;

               if (itbr[mid] < i)
                  lo = mid + 1;
               else if (itbr[mid] > i)
                  hi = mid - 1;
               else 
                  return mid;
            }

            return lo;
         }

         template <typename Int>
         long found(const std::vector<Int>& itbr, const Int i, const long pos)
         {
            return ((pos < itbr.size()) && (itbr[pos] == i));
         }

         // private member variables:
         const CncsmSU3xSU2Basis& bra_;
         const CncsmSU3xSU2Basis& ket_;

         long bra_dim_, ket_dim_;

         double hw_;

         COO_Matrix coo_, reduced_;

         long idiag_, jdiag_, ndiag_;

         MPI_Comm comm_;
         int nprocs_, rank_;

         MPI_Comm comm_diag_, comm_row_, comm_col_;

         std::vector<int8_t> bra_bitmask_, ket_bitmask_;

         std::vector<float> x_; // multiplication results
   };
}
