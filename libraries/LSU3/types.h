#ifndef LSU3_TYPES_H
#define LSU3_TYPES_H

#include <boost/functional/hash.hpp>

#include "std.h"

namespace lsu3
{
    typedef uint8_t  T_alpha;
    typedef float    T_coeff;
    typedef uint32_t T_ij_np;
    typedef uint32_t T_i_np_0;
    typedef uint8_t  T_kappa;
    typedef uint8_t  T_L;
    typedef uint8_t  T_lm;      // lambda
    typedef uint8_t  T_mu;      // mu
    typedef int8_t   T_n;       // n1,...,n6 (na, nb, nc, nd)
    typedef uint32_t T_pos;
    typedef float    T_rme;
    typedef uint8_t  T_S2;      // 2 * S

    // HoShellsn1n2: (n1, n2)
    typedef cpp0x::tuple<T_n, T_n> HoShellsn1n2;

    enum { kHoShelln1n2_n1 = 0, kHoShelln1n2_n2 = 1 };

    inline T_n  get_n1(const HoShellsn1n2& arg)
    { return cpp0x::get<kHoShelln1n2_n1>(arg); }
    inline T_n& get_n1(      HoShellsn1n2& arg)
    { return cpp0x::get<kHoShelln1n2_n1>(arg); }

    inline T_n  get_n2(const HoShellsn1n2& arg)
    { return cpp0x::get<kHoShelln1n2_n2>(arg); }
    inline T_n& get_n2(      HoShellsn1n2& arg)
    { return cpp0x::get<kHoShelln1n2_n2>(arg); }

    inline std::size_t hash_combine(std::size_t& seed, const HoShellsn1n2& arg)
    {
        boost::hash_combine(seed, get_n1(arg));
        boost::hash_combine(seed, get_n2(arg));
        return seed;
    }

    // LmMuS2: (lambda, mu, 2 * S)
    typedef cpp0x::tuple<T_lm, T_mu, T_S2> LmMuS2;

    enum { kLmMuS2_lm = 0, kLmMuS2_mu = 1, kLmMuS2_S2 = 2 };

    inline T_lm  get_lm(const LmMuS2& arg)
    { return cpp0x::get<kLmMuS2_lm>(arg); }
    inline T_lm& get_lm(      LmMuS2& arg)
    { return cpp0x::get<kLmMuS2_lm>(arg); }

    inline T_mu  get_mu(const LmMuS2& arg)
    { return cpp0x::get<kLmMuS2_mu>(arg); }
    inline T_mu& get_mu(      LmMuS2& arg)
    { return cpp0x::get<kLmMuS2_mu>(arg); }

    inline T_S2  get_S2(const LmMuS2& arg)
    { return cpp0x::get<kLmMuS2_S2>(arg); }
    inline T_S2& get_S2(      LmMuS2& arg)
    { return cpp0x::get<kLmMuS2_S2>(arg); }

    inline std::size_t hash_combine(std::size_t& seed, const LmMuS2& arg)
    {
        boost::hash_combine(seed, get_lm(arg));
        boost::hash_combine(seed, get_mu(arg));
        boost::hash_combine(seed, get_S2(arg));
        return seed;
    }

    // NNLmMuS2: (HoShellsn1n2, LmMuS2)
    typedef cpp0x::tuple <HoShellsn1n2, LmMuS2> NNLmMuS2;

    enum { kNNLmMuS2_n1n2 = 0, kNNLmMuS2_lmmuS2 = 1 };

    inline const HoShellsn1n2& get_n1n2(const NNLmMuS2& arg)
    { return cpp0x::get<kNNLmMuS2_n1n2>(arg); }
    inline       HoShellsn1n2& get_n1n2(      NNLmMuS2& arg)
    { return cpp0x::get<kNNLmMuS2_n1n2>(arg); }

    inline T_n  get_n1(const NNLmMuS2& arg) { return get_n1(get_n1n2(arg)); }
    inline T_n& get_n1(      NNLmMuS2& arg) { return get_n1(get_n1n2(arg)); }

    inline T_n  get_n2(const NNLmMuS2& arg) { return get_n2(get_n1n2(arg)); }
    inline T_n& get_n2(      NNLmMuS2& arg) { return get_n2(get_n1n2(arg)); }

    inline const LmMuS2& get_LmMuS2(const NNLmMuS2& arg)
    { return cpp0x::get<kNNLmMuS2_lmmuS2>(arg); }
    inline       LmMuS2& get_LmMuS2(      NNLmMuS2& arg)
    { return cpp0x::get<kNNLmMuS2_lmmuS2>(arg); }

    inline T_lm  get_lm(const NNLmMuS2& arg)
    { return get_lm(get_LmMuS2(arg)); }
    inline T_lm& get_lm(      NNLmMuS2& arg)
    { return get_lm(get_LmMuS2(arg)); }

    inline T_mu  get_mu(const NNLmMuS2& arg)
    { return get_mu(get_LmMuS2(arg)); }
    inline T_mu& get_mu(      NNLmMuS2& arg)
    { return get_mu(get_LmMuS2(arg)); }

    inline T_S2  get_S2(const NNLmMuS2& arg)
    { return get_S2(get_LmMuS2(arg)); }
    inline T_S2& get_S2(      NNLmMuS2& arg)
    { return get_S2(get_LmMuS2(arg)); }

    inline std::size_t hash_combine(std::size_t& seed, const NNLmMuS2& arg)
    {
        hash_combine(seed, get_n1n2(arg));
        hash_combine(seed, get_LmMuS2(arg));
        return seed;
    }

    // PNIntKey: (NNLmMuS2, NNLmMuS2, LmMuS2)
    typedef cpp0x::tuple<NNLmMuS2, NNLmMuS2, LmMuS2> PNIntKey;

    enum { kPNIntKey_nnlmmuS2_p = 0, kPNIntKey_nnlmmuS2_n = 1, kPNIntKey_lmmus2 = 2 };

    inline const NNLmMuS2& get_NNLmMuS2_p(const PNIntKey& arg)
    { return cpp0x::get<kPNIntKey_nnlmmuS2_p>(arg); }
    inline       NNLmMuS2& get_NNLmMuS2_p(      PNIntKey& arg)
    { return cpp0x::get<kPNIntKey_nnlmmuS2_p>(arg); }

    inline const HoShellsn1n2& get_nanb(const PNIntKey& arg)
    { return get_n1n2(get_NNLmMuS2_p(arg)); }
    inline       HoShellsn1n2& get_nanb(      PNIntKey& arg)
    { return get_n1n2(get_NNLmMuS2_p(arg)); }

    inline T_n  get_na(const PNIntKey& arg) { return get_n1(get_nanb(arg)); }
    inline T_n& get_na(      PNIntKey& arg) { return get_n1(get_nanb(arg)); }

    inline T_n  get_nb(const PNIntKey& arg) { return get_n2(get_nanb(arg)); }
    inline T_n& get_nb(      PNIntKey& arg) { return get_n2(get_nanb(arg)); }

    inline const LmMuS2& get_LmMuS2_p(const PNIntKey& arg)
    { return get_LmMuS2(get_NNLmMuS2_p(arg)); }
    inline       LmMuS2& get_LmMuS2_p(      PNIntKey& arg)
    { return get_LmMuS2(get_NNLmMuS2_p(arg)); }

    inline T_lm  get_lmp(const PNIntKey& key) { return get_lm(get_LmMuS2_p(key)); }
    inline T_lm& get_lmp(      PNIntKey& key) { return get_lm(get_LmMuS2_p(key)); }

    inline T_lm  get_mup(const PNIntKey& key) { return get_mu(get_LmMuS2_p(key)); }
    inline T_lm& get_mup(      PNIntKey& key) { return get_mu(get_LmMuS2_p(key)); }

    inline T_lm  get_S2p(const PNIntKey& key) { return get_S2(get_LmMuS2_p(key)); }
    inline T_lm& get_S2p(      PNIntKey& key) { return get_S2(get_LmMuS2_p(key)); }

    inline const NNLmMuS2& get_NNLmMuS2_n(const PNIntKey& arg)
    { return cpp0x::get<kPNIntKey_nnlmmuS2_n>(arg); }
    inline       NNLmMuS2& get_NNLmMuS2_n(      PNIntKey& arg)
    { return cpp0x::get<kPNIntKey_nnlmmuS2_n>(arg); }

    inline const HoShellsn1n2& get_ncnd(const PNIntKey& arg)
    { return get_n1n2(get_NNLmMuS2_n(arg)); }
    inline       HoShellsn1n2& get_ncnd(      PNIntKey& arg)
    { return get_n1n2(get_NNLmMuS2_n(arg)); }

    inline T_n  get_nc(const PNIntKey& arg) { return get_n1(get_ncnd(arg)); }
    inline T_n& get_nc(      PNIntKey& arg) { return get_n1(get_ncnd(arg)); }

    inline T_n  get_nd(const PNIntKey& arg) { return get_n2(get_ncnd(arg)); }
    inline T_n& get_nd(      PNIntKey& arg) { return get_n2(get_ncnd(arg)); }

    inline const LmMuS2& get_LmMuS2_n(const PNIntKey& arg)
    { return get_LmMuS2(get_NNLmMuS2_n(arg)); }
    inline       LmMuS2& get_LmMuS2_n(      PNIntKey& arg)
    { return get_LmMuS2(get_NNLmMuS2_n(arg)); }

    inline T_lm  get_lmn(const PNIntKey& key) { return get_lm(get_LmMuS2_n(key)); }
    inline T_lm& get_lmn(      PNIntKey& key) { return get_lm(get_LmMuS2_n(key)); }

    inline T_lm  get_mun(const PNIntKey& key) { return get_mu(get_LmMuS2_n(key)); }
    inline T_lm& get_mun(      PNIntKey& key) { return get_mu(get_LmMuS2_n(key)); }

    inline T_lm  get_S2n(const PNIntKey& key) { return get_S2(get_LmMuS2_n(key)); }
    inline T_lm& get_S2n(      PNIntKey& key) { return get_S2(get_LmMuS2_n(key)); }

    inline const LmMuS2& get_LmMuS2(const PNIntKey& arg)
    { return cpp0x::get<kPNIntKey_lmmus2>(arg); }
    inline       LmMuS2& get_LmMuS2(      PNIntKey& arg)
    { return cpp0x::get<kPNIntKey_lmmus2>(arg); }

    inline T_lm  get_lm0(const PNIntKey& key) { return get_lm(get_LmMuS2(key)); }
    inline T_lm& get_lm0(      PNIntKey& key) { return get_lm(get_LmMuS2(key)); }

    inline T_lm  get_mu0(const PNIntKey& key) { return get_mu(get_LmMuS2(key)); }
    inline T_lm& get_mu0(      PNIntKey& key) { return get_mu(get_LmMuS2(key)); }

    inline T_lm  get_S20(const PNIntKey& key) { return get_S2(get_LmMuS2(key)); }
    inline T_lm& get_S20(      PNIntKey& key) { return get_S2(get_LmMuS2(key)); }

    inline std::size_t hash_combine(std::size_t& seed, const PNIntKey& arg)
    {
        hash_combine(seed, get_NNLmMuS2_p(arg));
        hash_combine(seed, get_NNLmMuS2_n(arg));
        hash_combine(seed, get_LmMuS2(arg));
        return seed;
    }
    inline std::size_t hash_combine(const PNIntKey& arg)
    {
        size_t seed = 0;
        return hash_combine(seed, arg);
    }

}

#endif
