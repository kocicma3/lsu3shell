#ifndef CALCULATE_ME_IDENTICAL_H
#define CALCULATE_ME_IDENTICAL_H
#include <SU3ME/global_definitions.h>
#include <SU3ME/ncsmSU3Basis.h>
#include <SU3ME/proton_neutron_ncsmSU3Basis.h>
#include <SU3ME/RME.h>
#include <LookUpContainers/WigEckSU3SO3CGTable.h>

typedef std::vector<std::pair<IdenticalFermionsNCSMBasis::IdType, float> > MatrixRow;

struct MECalculatorData
{
	static SU2::LABEL JJ0;	// value is set in functions defined in SU3ME/MeEvaluationHelpers.h
	SU3xSU2::RME* rmes_;
	WigEckSU3SO3CG* SU3SO3CGs_;
	TENSOR_STRENGTH* coeffs_;
	MECalculatorData(SU3xSU2::RME* rmes, WigEckSU3SO3CG* SU3SO3CGs, TENSOR_STRENGTH* coeffs)
		:rmes_(rmes), SU3SO3CGs_(SU3SO3CGs), coeffs_(coeffs) {}
	MECalculatorData(const MECalculatorData& MeCalcData) {rmes_ = MeCalcData.rmes_; SU3SO3CGs_ = MeCalcData.SU3SO3CGs_; coeffs_ = MeCalcData.coeffs_;}
};


//	pRME[0] ----> <af wf Sf ||| T^a0min ||| ai wi Si>_{rhotmin}, where af and ai are fixed
double Coeff_x_SU3SO3CG_x_RME(size_t k0_max, size_t a0_max,  size_t rhot_max,
								SU3::WIGNER* pSU3SO3CGs, 
								SU3xSU2::RME::DOUBLE* pRME, 
								TENSOR_STRENGTH* coeffs, const int fermionType);

void CalculateME_debbug(const size_t afmax, SU3xSU2::BasisIdenticalFermsCompatible& bra, const IdenticalFermionsNCSMBasis::IdType firstBraStateId, 
						const size_t aimax, SU3xSU2::BasisIdenticalFermsCompatible& ket, const IdenticalFermionsNCSMBasis::IdType firstKetStateId, 
						const std::vector<MECalculatorData>& MeCalcData, const int fermionType);

void CalculateME_Diagonal_LowerTriang(	const size_t afmax, SU3xSU2::BasisIdenticalFermsCompatible& bra, const IdenticalFermionsNCSMBasis::IdType firstBraStateId, 
								const size_t aimax, SU3xSU2::BasisIdenticalFermsCompatible& ket, const IdenticalFermionsNCSMBasis::IdType firstKetStateId, 
								const std::vector<MECalculatorData>& MeCalcData, const int fermionType, std::vector<MatrixRow>& Me);

void CalculateME_Diagonal_UpperTriang(	const size_t afmax, SU3xSU2::BasisIdenticalFermsCompatible& bra, const IdenticalFermionsNCSMBasis::IdType firstBraStateId, 
								const size_t aimax, SU3xSU2::BasisIdenticalFermsCompatible& ket, const IdenticalFermionsNCSMBasis::IdType firstKetStateId, 
								const std::vector<MECalculatorData>& MeCalcData, const int fermionType, std::vector<MatrixRow>& Me);


void CalculateME_nonDiagonal(	const size_t afmax, SU3xSU2::BasisIdenticalFermsCompatible& bra, const IdenticalFermionsNCSMBasis::IdType firstBraStateId, 
								const size_t aimax, SU3xSU2::BasisIdenticalFermsCompatible& ket, const IdenticalFermionsNCSMBasis::IdType firstKetStateId, 
								const std::vector<MECalculatorData>& MeCalcData, const int fermionType, std::vector<MatrixRow>& Me);
#endif 


