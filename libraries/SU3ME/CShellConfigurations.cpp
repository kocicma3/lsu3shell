#include <SU3ME/CShellConfigurations.h>
#include <iostream>
#include <set>

void CShellConfigurations::GenerateShellConfigurations(const size_t Nmax, const unsigned A)
{
	// Shell configurations are stored initially in ConfRepository
	// At the end they are tranfered into m_ShellConfigurations
	std::vector<std::set<tSHELLSCONFIG> > ConfRepository(Nmax+1); 
    // v0hwShellConfiguration stores the occupation numbers of HO shells for
    // 0hw model space.
    // Example:
    //             HO shell n =   0    1    2     3     4     5 ... nmax
    // v0hwShellConfiguration = { 2,   6,   3,    0,    0,    0 ... ,0 }   
    // The above configuration has 2 fermions in s-shell, 6 in p-shell and 3 in sd-shell
    tSHELLSCONFIG v0hwShellConfiguration;
  
    // setOfShellConfigurations stores a set of unique vShellConfigurations for a given Nhw subspace
    // It is a set because the algorithm that is being used can produce several identical shell configurations
    std::set<tSHELLSCONFIG> setOfShellConfigurations;

    // This has to be declared with int type since we subtract N, that is: iNParticlesLeft -= N;
    int iNParticlesLeft = A;
    unsigned nmax;   // the last HO shell available
    unsigned n = 0;

    // This loop creates 0hw shell configuration
    while (iNParticlesLeft > 0)
    {
		int N = (n+1)*(n+2); //number of fermions that can fit into n-th HO shell
		if ((iNParticlesLeft - N) > 0) {
	    	v0hwShellConfiguration.push_back(N);
	    	iNParticlesLeft -= N;
	    	n++;
		} else {
	    	v0hwShellConfiguration.push_back(iNParticlesLeft);
	    	iNParticlesLeft = 0;
		}
    }
    m_LastShell = nmax = Nmax + n; // This is maximal HO shell that can contain maximally 1 particle

	v0hwShellConfiguration.resize(nmax+1, 0); // We need to resize vShellConfiguration up to nmax+1 elements

    // There is only one 0hw shell configuration
    setOfShellConfigurations.insert(v0hwShellConfiguration);
    // Let's store it in ConfRepository[0]
    ConfRepository[0] = setOfShellConfigurations;

    // Iterate through N\hbar\Omega subspaces up to Nmax\hbar\Omega
    // and for each subspace (Nsubspace) generate allowed
    // shell configurations
    for (size_t Nsubspace = 1; Nsubspace <= Nmax; Nsubspace++)
    {
		// We will reuse setOfShellConfigurations data structure over and over again
		// So we need to clean it first
		setOfShellConfigurations.clear();

		std::set<tSHELLSCONFIG>::const_iterator cit = ConfRepository[Nsubspace-1].begin();
		for (; cit != ConfRepository[Nsubspace-1].end(); cit++) 
		{
	    	// vSeedConfiguration contains (Nsubspace-1) subspace distribution of fermions over the shells
	    	tSHELLSCONFIG vSeedShellConfiguration(*cit);
	    	for (size_t q = 0; q < nmax; q++) 
	    	{
			// vNewShellConfiguration contains Nsubspace subspace distribution of fermions over the shells
	    		tSHELLSCONFIG vNewShellConfiguration(vSeedShellConfiguration);
				if (vNewShellConfiguration[q] && (vNewShellConfiguration[q+1]+1) <= ((q+2)*(q+3))) 
				{
		    		vNewShellConfiguration[q]   -= 1;
				    vNewShellConfiguration[q+1] += 1;
				    // STL set<> datastructure takes care of duplicities
				    // Thus we need not explicitly check whether vNewShellConfiguration
				    // has been already encountered and stored
				    setOfShellConfigurations.insert(vNewShellConfiguration); 
				}
	    	}
		}
		// setOfShellConfigurations contains all (Nsubspace)hw shell configurations
		// let's store it in ConfRepository[Nsubspace]
		ConfRepository[Nsubspace] = setOfShellConfigurations;
    }
	// iterate through all shell configurations belonging to N = Nsubspace subspace
	// Transfer data from ConfRepository into m_ShellConfigurations
	for (size_t Nsubspace = 0; Nsubspace <= Nmax; Nsubspace++)
    {
		std::set<tSHELLSCONFIG>::const_iterator citShellConfs = ConfRepository[Nsubspace].begin();
		for (; citShellConfs != ConfRepository[Nsubspace].end(); ++citShellConfs)
		{
	    	m_ShellConfigurations[Nsubspace].push_back(*citShellConfs);
		}
	}
}

CShellConfigurations::CShellConfigurations(const size_t Nmax, const unsigned A):m_Nmax(Nmax), m_A(A), m_ShellConfigurations(Nmax+1)
{
	GenerateShellConfigurations(Nmax, A);
}

void CShellConfigurations::GetMinMaxFermionsShells(std::vector<std::pair<unsigned, unsigned> >& vMinMax) const
{
	std::cout << "OBSOLETE!!! This procedure does not make much sense as Min = 1 alway!!! and hence should be replaced by GetMaxFermionsShells" << std::endl; 
	std::vector<std::pair<unsigned, unsigned> > MinMaxArray(m_LastShell+1, std::make_pair(m_A, 0));

	for (size_t Nsubspace = 0; Nsubspace <= m_Nmax; Nsubspace++)
    {
		size_t nConfs = GetNumberOfConfigurations(Nsubspace);
		std::vector<tSHELLSCONFIG> ShellConfs(m_ShellConfigurations[Nsubspace]);
		size_t i, j;
		for (i = 0; i < nConfs; ++i)
		{
			tSHELLSCONFIG Configuration(ShellConfs[i]);
			for (j = 0; j <= m_LastShell; ++j)
			{
				if (!Configuration[j]) {
					continue;
				}
				if (Configuration[j] > MinMaxArray[j].second) {
					MinMaxArray[j].second = Configuration[j];
				}
				if (Configuration[j] < MinMaxArray[j].first) {
					MinMaxArray[j].first = Configuration[j];
				}
			}
		}
	}
	vMinMax = MinMaxArray;
}

void CShellConfigurations::GetMaxFermionsShells(std::vector<unsigned>& vMax) const
{
	std::vector<unsigned> MaxArray(m_LastShell+1, 0);

	for (size_t Nsubspace = 0; Nsubspace <= m_Nmax; Nsubspace++)
    {
		size_t nConfs = GetNumberOfConfigurations(Nsubspace);
		std::vector<tSHELLSCONFIG> ShellConfs(m_ShellConfigurations[Nsubspace]);
		size_t i, j;
		for (i = 0; i < nConfs; ++i)
		{
			tSHELLSCONFIG Configuration(ShellConfs[i]); // Configuration = distribution of fermions over HO shells
			for (j = 0; j <= m_LastShell; ++j)
			{
				if (!Configuration[j]) {
					continue;
				}
				if (Configuration[j] > MaxArray[j]) {
					MaxArray[j] = Configuration[j];
				}
			}
		}
	}
	vMax = MaxArray;
}


void CShellConfigurations::ShowConfigurations()
{
	for (size_t Nsubspace = 0; Nsubspace <= m_Nmax; Nsubspace++)
    {
		size_t nConfs = GetNumberOfConfigurations(Nsubspace);
		size_t i, j;
		std::cout << Nsubspace << "hw model space contains " << nConfs << " configurations:" << std::endl;
		std::vector<tSHELLSCONFIG> ShellConfs(m_ShellConfigurations[Nsubspace]);
		for (i = 0; i < nConfs; ++i)
		{
			tSHELLSCONFIG Configuration(ShellConfs[i]);
			for (j = 0; j <= m_LastShell; ++j)
			{
				std::cout << Configuration[j] << " ";
			}
			std::cout << std::endl;
		}
	}
}
