#ifndef NCSMSU3BASIS
#define NCSMSU3BASIS
#include <SU3ME/BaseSU3Irreps.h>
#include <SU3ME/SU3xSU2Basis.h>
#include <SU3ME/distr2gamma2omega.h>
#include <climits>

/*
struct IncludeAllSpins
{
	inline bool operator() (const SU2_VEC& S) const { return false; }
};

struct IncludeAllSU3
{
	inline bool operator() (const SU3_VEC& ir) const { return false; }
};
*/


struct SU3C2GreaterEqual
{
	int C2min_;
	SU3C2GreaterEqual(const int C2min): C2min_(C2min) {}
//	SU3C2GreaterEqual(const SU3C2GreaterEqual& Sel): C2min_(Sel.C2min_) {}
	inline bool operator() (const SU3::LABELS& ir) const 
	{ 
		return ir.C2() < C2min_; 
//		return ir.lm != 4 || ir.mu != 2;
	}
};

struct SpinLessEqual
{
	int Smax_;
	SpinLessEqual(const int S): Smax_(S) {}
	inline bool operator() (const SU2::LABEL& S) const 
	{ 
		return S > Smax_; 
//		return S != 5;
	}
};




class IdenticalFermionsNCSMBasis: public CBaseSU3Irreps
{
	public:
	typedef unsigned long IdType;

	struct NhwSubspace
	{
//		NhwSubspace(const char N, const int C2min, const int S2max): N_(N), C2SelectionRule_(C2min), SpinSelectionRule_(S2max) {}
		NhwSubspace(const char N, const SU3C2GreaterEqual& C2SelectionRule, const SpinLessEqual& SpinSelectionRule): N_(N), C2SelectionRule_(C2SelectionRule), SpinSelectionRule_(SpinSelectionRule) {}
		char N() const {return N_;}
		const SU3C2GreaterEqual& SU3SelectionRule() const {return C2SelectionRule_;}
		const SpinLessEqual& 	 SpinSelectionRule() const {return SpinSelectionRule_;}
		private:
		char N_;
		SU3C2GreaterEqual C2SelectionRule_;
		SpinLessEqual	SpinSelectionRule_;	
	};

	typedef std::vector<NhwSubspace> NCSMModelSpace;

	IdenticalFermionsNCSMBasis(const int A, const NCSMModelSpace& modelSpace, int Jcut = INT_MAX);
	IdenticalFermionsNCSMBasis(const IdenticalFermionsNCSMBasis& basis): 
		CBaseSU3Irreps(basis), ncsmModelSpace_(basis.ncsmModelSpace_), distributions_(basis.distributions_), vOmega_(basis.vOmega_),
		currentDistribution_(basis.currentDistribution_), currentGamma_(basis.currentGamma_), currentOmega_(basis.currentOmega_), 
		maxnSU3Omegas_(basis.maxnSU3Omegas_), maxnSU2Omegas_(basis.maxnSU2Omegas_), maxnOmegasInGamma_(basis.maxnOmegasInGamma_), dim_(basis.dim()) {}
	
	inline bool hasDistr() const {return currentDistribution_ != distributions_.end();}
	inline bool hasGamma() const {return currentGamma_ != currentDistribution_->vgamma_.end();}
	inline bool hasOmega() const {return currentOmega_ != vOmega_.end();}

	inline void nextDistr() { ++currentDistribution_; }
	inline void nextGamma()
	{
		++currentGamma_; 
		if (!vOmega_.empty())
		{
			omega_su2_.resize(0);
			omega_su3_.resize(0);
			vOmega_.resize(0);
		}
	}
	inline void nextOmega() { ++currentOmega_; } 

	inline void rewindDistr() { currentDistribution_ = distributions_.begin();}
	inline void rewindGamma() { currentGamma_ = currentDistribution_->vgamma_.begin(); }
	void rewindOmega();

	inline void setDistr(const IdenticalFermionsNCSMBasis& basis) { currentDistribution_ = distributions_.begin() + (basis.currentDistribution_ - basis.distributions_.begin());}
	inline void setGamma(const IdenticalFermionsNCSMBasis& basis) { currentGamma_ = currentDistribution_->vgamma_.begin() + (basis.currentGamma_ - basis.currentDistribution_->vgamma_.begin());}
	inline void setOmega(const IdenticalFermionsNCSMBasis& basis) 
	{ 
		vOmega_ = basis.vOmega_;
		currentOmega_ = vOmega_.begin() + (basis.currentOmega_ - basis.vOmega_.begin());
	}

	const tSHELLSCONFIG& getDistr() const {return currentDistribution_->distribution_;}
	const UN::SU3xSU2_VEC& getGamma() const {return currentGamma_->first;}
	inline int getGammaMult() const
	{
		int ntot = 1;
		for (size_t i = 0; i < currentGamma_->first.size(); ++i)
		{ 
			ntot *= currentGamma_->first[i].mult;
		}
		return ntot;
	}
	const SU3xSU2_VEC& getOmega() const {return currentOmega_->first;}
	inline int getOmegaMult() const
	{
		int ntot = 1;
		for (size_t i = 0; i < currentOmega_->first.size(); ++i)
		{ 
			ntot *= currentOmega_->first[i].rho;
		}
		return ntot;
	}

	inline int Nmax() const {return ncsmModelSpace_.back().N();}
	inline IdType dim() const {return dim_;}

	IdType dimDistr() const;
	IdType dimGamma() const;
	IdType dimOmega() const;

	inline IdType getFirstStateIdDistr() const {return currentDistribution_->vgamma_.front().second;}
	inline IdType getFirstStateIdGamma() const {return currentGamma_->second;}
	inline IdType getFirstStateIdOmega() const {return currentOmega_->second;}

	inline SU3xSU2::LABELS getCurrentSU3xSU2() const
	{
		if (currentOmega_->first.empty())	//	vector of inter-shell coupled SU3xSU2 is empty ==> we are dealing with a single-shell U(N)>SU3xSU2 many-body basis state
		{
			return SU3xSU2::LABELS(currentGamma_->first[0].lm, currentGamma_->first[0].mu, currentGamma_->first[0].S2);
		}
		else
		{
			return currentOmega_->first.back();
		}
	}

	inline size_t maxnSU3Omegas() const {return maxnSU3Omegas_;}
	inline size_t maxnSU2Omegas() const {return maxnSU2Omegas_;}
	inline size_t maxnOmegasInGamma() const {return maxnOmegasInGamma_;}

	private:
	inline const SU3C2GreaterEqual& CurrentSU3SelectionRule() const {return currentDistribution_->pNhwsubspace_->SU3SelectionRule();}
	inline const SpinLessEqual& CurrentSpinSelectionRule() const {return currentDistribution_->pNhwsubspace_->SpinSelectionRule();}
	size_t GenerateGammasAndGammaIds(const tSHELLSCONFIG& distribution, const IdType firstStateId, const NhwSubspace& Nhwsubspace, std::vector<std::pair<UN::SU3xSU2_VEC, IdType> >& vgamma);

	void ShowCurrentDistr();

	struct Distr 
	{
		Distr(const NhwSubspace* pNhwsubspace, const tSHELLSCONFIG& distribution, const std::vector<std::pair<UN::SU3xSU2_VEC, IdType> >& vgamma):
		pNhwsubspace_(pNhwsubspace), distribution_(distribution), vgamma_(vgamma) {}
		inline IdType getId() const {return vgamma_.front().second;}

		const NhwSubspace* pNhwsubspace_;	//	pointer to N, SU3 selection rule, and spin selection rule
		tSHELLSCONFIG distribution_;
		inline operator IdType() const {return getId();}
		std::vector<std::pair<UN::SU3xSU2_VEC, IdType> > vgamma_;
	};

	inline size_t getNumberOfDistr() const {return distributions_.size();}

	IdType dim_;
	NCSMModelSpace ncsmModelSpace_;

	std::vector<Distr> distributions_;	// each element corresponds to 
	std::vector<std::pair<SU3xSU2_VEC, IdType> > vOmega_;
	std::vector<SU3_VEC> omega_su3_;
	std::vector<SU2_VEC> omega_su2_;
			

	std::vector<Distr>::const_iterator currentDistribution_;
	std::vector<std::pair<UN::SU3xSU2_VEC, IdType> >::const_iterator currentGamma_;
	std::vector<std::pair<SU3xSU2_VEC, IdType> >::const_iterator currentOmega_;
	size_t maxnOmegasInGamma_, maxnSU3Omegas_, maxnSU2Omegas_;
};

//	This function is called by
//	IdenticalFermionsNCSMBasis::GenerateGammasAndGammaIds. As the input takes
//	list of U(N)>SU(3)xSU(2) irreps [gamma] and su3 and su3 selection rules
//	[excludeSU3] and [excludeSpin].  It calculates number of basis states, and
//	also maximal number od SU(3) couplings [maxnSU3Omegas] which is returned by
//	gamma2su3vec(...) function, and maximal number of spin couplings
//	[maxnSU2Omegas] returned by gamma2su2sel(...). The third value that is
//	being calculated is maximal number of possible SU(3)xSU(2) irreps resulting
//	from intercoupling of gamma [maxnOmegasInGamma]. The third value that is
//	being calculated is maximal number of possible SU(3)xSU(2) irreps resulting
//	from intercoupling of gamma [maxnOmegasInGamma].
//
//	maxnSU3Omegas, maxnSU2Omegas, and maxnOmegasInGamma are used to determine
//	size of memory that is preallocated in the constructor of IdenticalFermionsNCSMBasis
template <class Su3SelectionRule, class Su2SelectionRule>
unsigned long GetNumberOfStatesInGamma(const UN::SU3xSU2_VEC& gamma, const Su3SelectionRule& excludeSU3, const Su2SelectionRule& excludeSpin, size_t& maxnSU3Omegas, size_t& maxnSU2Omegas, size_t& maxnOmegasInGamma)
{
	assert(!gamma.empty());

	std::vector<SU3xSU2_VEC> omega;
	size_t a = gamma_mult(gamma);	//	a = aplha_{1} * alpha_{2} * alpha_{3} .... *alpha_{N}
	
	if (gamma.size() > 1)
	{
		size_t nSU3Omegas, nSU2Omegas;
		gamma2omega(gamma, excludeSU3, excludeSpin, nSU3Omegas, nSU2Omegas, omega);	//	generate vector of all omegas in gamma
		if (nSU3Omegas > maxnSU3Omegas)
		{
			maxnSU3Omegas = nSU3Omegas;
		}
		if (nSU2Omegas > maxnSU2Omegas)
		{
			maxnSU2Omegas = nSU2Omegas;
		}
	}
	else	//	gamma = {a (lm mu)S} ==> omega = {1(lm mu)S}
	{
		SU3::LABELS ir(gamma[0].lm, gamma[0].mu);
		if (excludeSU3(ir))
		{
			return 0;
		}
		SU2::LABEL S2(gamma[0].S2);
		if (excludeSpin(S2))
		{
			return 0;
		}
		omega.push_back(SU3xSU2_VEC(1, SU3xSU2::LABELS(ir, S2)));
	}

	if (omega.size() > maxnOmegasInGamma)
	{
		maxnOmegasInGamma = omega.size();
	}

	unsigned long nstates = 0;
	for (size_t i = 0; i < omega.size(); ++i)
	{
		nstates += a*omega_mult(omega[i])*SU3xSU2::BasisIdenticalFermsCompatible::getDim(omega[i].back());
	}
	return nstates;
}
#endif
