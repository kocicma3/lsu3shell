#include <SU3ME/Tensor.h>

#include <functional>
#include <algorithm>
#include <numeric>
#include <stdexcept>
#include <iostream>

typedef std::pair<EPS1LM1EPS2LM2EPS3LM3, std::vector<double> > SU3CGdataItem;

struct less_EPS3LM3 
{
	bool operator()(const SU3CGdataItem& lhs, const SU3CGdataItem& rhs)
	{
		return lhs.first.eps3() < rhs.first.eps3() || (lhs.first.eps3() == rhs.first.eps3() && lhs.first.LM3() < rhs.first.LM3());
	}
	bool operator()( const SU3CGdataItem& lhs, const EPS1LM1EPS2LM2EPS3LM3& rhs ) const 
	{ 
		return lhs.first.eps3() < rhs.eps3() || (lhs.first.eps3() == rhs.eps3() && lhs.first.LM3() < rhs.LM3()); 
	};
   	bool operator()( const EPS1LM1EPS2LM2EPS3LM3& lhs, const SU3CGdataItem& rhs ) const 
	{
		return lhs.eps3() < rhs.first.eps3() || (lhs.eps3() == rhs.first.eps3() && lhs.LM3() < rhs.first.LM3()); 
	};
};

struct Resizer
{
	private:
	size_t nElems;
	public:
	Resizer(size_t n): nElems(n) {};
	void operator() (std::vector<double>& v)
	{
		v.resize(nElems);
		std::vector<double>(v).swap(v);
	}
};

namespace SU3xSU2 {

// This constructore creates Tensors representing single creation/annihilation operator
Tensor::Tensor(const SU3xSU2::LABELS& RhoLmMuS2, OperatorType Type): SU3xSU2::LABELS(RhoLmMuS2)
{
	m_nBasisOperators = 1; 
	m_max_rho = 1;
	m_OperatorType = Type;
	m_Structure.push_back((Type == CREATION) ? (RhoLmMuS2.lm + 1) : -1*(RhoLmMuS2.mu + 1));

	size_t n = abs(m_Structure[0])-1;
	size_t nsps = nSPS(n);
	if (nsps > nspsmax) 
	{
		std::cerr << "Construction of tensors for harmonic ocillator shell n=" << (int)abs(m_Structure[0])-1 << " has not been implemented yet." << std::endl;
		exit(EXIT_FAILURE);
	}
	m_annihilStart = (m_Structure[0] > 0); 	//	if m_Structure[0] > 0 ==> Tensor == a+ and m_annihilStart = 1. As a consequence (m_Structure.size()==1)-m_annihilStart = 0
											//	and hence method GetNumberOfAnnihilators() returns 0 in case a+ tensor
//	alternatively, one can also use std::find_if, but that is too cumbersome in this trivial case											
//	m_annihilStart = std::find_if(m_Structure.begin(), m_Structure.end(), std::bind2nd(std::less<char>(), 0)) - m_Structure.begin();
}

Tensor::Tensor(	const Tensor& X, const size_t rhoX, 
				const Tensor& Y, const size_t rhoY, 
				const SU3xSU2::LABELS& ResultXY, bool fDiagnostic): SU3xSU2::LABELS(ResultXY)
{
	if (!SU2::mult(X.S2, Y.S2, S2))
	{
		std::cerr << "Error in Tensor::Tensor(X, Y, LABELS):  intrinsic spin does not couple (" << (int)X.S2 << "/2 x " << (int)Y.S2 << "/2 -> " << (int)S2 << "/2)" << std::endl;
		exit(EXIT_FAILURE);
	}
	m_max_rho = SU3::mult(X, Y, *this);
	if (!m_max_rho) {
		std::cerr << "Error in Tensor::Tensor(X, Y, LABELS):  (" << (int)X.lm << " " << (int)X.mu;
		std::cerr << ") and (" << (int)Y.lm << " " << (int)Y.mu << ") does not couple to ";
		std::cerr << "(" << (int)ResultXY.lm << " " << (int)ResultXY.mu << ")" << std::endl;
		exit(EXIT_FAILURE);
	}
	m_nBasisOperators = X.GetNBasisOperators() + Y.GetNBasisOperators();
	m_OperatorType = GENERAL;
	m_Structure.insert(m_Structure.begin(), X.m_Structure.begin(), X.m_Structure.end());
	m_Structure.insert(m_Structure.end(), Y.m_Structure.begin(), Y.m_Structure.end());

	assert(m_nBasisOperators == m_Structure.size());

//	find the first annihilation operator in m_Structure, that is, the first
//	negative element
	m_annihilStart = std::find_if(m_Structure.begin(), m_Structure.end(), std::bind2nd(std::less<char>(), 0)) - m_Structure.begin();

//	22.05.2010: I do not understand the logic of the following comment and setting rho = -1 ==> I decided to comment rho = -1 line.
//
// 	30.05.2010: This constructor generates all tensor components for all multiplicities rho
// 	and hence we set rho = -1
//	rho = -1; 
//	However, in some logic rho also corresponds to rho_max ... and hence it should
//	be in this case equal to m_max_rho.
//	At least in some part of code I was using Tensor.rho to get rho0_max and it failed since Tensor.rho = -1.	
//	It is probably safer to set:
	rho = m_max_rho;
	std::vector<SU3xSU2::CANONICAL> vXYlabels;
//	Generate canonical basis
	SU3xSU2::GetCanonicalBasis(ResultXY, vXYlabels, false); // false = sorting turned off
	GenerateComponents(X, rhoX, Y, rhoY, vXYlabels, fDiagnostic);
}

Tensor::Tensor( const Tensor& X,	const size_t rhoX, 
				const Tensor& Y,	const size_t rhoY, 
				const SU3xSU2::LABELS& ResultXY,
				const std::vector<SU3xSU2::CANONICAL>& Components2Costruct, bool fDiagnostic): SU3xSU2::LABELS(ResultXY)
{
	if (!SU2::mult(X.S2, Y.S2, S2))
	{
		std::cerr << "Error in Tensor::Tensor(X, Y, LABELS):  intrinsic spin does not couple." << std::endl;
		exit(EXIT_FAILURE);
	}
	m_max_rho = SU3::mult(X, Y, *this);
	if (!m_max_rho) {
		std::cerr << "Error in Tensor::Tensor(X, Y, LABELS):  (" << X.lm << " " << X.mu;
		std::cerr << ") and (" << Y.lm << " " << Y.mu << ") does not couple to ";
		std::cerr << "(" << ResultXY.lm << " " << ResultXY.mu << ")" << std::endl;
		exit(EXIT_FAILURE);
	}
	m_nBasisOperators = X.GetNBasisOperators() + Y.GetNBasisOperators();
	m_OperatorType = GENERAL;
	m_Structure.insert(m_Structure.begin(), X.m_Structure.begin(), X.m_Structure.end());
	m_Structure.insert(m_Structure.end(), Y.m_Structure.begin(), Y.m_Structure.end());

//	find the first annihilation operator in m_Structure, that is, the first
//	negative element
	m_annihilStart = std::find_if(m_Structure.begin(), m_Structure.end(), std::bind2nd(std::less<char>(), 0)) - m_Structure.begin();

//	22.05.2010: I do not understand the logic of the following comment and setting rho = -1 ==> I decided to comment rho = -1 line.
//
// 	We generate tensor components provided in the list Components2Costruct for
// 	all multiplicities rho and hence we set rho = -1 since it is redundant
//	rho = -1; 
//	However, in some logic rho also corresponds to rho_max ... and hence it should
//	be in this case equal to m_max_rho.
//	At least in some part of code I was using Tensor.rho to get rho0_max and it failed since Tensor.rho = -1.	
//	It is probably safer to set:
	rho = m_max_rho;
	GenerateComponents(X, rhoX, Y, rhoY, Components2Costruct, fDiagnostic);
}



void Tensor::GenerateComponents(const Tensor& X, const size_t rhoX, 	
								const Tensor& Y, const size_t rhoY, 
								const std::vector<SU3xSU2::CANONICAL>& Components2Costruct, bool fDiagnostic)
{
	if (!m_TensorComponents.empty())
	{
		m_TensorComponents.clear();
	}

	std::vector<std::vector<tSpinCG> > SpinCGRepository;	//	SpinCGRepository holds all non-zero SU(2) coefficients
	std::vector<SU3CGdataItem> vSU3CGs;
	std::pair<std::vector<SU3CGdataItem>::const_iterator, std::vector<SU3CGdataItem>::const_iterator> SU3CGRange;
	std::vector<SU3CGdataItem>::const_iterator SU3CGdataIter;
	EPS1LM1EPS2LM2EPS3LM3 eps3LM3toFind;
	double dSpinCG, dCG;
	std::vector<double> Coeff(m_max_rho, 0.0);
	size_t iSpin, nSpinCG, irho, j, nTensorComponents;
	SU3xSU2::CANONICAL Xlabels, Ylabels;
	CSU3CGMaster SU3CGFactory;

//	Calculate spin SU(2) CG's 
	GenerateSpinCGs(X.S2, Y.S2, S2, SpinCGRepository);
//	Calculate SU(3) CG's
	SU3CGFactory.GetSU2U1(X, Y, *this, vSU3CGs);
//	sort SU(3) CG's according to {eps3, LM3}
	std::sort(vSU3CGs.begin(), vSU3CGs.end(), less_EPS3LM3());

	nTensorComponents = Components2Costruct.size();
//	iterate over Components2Costruct {eps0 LM0 M_LM0 Sigma0}
	for (size_t i = 0; i < nTensorComponents; ++i)
	{
		TENSOR_COMPONENT_ALL_RHO CurrentOperator;
//		make sure enough memory is allocated for m_max_rho	vectors
		CurrentOperator.first.resize(m_max_rho);

		eps3LM3toFind.eps3(Components2Costruct[i].eps);
		eps3LM3toFind.LM3(Components2Costruct[i].LM);

		SU3CGRange = std::equal_range(vSU3CGs.begin(), vSU3CGs.end(), eps3LM3toFind, less_EPS3LM3());

		if (fDiagnostic) std::cout << "Tensor: {" << Components2Costruct[i] << "}:" << std::endl;

//	iterate over eps1 lm1 eps2 lm2 
		for (SU3CGdataIter = SU3CGRange.first; SU3CGdataIter != SU3CGRange.second; ++SU3CGdataIter)
		{
//	SU3CG contains a vector of SU(3) CG's with m_max_rho elements
			const std::vector<double>& SU3CG = (*SU3CGdataIter).second; 
//	Generally, SU(3) CG's are not guaranteed to be non-zero (for instance
//	< (0 2) -1  1; (0 2) -1  1 || (1 2) -2  2  > == 0) and hence we need
//	to check whether a given SU(3) CG's are not all equal to zero
			if (std::count_if(SU3CG.begin(), SU3CG.end(), Negligible) == m_max_rho) {
				continue;
			}
			Xlabels.eps = (*SU3CGdataIter).first.eps1(); 
			Xlabels.LM  = (*SU3CGdataIter).first.LM1();
			Ylabels.eps = (*SU3CGdataIter).first.eps2();
			Ylabels.LM  = (*SU3CGdataIter).first.LM2();
//	iterate over M_LM1 M_LM2				
			for (Xlabels.M_LM = -Xlabels.LM; Xlabels.M_LM <= Xlabels.LM; Xlabels.M_LM += 2)
			{
				Ylabels.M_LM = Components2Costruct[i].M_LM - Xlabels.M_LM;
#if defined(HECHT)				
				dCG = clebschGordan(Xlabels.LM, Xlabels.M_LM, Ylabels.LM, Ylabels.M_LM, Components2Costruct[i].LM, Components2Costruct[i].M_LM);
#elif  defined(DRAAYER)
				dCG = clebschGordan(Xlabels.LM, -Xlabels.M_LM, Ylabels.LM, -Ylabels.M_LM, Components2Costruct[i].LM, -Components2Costruct[i].M_LM);
#endif
				if (Negligible(dCG)) {
					continue;
				}
//	SpinCGRepository[iSpin] contains non-vanishing spin CG's needed for (S1 x S2) -> (S2 ;Components2Costruct[i].Sigma) construction
				iSpin = (S2 - Components2Costruct[i].Sigma)/2;
//	vector of non-vanishing spin SU(2) CG ... clebschGordan(X.S2, *, Y.S2, *, S2, Components2Costruct[i].Sigma)
				const std::vector<tSpinCG>& rSpinCGrow = SpinCGRepository[iSpin]; 
				nSpinCG = rSpinCGrow.size();
//	iterate over X.Sigma, Y.Sigma
				for (j = 0; j < nSpinCG; ++j)
				{
					Xlabels.Sigma = (rSpinCGrow[j].first)[0];
					Ylabels.Sigma = (rSpinCGrow[j].first)[1];
					dSpinCG = rSpinCGrow[j].second;
////////////////////////////////////////////////////////////////////////////////////////////
//	now I know components of X and Y needed
//						iterate over multiplicity rho (SU3::ResultXY) <--- (SU3::X x SU3::Y)
					for (irho = 0; irho < m_max_rho; ++irho)
					{
						Coeff[irho] = dSpinCG*dCG*SU3CG[irho];

if (fDiagnostic) 
{
	std::cout << Coeff[irho] << " * {";
	if (X.m_OperatorType < GENERAL) 
	{
		std::cout << ((X.m_OperatorType == CREATION) ? CreationTensorToSpsIndex(Xlabels): AnnihilationTensorToSpsIndex(Xlabels));
	} else {
		std::cout << Xlabels;
	}
	if (Y.m_OperatorType < GENERAL)
	{
		std::cout << ", " << ((Y.m_OperatorType == CREATION) ? CreationTensorToSpsIndex(Ylabels) : AnnihilationTensorToSpsIndex(Ylabels)) << "}\t\t";
	}
	else 
	{
		std::cout <<  ", " << Ylabels << "}\t\t";
	}
}

					}
					Add(X, rhoX, Xlabels, Y, rhoY, Ylabels, Coeff, CurrentOperator);
////////////////////////////////////////////////////////////////////////////////////////////
if (fDiagnostic) 
{										
						std::cout << std::endl;
}
				}
			}
		}
		if ((m_TensorComponents.insert(std::make_pair(Components2Costruct[i], CurrentOperator))).second == false)
		{
			std::cerr << "Inserting tensor operator into m_TensorComponents data structure has failed!" << std::endl;
			exit(EXIT_FAILURE);
		}
if (fDiagnostic)
{	
			std::cout << std::endl;
}			
	}
}


void Tensor::EliminateRedundancy()
{
	assert(m_nBasisOperators > 1);

	if  (!IsPureType())
	{
		std::cerr << "Error: the operator is not composed entirely from creation/annihilation operators" << std::endl;
		exit(EXIT_FAILURE);
	}
	size_t n = abs(m_Structure[0]) - 1;
	size_t nsps = (n + 1)*(n + 2)/2;

	assert(nsps <= nspsmax);

	if (nsps <= 32) 
	{
		Eliminator<UN::BASIS_STATE_32BITS>();
	}
	else if (nsps <= 64) 
	{
		Eliminator<UN::BASIS_STATE_64BITS>();
	}
	else if (nsps <= 128) 
	{
		Eliminator<UN::BASIS_STATE_128BITS>();
	}
	else if (nsps <= 256)
	{
		Eliminator<UN::BASIS_STATE_256BITS>();
	}
	else 
	{
		std::string error_message("Calculation of single-shell rme for harmonic oscillator shell n = ");
		error_message += n;
		error_message += " has not been implemented yet!";
		throw std::logic_error(error_message);
	}
}

void Tensor::RenormalizeTensorComponents()
{
	size_t irho;
	double dNorm;
	std::map<SU3xSU2::CANONICAL, TENSOR_COMPONENT_ALL_RHO>::iterator it = m_TensorComponents.begin();
	for (; it != m_TensorComponents.end(); ++it)
	{
		std::vector<std::vector<double> >& rCoeffs = (it->second).first;
		for (irho = 0; irho < m_max_rho; ++irho)
		{
			dNorm = sqrt(std::inner_product(rCoeffs[irho].begin(), rCoeffs[irho].end(), rCoeffs[irho].begin(), 0.0));
			std::cout << "Norm = " << dNorm << std::endl;
			std::transform(rCoeffs[irho].begin(), rCoeffs[irho].end(), rCoeffs[irho].begin(), std::bind2nd(std::divides<double>(), dNorm));
		}
	}
}

void Tensor::ShowTensorComponents()
{
	std::cout << "Operator structure: ";
	for (size_t i = 0; i < m_nBasisOperators; ++i)
	{
		std::cout << ((m_Structure[i] > 0) ? "ad_{" : "at_{");
		std::cout << ((m_Structure[i] > 0) ? (m_Structure[i]-1) : -1*(m_Structure[i]+1));
		std::cout << "}";
	}
	std::cout << std::endl;

	std::map<SU3xSU2::CANONICAL, TENSOR_COMPONENT_ALL_RHO>::const_iterator cit = m_TensorComponents.begin();
	for (; cit != m_TensorComponents.end(); ++cit)
	{
		SU3xSU2::CANONICAL TensorLabels(cit->first);
		std::cout << "Tensor: {" << TensorLabels << "}:" << std::endl;
		const std::vector<std::vector<double> >& rCoeffs = (cit->second).first;

		const std::vector<SPS_INDEX>& Indices = (cit->second).second;
		size_t nCoeffs = rCoeffs[0].size();
		size_t isps = 0;
		for (size_t j = 0; j < nCoeffs; ++j)
		{
			for (size_t irho = 0; irho < m_max_rho; ++irho)
			{
				std::cout << rCoeffs[irho][j] << "\t";
			}
			std::cout << "{";
			for (size_t k = 0; k < m_nBasisOperators - 1; ++k)
			{
				std::cout << Indices[isps++] << ", ";
			}
			std::cout << Indices[isps++] << "}" << std::endl;
		}
		std::cout << std::endl;
	}
}
void Tensor::ShowTensorComponent(const SU3xSU2::CANONICAL& TensorComponent)
{
	std::cout << "Operator structure: ";
	for (size_t i = 0; i < m_nBasisOperators; ++i)
	{
		std::cout << ((m_Structure[i] > 0) ? "ad_{" : "at_{");
		std::cout << ((m_Structure[i] > 0) ? (m_Structure[i]-1) : -1*(m_Structure[i]+1));
		std::cout << "}";
	}
	std::cout << std::endl;

	std::map<SU3xSU2::CANONICAL, TENSOR_COMPONENT_ALL_RHO>::const_iterator cit = m_TensorComponents.find(TensorComponent);
	if (cit != m_TensorComponents.end())
	{
		std::cout << "Tensor: {" << TensorComponent << "}:" << std::endl;
		const std::vector<std::vector<double> >& rCoeffs = (cit->second).first;

		const std::vector<SPS_INDEX>& Indices = (cit->second).second;
		size_t nCoeffs = rCoeffs[0].size();
		size_t isps = 0;
		for (size_t j = 0; j < nCoeffs; ++j)
		{
			for (size_t irho = 0; irho < m_max_rho; ++irho)
			{
				std::cout << rCoeffs[irho][j] << "\t";
			}
			std::cout << "{";
			for (size_t k = 0; k < m_nBasisOperators - 1; ++k)
			{
				std::cout << Indices[isps++] << ", ";
			}
			std::cout << Indices[isps++] << "}" << std::endl;
		}
		std::cout << std::endl;
	}
	else 
	{
		std::cerr << "Unable to find component " << TensorComponent << " in the tensor data structure." << std::endl;
	}
}


Tensor::TENSOR_COMPONENT_SINGLE_RHO Tensor::Component(const size_t irho, const SU3xSU2::CANONICAL& labels) const
{
	assert(m_OperatorType == GENERAL);
	
	std::map<SU3xSU2::CANONICAL, TENSOR_COMPONENT_ALL_RHO>::const_iterator cit = m_TensorComponents.find(labels);
	if (cit == m_TensorComponents.end()) 
	{
		std::cerr << "tensor component " << labels << " was not found" << std::endl;
		exit(EXIT_FAILURE);
	}
	return std::make_pair(&(((cit->second).first)[irho]), &((cit->second).second));
}

void Tensor::Add(
				const Tensor& X, const size_t rhoX, const SU3xSU2::CANONICAL& Xlabels, 
				const Tensor& Y, const size_t rhoY, const SU3xSU2::CANONICAL& Ylabels, 
				const std::vector<double>& Coeff, TENSOR_COMPONENT_ALL_RHO& CurrentOperator) const 
{
	if (X.m_OperatorType == GENERAL && Y.m_OperatorType == GENERAL)
	{
		TENSOR_COMPONENT_SINGLE_RHO Xtensor = X.Component(rhoX, Xlabels);
		TENSOR_COMPONENT_SINGLE_RHO Ytensor = Y.Component(rhoY, Ylabels);

		const std::vector<SPS_INDEX>*  Xindices = Xtensor.second;
		const std::vector<SPS_INDEX>*  Yindices = Ytensor.second;
		const std::vector<double>*  Xcoeff = Xtensor.first;
		const std::vector<double>*  Ycoeff = Ytensor.first;

		double dxy;

		size_t nx = Xcoeff->size();
		size_t ny = Ycoeff->size();
		size_t indexY, iy, irho;

		std::vector<SPS_INDEX>::const_iterator XindicesStartPos = Xindices->begin();
		std::vector<SPS_INDEX>::const_iterator XindicesEndPos = XindicesStartPos + X.m_nBasisOperators;
		for (size_t ix = 0; ix < nx; ++ix)
		{
			std::vector<SPS_INDEX>::const_iterator YindicesStartPos = Yindices->begin();
			std::vector<SPS_INDEX>::const_iterator YindicesEndPos = YindicesStartPos + Y.m_nBasisOperators;
			for (iy = 0; iy < ny; ++iy)
			{
				dxy = Xcoeff->operator[](ix)*Ycoeff->operator[](iy);
				for (irho = 0; irho < m_max_rho; ++irho)
				{
					(CurrentOperator.first)[irho].push_back(Coeff[irho]*dxy);
				}
				(CurrentOperator.second).insert((CurrentOperator.second).end(), XindicesStartPos, XindicesEndPos);
				(CurrentOperator.second).insert((CurrentOperator.second).end(), YindicesStartPos, YindicesEndPos);
				YindicesStartPos = YindicesEndPos;
				YindicesEndPos  += Y.m_nBasisOperators;
			}
			XindicesStartPos = XindicesEndPos;
			XindicesEndPos  += X.m_nBasisOperators;
		}
	}
	else if (X.m_OperatorType < GENERAL && Y.m_OperatorType < GENERAL) // (ad ad)  or (at at) or (ad at)
	{
		size_t indexX = (X.m_OperatorType == CREATION) ? CreationTensorToSpsIndex(Xlabels) : AnnihilationTensorToSpsIndex(Xlabels);
		size_t indexY = (Y.m_OperatorType == CREATION) ? CreationTensorToSpsIndex(Ylabels) : AnnihilationTensorToSpsIndex(Ylabels);
		char phase = ((X.m_OperatorType == ANNIHILATION) ? AnnihilationPhase(Xlabels) : 1)*((Y.m_OperatorType == ANNIHILATION) ? AnnihilationPhase(Ylabels) : 1);

		for (size_t irho = 0; irho < m_max_rho; ++irho)
		{
			(CurrentOperator.first)[irho].push_back(phase*Coeff[irho]);
		}
		(CurrentOperator.second).push_back(indexX);
		(CurrentOperator.second).push_back(indexY);
	}
	else if (X.m_OperatorType == GENERAL)
	{
		TENSOR_COMPONENT_SINGLE_RHO Xtensor = X.Component(rhoX, Xlabels);
		const std::vector<SPS_INDEX>*  Xindices = Xtensor.second;
		const std::vector<double>*  Xcoeff = Xtensor.first;
		size_t nx = Xcoeff->size();

		size_t indexY = (Y.m_OperatorType == CREATION) ? CreationTensorToSpsIndex(Ylabels) : AnnihilationTensorToSpsIndex(Ylabels);
		char phase = (Y.m_OperatorType == ANNIHILATION) ? AnnihilationPhase(Ylabels) : 1;
		size_t irho;
		double dx;
	
		std::vector<SPS_INDEX>::const_iterator XindicesStartPos = Xindices->begin();
		std::vector<SPS_INDEX>::const_iterator XindicesEndPos = XindicesStartPos + X.m_nBasisOperators;
		for (size_t ix = 0; ix < nx; ++ix)
		{
			dx = phase*Xcoeff->operator[](ix);
			for (irho = 0; irho < m_max_rho; ++irho)
			{
				(CurrentOperator.first)[irho].push_back(Coeff[irho]*dx);
			}
			(CurrentOperator.second).insert((CurrentOperator.second).end(), XindicesStartPos, XindicesEndPos);
			(CurrentOperator.second).push_back(indexY);
			XindicesStartPos = XindicesEndPos;
			XindicesEndPos  += X.m_nBasisOperators;
		}
	}
	else if (Y.m_OperatorType == GENERAL)
	{
		TENSOR_COMPONENT_SINGLE_RHO Ytensor = Y.Component(rhoY, Ylabels);
		const std::vector<SPS_INDEX>*  Yindices = Ytensor.second;
		const std::vector<double>*  Ycoeff = Ytensor.first;
		size_t ny = Ycoeff->size();

		size_t indexX = (X.m_OperatorType == CREATION) ? CreationTensorToSpsIndex(Xlabels) : AnnihilationTensorToSpsIndex(Xlabels);
		char phase = ((X.m_OperatorType == ANNIHILATION) ? AnnihilationPhase(Xlabels) : 1);
		size_t irho;
		double dy;

		std::vector<SPS_INDEX>::const_iterator YindicesStartPos = Yindices->begin();
		std::vector<SPS_INDEX>::const_iterator YindicesEndPos = YindicesStartPos + Y.m_nBasisOperators;
		for (size_t iy = 0; iy < ny; ++iy)
		{
			dy = phase*Ycoeff->operator[](iy);
			for (irho = 0; irho < m_max_rho; ++irho)
			{
				(CurrentOperator.first)[irho].push_back(Coeff[irho]*dy);
			}
			(CurrentOperator.second).push_back(indexX);
			(CurrentOperator.second).insert((CurrentOperator.second).end(), YindicesStartPos, YindicesEndPos);
			YindicesStartPos = YindicesEndPos;
			YindicesEndPos += Y.m_nBasisOperators;
		}
	}
}

void Tensor::GenerateSpinCGs(	const SU2::LABEL& S1, 
								const SU2::LABEL& S2, 
								const SU2::LABEL& S0, 
								std::vector<std::vector<tSpinCG> >& SpinCGRepository) const
{
	double dSpinCG;
	CTuple<U1::LABEL, 2> Sigma12;
	U1::LABEL Sigma0;
	std::vector<tSpinCG> SpinCGrow;
	SpinCGrow.reserve(SU2::dim(S1));

	SpinCGRepository.reserve(SU2::dim(S0)); 
	for (Sigma0 = S0; Sigma0 >= -S0; Sigma0 -= 2)
	{
		for (Sigma12[0] = -S1; Sigma12[0] <= S1; Sigma12[0] += 2)
		{
			Sigma12[1] = Sigma0 - Sigma12[0];
			dSpinCG = clebschGordan(S1, Sigma12[0], S2, Sigma12[1], S0, Sigma0);
			if (!Negligible(dSpinCG))
			{
                SpinCGrow.push_back(std::make_pair(Sigma12, dSpinCG));
			}
		}
		SpinCGRepository.push_back(SpinCGrow);
		SpinCGrow.resize(0);
	}
}

template <typename UN_BASIS_STATE> 
void Tensor::Eliminator()
{
	typedef std::map<UN_BASIS_STATE, std::vector<double>, UN::CMP_BASIS_STATES<UN_BASIS_STATE> > UNIQUE_TERMS;

	int phase;
	size_t isps, irho, k, j, nTerms;
	UN_BASIS_STATE State;
	size_t nbits = State.first.size();
	typename UNIQUE_TERMS::iterator TermIter;

	TENSOR_STORAGE::iterator TensorComponentIter = m_TensorComponents.begin();
//	iterate over tensorial components	
	for (; TensorComponentIter != m_TensorComponents.end(); ++TensorComponentIter)
	{
//	(TensorComponentIter->second).second  ... array of sps indices of fermion creation/annihilation operators;
//	m_nBasisOperators .. number of creation/annihilation operators => nTerms = #(sps indices)/m_nBasisOperators
		nTerms = (TensorComponentIter->second).second.size()/m_nBasisOperators;

//	mUniqueTerms is a map of bitsets associated with a vector of coefficients
//	for each multiplicity rho		
		UNIQUE_TERMS mUniqueTerms;
		std::vector<std::vector<double> >& Coeffs = (TensorComponentIter->second).first;
		std::vector<SPS_INDEX>& Indices = (TensorComponentIter->second).second;
//	iterate over tensorial component terms
		for (isps = 0, j = 0; j < nTerms; ++j)
		{
//	this loop will be performed up to (irho == m_max_rho) if all coefficients are equal to zero
			for (irho = 0; irho < m_max_rho; ++irho)
			{
				if (!Negligible(Coeffs[irho][j]))
				{
					break;
				}
			}			
			if (irho == m_max_rho) // all coefficients of the j-the term are equal to zero
			{
				isps += m_nBasisOperators;	//	jump over this term
				continue;					//	and skip j-the term
			}
//	first transform representation of j-th term from an array of shorts into two
//	bitsets. One for spin up (State.first) and another for spin down
//	(State.second).
			State.first.reset();
			State.second.reset();
			phase = 0;
			for (k = 0; k < m_nBasisOperators; ++k, ++isps)
			{
				if (Indices[isps] > 0) // sps with spin up
				{
					phase += (typename UN_BASIS_STATE::first_type(State.first) >>= (Indices[isps])).count() + State.second.count();
//	set bit at location (Indices[isps]-1) to 1. Trick: keep in mind that the
//	first, that is 0th, bit corresponds sps index equal to +1 or -1.
					State.first.set(Indices[isps]-1, 1); 
				}
				else // sps with spin down 
				{
					if (Indices[isps] == 0)
					{
						std::cout << TensorComponentIter->first << std::endl;
						assert(Indices[isps] != 0);
					}
//	sps Indices will be replaced/rewritten. Since the value Indices[isps] is
//	negative we need to multiply it by -1 to be able to use for bitwise shift.
					Indices[isps] *= -1;
					phase += (typename UN_BASIS_STATE::second_type(State.second) >>= (Indices[isps])).count();
//	set bit at location index-1 to 1.
					State.second.set(Indices[isps]-1, 1);
				}
			}
			phase = (phase % 2) ? -1.0 : 1.0;
//	check whether some of sps indices occurs more than once => such a term gives rise to vacuum => purely redundant
			if (State.first.count() + State.second.count() < m_nBasisOperators)
			{
				continue;
			}
//	is a term already stored ?
			TermIter = mUniqueTerms.find(State);
			if (TermIter != mUniqueTerms.end())
//	yes	=> add coefficients	
			{
				std::vector<double>& rCoeffsRho = TermIter->second;
				for (irho = 0; irho < m_max_rho; ++irho)
				{
					rCoeffsRho[irho] += phase*Coeffs[irho][j];
				}
			} else 
//	no	=> create vector of coefficients and store it.
			{
				std::vector<double> CoeffsRho(m_max_rho);
				for (irho = 0; irho < m_max_rho; ++irho)
				{
					CoeffsRho[irho] = phase*Coeffs[irho][j];
				}
				mUniqueTerms.insert(std::make_pair(State, CoeffsRho));
			}
		}
//	at this point mUniqueTerms is set. In the next step we will replace
//	corresponding data in m_TensorComponents.

//	resize arrays of coefficients
		std::for_each(Coeffs.begin(), Coeffs.end(), Resizer(mUniqueTerms.size()));
//	resize vector of sps indices
		Indices.resize(mUniqueTerms.size()*m_nBasisOperators);
		std::vector<SPS_INDEX>(Indices).swap(Indices);

		TermIter = mUniqueTerms.begin();
		for (isps = 0, j = 0; TermIter != mUniqueTerms.end(); ++TermIter, ++j)
		{
			State = TermIter->first;
//			short nSpinUp = State.first.count();
//			short nSpinDown = State.second.count();
			for (k = 0; k < nbits; ++k)
			{
				if (State.first.test(k)) {
					Indices[isps++] = k+1;
/*					
					if (--nSpinUp) {
						break;
					}
*/					
				}
			}
			for (k = 0; k < nbits; ++k)
			{
				if (State.second.test(k)) {
					Indices[isps++] = -1*((int)k+1);
/*					
					if (--nSpinDown) {
						break;
					}
*/					
				}
			}
			std::vector<double>& rCoeffs = TermIter->second;
			for (irho = 0; irho < m_max_rho; ++irho)
			{
				Coeffs[irho][j] = rCoeffs[irho];
			}
		}
	}
}

Creation::Creation(size_t n): Tensor(SU3xSU2::LABELS(n, 0, 1), CREATION) 
{
	std::vector<SU3xSU2::CANONICAL> HOShellComponents;
	std::vector<std::vector<double> > vvCoeffs(1, std::vector<double>(1, 1.0));
	std::vector<SPS_INDEX> SpsIndex(1, 0);

	size_t nComponents = SU3xSU2::GetCanonicalBasis(*this, HOShellComponents, false); // false = sorting turned off
	for (size_t i = 0; i < nComponents; ++i)
	{
		SpsIndex[0] = CreationTensorToSpsIndex(HOShellComponents[i]);
		m_TensorComponents.insert(std::make_pair(HOShellComponents[i], std::make_pair(vvCoeffs, SpsIndex)));
	}
}

Annihilation::Annihilation(size_t n): Tensor(SU3xSU2::LABELS(0, n, 1), ANNIHILATION)
{
	std::vector<SU3xSU2::CANONICAL> HOShellComponents;
	std::vector<std::vector<double> > vvCoeffs(1, std::vector<double>(1, 1.0));
	std::vector<SPS_INDEX> SpsIndex(1, 0);

	size_t nComponents = SU3xSU2::GetCanonicalBasis(*this, HOShellComponents, false); // false = sorting turned off
	for (size_t i = 0; i < nComponents; ++i)
	{
		SpsIndex[0] 	= AnnihilationTensorToSpsIndex(HOShellComponents[i]);
		vvCoeffs[0][0] 	= AnnihilationPhase(HOShellComponents[i]);
		m_TensorComponents.insert(std::make_pair(HOShellComponents[i], std::make_pair(vvCoeffs, SpsIndex)));
	}
}

void Tensor::ShowTermIndices(const SU3xSU2::CANONICAL& Component, size_t itensor_term)
{
	TENSOR_STORAGE::iterator TensorComponent = m_TensorComponents.find(Component);
	if (TensorComponent == m_TensorComponents.end())
	{
		std::cerr << "Error in Tensor::ShowTerm ... tensorial component " << Component << " was not found." << std::endl;
		exit(EXIT_FAILURE);
	}
	const std::vector<SPS_INDEX>& TensorIndices = (TensorComponent->second).second;
	size_t isps = itensor_term*m_nBasisOperators;
	std::cout << "{";
	for (size_t k = 0; k < m_nBasisOperators - 1; ++k, ++isps)
	{
		std::cout << TensorIndices[isps] << " ";
	}
	std::cout << TensorIndices[isps] << "}" << std::endl;
}

size_t Tensor::GetdA() const
{
	size_t nBasisOperators = GetNBasisOperators();
	int dA = 0;
	for(size_t i = 0; i < nBasisOperators; ++i)
	{
		dA += (m_Structure[i] > 0) ? 1 : -1;
	}
	return dA;
}

}
