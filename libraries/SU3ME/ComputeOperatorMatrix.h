#ifndef COMPUTE_OPERATOR_MATRIX
#define COMPUTE_OPERATOR_MATRIX

#include <LSU3/ncsmSU3xSU2Basis.h>
#include <LSU3/VBC_Matrix.h>
#include <SU3ME/proton_neutron_ncsmSU3BasisFastIteration.h>
#include <SU3ME/InteractionPPNN.h>
#include <SU3ME/CInteractionPN.h>
#include <SU3ME/MeEvaluationHelpers.h>
#include <SU3ME/CalculateMe_proton_neutron_ncsmSU3BasisJcut.h>
#include <SU3ME/CalculateMe_proton_neutron_ncsmSU3BasisJfixed.h>

#include <vector>

//	Thread friendly, matrix stored in VBC
uintmax_t CalculateME_Scalar_UpperTriang_OpenMP(	
					const CInteractionPPNN& interactionPPNN, 
					const CInteractionPN& interactionPN,
					const lsu3::CncsmSU3xSU2Basis& bra, 
					const lsu3::CncsmSU3xSU2Basis& ket, 
					const unsigned int idiag, 
					const unsigned int jdiag,
                    lsu3::VBC_Matrix& vbc);

//	Matrix stored in CSR
uintmax_t CalculateME_Scalar_UpperTriang(	
					const CInteractionPPNN& interactionPPNN, 
					const CInteractionPN& interactionPN,
					const lsu3::CncsmSU3xSU2Basis& bra, 
					const lsu3::CncsmSU3xSU2Basis& ket, 
					const unsigned long firstStateId_I, const unsigned int idiag, 
					const unsigned long firstStateId_J, const unsigned int jdiag,
					std::vector<float>& vals, std::vector<size_t>& column_indices, std::vector<size_t>& row_ptr);

//	Thread friendly, matrix stored in CSR; non scalar operators
unsigned long CalculateME_NonScalar_FullMatrix_OpenMP_OneBodyOnly(
					const CInteractionPPNN& interactionPPNN,
					const CInteractionPN& interactionPN, // REDUNDANT!!!! TODO: Extract PN part of algorithm into a separate subroutine
					const lsu3::CncsmSU3xSU2Basis& bra, 
					const lsu3::CncsmSU3xSU2Basis& ket, 
					std::vector<float>& vals, std::vector<size_t>& column_indices, std::vector<size_t>& row_ptrs,
// each thread store block identification number in ipin_block_ids vector
					std::vector<uint32_t>& ipin_block_ids);

//	Thread friendly, matrix stored in CSR 
void CalculateME_Scalar_FullMatrix_OpenMP(
					const CInteractionPPNN& interactionPPNN, 
					const CInteractionPN& interactionPN,
					const lsu3::CncsmSU3xSU2Basis& bra, 
					const lsu3::CncsmSU3xSU2Basis& ket, 
					std::vector<float>& vals, std::vector<size_t>& column_indices, std::vector<size_t>& row_ptrs,
// each thread store block identification number in ipin_block_ids vector
					std::vector<uint32_t>& ipin_block_ids);
#endif
