#ifndef CRMECALCULATOR_H
#define CRMECALCULATOR_H

#include <LookUpContainers/WigEckSU3SO3CGTable.h>
#include <mk/pools.h>
#include <SU3ME/RME.h>
#include <SU3ME/rmeTable.h>

#include <boost/container/small_vector.hpp>

#include <algorithm>

class CRMECalculator 
{
	public:
	boost::container::small_vector<SU3xSU2::LABELS, storage_limits::initial_small_vector_size> m_Omega_t;
	boost::container::small_vector<SU3xSU2::RME*, storage_limits::initial_small_vector_size> m_Gamma_t;
	char phase_;
	WigEckSU3SO3CGTable* m_pSU3SO3CGTable;

	CRMECalculator(const boost::container::small_vector_base<SU3xSU2::RME*>& Gamma_t, const boost::container::small_vector_base<SU3xSU2::LABELS>& Omega_t, WigEckSU3SO3CGTable* pSU3SO3CGTable, const int phase)
	:m_Omega_t(Omega_t), m_Gamma_t(Gamma_t), phase_(phase),  m_pSU3SO3CGTable(pSU3SO3CGTable) {}
	//	omega_ket x m_Omega_t ---> omega_bra
	//	Note that m_Omega_t = empty (e.g. in case of <0 0 6 0 | T | 0 0 6 >), then omega_bra and omega_ket must be also empty
	//	and method Couple returns true
	bool Couple(const boost::container::small_vector_base<SU3xSU2::LABELS>& omega_bra, const boost::container::small_vector_base<SU3xSU2::LABELS>& omega_ket) const;
	SU3xSU2::RME* CalculateRME(	const UN::SU3xSU2_VEC& gamma_bra, const boost::container::small_vector_base<SU3xSU2::LABELS>& omega_bra,
								const UN::SU3xSU2_VEC& gamma_ket, const boost::container::small_vector<SU3xSU2::LABELS, storage_limits::initial_small_vector_size>& omega_ket)
	{

		if (m_Omega_t.empty())	//	no multi-shell coupling ==> the tensor is single-shell tensor and we can return rme rightaway
		{
			assert(phase_ == 1); // since the operator is a mere one single-shell tensor ==> no a+/a exchanging is needed ==> phase_1 == -1 should never occur
			assert(omega_ket.empty() && omega_bra.empty());
			assert(gamma_bra.size() == 1 && gamma_ket.size() == 1);
			//	allocate memory for copy of rmes from m_Gamma_t[0]
			SU3xSU2::RME::DOUBLE* prme = new SU3xSU2::RME::DOUBLE[m_Gamma_t[0]->m_ntotal];
			//	copy m_Gamma_t[0] rmes into a allocated array
			memcpy(prme, m_Gamma_t[0]->m_rme, m_Gamma_t[0]->m_ntotal*sizeof(SU3xSU2::RME::DOUBLE));
			//	user needs to (1) delete SU3xSU3::RME->m_rme and then (2) delete SU3xSU2::RME 
			return new SU3xSU2::RME(gamma_bra[0], m_Gamma_t[0]->m_tensor_max, m_Gamma_t[0]->Tensor, gamma_ket[0], prme);
		}

		if (Couple(omega_bra, omega_ket))
		{
			SU3xSU2::RME *presultingRME = new SU3xSU2::RME();
			CalculateRME_2(gamma_bra, omega_bra, m_Gamma_t, m_Omega_t, gamma_ket, omega_ket, *presultingRME);
			if (phase_ == -1)
			{
				for (size_t i = 0; i < presultingRME->m_ntotal; ++i)
				{
					presultingRME->m_rme[i] *= -1.0;
				}
			}
			//	user needs to (1) delete presultingRME->m_rme (allocated inside of CalculateRME_2 and then (2) delete presultingRME itself
			return presultingRME;
		}
		else
		{
			return NULL;	//	does not couple omega_ket x Omega_t ---> omega_bra
		}
	}


	inline const boost::container::small_vector_base<SU3xSU2::LABELS>& GetOmega() const {return m_Omega_t;}
	void GetGamma(SU3xSU2_VEC& Gamma) const 
	{
		for (size_t i = 0; i < m_Gamma_t.size(); ++i)
		{
			Gamma.push_back(m_Gamma_t[i]->Tensor);
		}
	}

	~CRMECalculator()
	{
		for (size_t i = 0; i < m_Gamma_t.size(); ++i)
		{
			m_Gamma_t[i]->~RME();
			mk::pool::rme.free(m_Gamma_t[i]);
		}
	}

	void Show() const;
	void ShowUserFriendly() const;
};
#endif
