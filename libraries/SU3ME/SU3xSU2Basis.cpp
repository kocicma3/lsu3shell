#include <SU3ME/SU3xSU2Basis.h>
namespace SU3xSU2
{
int	BasisIdenticalFermsCompatible::Jcut_ = 255; 

BasisJfixed::BasisJfixed(const SU3xSU2::LABELS& ir, SU2::LABEL JJ): SU3xSU2::LABELS(1, ir.lm, ir.mu, ir.S2), dim_(0), JJ_(JJ)
{
	SO3::LABEL LLmax = 2*(lm + mu);
	for (SO3::LABEL LL = 0; LL <= LLmax; LL += 2)
	{
		unsigned char kmax = SU3::kmax(ir, LL/2);
//	if JJ == -1 -->include all k L pairs of states
		if (kmax && (JJ_ == 0xFF || SU2::mult(LL, S2, JJ_)))
		{
			allowed_LLkappa_.push_back(std::make_pair(LL, kmax));
			dim_ += kmax;
		}
	}
	rewind();
}

BasisJcut::BasisJcut(const SU3xSU2::LABELS& ir, const SU2::LABEL JJcut): SU3xSU2::LABELS(1, ir.lm, ir.mu, ir.S2), Jcut_(JJcut), dim_(0)
{
	//	I use inline function Lmax() insteading saving Lmax_ between variables of Basis in order to save memory size of Basis data structure
	kmax_ = new unsigned char[Lmax() + 1];
	memset(kmax_, 0, Lmax()*sizeof(unsigned char));	//	we need to set all elements of kmax_ to zero
													//	as the algorithm is based on searching for the
													//	first non-zero element. If I do not set elements
													//	of array to 0, they will have a random values ...
	for (L_ = 0; L_ <= Lmax(); ++L_)
	{
		if (Jmin() <= Jcut_)
		{
			kmax_[L_] = SU3::kmax(ir, L_);
			if (kmax_[L_])
			{
				dim_ += kmax_[L_]*((Jmax() - Jmin() + 2)/2);// kmax*( )/2 does not work properly if Jcut is odd ==> kmax*( ( )/2 ) is OK
			}
		}
		//else Jmin = 2L - S_ > Jcut_ and hence it is beyond model space and hence it is not counted
	}
	rewind();
}


}
