#ifndef	OPERATORDATASTRUCTURE_H
#define OPERATORDATASTRUCTURE_H
#include <SU3NCSMUtils/CTuple.h>
#include <SU3ME/global_definitions.h>

/////////////////////////////////////////////////////////////////////////////////////
//	The following set of methods are designed to handle reading, writing,
//	sorting, and searching for an element, of vector of set of labels and associated
//	set of values: vector<pair<Labels, Values> > where 
//	type Labels  = CTuple<LabelType, nLabels> 
//	type Values  = CTuple<ValueType, nValues>
//
//	This data structure can represent two-body matrix elements, JT-coupled basis
//	stored according JPV, J-coupled basis, m-scheme basis, and with a definition
//	how to read from iostream SU3xSU2, SU3, etc ::LABELS, one can use it to save
//	interactions in a SU(3)-tensorial structure
/////////////////////////////////////////////////////////////////////////////////////
template <typename L, typename V>
class OperatorDataStructure
{
	public:
	typedef L  Labels;
	typedef V  Values;
	
	typedef std::pair<Labels, Values> TERM;

	typedef typename L::Data_Type LabelType;
	typedef typename V::Data_Type ValueType;
	protected:
	std::vector<TERM> m_Terms;

	// m_Properties is a set of bits that could describe various features of OperatorDataStructure 
	std::bitset<9> m_Properties;
	protected:
	struct OrderByLabels
	{	
		inline bool operator()(const TERM& left, 
						const TERM& right)
		{
			return left.first < right.first;
		}
	};

	inline size_t GetNumberOfValues() const {return Values::NELEMENTS;};
	inline size_t GetNumberOfLabels() const {return Labels::NELEMENTS;};
	public:
	OperatorDataStructure(const std::string& sFileName);
	OperatorDataStructure() {};
	OperatorDataStructure(const OperatorDataStructure& Operator) { m_Terms = Operator.m_Terms; m_Properties = Operator.m_Properties;}
	//	methods that set and test properties of OperatorDataStructure
	inline void set(const int iType) {m_Properties.set(iType);}
	inline bool test(const int& iType) const {return m_Properties.test(iType);}
	inline bool operator==(const int iType) const {return m_Properties.test(iType);}

	void Show();
	void SaveDataFile(const std::string& sFileName);
	void ReadDataFile(const std::string& sFileName);

	inline void Sort() { std::sort(m_Terms.begin(), m_Terms.end(), OrderByLabels()); }

	bool FindEntry(const L& labels, V& values) const;
	
	inline void push_back(const TERM& Term) { m_Terms.push_back(Term);}
	inline size_t size() const {return m_Terms.size();}
	inline void clear() { m_Terms.clear();}
	inline ValueType GetValue(const size_t iTerm, const size_t iValue) const { return m_Terms[iTerm].second[iValue];}
	inline const Labels& GetLabels(const size_t iTerm) const { return m_Terms[iTerm].first;}
};

//////////////////////////////////////////////////////////////////////////////////////////////
//							OperatorDataStructure
//////////////////////////////////////////////////////////////////////////////////////////////
template <typename L, typename V>
OperatorDataStructure<L, V>::OperatorDataStructure(const std::string& sFileName)
{
	size_t nLabels = GetNumberOfLabels();
	size_t nValues = GetNumberOfValues();
	TERM Term;
	size_t nTerms, j, i;
	int t; // t is used in case LabelType == char. Reason: reading number "10" from text file would produce two char indices 1 and 0 => screws the whole data structure
	bool bIndexIsChar = (sizeof(LabelType) == 1);	// Is LabelType == char ?
	bool bValueIsChar = (sizeof(ValueType) == 1);	// Is Value::Data_Type == char ?

	std::ifstream inter_file(sFileName.c_str());
	if (!inter_file) {
		std::cerr << "Unable to open file " << sFileName << std::endl;
		exit(EXIT_FAILURE);
	}

	while (true)
	{
		for (j = 0; j < nLabels; ++j)
		{
			if (bIndexIsChar) {
				inter_file >> t;
				Term.first[j] = t;
			} else {
				inter_file >> Term.first[j];
			}
		}
		if (inter_file.eof())
		{
			break;
		}

		for (j = 0; j < nValues; ++j)
		{
			if (bValueIsChar) 
			{
				inter_file >> t;
				Term.second[j] = t;
			} 
			else 
			{
				inter_file >> Term.second[j];
			}
		}
		if (inter_file.eof())
		{
			break;
		}

		m_Terms.push_back(Term);
	}
}

template <typename L, typename V>
void OperatorDataStructure<L, V>::SaveDataFile(const std::string& sFileName) 
{
	size_t nLabels = GetNumberOfLabels();
	size_t nValues = GetNumberOfValues();
	TERM Term;
	int t; // needed of LabelType == char  => reading "10" produces two char indices 1 and 0 => screws the whole data structure
	bool bIndexIsChar = (sizeof(LabelType) == 1);  // Is LabelType == char ?
	bool bValueIsChar = (sizeof(ValueType) == 1);  // Is LabelType == char ?
	size_t i, j, nTerms = m_Terms.size();

	std::ofstream inter_file(sFileName.c_str(), std::ios_base::trunc);
	inter_file.precision(std::numeric_limits<double>::digits10);
//	inter_file.precision(10);
	if (!inter_file) {
		std::cerr << "Unable to open file " << sFileName << std::endl;
		exit(EXIT_FAILURE);
	}

	inter_file << nTerms << std::endl;
	for (i = 0; i < nTerms; ++i)
	{
		for (j = 0; j < nLabels; ++j)
		{
			if (bIndexIsChar) {
				t = m_Terms[i].first[j];
				inter_file << t << " ";
			} else {
				inter_file << m_Terms[i].first[j] << " ";
			}
		}
		for (j = 0; j < nValues - 1; ++j)
		{
			if (bValueIsChar) {
				t = m_Terms[i].second[j];
				inter_file << t << " ";
			} else {
				inter_file << m_Terms[i].second[j] << " ";
			}
		}
		if (bValueIsChar) {
			t = m_Terms[i].second[nValues-1];
			inter_file << t;
		} else {
			inter_file << m_Terms[i].second[nValues-1];
		}
		inter_file << std::endl;
	}
}

template <typename L, typename V>
void OperatorDataStructure<L, V>::ReadDataFile(const std::string& sFileName) 
{
	size_t nLabels = GetNumberOfLabels();
	size_t nValues = GetNumberOfValues();
	bool bIndexIsChar = (sizeof(LabelType) == 1);  // Is LabelType == char ?
	bool bValueIsChar = (sizeof(ValueType) == 1);  // Is LabelType == char ?
	size_t i, j, nTerms = m_Terms.size();

	std::ifstream inter_file(sFileName.c_str());
//	inter_file.precision(10);
	if (!inter_file) {
		std::cerr << "Unable to open file " << sFileName << std::endl;
		exit(EXIT_FAILURE);
	}

	inter_file  >> nTerms;
	for (i = 0; i < nTerms; ++i)
	{
		for (j = 0; j < nLabels; ++j)
		{
			if (bIndexIsChar) {
				int t; // needed of LabelType == char  => reading "10" produces two char indices 1 and 0 => screws the whole data structure
				inter_file >> t;
				m_Terms[i].first[j] = t;
			} else {
				inter_file >> m_Terms[i].first[j];
			}
		}
		for (j = 0; j < nValues; ++j)
		{
			if (bValueIsChar) {
				int t; // needed of LabelType == char  => reading "10" produces two char indices 1 and 0 => screws the whole data structure
				inter_file >> t;
				m_Terms[i].second[j] = t;
			} else {
				inter_file >> m_Terms[i].second[j];
			}
		}
	}
}

template<typename L, typename V>
bool OperatorDataStructure<L, V>::FindEntry(const L& labels, V& values) const
{
//	in order to speed up looking for an 
//	static CTuple<ELEMTYPE, nOperators> ZeroValue(ELEMTYPE()); // I assume that default constructor set ELEMTYPE to zero
	TERM EntryToFind(labels, values);
	typename std::vector<TERM>::const_iterator cit = std::lower_bound(m_Terms.begin(), m_Terms.end(), EntryToFind, OrderByLabels());
	if (cit != m_Terms.end() && (cit->first == EntryToFind.first))
	{
//TODO: What method is called in this case ???? ... debug it!!!		
		values = cit->second;
		return true;
	}
	else 
	{
//		Value = ZeroValue;
		return false;
	}
}

template <typename L, typename V>
void OperatorDataStructure<L, V>::Show()
{
	size_t nLabels = GetNumberOfLabels();
	size_t nValues = GetNumberOfValues();
	bool bIndexIsChar = (sizeof(LabelType) == 1);  // Is LabelType == char ?

	for (size_t i = 0; i < m_Terms.size(); ++i)
	{
		for (size_t j = 0; j < nLabels; ++j)
		{
			if (bIndexIsChar) // char 
			{
				std::cout << (int)m_Terms[i].first[j] << " "; 
			} 
			else 
			{
				std::cout << m_Terms[i].first[j] << " "; 
			}
		}
		std::cout << "\t";
		for (size_t j = 0; j < nValues; ++j)
		{
			std::cout << m_Terms[i].second[j] << " \t";
		}
		std::cout << std::endl;
	}
}
#endif
