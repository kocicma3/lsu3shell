#ifndef CSTATE_H
#define CSTATE_H
#include <UNU3SU3/UNU3SU3Basics.h>
#include <SU3ME/Tensor.h>

#include <fstream>
#include <iostream>

using std::cout;
using std::endl;
using namespace SU3xSU2;

template<typename UN_BASIS_STATE>
class CState 
{
public:	
	unsigned m_N; 
	unsigned m_A;
	std::vector<UN_BASIS_STATE> m_State;
	std::vector<std::vector<double> > m_Coeffs;
public:
//	CState(const SU3xSU2::Tensor& tensor);

//	Note that we need HO shell n to be able to iterate only through physically
//	possible sps and not through UN_BASIS_STATE.first.size() which is ALWAYS >
//	then the actual number of sps
	CState(std::ifstream& ifs, const unsigned maxMult, const unsigned n, const unsigned A, bool bPerformSort = false);
	void ShowState();
	inline size_t GetNTerms() const {return m_State.size();}
	inline size_t GetMult() const {return m_Coeffs.size();}
};

//TODO: TEST for bPerformSort = true & false
template <typename UN_BASIS_STATE>
CState<UN_BASIS_STATE>::CState(std::ifstream& ifs, const unsigned maxMult, const unsigned n, const unsigned A, bool bPerformSort)
{
	size_t nBasisStates, i, j, imult;
	int SpsIndex;
	std::vector<double> Row(maxMult);
	UN_BASIS_STATE State;

	m_N = (n+1)*(n+2)/2;
	m_A = A;

	ifs  >> nBasisStates;
	m_State.reserve(nBasisStates);

	m_Coeffs.resize(maxMult);
	for (imult = 0; imult < maxMult; ++imult)	
	{
		m_Coeffs[imult].resize(nBasisStates);
	}

	if (bPerformSort == false)  // NOTE: You'd better make sure it is already sorted!!!!
	{
		for (i = 0; i < nBasisStates; ++i)
		{
			for (imult = 0; imult < maxMult; ++imult)
			{
				ifs >> Row[imult];
				m_Coeffs[imult][i] = Row[imult];
			}
			State.first.reset();
			State.second.reset();
			for (j = 0; j < A; ++j)
			{
				ifs >> SpsIndex;
				if (SpsIndex > 0)
				{
					State.first.set(SpsIndex-1, 1);
				} else {
					State.second.set(abs(SpsIndex)-1, 1);
				}
			}
			m_State.push_back(State);
		}
	} 
	else // file contains an unsorted CState
	{
		typedef std::map<UN_BASIS_STATE, std::vector<double>, UN::CMP_BASIS_STATES<UN_BASIS_STATE> > UNIQUE_TERMS;
		UNIQUE_TERMS mUniqueTerms;
		typename UNIQUE_TERMS::iterator TermIter;

		for (i = 0; i < nBasisStates; ++i)
		{
			for (imult = 0; imult < maxMult; ++imult)
			{
				ifs >> Row[imult];
			}
			State.first.reset();
			State.second.reset();
			for (j = 0; j < A; ++j)
			{
				ifs >> SpsIndex;
				if (SpsIndex > 0)
				{
					State.first.set(SpsIndex-1, 1);
				} else {
					State.second.set(abs(SpsIndex)-1, 1);
				}
			}
			TermIter = mUniqueTerms.find(State);
			if (TermIter == mUniqueTerms.end())
			{
				mUniqueTerms.insert(make_pair(State, Row));
			} 
			else
			{
				std::vector<double>& rCoeffs = TermIter->second;
				for (imult = 0; imult < maxMult; ++imult)
				{
					rCoeffs[imult] += Row[imult];
				}
			}
		}
		for(TermIter = mUniqueTerms.begin(), i = 0; TermIter != mUniqueTerms.end(); ++TermIter, ++i)
		{
			m_State.push_back(TermIter->first);
			for (imult = 0; imult < maxMult; ++imult)
			{
				m_Coeffs[imult][i] = (TermIter->second)[imult];
			}
		}
	} 
}
template <typename UN_BASIS_STATE>
void CState<UN_BASIS_STATE>::ShowState()
{
	size_t maxMult = m_Coeffs.size();

	for (size_t i = 0; i < m_State.size(); ++i)
	{
		for (size_t imult = 0; imult < maxMult; ++imult)
		{
			cout << m_Coeffs[imult][i] << " ";
		}
		cout << "\t{";
		typename UN_BASIS_STATE::first_type Up(m_State[i].first);
		typename UN_BASIS_STATE::second_type Down(m_State[i].second);
		//	start with the left most bit ... this is the order of bits in UN_BASIS_STATE  | 0 0 0 ... 1 1>
		for (size_t ibit = 1; ibit <= m_N; ++ibit)
		{
			if (Up.test(m_N - ibit)) 
			{
				cout << (m_N - ibit + 1) << " ";
			}
		}
		//	start with the left most bit ... this is the order of bits in UN_BASIS_STATE  | 0 0 0 ... 1 1>
		for (size_t ibit = 1; ibit <= m_N; ++ibit)
		{
			if (Down.test(m_N - ibit)) 
			{
				cout << -1*((int)m_N - (int)ibit + 1) << " ";
			}
		}
		cout << "}" << endl;
	}
}
#endif 
