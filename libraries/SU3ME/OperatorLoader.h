#ifndef OPERATORLOADER_H
#define OPERATORLOADER_H

#include <SU3ME/InteractionPPNN.h>
#include <SU3ME/CInteractionPN.h>

const static float fmass = 938.92;

class COperatorLoader
{
	std::vector<std::pair<std::string, float> > two_body_PPNN_;
	std::vector<std::pair<std::string, float> > two_body_PN_;
	std::vector<std::pair<std::string, float> > one_body_;
	public:
	inline float TrelCoeff(const unsigned A, const float hw) const {return (2.0*hw)/(float)A;}
	inline float VcoulCoeff(const float hw) {return sqrt(fmass/938.093)*sqrt(hw/10.0);}
	inline float BdBCoeff(const unsigned A) {return 1.0/(float)A;}
	inline double NcmCoeff(const uint8_t A) const {return 1.0/(double)A;}
	public:
	inline void AddOneBodyOperator(const std::string& observable_1b, const float dcoeff = 1.0) {one_body_.push_back(std::make_pair(observable_1b, dcoeff));}
	inline void AddTwoBodyOperatorPPNN(const std::string& observable_2b_PPNN, const float dcoeff = 1.0) {two_body_PPNN_.push_back(std::make_pair(observable_2b_PPNN, dcoeff));}
	inline void AddTwoBodyOperatorPN(const std::string& observable_2b_PN, const float dcoeff = 1.0) {two_body_PN_.push_back(std::make_pair(observable_2b_PN, dcoeff));}
	void AddOperator(const std::string& observable_2b_PN, const std::string& observable_2b_PPNN, const std::string& observable_1b, const float dcoeff = 1.0);

	void AddTrel(const unsigned A, const float hw);
	void AddVcoul(const float hw);
	void AddVnn();
	void AddBdB(const int A, const int lambda);
	void AddL2();
	void Add_r2_mass_intrinsic(const int A);

	void AddNcm(const uint8_t A, const double lambda);
	void AddAB00(const double lambda);
	public:
	void Load(int my_rank, CInteractionPPNN& interactionPPNN, CInteractionPN& interactionPN, bool print_output = false);

	COperatorLoader() {
	  if (su3shell_data_directory == NULL) {
	    std::cerr<<"SU3SHELL_DATA environment variable is not set."<<std::endl;
	    exit (-1);
	  }
	}
};
#endif
