#ifndef U9U6U3_TESTS_H
#define  U9U6U3_TESTS_H

#include <UNU3SU3/UNU3SU3Basics.h>

#include <map>

#include <boost/array.hpp>

typedef boost::array<SU3::LABELS, 9> U9LABELS;
typedef boost::array<SU3::LABELS, 6> U6LABELS;
typedef boost::array<SU3::LABELS, 3> U3LABELS;

typedef std::map<U9LABELS, std::pair<uint32_t, uint32_t> > U9LIST;
typedef std::map<U6LABELS, std::pair<uint32_t, uint32_t> > U6LIST;
typedef std::map<U3LABELS, std::pair<uint32_t, uint32_t> > U3LIST;


#endif
