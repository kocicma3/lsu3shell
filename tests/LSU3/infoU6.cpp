#include "U9U6U3tests.h"
#include <UNU3SU3/CSU3Master.h>

#include <iostream>
#include <fstream>

#include <boost/archive/binary_iarchive.hpp> 
#include <boost/serialization/map.hpp>
#include <boost/serialization/array.hpp>
using std::cout;
using std::endl;

void ShowData(U6LIST& u6list, bool show_content)
{
	typedef uint32_t NUMBER_COEFFS;
	typedef uint32_t FREQUENCY;
	std::map<NUMBER_COEFFS, FREQUENCY> statistics;
	
	CSU3CGMaster su3lib;

	uint32_t ncoeffs(0);

	if (show_content)
	{
		U6LABELS u6Labels;
		cout << "ir1 ir2 ir ir3 ir12 ir23   \t #occurences  \t#coeffs\t {coeffs}" << endl;

		for (U6LIST::iterator it = u6list.begin(); it != u6list.end(); ++it)
		{
			int number_coeffs = it->second.first;
			std::vector<double> du6(number_coeffs, 0.0);

			ncoeffs += number_coeffs;
			statistics[number_coeffs] += 1;

			u6Labels = it->first;

			cout << "(" << (int)u6Labels[0].lm << " " << (int)u6Labels[0].mu << ") ";
			cout << "(" << (int)u6Labels[1].lm << " " << (int)u6Labels[1].mu << ") ";
			cout << "(" << (int)u6Labels[2].lm << " " << (int)u6Labels[2].mu << ") ";
			cout << "(" << (int)u6Labels[3].lm << " " << (int)u6Labels[3].mu << ") ";
			cout << "(" << (int)u6Labels[4].lm << " " << (int)u6Labels[4].mu << ") ";
			cout << "(" << (int)u6Labels[5].lm << " " << (int)u6Labels[5].mu << ") ";
			cout << "\t" << it->second.second << "\t" << number_coeffs << "\t";
			su3lib.Get6lm(CSU3CGMaster::U6LM, u6Labels[0], u6Labels[1], u6Labels[2], u6Labels[3], u6Labels[4], u6Labels[5], du6);
			for (int i = 0; i < number_coeffs;++i)
			{
				cout << du6[i] << " ";
			}
			cout << endl;
		}
	}
	else
	{
		for (U6LIST::iterator it = u6list.begin(); it != u6list.end(); ++it)
		{
			uint32_t number_coeffs = it->second.first;
			statistics[number_coeffs] += 1;
			ncoeffs += number_coeffs;
		}
	}

	cout << "U6 coeffs size\t frequency" << endl;
	for (std::map<NUMBER_COEFFS, FREQUENCY>::iterator it = statistics.begin(); it != statistics.end(); ++it)
	{
		cout << it->first << "\t\t" << it->second << "\t" << 100.0*(it->second/(1.0*u6list.size())) << "%" << endl;
	}
	cout << "#U6: " << u6list.size() << endl;
	cout << "total size of all coeffs: " << ncoeffs << endl;
}


int main(int argc,char **argv)
{
	if (argc != 3)
	{
		cout << "Usage: "<< argv[0] <<" <U6 file name> <1 or 0>" << endl;
		cout << "1: list content of file with U6 coefficients" << endl;
		return EXIT_FAILURE;
	}

	std::ifstream input_file(argv[1],  std::ios::binary);
	if (!input_file)
	{
		std::cerr << "Could not open '" << argv[1] << "' input file with 6-(lm mu) coefficients" << endl;
		return EXIT_FAILURE;
	}

	U6LIST u6list;
	boost::archive::binary_iarchive ia(input_file);
	ia  >> u6list;

	bool list_content = (argv[2][0] == '1');

	ShowData(u6list, list_content);

	return EXIT_SUCCESS;
}
