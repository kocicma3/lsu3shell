#include <mpi.h>
#include <SU3ME/global_definitions.h>
#include <SU3ME/ModelSpaceExclusionRules.h>
#include <SU3ME/global_definitions.h>


#include <LSU3/CInteractionPN_MPI.h>
#include <LSU3/ncsmSU3xSU2Basis.h>
#include <LSU3/Cadtarmes.h>
#include <LSU3/Cadta_table.h>
#include <LSU3/types.h>
#include <SU3ME/MeEvaluationHelpers.h>

void MapRankToColRow_MFDn_Compatible(const uint32_t my_rank, const uint16_t ndiag, uint16_t& row, uint16_t& col)
{
	assert(ndiag * (ndiag + 1) / 2 >= my_rank); 
	int executing_process_id(0);
	for (size_t i = 0; i < ndiag; ++i)
	{
		row = 0;
		for (col = i; col < ndiag; ++col, ++row, ++executing_process_id)
		{
			if (my_rank == executing_process_id)
			{
				return;
			}
		}
	}
}

void CreateList_ipjp_injn_full(	const lsu3::CncsmSU3xSU2Basis& bra_basis, const lsu3::CncsmSU3xSU2Basis& ket_basis, std::vector<std::pair<uint32_t, uint32_t> >& ipjp_list, std::vector<std::pair<uint32_t, uint32_t> >& injn_list)
{
	SingleDistribution distr_bra, distr_ket;
	int delta;

	const uint32_t number_ipin_blocks = bra_basis.NumberOfBlocks();
	const uint32_t number_jpjn_blocks = ket_basis.NumberOfBlocks();

	std::set<uint32_t> ip_unique, jp_unique, in_unique, jn_unique;
//	create a list of all unique ip, in, jp, and jn indices
	for (int ipin_block = 0; ipin_block < number_ipin_blocks; ipin_block++)
	{
		ip_unique.insert(bra_basis.getProtonIrrepId(ipin_block));
		in_unique.insert(bra_basis.getNeutronIrrepId(ipin_block));
	}
	for (int jpjn_block = 0; jpjn_block < number_jpjn_blocks; jpjn_block++)
	{
		jp_unique.insert(ket_basis.getProtonIrrepId(jpjn_block));
		jn_unique.insert(ket_basis.getNeutronIrrepId(jpjn_block));
	}

	int32_t icurrentKetDistr, icurrentBraDistr, ilastKetDistr, ilastBraDistr;
	ilastBraDistr = -1;
//	loop over unique ip indices	
	for (std::set<uint32_t>::iterator ip = ip_unique.begin(); ip != ip_unique.end(); ++ip)
	{
		SU2::LABEL SSf = (bra_basis.getProtonSU3xSU2(*ip)).S2;

		icurrentBraDistr = bra_basis.getIndex_p<lsu3::CncsmSU3xSU2Basis::kDistr>(*ip);
		if (icurrentBraDistr != ilastBraDistr)
		{
			distr_bra.resize(0);
			bra_basis.getDistr_p(*ip, distr_bra);
			ilastBraDistr = icurrentBraDistr;
		}

		ilastKetDistr = -1;
//	loop over all unique jp indices		
		for (std::set<uint32_t>::iterator jp = jp_unique.begin(); jp != jp_unique.end(); ++jp)
		{
			icurrentKetDistr = ket_basis.getIndex_p<lsu3::CncsmSU3xSU2Basis::kDistr>(*jp);
			if (icurrentKetDistr != ilastKetDistr)
			{
				distr_ket.resize(0);
				ket_basis.getDistr_p(*jp, distr_ket);
				ilastKetDistr = icurrentKetDistr;
				delta = getDifferences(distr_bra, distr_ket);
			}
//	if one-body operator can connect bra distribution with ket distribution	&&
//	Si and Sf can be connected by S0=0 or S0=1 operators ==> store (ip jp) pair		
			if (delta <= 2 && abs((ket_basis.getProtonSU3xSU2(*jp)).S2 - SSf) <= 2)	// ==> jn ca be transformed into in through some a+a operator
			{
				ipjp_list.push_back(std::make_pair(*ip, *jp));
			}
		}
	}
	cout << "number of possibly non-vanishing (ip jp): " << ipjp_list.size() << endl;

	ilastBraDistr = -1;
	for (std::set<uint32_t>::iterator in = in_unique.begin(); in != in_unique.end(); ++in)
	{
		SU2::LABEL SSf = (bra_basis.getProtonSU3xSU2(*in)).S2;
		icurrentBraDistr = bra_basis.getIndex_n<lsu3::CncsmSU3xSU2Basis::kDistr>(*in);
		if (icurrentBraDistr != ilastBraDistr)
		{
			distr_bra.resize(0);
			bra_basis.getDistr_n(*in, distr_bra);
			ilastBraDistr = icurrentBraDistr;
		}

		ilastKetDistr = -1;
		for (std::set<uint32_t>::iterator jn = jn_unique.begin(); jn != jn_unique.end(); ++jn)
		{
			icurrentKetDistr = ket_basis.getIndex_n<lsu3::CncsmSU3xSU2Basis::kDistr>(*jn);
			if (icurrentKetDistr != ilastKetDistr)
			{
				distr_ket.resize(0);
				ket_basis.getDistr_n(*jn, distr_ket);
				ilastKetDistr = icurrentKetDistr;
				delta = getDifferences(distr_bra, distr_ket);
			}
			if (delta <= 2 && abs((ket_basis.getProtonSU3xSU2(*jn)).S2 - SSf) <= 2)	// ==> jn ca be transformed into in through some a+a operator
			{
				injn_list.push_back(std::make_pair(*in, *jn));
			}
		}
	}
	cout << "number of possibly non-vanishing (in jn): " << injn_list.size() << endl;
}

void CreateList_ipjp_injn_diagonal(	const lsu3::CncsmSU3xSU2Basis& basis, 
									std::vector<std::pair<uint32_t, uint32_t> >& ipjp_list, std::vector<std::pair<uint32_t, uint32_t> >& injn_list)
{
	SingleDistribution distr_bra_p, distr_ket_p;
	SingleDistribution distr_bra_n, distr_ket_n;
	int delta_p, delta_n;

	const uint32_t number_ipin_blocks = basis.NumberOfBlocks();
	const uint32_t number_jpjn_blocks = basis.NumberOfBlocks();
	uint32_t ip, jp, in, jn;

	int32_t icurrentKetDistr_p, icurrentBraDistr_p, ilastKetDistr_p, ilastBraDistr_p;
	int32_t icurrentKetDistr_n, icurrentBraDistr_n, ilastKetDistr_n, ilastBraDistr_n;

	std::set<std::pair<uint32_t, uint32_t> > unique_ipjp, unique_injn;

	ilastBraDistr_p = -1; ilastBraDistr_n = -1;
	for (int ipin_block = 0; ipin_block < number_ipin_blocks; ipin_block++)
	{
		ip = basis.getProtonIrrepId(ipin_block);
		SU2::LABEL SSfp = (basis.getProtonSU3xSU2(ip)).S2;
		icurrentBraDistr_p = basis.getIndex_p<lsu3::CncsmSU3xSU2Basis::kDistr>(ip);
		if (icurrentBraDistr_p != ilastBraDistr_p)
		{
			distr_bra_p.resize(0);
			basis.getDistr_p(ip, distr_bra_p);
			ilastBraDistr_p = icurrentBraDistr_p;
		}

		in =  basis.getNeutronIrrepId(ipin_block);
		SU2::LABEL SSfn = (basis.getNeutronSU3xSU2(in)).S2;
		icurrentBraDistr_n = basis.getIndex_n<lsu3::CncsmSU3xSU2Basis::kDistr>(in);
		if (icurrentBraDistr_n != ilastBraDistr_n)
		{
			distr_bra_n.resize(0);
			basis.getDistr_n(in, distr_bra_n);
			ilastBraDistr_n = icurrentBraDistr_n;
		}

		ilastKetDistr_p = -1; ilastKetDistr_n = -1;
		for (int jpjn_block = ipin_block; jpjn_block < number_jpjn_blocks; jpjn_block++)
		{
			jp = basis.getProtonIrrepId(jpjn_block);
			icurrentKetDistr_p = basis.getIndex_p<lsu3::CncsmSU3xSU2Basis::kDistr>(jp);
			if (icurrentKetDistr_p != ilastKetDistr_p)
			{
				distr_ket_p.resize(0);
				basis.getDistr_p(jp, distr_ket_p);
				ilastKetDistr_p = icurrentKetDistr_p;
				delta_p = getDifferences(distr_bra_p, distr_ket_p);
			}
			if (delta_p <= 2 && (abs((basis.getProtonSU3xSU2(jp)).S2 - SSfp) <= 2))
			{
				unique_ipjp.insert(std::make_pair(ip, jp));
			}
			
			jn = basis.getNeutronIrrepId(jpjn_block);
			icurrentKetDistr_n = basis.getIndex_n<lsu3::CncsmSU3xSU2Basis::kDistr>(jn);
			if (icurrentKetDistr_n != ilastKetDistr_n)
			{
				distr_ket_n.resize(0);
				basis.getDistr_n(jn, distr_ket_n);
				ilastKetDistr_n = icurrentKetDistr_n;
				delta_n = getDifferences(distr_bra_n, distr_ket_n);
			}
			if (delta_n <= 2 && (abs((basis.getNeutronSU3xSU2(jn)).S2 - SSfn) <= 2))
			{
				unique_injn.insert(std::make_pair(in, jn));
			}
		}
	}

	ipjp_list.insert(ipjp_list.end(), unique_ipjp.begin(), unique_ipjp.end());
	injn_list.insert(injn_list.end(), unique_injn.begin(), unique_injn.end());

	cout << "number of possibly non-vanishing (ip jp): " << ipjp_list.size() << endl;
	cout << "number of possibly non-vanishing (in jn): " << injn_list.size() << endl;
}


void ShowContent(const int nucleon_type, const lsu3::CncsmSU3xSU2Basis& bra_basis, const lsu3::CncsmSU3xSU2Basis& ket_basis, const std::vector<std::pair<uint32_t, uint32_t> >& ij_list, const lsu3::Cadta_table& adta_table, const lsu3::Cadtarmes& rmes)
{
	const float* rmes_array = rmes.rmes_begin();
	const uint32_t* tensor_ids = rmes.tensor_ids_begin();
	uint16_t ai_max, aj_max, rhot_max;
	uint32_t nrmes, irme;

	for (int i = 0; i < ij_list.size(); ++i)
	{
		uint32_t ibra = ij_list[i].first;
		uint32_t iket = ij_list[i].second;

		SU3xSU2::LABELS bra((nucleon_type == nucleon::PROTON) ? bra_basis.getProtonSU3xSU2(ibra) : bra_basis.getNeutronSU3xSU2(ibra));
		SU3xSU2::LABELS ket((nucleon_type == nucleon::PROTON) ? ket_basis.getProtonSU3xSU2(iket) : ket_basis.getNeutronSU3xSU2(iket));

		ai_max = (nucleon_type == nucleon::PROTON) ? bra_basis.getMult_p(ibra) : bra_basis.getMult_n(ibra);
		aj_max = (nucleon_type == nucleon::PROTON) ? ket_basis.getMult_p(iket) : ket_basis.getMult_n(iket);

		cout << "(" << ibra << " " << iket << "):" << endl;
		
		if (nucleon_type == nucleon::PROTON)
		{
			bra_basis.ShowProtonConf(ibra);
			cout << "   x   ";
			ket_basis.ShowProtonConf(iket);
		}
		else
		{
			bra_basis.ShowNeutronConf(ibra);
			cout << "   x   ";
			ket_basis.ShowNeutronConf(iket);
		}
		
		cout << endl;

		const lsu3::Cadtarmes::RMES_RECORD rmes_record(rmes.get(ij_list[i]));
		if (lsu3::Cadtarmes::IsEmpty(rmes_record))
		{
			continue;
		}
		uint32_t ifirst_tensor = cpp0x::get<lsu3::Cadtarmes::kTensorIdPtr>(rmes_record);
		uint32_t ifirst_rme = cpp0x::get<lsu3::Cadtarmes::kRmesPtr>(rmes_record);
		uint16_t ntensors = cpp0x::get<lsu3::Cadtarmes::kNumberOfTensors>(rmes_record);

		irme = ifirst_rme;
		for (int it = 0; it < ntensors; ++it)
		{
			SU3xSU2::LABELS t0(adta_table[tensor_ids[ifirst_tensor + it]]);

			cout << "\t(" << (int)t0.lm << " " << (int)t0.mu << ") 2S:" << (int)t0.S2 << "\trmes: ";

			rhot_max = SU3::mult(ket, t0, bra);
			nrmes = ai_max*aj_max*rhot_max;

			for (int i = 0; i < nrmes - 1; ++i)
			{
				cout << rmes_array[irme] << " ";
				++irme;
			}
			cout << rmes_array[irme] << endl;
			++irme;
		}
	}
}


// NOTE: This is just a test program ==> even though the code calls MPI
// functions, it is being executed by a single process only
int main(int argc, char **argv)
{
	if (argc != 3 && argc != 5)
	{
		cout << "Usage: " << argv[0] << " <model space definition> <interaction file name> [<ndiag> <my_rank>]" << endl;
		cout << "implicitly ndiag = 1 & my_rank = 0" << endl;
		return EXIT_FAILURE;
	}

	int32_t my_rank;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank); 

	if (my_rank > 0)
	{
		MPI_Finalize();
		return 0;
	}

    lsu3::CInteractionPN cpn;
	cout << "Reading interaction file ... "; cout.flush();
    cpn.readFile(argv[2], MPI_COMM_WORLD);
	cout << " Done" << endl;
    cpn.finalize(MPI_COMM_WORLD);

	uint16_t ndiag;

	if (argc == 5)
	{
		ndiag = atoi(argv[3]);
		my_rank = atoi(argv[4]);
	}
	else
	{
		ndiag = 1;
		my_rank = 0;
	}

	uint32_t nprocs = ndiag*(ndiag+1)/2;
	uint16_t idiag, jdiag;

//	find mapping my_rank --> (idiag, jdiag)
	MapRankToColRow_MFDn_Compatible(my_rank, ndiag, idiag, jdiag);

	cout << "interction file:" << argv[2] << endl;
	cout << "ndiag: " << ndiag << endl;
	cout << "my_rank: " << my_rank << "\t==> idiag:" << idiag << " jdiag:" << jdiag << endl;

	InitSqrtLogFactTables();

	proton_neutron::ModelSpace ncsmModelSpace(argv[1]);

	lsu3::CInteractionPN::Map adta_tensors_p;
	lsu3::CInteractionPN::Map adta_tensors_n;

    cpn.get_map_p(adta_tensors_p, MPI_COMM_WORLD);
    cpn.get_map_n(adta_tensors_n, MPI_COMM_WORLD);

//	Create data structure with all (lm0 mu0) 2S0 quantum numbers that occur in adta_tensors_p & adta_tensors_n 
	lsu3::Cadta_table adta_List(adta_tensors_p, adta_tensors_n);

	lsu3::CncsmSU3xSU2Basis bra_basis(ncsmModelSpace, idiag, ndiag);
	lsu3::CncsmSU3xSU2Basis ket_basis(ncsmModelSpace, bra_basis, jdiag, ndiag);

	std::vector<std::pair<uint32_t, uint32_t> > ipjp_list_SN;
	std::vector<std::pair<uint32_t, uint32_t> > injn_list_SN;

//	if (idiag != jdiag)
//	{
		CreateList_ipjp_injn_full(bra_basis, ket_basis, ipjp_list_SN, injn_list_SN);
//	}
//	else if (idiag == jdiag)
//	{
//		CreateList_ipjp_injn_diagonal(bra_basis, ipjp_list_SN, injn_list_SN);
//	}

	CBaseSU3Irreps baseSU3Irreps(ncsmModelSpace.Z(), ncsmModelSpace.N(), bra_basis.Nmax());

	lsu3::Cadtarmes prmes, nrmes;
	cout << "Calculating proton rmes ... "; cout.flush();	
	lsu3::Calculate_adta_rmes((int)nucleon::PROTON, baseSU3Irreps, adta_tensors_p, ipjp_list_SN, bra_basis, ket_basis, true, false, adta_List, prmes);
	cout << " Done" << endl;
	adta_tensors_p.clear(); 

	cout << "Calculating proton rmes ... "; cout.flush();	
	lsu3::Calculate_adta_rmes((int)nucleon::NEUTRON, baseSU3Irreps, adta_tensors_n, injn_list_SN, bra_basis, ket_basis, true, false, adta_List, nrmes);
	cout << " Done" << endl;
	adta_tensors_n.clear();

// TODO: 
// 	1) call CInteractionPN::get_pn_int(...)
// 	2) call CInteractionPN::clear() ===> to deallocate auxiliary structures 
	
//	cout << "Proton adta rmes:" << endl;
//	ShowContent(nucleon::PROTON, bra_basis, ket_basis, ipjp_list_SN, adta_List, prmes);

//	cout << "Neutron adta rmes:" << endl;
//	ShowContent(nucleon::NEUTRON, bra_basis, ket_basis, injn_list_SN, adta_List, prmes);

    MPI_Finalize();

	return 0;
}
