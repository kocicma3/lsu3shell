#include "U9U6U3tests.h"
#include <LookUpContainers/CSU39lm.h>

#include <iostream>
#include <fstream>

#include <boost/archive/binary_iarchive.hpp> 
#include <boost/archive/binary_oarchive.hpp> 
#include <boost/serialization/map.hpp>
#include <boost/serialization/array.hpp>

using std::cout;
using std::endl;

void GetU6Symbols(	const SU3::LABELS& ir1, const SU3::LABELS& ir2, const SU3::LABELS& ir12,
					const SU3::LABELS& ir3, const SU3::LABELS& ir4, const SU3::LABELS& ir34, 
					const SU3::LABELS& ir13, const SU3::LABELS& ir24, const SU3::LABELS& ir, 
					U6LIST& u6)

{
	U6LIST::iterator iter;
	U6LABELS key;
	uint32_t n;
	int rho12max 	= SU3::mult(ir1, ir2, ir12);
	int rho34max 	= SU3::mult(ir3, ir4, ir34);
	int rho13max 	= SU3::mult(ir1, ir3, ir13);
	int rho24max 	= SU3::mult(ir2, ir4, ir24);
	int rho1234max	= SU3::mult(ir12, ir34, ir);
	int rho1324max	= SU3::mult(ir13, ir24, ir);

	int rho123max, rho04max, rho132max; // multiplicities for 6lm and z6lm coefficients

	SU3_VEC ir0_vector;
	SU3::Couple(ir13, ir2, ir0_vector);

	for (SU3_VEC::const_iterator ir0 = ir0_vector.begin(); ir0 != ir0_vector.end(); ++ir0)
	{
		rho123max = SU3::mult(ir12, ir3, *ir0);
		if (!rho123max) {
			continue;
		}
		rho04max = SU3::mult(*ir0, ir4, ir);
		if (!rho04max) {
			continue;
		}
		rho132max = ir0->rho;

		key[0] = ir13;
		key[1] = ir2;
		key[2] = ir;
		key[3] = ir4;
		key[4] = *ir0;
		key[5] = ir24;
		iter = u6.find(key);
		if (iter == u6.end())
		{
			n = rho132max * rho04max * rho24max * rho1324max;
			u6.insert(std::make_pair(key, std::make_pair(n, 1)));
		}
		else
		{
			iter->second.second += 1;
		}

		key[0] = ir12;
		key[1] = ir3;
		key[2] = ir;
		key[3] = ir4;
		key[4] = *ir0;
		key[5] = ir34;
		iter = u6.find(key);
		if (iter == u6.end())
		{
			n = rho12max * rho123max * rho13max * rho132max;
			u6.insert(std::make_pair(key, std::make_pair(n, 1)));
		}
		else
		{
			iter->second.second += 1;
		}

//	Lets just focus on U6 coefficients for now
/*
		key[0] = ir2;
		key[1] = ir1;
		key[2] = *ir0;
		key[3] = ir3;
		key[4] = ir12;
		key[5] = ir13;
		n = rho12max * rho123max * rho13max * rho132max;
		z6.insert(std::make_pair(key, n));
*/		
	}
}



void GenerateU6List(U9LIST& u9list, U6LIST& u6list)
{
	for (U9LIST::iterator it = u9list.begin(); it != u9list.end(); ++it)
	{
		GetU6Symbols(it->first[0], it->first[6], it->first[3], it->first[1], it->first[7], it->first[4], it->first[2], it->first[8], it->first[5], u6list);
	}

	cout << "#u6 symbols:" << u6list.size() << endl;
	uint32_t nu6(0);
	for (U6LIST::iterator it = u6list.begin(); it != u6list.end(); ++it)
	{
		nu6 += it->second.first;
	}
	cout << "#u6 coeffs:" << nu6 << endl;

/*
	cout << "#z6 symbols:" << z6.size() << endl;
	uint32_t nz6(0);
	for (map<boost::array<SU3::LABELS, 6>, uint32_t>::iterator it = z6.begin(); it != z6.end(); ++it)
	{
		nz6 += it->second;
	}
	cout << "#z6 coeffs:" << nz6 << endl;
*/
}

int main(int argc,char **argv)
{
	if (argc != 3)
	{
		cout << "Usage: "<< argv[0] <<" <U9 file name [input]> <U6 file name [output]>" << endl;
		return EXIT_FAILURE;
	}

	std::ifstream input_file(argv[1],  std::ios::binary);
	if (!input_file)
	{
		std::cerr << "Could not open '" << argv[1] << "' input file with 9-(lm mu) coefficients" << endl;
		return EXIT_FAILURE;
	}

	U9LIST u9list;
	boost::archive::binary_iarchive ia(input_file);
	ia  >> u9list;


	U6LIST u6list;
	GenerateU6List(u9list, u6list);
	std::ofstream output_file(argv[2],  std::ios::binary);
	if (!output_file)
	{
		std::cerr << "Could not open '" << argv[2] << "' output file with 6-(lm mu) coefficients." << endl;
		return EXIT_FAILURE;
	}

	boost::archive::binary_oarchive oa(output_file);
	oa  << u6list;
}
