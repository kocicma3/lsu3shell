#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <UNU3SU3/UNU3SU3Basics.h>
#include <UNU3SU3/CSU3Master.h>
#include <SU3ME/global_definitions.h>

using std::cout;
using std::endl;
using std::fstream;
using std::ifstream;
using std::vector;
using std::map;

enum {kn1 = 0, kn2 = 1, kn3 = 2, kn4 = 3, klm1 = 4, kmu1 = 5, kSS1 = 6, klm2 = 7, kmu2 = 8, kSS2 = 9, klm0 = 10, kmu0 = 11, kSS0 = 12};


void ShowKey(const CTuple<int, 13>& key)
{
	cout << key[kn1] << " " << key[kn2] << " " << key[kn3] << " " << key[kn4];
	cout << "\t\t";
	cout << key[klm1] << " " << key[kmu1] << " " << key[kSS1] << "\t";
	cout << key[klm2] << " " << key[kmu2] << " " << key[kSS2] << "\t";
	cout << SU3::mult(SU3::LABELS(key[klm1], key[kmu1]), SU3::LABELS(key[klm2], key[kmu2]),SU3::LABELS(key[klm0], key[kmu0])) << " ";
	cout << key[klm0] << " " << key[kmu0] << " " << key[kSS0] << endl;
}

void GetConjugateKey(const CTuple<int, 13>& key, CTuple<int, 13>& resulting_key)
{
	resulting_key[kn1] = key[kn4];
	resulting_key[kn2] = key[kn3];
	resulting_key[kn3] = key[kn2];
	resulting_key[kn4] = key[kn1];

	resulting_key[klm1] = key[kmu2];
	resulting_key[kmu1] = key[klm2];
	resulting_key[kSS1] = key[kSS2];

	resulting_key[klm2] = key[kmu1];
	resulting_key[kmu2] = key[klm1];
	resulting_key[kSS2] = key[kSS1];

	resulting_key[klm0] = key[kmu0];
	resulting_key[kmu0] = key[klm0];
	resulting_key[kSS0] = key[kSS0];
}

bool Equal(const vector<double>& coeff1, const vector<double>& coeff2)
{
	assert(coeff1.size() == coeff2.size());

	for (int i = 0; i < coeff1.size(); ++i)
	{
		if (!Negligible3(coeff1[i] - coeff2[i]))
		{
			return false;
		}
	}
	return true;
}

int main(int argc, char* argv[])
{
	if (argc != 2)
	{
		cout << "Usage: " << argv[0] << " <file name>" << endl;
		cout << "The argument is a two-body interaction file in SU(3) tensor form <file name> before HO shell recoupling transformation.\n";
		cout << "Code tests whether it contains the whole matrix by testing conjugation relations derived in devel/docs/Conjugation." << endl;
		return EXIT_FAILURE;
	}
	
	CSU3CGMaster su3lib;

	ifstream file(argv[1]);

	if (!file)
	{
		cout << "Could not open file '" << argv[1] << "'!" << endl;
		return EXIT_FAILURE;
	}

	double dPP, dNN, dPN;
	map<CTuple<int, 13>, vector<double> > data;

//	Read file with unrecoupled SU(3) tensors of two-body scalar interaction 
//	store in std::map data 
	while (true)
	{
		int n1, n2, n3, n4;
		file >> n1 >> n2 >> n3 >> n4;
		if (!file)
		{
			break;
		}
		int t, lm1, mu1, lm2, mu2, lm0, mu0;
		int SS1, SS2, SS0;

		file >> t >> lm1 >> mu1 >> SS1;
		file >> t >> lm2 >> mu2 >> SS2;
		file >> t >> lm0 >> mu0 >> SS0;

		SU3xSU2::LABELS w0(1, lm0, mu0, SS0);

		int rho0max = SU3::mult(SU3::LABELS(lm1, mu1), SU3::LABELS(lm2,mu2), w0);
		int k0max = SU3::kmax(w0, SS0/2);
		int ncoeffs = 3*rho0max*k0max;
		vector<double> coeffs(ncoeffs, 0);
//	NOTE: proton-proton and neutron-neutron interaction terms has form
		for (int k0 = 0, i = 0; k0 < k0max; ++k0)
		{
			for (int rho0 = 0; rho0 < rho0max; ++rho0, i +=3)
			{
				file >> dPP >> dNN >> dPN;
				coeffs[i]   = dPP;
				coeffs[i+1] = dNN;
				coeffs[i+2] = dPN;
			}
		}

//	Do not store tensors with all coefficients < 1.e-5 as its conjugate
//	counterpart may be missing due to machine precision
		if (std::count_if(coeffs.begin(), coeffs.end(), Negligible5) == ncoeffs)
		{
			continue;
		}

		CTuple<int, 13> key;

		key[0] = n1;
		key[1] = n2;
		key[2] = n3;
		key[3] = n4;
		key[4] = lm1;
		key[5] = mu1;
		key[6] = SS1;
		key[7] = lm2;
		key[8] = mu2;
		key[9] = SS2;
		key[10] = lm0;
		key[11] = mu0;
		key[12] = SS0;

		data[key] = coeffs;
	}

	map<CTuple<int, 13>, vector<double> >::iterator it = data.begin();
//	iterate over interaction terms	
	for (; it != data.end(); ++it)
	{
		CTuple<int, 13> conj_key;
//	For each interaction term generate quantum labels of its conjugate
		GetConjugateKey(it->first, conj_key);

//	Try to find it ...
		map<CTuple<int, 13>, vector<double> >::iterator it_conjugate = data.find(conj_key);
//	As long as all interaction terms are presented and interaction is Hermitian
//	this condition should be false
		if (it_conjugate == data.end())
		{
			cout << "Strange!!! Tensor";
			ShowKey(it->first);
			cout << "does not have its conjugate ";
			ShowKey(conj_key);
			return EXIT_FAILURE;
		}

		SU3::LABELS ir1(conj_key[klm1], conj_key[kmu1]);
		SU3::LABELS ir2(conj_key[klm2], conj_key[kmu2]);
		SU3::LABELS ir0(conj_key[klm0], conj_key[kmu0]);

		int rho0max = SU3::mult(ir1, ir2, ir0);
		int S0 = conj_key[kSS0]/2;
		int k0max = SU3::kmax(ir0, S0);
		int ncoeffs = k0max*rho0max*3;

		vector<double> conjugate_coeffs(ncoeffs, 0.0);

		int main_phase = 1;
// For (lm0 == mu0) we need to pick up (-)^(lm0*S0)	 phase 
		if (ir0.lm == ir0.mu)
		{
			main_phase = MINUSto(ir0.lm*S0);
		}

		if (rho0max == 1)
		{
			main_phase *= MINUSto(S0);
			for (int k0 = 0, i = 0; k0 < k0max; ++k0)
			{
				for (int rho0 = 0; rho0 < rho0max; ++rho0)
				{
					for (int type = 0; type < 3; ++type, ++i)
					{
						conjugate_coeffs[i] = main_phase*it->second[i];
					}
				}
			}
		}
		else
		{
			assert( (it->first[kn1] + it->first[kn2] + it->first[kn3] + it->first[kn4])%2 == 0);
//	the maximal value of rho0 during iteration is (rho0max-1).
			main_phase *= MINUSto(ir1.lm + ir1.mu + ir2.lm + ir2.mu + (rho0max-1) - S0);

			vector<double> phi(rho0max*rho0max, 0.0);
			su3lib.GetPhi(ir2, ir1, ir0, phi);

			for (int k0 = 0, index_k0rho0 = 0; k0 < k0max; ++k0)
			{
				for (int rho0 = 0; rho0 < rho0max; ++rho0, index_k0rho0 += 3)
				{
					double dPP(0), dNN(0), dPN(0);

					int index_k0rho0p  = 3*k0*rho0max;
					int index_phi = rho0*rho0max;
					for (int rho0p = 0; rho0p < rho0max; ++rho0p, index_k0rho0p += 3, ++index_phi)
					{
						int phase_rho0p = MINUSto(rho0p);

						dPP += phase_rho0p*it->second[index_k0rho0p]*phi[index_phi];
						dNN += phase_rho0p*it->second[index_k0rho0p + 1]*phi[index_phi];
						dPN += phase_rho0p*it->second[index_k0rho0p + 2]*phi[index_phi];
					}
					conjugate_coeffs[index_k0rho0]   = main_phase*dPP;
					conjugate_coeffs[index_k0rho0+1] = main_phase*dNN;
					conjugate_coeffs[index_k0rho0+2] = main_phase*dPN;
				}
			}
		}
//	Check if conjugate_coeffs calculated using formulas found in
//	dev/docs/Conjugation/Conjugation.pdf
		if (!Equal(conjugate_coeffs, it_conjugate->second))
		{
			ShowKey(it->first);
			for (int j = 0; j < ncoeffs; ++j)
			{
				cout << conjugate_coeffs[j] << " ";
			}
			cout << endl;
			for (int j = 0; j < ncoeffs; ++j)
			{
				cout << it_conjugate->second[j] << " ";
			}
			cout << endl;
		}
	}
	cout << "Operator is Hermitian." << endl;
	return EXIT_SUCCESS;
}
